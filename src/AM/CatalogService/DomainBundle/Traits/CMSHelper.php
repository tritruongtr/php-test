<?php

namespace AM\CatalogService\DomainBundle\Traits;

trait CMSHelper
{
    protected $siteID;
    protected $legacyIssueID;

    private $defaultExcludeQueryStrings = [
        'catalog_id',
        'content_rating',
        'limit',
        'offset',
        'use_default_limit',
        'page',
        'type',
        'access_token',
        '_'
    ];

    public function config($siteID, $legacyIssueID)
    {
        $this->siteID = $siteID;
        $this->legacyIssueID = $legacyIssueID;

        return $this;
    }

    public function queryStringWalk(array $queryString = [], array $excludeKeys = [], array $includeKeys = [])
    {
        $excludeKeys = array_merge($this->defaultExcludeQueryStrings, $excludeKeys);

        if (empty($queryString) || empty($excludeKeys)) {
            return $queryString;
        }

        foreach ($excludeKeys as $excludeKey) {
            if (!in_array($excludeKey, $includeKeys)) {
                unset($queryString[$excludeKey]);
            }
        }

        return $queryString;
    }

    public function queryStringToRedisKey(array $queryString, $defaultKey = 'all')
    {
        $redisKey = '';
        if (empty($queryString)) {
            return $defaultKey;
        }

        foreach ($queryString as $key => $value) {
            if ($value == null) {
                continue;
            }

            $redisKey .= "{$key}_{$value}.";
        }

        $redisKey = substr($redisKey, 0, -1);

        if (empty($redisKey)) {
            return $defaultKey;
        }

        return $redisKey;
    }
}