<?php

namespace AM\CatalogService\DomainBundle\Repository\Issue;

class CacheIssueRepository
{
    const KEY_NAME_TOTAL = 'catalog:issues:%s:total';
    const KEY_NAME_DATA = 'catalog:issues:%s:data';
    const CACHE_EXPIRE = 7200; // 2 hours

    private $excludeQueryStrings = [
        'use_default_limit',
        'sort',
        'sort_by',
        'order_by',
        'sort_by_list_id',
        'only_id',
        'limit',
        'offset',
        'page',
        'access_token',
        '_',
    ];

    private $redis;
    private $queryString = [];

    public function __construct(\Predis\ClientInterface $redis, $queryString, $requestData)
    {
        $this->redis = $redis;
        $this->queryString = $this->queryStringWalk($queryString, $this->excludeQueryStrings);
        $this->queryString = array_merge($this->queryString, $requestData);
        ksort($this->queryString);
    }

    public function getPagination($limit, $offset)
    {
        $total = $this->get('total');

        if ($total) {
            $pagination = new \Zenith\CoreService\PaginationBundle\Pagination($total, $offset, $limit);
            return $pagination->getResult();
        }

        return false;
    }

    public function getRedis()
    {
        return $this->redis;
    }

    public function getQueryString()
    {
        return $this->queryString;
    }

    public function get($type)
    {
        try {
            $keyName = $this->getKeyName($type);
            $data = $this->getRedis()->GET($keyName);
        } catch (\Exception $e) {
            $data = false;
        }

        if (empty($data)) {
            return false;
        }

        return $data;
    }

    public function queryStringWalk(array $queryString = [], array $excludeKeys = [])
    {
        if (empty($queryString) || empty($excludeKeys)) {
            return $queryString;
        }

        foreach ($excludeKeys as $excludeKey) {
            unset($queryString[$excludeKey]);
        }

        return $queryString;
    }

    public function queryStringToRedisKey(array $queryString, $defaultKey = 'all')
    {
        $redisKey = '';
        if (empty($queryString)) {
            return $defaultKey;
        }

        foreach ($queryString as $key => $value) {
            if ($value == null) {
                continue;
            }

            $redisKey .= "{$key}_{$value}.";
        }

        $redisKey = substr($redisKey, 0, -1);

        if (empty($redisKey)) {
            return $defaultKey;
        }

        return $redisKey;
    }

    public function getKeyName($type)
    {
        switch ($type) {
            case 'total':
                $keyName = static::KEY_NAME_TOTAL;
                break;

            case 'data':
                $keyName = static::KEY_NAME_DATA;
                break;

            default:
                throw new \Exception('Unrecognied key type');
                break;
        }
        $queryString = $this->queryStringToRedisKey($this->queryString);

        return sprintf($keyName, $queryString);
    }

    public function create($data, $type)
    {
        $keyName = $this->getKeyName($type);

        try {
            $this->redis->SET($keyName, $data);
            $this->redis->EXPIRE($keyName, static::CACHE_EXPIRE);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function updateTotalKey($number)
    {
        $this->queryString = [];
        try {
            $keyName = $this->getKeyName('total');
            $value = $this->redis->GET($keyName);
        } catch (\Exception $e) {
            $value = 0;
        }

        if ((int) $value > 0) {
            $value = $value + $number;
            return $this->update($value, 'total', $keyName);
        }

        return false;
    }

    public function update($data, $type, $keyName = '')
    {
        if (empty($keyName)) {
            $keyName = $this->getKeyName($type);
        }

        try {
            $this->getRedis()->SET($keyName, $data);
            $this->getRedis()->EXPIRE($keyName, static::CACHE_EXPIRE);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
