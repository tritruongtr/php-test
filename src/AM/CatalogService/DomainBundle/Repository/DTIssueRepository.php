<?php
/**
 * @author Can Ly <can.ly@audiencemedia.com>
 */
namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\Common\Util\Inflector;

use AM\CatalogService\Domain\Issue\Issue;
use AM\CatalogService\Domain\IssueMetadata\IssueMetadata;
use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Publisher\Publisher;

use Zenith\CoreService\DoctrineRepository\DTBaseRepository;
use Zenith\CoreService\PaginationBundle\Pagination;

class DTIssueRepository extends DTBaseRepository
{
    private $publicationRepo;
    private $publisherRepo;
    private $metadataRepo;
    private $assetService;

    public function setAssetService($assetService)
    {
        $this->assetService = $assetService;
    }

    public function setRepo($publicationRepo, $publisherRepo, $metadataRepo, $cacheIssueRepo)
    {
        $this->publicationRepo = $publicationRepo;
        $this->publisherRepo = $publisherRepo;
        $this->metadataRepo = $metadataRepo;
        $this->cacheIssueRepo = $cacheIssueRepo;
    }

    public function appendItems(array & $issues, array $itemsInfo = [])
    {
        if (empty($issues) || empty($itemsInfo)) {
            return;
        }

        $items = [];
        foreach ($itemsInfo as $itemInfo) {
            if (count($itemInfo) != 2) {
                throw new \Exception("Item should be contains 2 elements: name and group");
            }

            list($item, $group) = $itemInfo;

            switch ($item) {
                case 'publication':
                    $publicationIDs = array_column($issues, 'publication_id');
                    $groupProps = Publication::getResponseGroups($group);
                    if (!in_array('publisherId', $groupProps)) {
                        // need to this property to SELECT DQL if you wanna append
                        // publisher property in publication
                        $groupProps[] = 'publisherId';
                    }

                    $publicationResult = $this->publicationRepo->getListByQueryParams(
                        ['id' => $publicationIDs], true, $groupProps
                    );

                    $publisherIDs = array_column($publicationResult['data'], 'publisher_id');

                    $publisherResult = $this->publisherRepo->getListByQueryParams(
                        ['id' => $publisherIDs], true, Publisher::getResponseGroups('issue_list')
                    );

                    break;

                case 'metadata':
                    $issueIDs = array_column($issues, 'id');
                    $groupProps = IssueMetadata::getResponseGroups($group);
                    if (!in_array('issueId', $groupProps)) {
                        // need to this property to SELECT DQL if you wanna append
                        // publisher property in publication
                        $groupProps[] = 'issueId';
                    }
                    $metadataResult = $this->metadataRepo->getListByQueryParams(
                        ['issue_id' => $issueIDs], true, $groupProps
                    );

                    break;

                default:
                    break;
            }

            $items[] = $item;
        }

        foreach ($issues as & $issue) {
            self::bigint2Int(['id', 'publication_id', 'created_by', 'modified_by'], $issue);

            foreach ($items as $item) {
                switch ($item) {
                    case 'publication':
                        $publications = $publicationResult['data'];
                        $this->modifiedIssue($issue);
                        $this->appendPublication($issue, $publications);
                        $this->appendPublisher($issue['publication'], $publisherResult['data']);
                        break;

                    case 'metadata':
                        $metadata = $metadataResult['data'];
                        $this->appendMetadata($issue, $metadata);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public function appendMetadata(&$issue, array $metadata = [])
    {
        foreach ($metadata as $data) {
            if ($data['issue_id'] != $issue['id']) {
                continue;
            }

            $issue['metadata'][] = $data;
        }

        if (!isset($issue['metadata'])) {
            $issue['metadata'] = [];
        }
    }

    public function modifiedIssue(&$issue)
    {
        if ($issue['sequence_no'] == '' && $issue['volume_no'] != '' && $issue['issue_no'] != '') {
            $issue['sequence_no'] = $issue['volume_no'] . '.' . $issue['issue_no'];
        } else if ($issue['sequence_no'] == '' && $issue['volume_no'] == '' && $issue['issue_no'] != '') {
            $issue['sequence_no'] = $issue['issue_no'];
        }
    }

    public function appendPublisher(&$publication, array $publishers = [])
    {
        $index = array_search($publication['publisher_id'], array_column($publishers, 'id'));
        if ($index === false) {
            $publication['publisher'] = new \stdClass();
        } else {
            $publication['publisher'] = $publishers[$index];
        }
    }

    public function appendPublication(&$issue, array $publications = [])
    {
        $index = array_search($issue['publication_id'], array_column($publications, 'id'));
        if ($index === false) {
            $issue['publication'] = new \stdClass();
        } else {
            $issue['publication'] = $publications[$index];
            $issue['publication']['id'] = (int) $issue['publication']['id'];
            self::bigint2Int(['id', 'publisher_id', 'created_by', 'modified_by'], $issue['publication']);
            $issue['publication'] = self::intlProcessor($issue['publication']);
        }
    }

    /**
     * Get list of publications by query string parameters
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array  $params
     *
     * @return array
     */
    public function getListByQueryParams(array $params, $ignoreLimit = false, array $selectProps = [])
    {
        $fieldMappings = $this->getFieldMappings();
        $queryBuilder = $this->createQueryBuilder('i');
        $rootAlias = $queryBuilder->getRootAlias();

        $selects = $this->buildSelectDQL($selectProps, $fieldMappings, $rootAlias);
        if (!empty($selects)) {
            if(!empty($params['id']) && isset($params['sort_by_list_id']) && (int) $params['sort_by_list_id'] == 1) {
                $selects .= ', field(i.id, '.$params['id'].') as HIDDEN field';
            }
            $queryBuilder->select($selects);
        }

        $queryBuilder = $this->getQuery($queryBuilder, $params);
        $queryBuilder->groupBy("$rootAlias.id");

        $limit = null;
        $offset = null;
        if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);

            $pagination = $this->getPagination($queryBuilder, $limit, $offset);
        } else {
            $pagination = [];
        }

        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        return array('data' => $data, 'pagination' => $pagination);
    }

    public function getCMSInfo($issueID)
    {
        $query = $this->_em->createQuery(
            "SELECT i.legacyIssueId as legacy_issue_id, p.siteId as site_id
             FROM AM\CatalogService\Domain\Issue\Issue i
             JOIN i.publication p
             WHERE i.id = $issueID
            "
        );

        return $data = $query->getResult();
    }

    private function getQuery($queryBuilder, array $params)
    {
        $classMetaInfo = $this->getClassMetadata();
        $expr = $queryBuilder->expr();

        $newParams = [];
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $orderBy = 'DESC';
        if (isset($newParams['orderBy'])) {
            $lowerOrderBy = strtolower($newParams['orderBy']);
            if (in_array($lowerOrderBy, ['asc', 'desc'])) {
                $orderBy = strtoupper($lowerOrderBy);
            }
        }

        $dateType = 'coverDate';
        if (isset($newParams['dateType'])) {
            $dateType = lcfirst(Inflector::classify($newParams['dateType']));
            if (!in_array($dateType, ['publishDate', 'coverDate'])) {
                $dateType = 'coverDate';
            }
        }

        $joinPublication = false;
        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'fromDate':
                    if (!empty($value) && (int) $value >= 0) {
                        $queryBuilder->andWhere(
                            $expr->gte("i.$dateType", ':from_date')
                        )->setParameter('from_date', $value);
                    }
                    break;

                case 'toDate':
                    if (!empty($value) && (int) $value >= 0) {
                        $queryBuilder->andWhere(
                            $expr->lte("i.$dateType", ':to_date')
                        )->setParameter('to_date', $value);
                    }
                    break;

                case 'sortBy':
                    if (!empty($value)) {
                        $columns = explode(',', $value);
                        // remove empty columns and dupliate columns
                        $columns = array_unique(array_filter($columns));

                        if (!empty($columns)) {
                            foreach ($columns as $column) {
                                $columnName = lcfirst(Inflector::classify($column));
                                if (in_array($columnName, ['publishDate', 'coverDate', 'createdAt', 'modifiedAt'])) {
                                    $queryBuilder->addOrderBy("i.$columnName", $orderBy);
                                }
                            }
                        }
                    }
                    break;

                case 'publicationId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $publicationIdList = explode(',', $value);
                        $queryBuilder->andWhere($expr->in('i.publicationId', $publicationIdList));

                    }
                    break;

                case 'id':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('i.id', $value));
                    }
                    break;

                case 'categoryId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $queryBuilder->join('ip.publicationsCategories', 'pc')
                        ->andWhere($expr->in('pc.categoryId', $value));
                    }
                    break;

                case 'rootCategoryId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $queryBuilder->join('ip.publicationsCategories', 'pc1')
                        ->andWhere($expr->in('pc1.rootParentCategoryId', $value));
                    }
                    break;

                case 'type':
                    if ($value != '') {
                        $queryBuilder->andWhere($expr->in('i.type', $value));
                    }
                    break;

                case 'catalogId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $queryBuilder->join('ip.catalogsPublications', 'cp')
                        ->andWhere('cp.catalogId = :catalog_id')
                        ->setParameter('catalog_id', $value);
                    }
                    break;

                case 'issueRemoteId':
                    if (!empty($value)) {
                        $queryBuilder->andWhere("i.remoteIdentifier = :value")
                        ->setParameter('value', $value);
                    }
                    break;

                case 'legacyIssueId':
                    if (!empty($value)) {
                        $queryBuilder->andWhere("i.legacyIssueId = :value")
                        ->setParameter('value', $value);
                    }
                    break;

                case 'publicationRemoteId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $queryBuilder->andWhere("ip.remoteIdentifier = :publicationRemoteId")
                        ->setParameter('publicationRemoteId', $value);
                    }
                    break;

                case 'language':
                case 'locale':
                case 'countryCode':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }

                        $queryBuilder
                            ->andWhere("ip.$key = :$key")
                            ->setParameter($key, $value);
                    }
                    break;

                case 'contentRating':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }

                        $contentRatingParts = explode('-', $value);
                        $totalParts = count($contentRatingParts);
                        if ($totalParts == 1) {
                            $queryBuilder->andWhere($expr->in('ip.contentRating', $value));
                        } elseif ($totalParts == 2) {
                            $queryBuilder->andWhere(
                                $expr->between(
                                    'ip.contentRating', $contentRatingParts[0], $contentRatingParts[1]
                                )
                            );
                        }
                    }
                    break;

                case 'status':
                    if (!empty($value)) {
                        $values = explode(',', $value);
                        $queryBuilder->andWhere(
                            $expr->in("i.$key", ":$key")
                        )->setParameter("$key", $values);
                    }
                    break;

                case 'sort':
                    $columns = explode(',', $newParams['sort']);
                    // remove empty columns and dupliate columns
                    $columns = array_unique(array_filter($columns));
                    if (!empty($columns)) {
                        foreach ($columns as $column) {
                            $tmpColumn = explode('-', $column);
                            if (2 == count($tmpColumn) && ('' == $tmpColumn[0])) {
                                $column = $tmpColumn[1];
                                $orderBy = 'desc';
                            } else {
                                $orderBy = 'asc';
                            }
                            $columnName = lcfirst(Inflector::classify($column));
                            if ($classMetaInfo->hasField($columnName)) {
                                $queryBuilder->addOrderBy('i.'.$columnName, $orderBy);
                            }
                        }
                    } else {
                        $queryBuilder->addOrderBy('i.id', $orderBy);
                    }
                    break;
                case 'sortByListId':
                    if (!empty($newParams['id']) && (int) $newParams['sortByListId'] == 1) {
                        $queryBuilder->orderBy('field');
                    }
                    break;
                default:
                    break;
            }
        }

        return $queryBuilder;
    }

    public function getListEmptyRemoteId()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT i
             FROM
                AM\CatalogService\Domain\Issue\Issue i
             WHERE i.remoteIdentifier is NULL"
        );

        return $data = $query->getArrayResult();
    }

    public function getListUnassignCategory($projectId = null)
    {
        $projectCond = $projectId !== null ? "AND (pcc.projectId = $projectId)" : '';

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT i
             FROM AM\CatalogService\Domain\Issue\Issue i
             JOIN i.publication p
             WHERE i.publication IN (
                SELECT p1.id
                FROM AM\CatalogService\Domain\Publication\Publication p1
                WHERE NOT EXISTS (
                    SELECT p1.id
                    FROM AM\CatalogService\Domain\PublicationCategory\PublicationCategory pc
                    JOIN pc.category pcc
                    WHERE pc.publication = p1.id
                    $projectCond
                )
             )"
        );

        return $data = $query->getArrayResult();
    }

    public function getListNotInTheList($list = null)
    {
        $em = $this->getEntityManager();
        if ($list == null) {
            $query = $em->createQuery(
                "SELECT i FROM AM\CatalogService\Domain\Issue\Issue i"
            );

            return $data = $query->getArrayResult();
        }

        $query = $em->createQuery(
            "SELECT i
             FROM
                AM\CatalogService\Domain\Issue\Issue i
             WHERE i.id NOT IN ($list)"
        );

        return $data = $query->getArrayResult();
    }

    public function getDesignPack($designPackUrl = null)
    {
        if ($designPackUrl == null) {
            return;
        }

        return [
            'id' => 'reader2_designpack_default_201603031617',
            'url' => $designPackUrl,
            'version' => 1,
            'reader_version' => '2'
        ];
    }

    /**
     * Get last issue (1 issue per publication only) by category
     * List issue  follow rule at
     * https://audiencemedia.jira.com/wiki/display/ZEN/Rules+for+content+handlers
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     */
    public function getListMyInterests($categoryId, $catalogId, $contentRating)
    {
        $em = $this->getEntityManager();
        $query =
            "SELECT pub_id, issue_id FROM (
                  SELECT
                    i.publication_id AS pub_id,
                    i.id AS issue_id,
                    i.cover_date AS cover_date
                  FROM
                     publications p,
                     catalogs_publications cp,
                     publications_categories pc,
                     issues i
                  WHERE
                      p.id = cp.publication_id
                      AND p.id = pc.publication_id
                      AND p.id = i.publication_id
                      AND i.status=1
                      AND p.status=1
                      AND cp.status = 1
                      AND p.content_rating BETWEEN :content_from and :content_to
                      AND cp.catalog_id = :catalog_id
                      AND pc.root_parent_category_id = :category_id
                  ORDER BY i.cover_date DESC LIMIT 50
            ) AS vs GROUP BY vs.pub_id ORDER BY vs.cover_date DESC limit 10";

        $expContentRating = explode('-', $contentRating);

        $data = $em->getConnection()->prepare($query);
        $data->bindValue(':content_from', $expContentRating[0]);
        $data->bindValue(':content_to', $expContentRating[1]);
        $data->bindValue(':catalog_id', $catalogId);
        $data->bindValue(':category_id', $categoryId);
        $data->execute();
        $data = $data->fetchAll();

        $result = [];
        foreach ($data as $key => $value) {
            $result[] = $value['pub_id'].'_'.$value['issue_id'];
        }
        return  $result;
    }

    /**
     * Get last issue (1 issue per publication only) by catalog
     * List issue  follow rule at
     * https://audiencemedia.jira.com/wiki/display/ZEN/Rules+for+content+handlers
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     */
    public function getNewIssueList($params, $group)
    {
        $em = $this->getEntityManager();
        $table = '';
        $where = '';

        $fieldMappings = $this->getFieldMappings();
        $selects = $this->buildSelectSQL($group, $fieldMappings, 'i');

        //more option
        $selects .= ',p.name as publication_name';

        if (isset($params['category_id']) && (int)$params['category_id']) {
            $table .= "publications_categories pc,";
        }

        if (isset($params['category_id']) && (int)$params['category_id']) {
            $where .= " AND p.id = pc.publication_id
                        AND pc.root_parent_category_id = :category_id";
        }

        $page = "LIMIT :limit OFFSET :offset";

        $sql = "SELECT
                      $selects
                  FROM
                     publications p,
                     catalogs_publications cp,
                     $table
                     issues i
                  WHERE
                      p.id = cp.publication_id
                      AND p.id = i.publication_id
                      AND p.status = 1
                      AND cp.status = 1
                      AND p.content_rating BETWEEN :content_from and :content_to
                      AND cp.catalog_id = :catalog_id
                      $where
                      AND i.status= 1
                      AND i.cover_date in (
				 	      SELECT MAX(cover_date)
				 	      FROM issues ii
				 	      WHERE  ii.publication_id=i.publication_id
				 	      AND ii.status = 1 ORDER BY ii.cover_date DESC)
                  GROUP BY i.publication_id, i.cover_date
                  ORDER BY i.cover_date DESC";

        $expContentRating = explode('-', $params['content_rating']);

        $data = $em->getConnection()->prepare($sql.' '.$page);
        $data->bindValue(':content_from', $expContentRating[0]);
        $data->bindValue(':content_to', $expContentRating[1]);
        $data->bindValue(':catalog_id', $params['catalog_id']);

        if (isset($params['category_id']) && (int)$params['category_id']) {
            $data->bindValue(':category_id', $params['category_id']);
        }

        $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : (int)$params['limit'];
        $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : (int)$params['offset'];

        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);

        $data->execute();
        $result = $data->fetchAll();

        //total
        $data = $em->getConnection()->prepare($sql);
        $data->bindValue(':content_from', $expContentRating[0]);
        $data->bindValue(':content_to', $expContentRating[1]);
        $data->bindValue(':catalog_id', $params['catalog_id']);
        if (isset($params['category_id']) && (int)$params['category_id']) {
            $data->bindValue(':category_id', $params['category_id']);
        }
        $data->execute();
        $total = $data->rowCount();

        $pagination = new Pagination($total, $offset, $limit);
        return array('data' => $result, 'pagination' => $pagination->getResult());
    }

    public function getLinearPDF($siteID, $legacyIssueID, $hasPDF = 0)
    {
        if ($hasPDF === 0) {
            return null;
        }

        $urlParts = [
            $this->assetService,
            "var/site_$siteID",
            "storage/original/issues/$legacyIssueID/pdf/single-linear.pdf"
        ];

        return implode('/', $urlParts);
    }

    public function getPagination($queryBuilder, $limit, $offset)
    {
        $pagination = $this->cacheIssueRepo->getPagination($limit, $offset);
        if (!$pagination) {
            $pagination = parent::getPagination($queryBuilder, $limit, $offset);
            if ($pagination['count'] > 0) {
                $this->cacheIssueRepo->create($pagination['count'], 'total');
            }
        }

        return $pagination;
    }

    public function create($entity)
    {
        parent::create($entity);
        $this->cacheIssueRepo->updateTotalKey(1);
    }

    public function delete($entity)
    {
        parent::delete($entity);
        $this->cacheIssueRepo->updateTotalKey(-1);
    }
}
