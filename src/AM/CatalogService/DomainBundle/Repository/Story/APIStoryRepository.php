<?php

namespace AM\CatalogService\DomainBundle\Repository\Story;

class APIStoryRepository extends AbstractStory
{
    public function __construct($legacyRepo, $cacheStoryRepo)
    {
        $this->legacyRepo = $legacyRepo;
        $this->cacheStoryRepo = $cacheStoryRepo;
    }

    public function getStory($issueID, $storyID)
    {
        $story = $this->legacyRepo
            ->setUp($this->siteID)
            ->getIssueSingleStory($this->legacyIssueID, $storyID)
        ;

        if (empty($story)) {
            return [];
        }

        return $story;
    }

    public function getStoryList($issueID)
    {
        $stories = $this->legacyRepo
            ->setUp($this->siteID)
            ->getIssueStories($this->legacyIssueID)
        ;

        if (empty($stories)) {
            return [];
        }

        foreach ($stories as &$story) {
            $story = $this->formatStoryLayout($story);
        }

        $this->writeTocache($stories, $issueID);

        return $stories;
    }

    public function writeTocache($stories, $issueID)
    {
        $this->cacheStoryRepo->save($stories, $issueID);
    }
}
