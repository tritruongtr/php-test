<?php

namespace AM\CatalogService\DomainBundle\Repository\Story;

class StoryRepository
{
    private $siteID = null;
    private $legacyIssueID = null;

    public function __construct($cacheStoryRepo, $apiStoryRepo)
    {
        $this->cacheStoryRepo = $cacheStoryRepo;
        $this->apiStoryRepo = $apiStoryRepo;
    }

    public function config($siteID, $legacyIssueID)
    {
        $this->siteID = $siteID;
        $this->legacyIssueID = $legacyIssueID;

        return $this;
    }

    public function getStory($issueID, $storyID)
    {
        $story = $this->cacheStoryRepo->getStory($issueID, $storyID);

        if (empty($story)) {
            if (empty($this->siteID) || empty($this->legacyIssueID)) {
                // log
                return [];
            }

            $story = $this->apiStoryRepo
                ->config($this->siteID, $this->legacyIssueID)
                ->getStory($issueID, $storyID)
            ;
        }

        return $story;
    }

    public function getStoryList($issueID)
    {
        $stories = $this->cacheStoryRepo->getStoryList($issueID);

        if (empty($stories)) {
            if (empty($this->siteID) || empty($this->legacyIssueID)) {
                // log
                return [];
            }

            $stories = $this->apiStoryRepo
                ->config($this->siteID, $this->legacyIssueID)
                ->getStoryList($issueID)
            ;
        }

        return $stories;
    }
}