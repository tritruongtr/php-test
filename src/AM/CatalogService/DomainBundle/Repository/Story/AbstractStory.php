<?php

namespace AM\CatalogService\DomainBundle\Repository\Story;

use AM\CatalogService\DomainBundle\Traits\CMSHelper;

abstract class AbstractStory
{
    use CMSHelper;

    abstract public function getStoryList($issueID);

    protected function formatStoryLayout(array $story)
    {
        if (!empty($story['layout'])) {
            return $story;
        }

        $story['layout'] = json_decode('{}');

        return $story;
    }
}
