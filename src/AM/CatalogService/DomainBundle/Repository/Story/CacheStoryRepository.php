<?php

namespace AM\CatalogService\DomainBundle\Repository\Story;

class CacheStoryRepository extends AbstractStory
{
    const KEY_NAME = 'catalog:issues:%d:%s:stories';
    const CACHE_EXPIRE = 604800; // 1 week

    private $excludeQueryStrings = [
        'issue_has_xml',
        'publication_allow_xml'
    ];

    public function __construct($redis, $queryString, $logger)
    {
        $this->logger = $logger;
        $this->redis = $redis;
        $this->queryString = $this->queryStringWalk($queryString, $this->excludeQueryStrings);
        ksort($this->queryString);
    }

    public function getStoryList($issueID)
    {
        try {
            $keyName = $this->getKeyName($issueID);
            $stories = $this->redis->GET($keyName);
        } catch (\Exception $e) {
            $stories = null;
        }

        if (empty($stories)) {
            return [];
        }

        $stories = json_decode($stories, true);
        foreach ($stories as &$story) {
            $story = $this->formatStoryLayout($story);
        }

        return $stories;
    }

    public function getStory($issueID, $storyID)
    {
        $stories = $this->getStoryList($issueID);

        if (empty($stories)) {
            return [];
        }

        foreach ($stories as $story) {
            if ($story['id'] == $storyID) {
                return $story;
            }
        }

        return [];
    }

    public function getKeyName($issueID)
    {
        $queryString = $this->queryStringToRedisKey($this->queryString);

        return sprintf(static::KEY_NAME, $issueID, $queryString);
    }

    public function save($stories, $issueID)
    {
        $this->logger->info('Starting write to cache');
        if (is_array($stories)) {
            $stories = json_encode($stories);
        }

        try {
            $keyName = $this->getKeyName($issueID);
            $this->redis->SET($keyName, $stories);
            $this->redis->EXPIRE($keyName, static::CACHE_EXPIRE);
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
