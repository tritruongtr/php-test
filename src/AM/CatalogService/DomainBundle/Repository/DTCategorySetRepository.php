<?php

namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use AM\CatalogService\Domain\CategorySet\CategorySet;
use Zenith\CoreService\DoctrineRepository\DTBaseRepository;

/**
 * @author Ca Pham <ca.pham@audiencemedia.com>
 */
class DTCategorySetRepository extends DTBaseRepository
{
    public function buildDQL(QueryBuilder &$queryBuilder, $newParams = [], $rootAlias)
    {
        if (!empty($newParams)) {
            $expr = $queryBuilder->expr();
            $fieldMappings = $this->getFieldMappings();
            foreach ($newParams as $key => $value) {
                if (!isset($fieldMappings[$key]) || empty($value)) {
                    continue;
                }
            
                switch ($key) {
                    case 'name':
                        $queryBuilder->andWhere($rootAlias . '.name LIKE :name')
                            ->setParameter('name', "%$value%");
                        break;
                    default:
                        $queryBuilder->andWhere(
                            $expr->eq("$rootAlias." . $key, ':' . $key)
                        )->setParameter($key, $value);
                        break;
                }
            }
        }
    }
}
