<?php
/**
 * @author Can Ly <can.ly@audiencemedia.com>
 */
namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Util\Inflector;

use AM\CatalogService\Domain\IssueMetadata\IssueMetadata;
use AM\CatalogService\Domain\IssueMetadata\IssueMetadataRepository;

use Zenith\CoreService\DoctrineRepository\DTBaseRepository;

class DTIssueMetadataRepository extends DTBaseRepository
{
    public function getListByQueryParams(array $params, $ignoreLimit = false, array $selectProps = [])
    {
        $fieldMappings = $this->getFieldMappings();
        $queryBuilder = $this->createQueryBuilder('i');
        $rootAlias = $queryBuilder->getRootAlias();

        $selects = $this->buildSelectDQL($selectProps, $fieldMappings, $rootAlias);
        if (!empty($selects)) {
            $queryBuilder->select($selects);
        }

        $queryBuilder = $this->getQuery($queryBuilder, $params);
        $queryBuilder->groupBy("$rootAlias.id");

        $limit = null;
        $offset = null;
        if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        if (!empty($data)) {
            foreach ($data as & $metadata) {
                if (isset($metadata['key_value'])) {
                    $metadata['key_value'] = @unserialize($metadata['key_value']);
                    if ($metadata['key_value'] === false) {
                        $metadata['key_value'] = new \stdClass();
                    }
                }
            }
        }

        $pagination = $this->getPagination($queryBuilder, $limit, $offset);
        return array('data' => $data, 'pagination' => $pagination);
    }

    private function getQuery($queryBuilder, array $params)
    {
        $classMetaInfo = $this->getClassMetadata();
        $expr = $queryBuilder->expr();

        $newParams = [];
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $joinPublication = false;
        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'id':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('i.id', $value));
                    }
                    break;

                case 'issueId':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('i.issueId', $value));
                    }
                    break;

                default:
                    break;
            }
        }
        return $queryBuilder;
    }

    /**
     * Get single metadata by issue id and key name
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  integer $id  ID of issue
     * @param  string  $key Key name
     *
     * @return IssueMetadata
     */
    public function findOneByIssueIdKey($id, $key)
    {
        $metadata = $this->findOneBy(['issueId' => $id, 'keyName' => $key]);

        if (!$metadata instanceof IssueMetadata) {
            return false;
        }

        return $metadata;
    }
}