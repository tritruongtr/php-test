<?php

namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use AM\CatalogService\Domain\Publisher\Publisher;
use AM\CatalogService\Domain\Publisher\PublisherRepository;
use AM\CatalogService\Domain\Catalog\Catalog;

use Zenith\CoreService\Intl\Country;
use Zenith\CoreService\Intl\Locale;
use Zenith\CoreService\Intl\Language;

class DTPublisherRepository extends EntityRepository implements PublisherRepository
{
    private $fieldMappings;
    private $classMetadata;

    public function getClassMetadata()
    {
        if (!empty($this->classMetadata)) {
            return $this->classMetadata;
        }

        $this->classMetadata = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        return $this->classMetadata;
    }

    public function getFieldMappings()
    {
        if (!empty($this->fieldMappings)) {
            return $this->fieldMappings;
        }

        $this->fieldMappings = $this->getClassMetadata()->fieldMappings;

        return $this->fieldMappings;
    }

    public function buildSelectDQL(array $selectProps, array $fieldMappings, $rootAlias)
    {
        if (empty($selectProps)) {
            return null;
        }

        $selectProps = array_unique($selectProps);
        $selects = [];
        foreach ($selectProps as $selectProp) {
            if (isset($fieldMappings[$selectProp])) {
                $underscoreProp = Inflector::tableize($selectProp);
                $selects[] = "$rootAlias.$selectProp AS $underscoreProp";
            }
        }

        if (!empty($selects)) {
            $selects = implode(',', $selects);
            return $selects;
        }

        return null;
    }

    /**
     * Get list of publishers by query string parameters
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array  $params
     *
     * @return array
     */
    public function getListByQueryParams(array $params, $ignoreLimit = false, array $selectProps = [])
    {
        $fieldMappings = $this->getFieldMappings();
        $queryBuilder = $this->createQueryBuilder('p');
        $rootAlias = $queryBuilder->getRootAlias();

        $selects = $this->buildSelectDQL($selectProps, $fieldMappings, $rootAlias);

        if (!empty($selects)) {
            $queryBuilder->select($selects);
        }

        $queryBuilder = $this->getQuery($queryBuilder, $params);

        if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $queryBuilder->groupBy("$rootAlias.id");

        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        foreach ($data as &$publisher) {
            $publisher = static::intlProcessor($publisher);
        }

        if (!$ignoreLimit)
        {
            $count = $this->count($params);
            $pagination = new \Zenith\CoreService\Pagination\Pagination($count, $offset, $limit);
            return array('data' => $data, 'pagination' => $pagination->getResult());
        }

        return array('data' => $data, 'pagination' => null);

    }

    /**
     * Intl processor, especially for country code, locale code and language code
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array  $data
     *
     * @return array
     */
    public static function intlProcessor(array $data = [], $removeOriginalProp = true)
    {
        if (isset($data['country_code'])) {
            $data['country'] = Country::instance()->getInfoByCode($data['country_code']);
            if ($removeOriginalProp === true) {
                unset($data['country_code']);
            }
        }

        if (isset($data['locale_code'])) {
            $data['locale'] = Locale::instance()->getInfoByCode($data['locale_code']);
            if ($removeOriginalProp === true) {
                unset($data['locale_code']);
            }
        }

        if (isset($data['language_code'])) {
            $data['language'] = Language::instance()->getInfoByCode($data['language_code']);
            if ($removeOriginalProp === true) {
                unset($data['language_code']);
            }
        }

        return $data;
    }

    public function count(array $params)
    {
        $params['offset'] = 0;
        $queryBuilder = $this->getQuery($this->createQueryBuilder('p')->select('COUNT(p.id)'), $params);
        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * Get list of publishers by query string parameters
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array  $params
     * @deprecated
     * @return array
     */
    public function getListByQueryParams_old(array $params)
    {
        $classMetaInfo = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        $queryBuilder = $this->createQueryBuilder('p');
        $expr = $queryBuilder->expr();

        $newParams = [];
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $orderBy = 'DESC';
        if (isset($newParams['orderBy'])) {
            $lowerOrderBy = strtolower($newParams['orderBy']);
            if (in_array($lowerOrderBy, ['asc', 'desc'])) {
                $orderBy = strtoupper($lowerOrderBy);
            }
        }

        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'sortBy':
                    if (!empty($value)) {
                        $columns = explode(',', $value);
                        // remove empty columns and dupliate columns
                        $columns = array_unique(array_filter($columns));
                        if (!empty($columns)) {
                            foreach ($columns as $column) {
                                $columnName = lcfirst(Inflector::classify($column));
                                if ($classMetaInfo->hasField($columnName)) {
                                    $queryBuilder->addOrderBy("p.$columnName", $orderBy);
                                }
                            }
                        }
                    }
                    $queryBuilder->addOrderBy('p.id', $orderBy);
                    break;

                case 'countryCode':
                    if (!empty($value)) {
                        $queryBuilder->andWhere(
                            $expr->eq("p.$key", ':country_code')
                        )->setParameter('country_code', $value);
                    }
                    break;

                case 'remoteIdentifier':
                    if (!empty($value)) {
                        $queryBuilder->andWhere(
                            $expr->eq("p.$key", ':remote_identifier')
                        )->setParameter('remote_identifier', $value);
                    }
                    break;

                case 'status':
                    $status = 1;
                    if (is_numeric($value)) {
                        $status = (int) $value;
                    }
                    $queryBuilder->andWhere(
                        $expr->eq("p.$key", ':status')
                    )->setParameter('status', $status);
                    break;

                case 'limit':
                    $limit = $value;
                    if (empty($limit) || (int) $limit <= 0) {
                        $limit = 10;
                    }
                    $queryBuilder->setMaxResults($limit);
                    break;

                case 'offset':
                    $offset = $value;
                    if (empty($offset) || (int) $offset <= 0) {
                        $offset = 0;
                    }
                    $queryBuilder->setFirstResult($offset);
                    break;

                default:
                    break;
            }
        }

        $query = $queryBuilder->getQuery();

        $paginator = new Paginator($query, /* $fetchJoinCollection = */ false);
        $count = $paginator->count();

        $data = $paginator->getIterator()->getArrayCopy();

        return array('data' => $data, 'count' => $count);
    }

    private function getQuery($queryBuilder, array $params)
    {
        $expr = $queryBuilder->expr();

        $classMetaInfo = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        $newParams = array();
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $orderBy = 'DESC';
        if (isset($newParams['orderBy'])) {
            $lowerOrderBy = strtolower($newParams['orderBy']);
            if (in_array($lowerOrderBy, array('asc', 'desc'))) {
                $orderBy = strtoupper($lowerOrderBy);
            }
        }

        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'name':
                    if (!empty($value)) {
                        $queryBuilder
                            ->andWhere($expr->like('p.name', ':name'))
                            ->setParameter('name', '%'.$value.'%');;
                    }
                    break;

                case 'sortBy':
                    if (!empty($value)) {
                        $columns = explode(',', $value);
                        // remove empty columns and dupliate columns
                        $columns = array_unique(array_filter($columns));
                        if (!empty($columns)) {
                            foreach ($columns as $column) {
                                $columnName = lcfirst(Inflector::classify($column));
                                if ($classMetaInfo->hasField($columnName)) {
                                    $queryBuilder->addOrderBy("p.$columnName", $orderBy);
                                }
                            }
                        }
                    }
                    $queryBuilder->addOrderBy('p.id', $orderBy);
                    break;

                case 'id':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('p.id', $value));
                    }
                    break;

                case 'countryCode':
                    if (!empty($value)) {
                        $queryBuilder->andWhere(
                            $expr->eq("p.$key", ':country_code')
                        )->setParameter('country_code', $value);
                    }
                    break;

                case 'remoteIdentifier':
                    if (!empty($value)) {
                        if ($value == 'null') {
                            $queryBuilder->andWhere("p.$key is NULL or p.$key = ''");
                        } else {
                            $queryBuilder->andWhere(
                                $expr->eq("p.$key", ':remote_identifier')
                            )->setParameter('remote_identifier', $value);
                        }
                    }
                    break;

                case 'status':
                    if (is_numeric($value)) {
                        $status = (int) $value;

                        $queryBuilder->andWhere(
                            $expr->eq("p.$key", ':status')
                        )->setParameter('status', $status);
                    }

                    break;

                case 'limit':
                    $limit = $value;
                    if (empty($limit) || (int) $limit <= 0) {
                        $limit = 10;
                    }
                    $queryBuilder->setMaxResults($limit);
                    break;

                case 'offset':
                    $offset = $value;
                    if (empty($offset) || (int) $offset <= 0) {
                        $offset = 0;
                    }
                    $queryBuilder->setFirstResult($offset);
                    break;

                default:
                    break;
            }
        }

        return $queryBuilder;
    }

    /**
     * Exclusion strategy by JMS group name
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  Entity|Collection $data     Entity or array collection of entity
     * @param  string            $JMSGroup Name of JMS group
     *
     * @return array                       Array after the exclusion was done
     */
    public function getFinalResultByJMSGroup($data, $JMSGroup)
    {
        $serializer = SerializerBuilder::create()->build();
        $json = $serializer->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(
                [$JMSGroup]
            )->setSerializeNull(true)
        );
        $arr = json_decode($json, true);
        return $arr;
    }


    public function getCatalogPublishers(Catalog $catalog, $params = array())
    {
        $publishers = [];
        $tempPublishers = [];

        foreach ($catalog->getCatalogPublication() as $catalogPublication) {
            $publication = $catalogPublication->getPublication();
            if ($params['content_rating'] != null && ($publication->get('content_rating') < $params['content_rating'][0] || $publication->get('content_rating') > $params['content_rating'][1])) {
                continue;
            }

            $publisher = $publication->get('publisher');

            if (in_array($publisher->get('id'), $tempPublishers)) {
                continue;
            }

            if($params['status'] != null && $publisher->get('status') != $params['status']) {
                continue;
            }

            $publishers[] = $publisher;
            $tempPublishers[] = $publisher->get('id');
        }

        return $publishers;
    }
}