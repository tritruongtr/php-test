<?php
namespace AM\CatalogService\DomainBundle\Repository;

use AM\CatalogService\Domain\Catalog\Catalog;
use AM\CatalogService\Domain\Category\Category;
use Doctrine\Common\Util\Inflector;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Zenith\CoreService\DoctrineRepository\DTBaseRepository;

class DTCategoryRepository extends DTBaseRepository
{
    private $publicationCategoryRepo;

    public function initialize($repo)
    {
        $this->publicationCategoryRepo = $repo;
    }
    /**
     * Get all categories by attribute
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @return array
     */
    public function all($params = array())
    {
        $classMetaInfo = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        $projectId = $params['project_id'];
        $pcat = $params['parent_id'];
        $remoteId = $params['remote_identifier'];
        $depth = $params['depth'];
        $limit = $params['limit'];
        $offset = $params['offset'];
        $ignoreLimit = isset($params['ignore_limit']) && ($params['ignore_limit'] == 1);

        if (null == $depth) {
            $depth = 30;
        }
        $condition = [];
        if ($pcat == null || (int) $pcat == 0) {
            $pcat = 0;
        } else {
            $pcat = (int) $pcat;
        }
        $condition['parentId'] = $pcat;
        
        if ($projectId != null) {
            $condition['projectId'] = (int) $projectId;
        }

        if ($remoteId != null) {
            $condition['remoteIdentifier'] = $remoteId;
        }

        if (isset($params['sort'])) {
            $arrSort = array();
            $columns = explode(',', $params['sort']);
            // remove empty columns and dupliate columns
            $columns = array_unique(array_filter($columns));
            if (!empty($columns)) {
                foreach ($columns as $column) {
                    $tmpColumn = explode('-', $column);
                    if (2 == count($tmpColumn) && ('' == $tmpColumn[0])) {
                        $column = $tmpColumn[1];
                        $orderBy = 'desc';
                    } else {
                        $orderBy = 'asc';
                    }
                    $columnName = lcfirst(Inflector::classify($column));
                    if ($classMetaInfo->hasField($columnName)) {
                        $arrSort[$columnName] = $orderBy;
                    }
                }
            }
        } else {
            $arrSort['id'] = 'desc';
        }

        if ($limit == null || (int) $limit <= 0) {
            $limit = 10;
        }

        if ($offset == null || (int) $offset <= 0) {
            $offset = 0;
        }
 
        if ($remoteId != null) {
            $all = $this->findBy(
                $condition,
                $arrSort
            );
            $count = count($all);

            $conFindBy = [$condition, $arrSort, $limit, $offset];
            if ($ignoreLimit) {
                $conFindBy = [$condition, $arrSort];
            }
            $categories = $this->findBy($conFindBy);
            $categories = $this->reformArray($categories, $projectId);

            return array('data' => $categories, 'count' => $count);
        }

        $hierarchical = $this->getHierarchical(true, $condition, $arrSort);
        $categories = $this->getChild($hierarchical, $pcat, $depth);
        if (empty($categories)) {
            // total = 0;
            return array('data' => [], 'count' => 0);
        }

        $categoryListTemp = $this->reformArray($categories, $projectId);
        // rsort($categoryListTemp);

        // total here
        $count = count($categoryListTemp);
        $data = $categoryListTemp;
        if (!$ignoreLimit) {
            $data = array_slice($categoryListTemp, $offset, $limit);
        }

        return array('data' => $data, 'count' => $count);
    }

    /**
     * Reform object categories
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @return array
     */
    public function reformArray($categories, $project = null)
    {
        foreach ($categories as $category) {
            $categoryId = $category->get('id');
            $projectId = $category->get('projectId');
            $image = $category->get('image');
            $parentId = $category->get('parentId');
            $params = array(
                'category_id' => $categoryId,
            );
            if ($project != null) {
                $params['project_id'] = $project;
            }
            $params['status'] = 1;
            $noOfPublications = $this->publicationCategoryRepo->count($params);
            $tempCat = [
                'id' => $categoryId,
                'project_id' => $projectId,
                'name' => $category->getName(),
                'description' => $category->getDescription(),
                'image' => $image,
                'slug' => $category->getSlug(),
                'remote_identifier' => $category->getRemoteIdentifier(),
                'parent_id' => $parentId,
                'no_of_publications' => (int) $noOfPublications,
                'status' => $category->get('status'),
                'priority' => $category->get('priority'),
                'category_set_id' => $category->get('category_set_id'),
                'created_at' => $category->get('createdAt'),
                'modified_at' => $category->get('modifiedAt')
            ];
            $arrCat[] = $tempCat;
        }
        return $arrCat;
    }

    /**
     * Format detail category
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @return array
     */
    public function formatDetailCategory($category, $project = null)
    {
        $parentId = $category->getParentCategoryId();
        $parent = $this->find($parentId);
        $serializer = SerializerBuilder::create()->build();
        $parentJson = $serializer->serialize(
            $parent,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )
        );
        $parent = json_decode($parentJson, true);
        if (empty($parent)) {
            $parent = [];
        }

        $children = $this->findBy(['parentId' => $category->getId()]);
        if (empty($children)) {
            $children = [];
        } else {
            $children = $this->reformArray($children);
        }
        $params = array(
            'category_id' => $category->getId(),
        );
        if ($project != null) {
            $params['project_id'] = $project;
        }
        $params['status'] = 1;
        $noOfPublications = $this->publicationCategoryRepo->count($params);
        $return = [
            'id' => $category->getId(),
            'project_id' => $category->get('projectId'),
            'name' => $category->getName(),
            'description' => $category->getDescription(),
            'image' => $category->get('image'),
            'slug' => $category->getSlug(),
            'remote_identifier' => $category->getRemoteIdentifier(),
            'parent_id' => $category->get('parentId'),
            'parent' => $parent,
            'children' => $children,
            'no_of_publications' => (int) $noOfPublications,
            'status' => $category->get('status'),
            'priority' => $category->get('priority'),
            'category_set_id' => $category->get('category_set_id'),
            'created_at' => $category->get('createdAt'),
            'modified_at' => $category->get('modifiedAt')
        ];

        return $return;
    }

    /**
     * Get categories hierarchical by id or parent id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  boolean $useParentKey TRUE will set parent id as Key
     *
     * @return array
     */
    public function getHierarchical($useParentKey = false, $condition = [], $arrSort = [])
    {
        $categories = $this->findBy($condition, $arrSort);

        if (empty($categories)) {
            return [];
        }

        $data = [];
        foreach ($categories as $category) {
            $parentId = $category->get('parentId');
            $id = $category->get('id');
            if ($useParentKey === false) {
                $data[$id] = $category;
            } else {
                $data[$parentId][$id] = $category;
            }
        }

        return $data;
    }

    /**
     * Get list of children categories
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array   $hierarchical See self::getHierarchical()
     * @param  integer $categoryId   Parent category id of list of children
     * @param  integer $depth        Depth level of list children
     * @param  array   $found        Pass result value to recursive function
     *
     * @return array
     */
    public function getChild($hierarchical, $categoryId, $depth = 1, $found = [])
    {
        if ($depth == 0) {
            return $found;
        }

        $depth--;

        if (isset($hierarchical[$categoryId])) {
            foreach ($hierarchical[$categoryId] as $childCategory) {
                $nextId = $childCategory->get('id');
                $found[$nextId] = $childCategory;
                $found += $this->getChild($hierarchical, $nextId, $depth, $found);
            }

        }

        return $found;
    }

    /**
     * Get root parent category id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array   $hierarchical  See self::getHierarchical()
     * @param  integer $categoryId    Root parent category id wanna get from this
     * @param  integer $foundParentId Pass result value to recursive function
     *
     * @return interger
     */
    public function getRootParentCategoryId($hierarchical, $categoryId, $foundParentId = 0)
    {
        if (!isset($hierarchical[$categoryId])) {
            // why can not be found???
            return $foundParentId;
        }

        $category = $hierarchical[$categoryId];
        $parentId = $category->get('parentId');
        if ($parentId == 0) {
            return $foundParentId;
        }

        $foundParentId = $parentId;
        return $this->getRootParentCategoryId($hierarchical, $parentId, $foundParentId);
    }

    public function getCatalogCategories(Catalog $catalog, $params = array())
    {
        $categories = [];
        $tempCategories = [];

        foreach ($catalog->getCatalogPublication() as $catalogPublication) {
            $publication = $catalogPublication->getPublication();
            if ($params['content_rating'] != null && ($publication->get('content_rating') < $params['content_rating'][0] || $publication->get('content_rating') > $params['content_rating'][1])) {
                continue;
            }

            $publicationsCategories = $publication->getPublicationCategory();

            foreach ($publicationsCategories as $publicationCategory) {
                $category = $publicationCategory->getCategory();

                $rootCategoryId = $publicationCategory->getRootParentCategoryId();

                if ($rootCategoryId != 0) {
                    $rootCategory = $this->find($rootCategoryId);
                    if ($rootCategory instanceof Category) {
                        $category = $rootCategory;
                        $category->setRepositoryCategory($this);
                    }

                }

                if (in_array($category->get('id'), $tempCategories)) {
                    continue;
                }

                if ($params['project_id'] != null && $params['project_id'] == $category->get('project_id')) {
                    $tempCategories[] = $category->get('id');
                    $category = $this->formatDetailCategory($category);
                    $categories[] = $category;
                }
            }
        }

        return $categories;
    }

    /**
     * Format tree category
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @return array
     */
    public function getCategory($parameters)
    {
        $classMetaInfo = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        $projectId = isset($parameters["project_id"]) ? $parameters["project_id"] : '-1';
        $contentRating = isset($parameters["content_rating"]) ? $parameters["content_rating"] : '';
        $queryContentRating = '1 = 1';

        if ($contentRating != '') {
            $explode = explode('-', $contentRating);
            if (count($explode) == 2) {
                $minRate = intval($explode[0]);
                $maxRate = intval($explode[1]);
                $queryContentRating = " p.`content_rating` BETWEEN " . $minRate . " AND " . $maxRate . " ";
            }
        }

        $languageCode = isset($parameters["language"]) ? $parameters["language"] : '-1';
        $localeCode = isset($parameters["locale"]) ? $parameters["locale"] : '-1';
        $catalogId = isset($parameters["catalog_id"]) ? $parameters["catalog_id"] : '-1';
        $sort = isset($parameters["sort"]) ? $parameters["sort"] : '-id';
        $expSort = explode('-', $sort);
        $limit = isset($parameters["limit"]) ? $parameters["limit"] : 10;
        $offset = isset($parameters["offset"]) ? $parameters["offset"] : 0;

        if (count($expSort) == 2 && $expSort[0] == '' && $expSort[1] != '') {
            $sortColumn = lcfirst(Inflector::classify($expSort[1]));
            $sortBy = 't.' . $expSort[1];
            $orderBy = "DESC";
        } else {
            $sortColumn = lcfirst(Inflector::classify($expSort[0]));
            $sortBy = 't.' . $expSort[0];
            $orderBy = "ASC";
        }

        $queryCategorySet = '';
        if (isset($parameters['category_set_id']) && is_numeric($parameters['category_set_id']) && 0 != $parameters['category_set_id']) {
            $queryCategorySet = ' AND cate.`category_set_id` = '.$parameters['category_set_id'];
        }

        $sql = "SELECT * FROM
                (
                    (
                        SELECT cate.*
                        FROM `catalogs` c
                            JOIN `catalogs_publications` cp ON cp.`catalog_id` = c.id
                            JOIN publications p ON cp.`publication_id` = p.id
                            JOIN `issues` i ON p.id = i.publication_id
                            JOIN `publications_categories` pc ON pc.`publication_id` = p.id
                            JOIN `categories` cate ON pc.`root_parent_category_id` = cate.id
                        WHERE $queryContentRating
                            AND i.`status` = 1
                            AND cp.`catalog_id` = $catalogId
                            AND p.`status` = 1
                            AND cp.`status` = 1
                            $queryCategorySet
                        GROUP BY cate.id
                        ORDER BY cate.`id`
                    )
                    UNION
                    (
                        SELECT cate.*
                        FROM `catalogs` c
                            JOIN `catalogs_publications` cp ON cp.`catalog_id` = c.id
                            JOIN publications p ON cp.`publication_id` = p.id
                            JOIN `issues` i ON p.id = i.publication_id
                            JOIN `publications_categories` pc ON pc.`publication_id` = p.id
                            JOIN `categories` cate ON pc.`category_id` = cate.id
                        WHERE $queryContentRating
                            AND i.`status` = 1
                            AND cp.`catalog_id` = $catalogId
                            AND p.`status` = 1
                            AND cp.`status` = 1
                            $queryCategorySet
                        GROUP BY cate.id
                        ORDER BY cate.`id`
                    )
                ) as t";

        if ($classMetaInfo->hasField($sortColumn)) {
            $sql .= " ORDER BY $sortBy $orderBy";
        }

        $em = $this->getEntityManager();
        $categories = $em->getConnection()->fetchAll($sql);
        $categories = $this->tree1($categories, $catalogId);

        $total = count($categories);
        $pagination = new \Zenith\CoreService\PaginationBundle\Pagination($total, $offset, $limit);
        $pagination = $pagination->getResult();

        $data = array_slice($categories, $offset, $limit);

        return ['data' => $data, 'pagination' => $pagination];
    }

    /**
     * Copy from tree() function and refactor it
     */
    public function tree1(array $categories = [], $catalogId = null, $cpStatus = null)
    {
        $categoryIDs = array_column($categories, 'id');
        if (empty($categoryIDs)) {
            return [];
        }

        $noOfPubData = $this->getNoOfPublications(
            $categoryIDs,
            $catalogId,
            /* $categoryStatus = */ 1, // @todo, newsstand should pass parameter instead
            /* $cpStatus = */ 1, // @todo, newsstand should pass parameter instead
            /* $issueStatus = 1 */ 1 // @todo, newsstand should pass parameter instead
        );

        $arr = [];
        foreach ($categories as $category) {
            if ($category['parent_id'] != 0) {
                continue;
            }

            $category = [
                'id' => (int) $category['id'],
                'project_id' => (int) $category['project_id'],
                'name' => $category['name'],
                'description' => $category['description'],
                'image' => $category['image'],
                'slug' => $category['slug'],
                'remote_identifier' => $category['remote_identifier'],
                'parent_id' => (int) $category['parent_id'],
                'status' => (int) $category['status'],
                'priority' => (int) $category['priority'],
            ];

            $this->appendNoOfPublications($category, $noOfPubData);

            foreach ($categories as $item) {
                if ($item['parent_id'] != $category['id']) {
                    continue;
                }

                $item = [
                    'id' => (int) $item['id'],
                    'project_id' => (int) $item['project_id'],
                    'name' => $item['name'],
                    'description' => $item['description'],
                    'image' => $item['image'],
                    'slug' => $item['slug'],
                    'remote_identifier' => $item['remote_identifier'],
                    'parent_id' => (int) $item['parent_id'],
                    'status' => (int) $item['status'],
                    'priority' => (int) $item['priority'],
                ];

                $this->appendNoOfPublications($item, $noOfPubData);

                $category['children'][] = $item;
            }

            $arr[] = $category;
        }

        return $arr;
    }

    /**
     * Format tree category
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @return array
     */
    public function tree($categories = [])
    {
        $arr = [];
        foreach ($categories as $category) {
            $category = [
                'id' => (int) $category['id'],
                'project_id' => (int) $category['project_id'],
                'name' => $category['name'],
                'description' => $category['description'],
                'image' => $category['image'],
                'slug' => $category['slug'],
                'remote_identifier' => $category['remote_identifier'],
                'parent_id' => (int) $category['parent_id'],
                'status' => (int) $category['status'],
                'priority' => (int) $category['priority'],
            ];
            $params = array(
                'category_id' => $category['id'],
                'project_id' => $category['project_id'],
                'status' => 1,
            );
            $noOfPublications = $this->publicationCategoryRepo->count($params);
            $category['no_of_publications'] = (int) $noOfPublications;
            if ($category['parent_id'] == 0) {
                foreach ($categories as $item) {
                    $item = [
                        'id' => (int) $item['id'],
                        'project_id' => (int) $item['project_id'],
                        'name' => $item['name'],
                        'description' => $item['description'],
                        'image' => $item['image'],
                        'slug' => $item['slug'],
                        'remote_identifier' => $item['remote_identifier'],
                        'parent_id' => (int) $item['parent_id'],
                        'status' => (int) $item['status'],
                        'priority' => (int) $item['priority'],
                    ];
                    if ($item['parent_id'] == $category['id']) {
                        $params = array(
                            'category_id' => $item['id'],
                            'project_id' => $item['project_id'],
                            'status' => 1,
                        );
                        $noOfPublications = $this->publicationCategoryRepo->count($params);
                        $item['no_of_publications'] = (int) $noOfPublications;
                        $category["children"][] = $item;
                    }
                }
                $arr[] = $category;
            }
        }
        return $arr;
    }

    public function getListParent($categoryId, &$listCategory = [])
    {
        $category = $this->find($categoryId);
        if ($category instanceof Category) {
            $parentCategory = $category->get('parent_id');
            if ($parentCategory != 0) {
                $listCategory[] = $parentCategory;
                $this->getListParent($parentCategory, $listCategory);
            }
        }
        return $listCategory;
    }

    public function getListChild($categoryId, &$listCategory = [])
    {
        $categories = $this->findBy(['parentId' => $categoryId]);
        if (!empty($categories)) {
            foreach ($categories as $item) {
                $category = $item->get('id');
                array_push($listCategory, $category);
                $this->getListChild($category, $listCategory);
            }
        }
        return $listCategory;
    }

    public function getNoOfPublications(
        $categoryIDs,
        $catalogId = null,
        $categoryStatus = null,
        $cpStatus = null,
        $issueStatus = null
    )
    {
        if (is_array($categoryIDs)) {
            $categoryIDs = implode(',', $categoryIDs);
        }

        if (empty($categoryIDs)) {
            return 0;
        }

        if ($catalogId === null) {
            $catalogId = '';
        }

        $catalogId = ($catalogId === null || $catalogId < 0) ? '1 = 1' : "cp.catalog_id = $catalogId";
        $categoryStatus = ($categoryStatus === null) ? '1 = 1' : "c.status = $categoryStatus";
        $cpStatus = ($cpStatus === null) ? '1 = 1' : "cp.status = $cpStatus";
        $issueStatus = ($issueStatus === null) ? '1 = 1' : "i.status = $issueStatus";

        $sql = "SELECT pc.*, COUNT(DISTINCT pc.publication_id) as no_of_publications
                FROM publications_categories pc
                    JOIN catalogs_publications cp ON cp.publication_id = pc.publication_id
                    JOIN categories c ON c.id = pc.category_id
                    JOIN issues i ON i.publication_id = pc.publication_id
                WHERE $catalogId
                    AND $categoryStatus
                    AND $cpStatus
                    AND $issueStatus
                    AND pc.category_id IN ($categoryIDs)
                GROUP BY pc.category_id"
        ;

        $em = $this->getEntityManager();
        return $em->getConnection()->fetchAll($sql);
    }

    public function appendNoOfPublications(array &$category, $noOfPubData)
    {
        $index = array_search($category['id'], array_column($noOfPubData, 'category_id'));
        if ($index === false) {
            $category['no_of_publications'] = 0;
        } else {
            $category['no_of_publications'] = (int) $noOfPubData[$index]['no_of_publications'];
        }
    }
}
