<?php

namespace AM\CatalogService\DomainBundle\Repository\Section;

use AM\CatalogService\DomainBundle\Traits\CMSHelper;

abstract class AbstractSection
{
    use CMSHelper;

    abstract public function getSectionList($issueID);
}
