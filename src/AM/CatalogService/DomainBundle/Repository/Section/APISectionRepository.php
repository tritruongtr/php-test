<?php

namespace AM\CatalogService\DomainBundle\Repository\Section;

class APISectionRepository extends AbstractSection
{
    public function __construct($legacyRepo, $cacheSectionRepo)
    {
        $this->legacyRepo = $legacyRepo;
        $this->cacheSectionRepo = $cacheSectionRepo;
    }

    public function getSectionList($issueID)
    {
        $sections = $this->legacyRepo
            ->setUp($this->siteID)
            ->getIssueSections($this->legacyIssueID)
        ;

        if (empty($sections)) {
            return [];
        }

        $this->writeTocache($sections, $issueID);

        return $sections;
    }

    public function writeTocache($sections, $issueID)
    {
        $this->cacheSectionRepo->save($sections, $issueID);
    }
}
