<?php

namespace AM\CatalogService\DomainBundle\Repository\Section;

class SectionRepository
{
    private $siteID = null;
    private $legacyIssueID = null;

    public function __construct($cacheSectionRepo, $apiSectionRepo)
    {
        $this->cacheSectionRepo = $cacheSectionRepo;
        $this->apiSectionRepo = $apiSectionRepo;
    }

    public function config($siteID, $legacyIssueID)
    {
        $this->siteID = $siteID;
        $this->legacyIssueID = $legacyIssueID;

        return $this;
    }

    public function getSectionList($issueID)
    {
        $sections = $this->cacheSectionRepo->getSectionList($issueID);

        if (empty($sections)) {
            if (empty($this->siteID) || empty($this->legacyIssueID)) {
                // log
                return [];
            }

            $sections = $this->apiSectionRepo
                ->config($this->siteID, $this->legacyIssueID)
                ->getSectionList($issueID)
            ;
        }

        return $sections;
    }
}