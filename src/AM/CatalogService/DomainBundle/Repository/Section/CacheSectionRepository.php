<?php

namespace AM\CatalogService\DomainBundle\Repository\Section;

class CacheSectionRepository extends AbstractSection
{
    const KEY_NAME = 'catalog:issues:%d:%s:sections';
    const CACHE_EXPIRE = 604800; // 1 week

    public function __construct($redis, $queryString)
    {
        $this->redis = $redis;
        $this->queryString = $this->queryStringWalk($queryString);
        ksort($this->queryString);
    }

    public function getSectionList($issueID)
    {
        try {
            $keyName = $this->getKeyName($issueID);
            $sections = $this->redis->GET($keyName);
        } catch (\Exception $e) {
            $sections = null;
        }

        if (empty($sections)) {
            return [];
        }

        $sections = json_decode($sections, true);

        return $sections;
    }

    public function getKeyName($issueID)
    {
        $queryString = $this->queryStringToRedisKey($this->queryString);

        return sprintf(static::KEY_NAME, $issueID, $queryString);
    }

    public function save($sections, $issueID)
    {
        if (is_array($sections)) {
            $sections = json_encode($sections);
        }

        try {
            $keyName = $this->getKeyName($issueID);
            $this->redis->SET($keyName, $sections);
            $this->redis->EXPIRE($keyName, static::CACHE_EXPIRE);
        } catch (\Exception $e) {

        }
    }
}
