<?php 
namespace AM\CatalogService\DomainBundle\Repository;
  
use Doctrine\ORM\EntityRepository;  
use AM\CatalogService\Domain\CategoryTranslation\CategoryTranslationRepository;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Zenith\CoreService\Intl\Locale;
use Zenith\CoreService\Intl\Language;
  
class DTCategoryTranslationRepository extends EntityRepository implements CategoryTranslationRepository
{
    /**
     * get filtered categories
     *
     * @param array $params
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function getFilteredCategories($params)
    {
        if (!isset($params['categories']) || !isset($params['language']) || !isset($params['locale'])) {
            return array();
        }

        $categories = $params['categories'];
        if (count($categories) == 0) {
            return array();
        }

        $listCategories = $this->getIdFromCategory($categories);
        $source = $this->translate($params['locale'], $listCategories);
        $this->replace($categories, $source, $params['locale']);
        return $categories;
    }

    /**
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     */
    protected function replace(&$categories, $source, $localeCode)
    {
        array_map(function($obj) use (&$categories, $localeCode){
            $name = $obj->get('name');
            $description = $obj->get('description');
            $categoryId = $obj->get('category_id');

            foreach ($categories as $key => $item) {
                if ($item['id'] == $categoryId) {
                    $categories[$key]['name'] = $name;
                    $categories[$key]['description'] = $description;

                    //translate sub category
                    if (isset($categories[$key]['children'])) {
                        $subCategory = $categories[$key]['children'];
                        $listCategories = $this->getIdFromCategory($subCategory);
                        $source = $this->translate($localeCode, $listCategories);
                        $this->replace($categories[$key]['children'], $source, $localeCode);
                    }
                    break;
                }
            }
        }, $source);
    }

    /**
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     */
    protected function getIdFromCategory($categories)
    {
        $listCategories = array_map(function($obj){
            return is_array($obj) ? $obj['id'] : $obj->get('id');
        }, $categories);

        return $listCategories;
    }

    /**
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     */
    protected function translate($localeCode, $listCategories)
    {
        $alias = 'ac';
        $queryBuilder = $this->createQueryBuilder($alias);
        if (count($listCategories) > 0) {
            $exprCategory = $queryBuilder->expr()->in($alias.'.categoryId', $listCategories);
            $queryBuilder->andWhere($exprCategory);
        }
        $exprLanguage = $queryBuilder->expr()->eq($alias.'.locale', ':locale');
        $queryBuilder->andWhere($exprLanguage)->setParameter('locale', $localeCode);
        $result = $queryBuilder->getQuery();

        return $result->getResult();
    }

    /**
     * Exclusion strategy by JMS group name
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @param  Entity|Collection $data     Entity or array collection of entity
     * @param  string            $JMSGroup Name of JMS group
     *
     * @return array                       Array after the exclusion was done
     */
    public function getFinalResultByJMSGroup($data, $JMSGroup)
    {
        $serializer = SerializerBuilder::create()->build();
        $json = $serializer->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(
                [$JMSGroup]
            )->setSerializeNull(true)
        );
        $arr = json_decode($json, true);
        return $arr;
    }
}
