<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\Common\Util\Inflector;
use Symfony\Component\HttpKernel\Exception as HTTPException;

use Zenith\CoreService\DoctrineRepository\DTBaseRepository;
use AM\CatalogService\Domain\PublicationReadingModes\PublicationReadingModes;

class DTPublicationReadingModesRepository extends DTBaseRepository
{
    /**
     * Get list of publications reading modes by query string parameters
     */
    public function getListByQueryParams(array $params, array $selectProps = [])
    {
        $fieldMappings = $this->getFieldMappings();
        $queryBuilder = $this->createQueryBuilder('i');
        $rootAlias = $queryBuilder->getRootAlias();
        
        $selects = $this->buildSelectDQL($selectProps, $fieldMappings, $rootAlias);
        if (!empty($selects)) {
            $queryBuilder->select($selects);
        }

        $queryBuilder = $this->getQuery($queryBuilder, $params);

        $limit = null;
        $offset = null;
        $ignoreLimit = isset($params['ignore_limit'])? $params['ignore_limit']: 0;
        if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        $pagination = $this->getPagination($queryBuilder, $limit, $offset);
        return array('data' => $data, 'pagination' => $pagination);
    }

    private function getQuery($queryBuilder, array $params)
    {
        $classMetaInfo = $this->getClassMetadata();
        $expr = $queryBuilder->expr();

        $newParams = [];
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $joinPublication = false;
        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'id':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('i.id', $value));
                    }
                    break;
                case 'publicationId':
                    if (!empty($value)) {
                        if ($joinPublication === false) {
                            $queryBuilder->join('i.publication', 'ip');
                            $joinPublication = true;
                        }
                        $publicationIdList = explode(',', $value);
                        $queryBuilder->andWhere($expr->in('i.publicationId', $publicationIdList));
                    }
                    break;
                
                case 'channel':
                    if (!empty($value)) {
                        $queryBuilder->andWhere(
                            $expr->eq("i.$key", ":$key")
                        )->setParameter("$key", $value);
                    }
                    break;

                case 'status':
                    $values = explode(',', $value);
                    $queryBuilder->andWhere(
                        $expr->in("i.$key", ":$key")
                    )->setParameter("$key", $values);
                    break;
               
                case 'sort':
                    $columns = explode(',', $newParams['sort']);
                    $columns = array_unique(array_filter($columns));
                    if (!empty($columns)) {
                        foreach ($columns as $column) {
                            $tmpColumn = explode('-', $column);
                            if (2 == count($tmpColumn) && ('' == $tmpColumn[0])) {
                                $column = $tmpColumn[1];
                                $orderBy = 'desc';
                            } else {
                                $orderBy = 'asc';
                            }
                            
                            $columnName = lcfirst(Inflector::classify($column));
                            if ($classMetaInfo->hasField($columnName)) {
                                $queryBuilder->addOrderBy('i.'.$columnName, $orderBy);
                            }
                        }
                    } else {
                        $queryBuilder->addOrderBy('i.id', 'DESC');
                    }
                    break;
                default:
                    break;
            }
        }

        return $queryBuilder;
    }
}