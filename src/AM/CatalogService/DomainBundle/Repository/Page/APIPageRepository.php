<?php

namespace AM\CatalogService\DomainBundle\Repository\Page;

class APIPageRepository extends AbstractPage
{
    public function __construct($legacyRepo, $cachePageRepo)
    {
        $this->legacyRepo = $legacyRepo;
        $this->cachePageRepo = $cachePageRepo;
    }

    public function getPageList($issueID)
    {
        $pages = $this->legacyRepo
            ->setUp($this->siteID)
            ->getIssuePages($this->legacyIssueID)
        ;

        if (empty($pages)) {
            return [];
        }

        $this->writeTocache($pages, $issueID);

        return $pages;
    }

    public function writeTocache($pages, $issueID)
    {
        $this->cachePageRepo->save($pages, $issueID);
    }
}
