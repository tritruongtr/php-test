<?php

namespace AM\CatalogService\DomainBundle\Repository\Page;

class CachePageRepository extends AbstractPage
{
    const KEY_NAME = 'catalog:issues:%d:%s:pages';
    const CACHE_EXPIRE = 604800; // 1 week

    private $excludeQueryStrings = [
        'issue_has_pdf',
        'publication_allow_pdf'
    ];

    private $includeQueryString = [
        'type'
    ];

    public function __construct($redis, $queryString)
    {
        $this->redis = $redis;
        $this->queryString = $this->queryStringWalk($queryString, $this->excludeQueryStrings, $this->includeQueryString);
        ksort($this->queryString);
    }

    public function getPageList($issueID)
    {
        try {
            $keyName = $this->getKeyName($issueID);
            $pages = $this->redis->GET($keyName);
        } catch (\Exception $e) {
            $pages = null;
        }

        if (empty($pages)) {
            return [];
        }

        $pages = json_decode($pages, true);

        return $pages;
    }

    public function getKeyName($issueID)
    {
        $queryString = $this->queryStringToRedisKey($this->queryString);

        return sprintf(static::KEY_NAME, $issueID, $queryString);
    }

    public function save($pages, $issueID)
    {
        if (is_array($pages)) {
            $pages = json_encode($pages);
        }

        try {
            $keyName = $this->getKeyName($issueID);
            $this->redis->SET($keyName, $pages);
            $this->redis->EXPIRE($keyName, static::CACHE_EXPIRE);
        } catch (\Exception $e) {

        }
    }
}
