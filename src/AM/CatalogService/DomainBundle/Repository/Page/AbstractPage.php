<?php

namespace AM\CatalogService\DomainBundle\Repository\Page;

use AM\CatalogService\DomainBundle\Traits\CMSHelper;

abstract class AbstractPage
{
    use CMSHelper;

    abstract public function getPageList($issueID);
}
