<?php

namespace AM\CatalogService\DomainBundle\Repository\Page;

class PageRepository
{
    private $siteID = null;
    private $legacyIssueID = null;

    public function __construct($cachePageRepo, $apiPageRepo)
    {
        $this->cachePageRepo = $cachePageRepo;
        $this->apiPageRepo = $apiPageRepo;
    }

    public function config($siteID, $legacyIssueID)
    {
        $this->siteID = $siteID;
        $this->legacyIssueID = $legacyIssueID;

        return $this;
    }

    public function getPageList($issueID)
    {
        $pages = $this->cachePageRepo->getPageList($issueID);

        if (empty($pages)) {
            if (empty($this->siteID) || empty($this->legacyIssueID)) {
                // log
                return [];
            }

            $pages = $this->apiPageRepo
                ->config($this->siteID, $this->legacyIssueID)
                ->getPageList($issueID)
            ;
        }

        return $pages;
    }
}