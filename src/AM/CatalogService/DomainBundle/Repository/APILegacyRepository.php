<?php

namespace AM\CatalogService\DomainBundle\Repository;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class APILegacyRepository
{
    protected $apiURI;
    protected $apiMethod;
    protected $apiParams = '';
    protected $client;
    protected $consoleDomain;

    public function __construct($consoleDomain, $legacyServiceUrl, $queryString)
    {
        $this->client = new Client();
        $this->apiMethod = $legacyServiceUrl;
        $this->consoleDomain = $consoleDomain;
        $this->apiParams = $queryString;
    }

    public function setUp($legacySiteId)
    {
        $this->apiURI = $this->getPublicationDomain($legacySiteId);

        return $this;
    }

    private function getPublicationDomain($legacySiteId)
    {
        return "http://site-$legacySiteId.audiencemedia.com";
    }

    public function getPublication($publicationRemoteId)
    {
        $result = null;
        $apiURI = $this->consoleDomain;
        $apiMethod = $this->apiMethod['get_publication_uri'];
        $requestURI = $apiURI.$apiMethod.'/'.$publicationRemoteId;
        $response = $this->getRequest($requestURI);

        if (isset($response['publication'])) {
            $result = $response['publication'];
        }

        return $result;
    }

    public function getIssue($legacyIssueId)
    {
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;
        $requestURI = $apiURI.$apiMethod.'/'.$legacyIssueId;

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;
        if (empty($result)) {
            return new \stdClass();
        }

        return $result;
    }

    public function getIssueSections($legacyIssueId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;
        parse_str($queryString, $output);
        $isPreview = '/content';
        if (isset($output['preview']) && $output['preview'] == '1') {
            $isPreview = '/preview';
        }
        $requestURI = $apiURI . $apiMethod . '/' . $legacyIssueId . $isPreview . '/sections';

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        return $result;
    }

    public function getIssueStories($legacyIssueId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;

        parse_str($queryString, $output);
        $isPreview = '/content';
        if (isset($output['preview']) && $output['preview'] == '1') {
            $isPreview = '/preview';
        }

        $requestURI = $apiURI . $apiMethod . '/' . $legacyIssueId . $isPreview . '/stories';

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        if (empty($result)) {
            return [];
        }

        return $result;
    }

    public function getIssueSingleStory($legacyIssueId, $legacyStoryId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;
        $requestURI = $apiURI.$apiMethod.'/'.$legacyIssueId.'/stories/'.$legacyStoryId;

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        return $result;
    }

    public function getIssueStory($legacyStoryId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_stories_uri'];
        $queryString = $this->apiParams;
        $requestURI = $apiURI.$apiMethod.'/'.$legacyStoryId;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        return $result;
    }

    public function getIssuePages($legacyIssueId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;
        parse_str($queryString, $output);
        $isPreview = '/content';
        if (isset($output['preview']) && $output['preview'] == '1') {
            $isPreview = '/preview';
        }
        $requestURI = $apiURI . $apiMethod . '/' . $legacyIssueId . $isPreview . '/pages';

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        return $result;
    }

    public function getIssueAdverts($legacyIssueId)
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issue_uri'];
        $queryString = $this->apiParams;
        $requestURI = $apiURI.$apiMethod.'/'.$legacyIssueId.'/ads';

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['data']) ? $response['data'] : $response;

        return $result;
    }

    public function getIssues()
    {
        $result = null;
        $apiURI = $this->apiURI;
        $apiMethod = $this->apiMethod['get_issues_uri'];
        $queryString = $this->apiParams;
        $requestURI = $apiURI.$apiMethod;

        if ($queryString != null) $requestURI .= '?'.$queryString;

        $response = $this->getRequest($requestURI);
        $result = isset($response['issues']) ? $response['issues'] : $response;

        return $result;
    }

    private function getRequest($apiURI)
    {
        try {
            $restResponse = $this->client->get(
                $apiURI,
                [
                    'stream' => true,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Accept-Encoding' => 'gzip'
                    ]
                ]
            );
            if ($restResponse->getStatusCode() !== 200) {
                return [];
            }
            $body = $restResponse->getBody();
            $data = '';
            while (!$body->eof()) {
                $data .= $body->read(1024);
            }
            $data = json_decode($data, true);

            if (isset($data)) {
                return $data;
            }
            return [];
        } catch (BadResponseException $e) {
            return null;
        } catch (RequestException $e) {
            return null;
        }
    }
}

?>