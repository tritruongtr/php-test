<?php
namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use AM\CatalogService\Domain\Partner\PartnerRepository;

class DTPartnerRepository extends EntityRepository implements PartnerRepository
{
    /**
     * Get list of partners by query string parameters
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  array  $params
     * @return array
     */
    public function getListByQueryParams(array $params)
    {
        $classMetaInfo = $this->_em->getClassMetadata(
            $this->getClassName()
        );

        $queryBuilder = $this->createQueryBuilder('p');
        $expr = $queryBuilder->expr();

        $newParams = array();
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $orderBy = 'DESC';
        if (isset($newParams['orderBy'])) {
            $lowerOrderBy = strtolower($newParams['orderBy']);
            if (in_array($lowerOrderBy, array('asc', 'desc'))) {
                $orderBy = strtoupper($lowerOrderBy);
            }
        }

        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'sortBy':
                    $columns = explode(',', $value);
                    // remove empty columns and dupliate columns
                    $columns = array_unique(array_filter($columns));
                    if (!empty($columns)) {
                        foreach ($columns as $column) {
                            $columnName = lcfirst(Inflector::classify($column));
                            if ($classMetaInfo->hasField($columnName)) {
                                $queryBuilder->addOrderBy("p.$columnName", $orderBy);
                            }
                        }
                    }

                    $queryBuilder->addOrderBy('p.id', $orderBy);
                    break;

                case 'parentId':
                    if (!empty($value)) {
                        $queryBuilder->andWhere(
                            $expr->eq("p.$key", ':field_value')
                        )->setParameter('field_value', $value);
                    }
                    break;

                case 'limit':
                    $limit = $value;
                    if (empty($limit) || (int) $limit <= 0) {
                        $limit = 10;
                    }
                    $queryBuilder->setMaxResults($limit);
                    break;

                case 'offset':
                    $offset = $value;
                    if (empty($offset) || (int) $offset <= 0) {
                        $offset = 0;
                    }
                    $queryBuilder->setFirstResult($offset);
                    break;

                default:
                    break;
            }
        }

        $query = $queryBuilder->getQuery();

        $paginator = new Paginator($query, /* $fetchJoinCollection = */ false);
        $count = $paginator->count();

        $data = $paginator->getIterator()->getArrayCopy();

        return array('data' => $data, 'count' => $count);
    }

    /**
     * Exclusion strategy by JMS group name
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  Entity|Collection $data     Entity or array collection of entity
     * @param  string            $JMSGroup Name of JMS group
     * @return array                       Array after the exclusion was done
     */
    public function getFinalResultByJMSGroup($data, $JMSGroup)
    {
        $serializer = SerializerBuilder::create()->build();
        $json = $serializer->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(
                [$JMSGroup]
            )->setSerializeNull(true)
        );
        $arr = json_decode($json, true);
        return $arr;
    }
}