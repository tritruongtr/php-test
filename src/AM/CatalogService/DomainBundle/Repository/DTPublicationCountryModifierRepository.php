<?php

namespace AM\CatalogService\DomainBundle\Repository;

use AM\CatalogService\Domain\PublicationCountryModifier\PublicationCountryModifier;
use AM\CatalogService\Domain\PublicationCountryModifier\PublicationCountryModifierRepository;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\DBALException;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;

/**
 * DTPublicationCountryModifierRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DTPublicationCountryModifierRepository extends EntityRepository implements PublicationCountryModifierRepository
{
    /**
     * Add Publication Country Modifier into Database
     *
     * @param PublicationCountryModifier $publicationCountryModifier
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function addPublicationCountryModifier(PublicationCountryModifier $publicationCountryModifier)
    {
        try {
            $this->_em->persist($publicationCountryModifier);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publicationCountryModifier
            ];
        } catch(DBALException $ex) {
            throw new HTTPException\BadRequestHttpException(
                $ex->getMessage()
            );
        }
    }

    /**
     * Update Publication Metadata from Database
     *
     * @param PublicationCountryModifier $publicationMetadata
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function updatePublicationCountryModifier($publicationCountryModifier)
    {
        try {
            $this->_em->persist($publicationCountryModifier);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publicationCountryModifier
            ];
        } catch(DBALException $ex) {
            throw new HTTPException\BadRequestHttpException(
                $ex->getMessage()
            );
        }
    }


    /**
     * Delete Publication Country Modifier from Database
     *
     * @param array $params
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function deletePublicationCountryModifier($params)
    {
        try {
            if (!isset($params['id']) || $params['id'] == 0) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication_country_modifier.not_found'
                );
            }

            $publicationCountryModifier = $this->find($params['id']);
            if (!$publicationCountryModifier instanceof PublicationCountryModifier) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication_country_modifier.not_found'
                );
            }

            $this->_em->remove($publicationCountryModifier);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publicationCountryModifier
            ];
        } catch(DBALException  $ex) {
            throw new HTTPException\BadRequestHttpException(
                $ex->getMessage()
            );
        }
    }


    /**
     * Find publication country modifier list
     *
     * @param int $publication_id id of publication
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function findPublicationCountryModifierList($publication_id)
    {
        $result = [];
        try {
            if ($publication_id == null) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication_country_modifier.not_found'
                );
            }


            $result = $this->findBy(['publicationId' => $publication_id]);

            $serializer = SerializerBuilder::create()->build();
            $publicationCountryModifierJson = $serializer->serialize(
                $result,
                'json',
                SerializationContext::create()->setGroups(
                    ['list']
                )
            );
            $publicationCountryModifier = json_decode($publicationCountryModifierJson, true);
            return ['status' => true,'data' => $publicationCountryModifier];

        } catch(DBALException $ex) {
            throw new HTTPException\BadRequestHttpException(
                $ex->getMessage()
            );
        }

    }
}
