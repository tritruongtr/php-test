<?php

namespace AM\CatalogService\DomainBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\Common\Util\Inflector;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;

use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Issue\Issue;
use AM\CatalogService\Domain\Publisher\Publisher;
use AM\CatalogService\Domain\PublicationReadingModes\PublicationReadingModes;

use Zenith\CoreService\DoctrineRepository\DTBaseRepository;

class DTPublicationRepository extends DTBaseRepository
{
    private $publisherRepo;
    private $issueRepo;
    private $readingModesRepo;

    public function setRepo($publisherRepo, $issueRepo, $readingModesRepo, $databaseLog)
    {
        $this->publisherRepo = $publisherRepo;
        $this->issueRepo = $issueRepo;
        $this->readingModesRepo = $readingModesRepo;
        $this->databaseLog = $databaseLog;
    }

    public function buildSelectDQL(array $selectProps, array $fieldMappings, $rootAlias)
    {
        if (empty($selectProps)) {
            return null;
        }

        $selectProps = array_unique($selectProps);
        $selects = [];
        foreach ($selectProps as $selectProp) {
            if (isset($fieldMappings[$selectProp])) {
                $underscoreProp = Inflector::tableize($selectProp);
                $selects[] = "$rootAlias.$selectProp AS $underscoreProp";
            }
        }

        if (!empty($selects)) {
            $selects = implode(',', $selects);
            return $selects;
        }

        return null;
    }

    public function appendItems(array & $publications, array $itemsInfo = [])
    {
        if (empty($publications) || empty($itemsInfo)) {
            return;
        }

        $items = [];
        foreach ($itemsInfo as $itemInfo) {
            if (count($itemInfo) != 2) {
                throw new \Exception("Item should be contains 2 elements: name and group");
            }

            list($item, $group) = $itemInfo;

            switch ($item) {
                case 'publisher':
                    $publisherIDs = array_column($publications, 'publisher_id');
                    $groupProps = Publisher::getResponseGroups($group);
                    $publisherResult = $this->publisherRepo->getListByQueryParams(
                        ['id' => $publisherIDs], true, $groupProps
                    );
                    break;

                case 'latest_issue':
                    $publicationIDs = array_column($publications, 'id');
                    $publicationIDs = implode(',', $publicationIDs);
                    $groupProps = Issue::getResponseGroups($group);
                    $selects = $this->buildSelectSQL($groupProps, $this->issueRepo->getFieldMappings(), 'i');
                    $status = Issue::STATUS_PUBLISHED;
                    $sql = "SELECT * FROM (SELECT $selects
                            FROM {$this->issueRepo->getClassMetadata()->table['name']} i
                            WHERE i.publication_id IN ($publicationIDs)
                            AND status = $status ORDER BY i.cover_date DESC) as vs
                            GROUP BY vs.publication_id
                           "
                    ;
                    $latestIssues = $this->_em->getConnection()->fetchAll($sql);
                    break;

                case 'categories':
                    $publicationIDs = array_column($publications, 'id');
                    $publicationIDs = implode(',', $publicationIDs);

                    $sql = "SELECT cat.*, pc.publication_id
                            FROM categories cat JOIN publications_categories pc
                            ON cat.id = pc.category_id
                            WHERE pc.publication_id IN ($publicationIDs)"
                    ;

                    $cats = $this->_em->getConnection()->fetchAll($sql);

                    break;

                default:
                    break;
            }

            $items[] = $item;
        }

        foreach ($publications as & $publication) {
            self::bigint2Int(
                ['id', 'publisher_id', 'created_by', 'modified_by'], $publication
            );

            $publication = $this->intlProcessor($publication);

            foreach ($items as $item) {
                switch ($item) {
                    case 'publisher':
                        $publishers = $publisherResult['data'];
                        $this->appendPublisher($publication, $publishers);
                        break;

                    case 'latest_issue':
                        $this->appendLatestIssue($publication, $latestIssues);
                        break;

                    case 'categories':
                        $this->appendCategories($publication, $cats);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    /**
     * Append categories to publication
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     * @param  Publication &$publication Publication entity
     * @param  array  $categories List categories by publication ids
     * @return void
     */
    public function appendCategories(&$publication, array $categories = [])
    {
        $catPubIds = array_column($categories, 'publication_id');
        $publication['categories'] = [];
        foreach ($catPubIds as $index => $value) {
            if ($publication['id'] != $value) {
                continue;
            }
            array_push($publication['categories'], $categories[$index]);
        }
        self::bigint2Int(['id', 'publication_id'],$publication['categories']);
    }

    public function appendPublisher(&$publication, array $publishers = [])
    {
        $index = array_search($publication['publisher_id'], array_column($publishers, 'id'));
        if ($index === false) {
            $publication['publisher'] = new \stdClass();
        } else {
            $publication['publisher'] = $publishers[$index];
        }
    }

    public function appendLatestIssue(&$publication, array $latestIssues = [])
    {
        $index = array_search($publication['id'], array_column($latestIssues, 'publication_id'));
        if ($index === false) {
            $publication['latest_issue'] = new \stdClass();
        } else {
            $publication['latest_issue'] = $latestIssues[$index];
            self::bigint2Int(
                ['id', 'publication_id', 'legacy_issue_id', 'status', 'type', 'preview', 'has_xml', 'has_pdf'],
                $publication['latest_issue']);
            //$publication['latest_issue']['id'] = $publication['latest_issue']['latest_issue_id'];
            self::toUTC(['cover_date', 'publish_date', 'created_at', 'modified_at'], $publication['latest_issue']);
            //unset($publication['latest_issue']['latest_issue_id']);
        }
    }

    public function appendReadingModes(&$publication, $param)
    {
        $newParam['channel'] = $param['channel'];
        $newParam['publication_id'] = $param['id'];
        $newParam['status'] = 1;
        $newParam['sort'] = 'priority';
        $readingModes = $this->readingModesRepo->getListByQueryParams($newParam, PublicationReadingModes::getResponseGroups('view'));
        $publication['reading_modes'] = $readingModes['data'];
    }

    /**
     * Add Publication into Database
     *
     * @param Publicaion $publication
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function addPublication(Publication $publication)
    {
        try {
            $this->_em->persist($publication);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publication
            ];
        } catch (DBALException $ex) {
            $this->databaseLog->error($ex->getMessage());
            throw new HTTPException\BadRequestHttpException(
              'entity.add.database_error'
            );
        }
    }

    /**
     * Update Publication from Database
     *
     * @param Publication $publication
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function updatePublication($publication)
    {
        try {
            $this->_em->persist($publication);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publication
            ];
        } catch(DBALException $ex) {
            $this->databaseLog->error($ex->getMessage());
            throw new HTTPException\BadRequestHttpException(
                'entity.update.database_error'
            );
        }
    }

    /**
     * Delete Publication from Database
     *
     * @param array $params
     * @return array
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * */
    public function deletePublication($params)
    {
        try {
            if (!isset($params['id']) || $params['id'] == 0) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }

            $publication = $this->find($params['id']);
            if (!$publication instanceof Publication) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }

            // return false if issue exist
            $issues = $publication->getIssues();
            if (!empty($issues)) {
                throw new HTTPException\ConflictHttpException(
                    'catalog.publication.issue.exist'
                );
            }

            $this->_em->remove($publication);
            $this->_em->flush();

            return [
                'status' => true,
                'data' => $publication
            ];
        } catch(DBALException $ex) {
            $this->databaseLog->error($ex->getMessage());
            throw new HTTPException\BadRequestHttpException(
                'entity.delete.database_error'
            );
        }
    }

    /**
     * Get list of publications by query string parameters
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param  array  $params
     *
     * @return array
     */
    public function getListByQueryParams(array $params, $ignoreLimit = false, array $selectProps = [])
    {
        $fieldMappings = $this->getFieldMappings();
        $queryBuilder = $this->createQueryBuilder('p');
        $rootAlias = $queryBuilder->getRootAlias();

        $selects = $this->buildSelectDQL($selectProps, $fieldMappings, $rootAlias);
        if (!empty($selects)) {
            if(!empty($params['id']) && isset($params['sort_by_list_id']) && (int) $params['sort_by_list_id'] == 1) {
                $selects .= ', field(p.id, '.$params['id'].') as HIDDEN field';
            }
            $queryBuilder->select($selects);
        }

        $queryBuilder = $this->getQuery($queryBuilder, $params);
        $queryBuilder->groupBy("$rootAlias.id");

        $limit = null;
        $offset = null;
        if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        $pagination = $this->getPagination($queryBuilder, $limit, $offset);
        return array('data' => $data, 'pagination' => $pagination);

        /*if (!$ignoreLimit) {
            $limit = (empty($params['limit']) || (int) $params['limit'] <= 0) ? 10 : $params['limit'];
            $offset = (empty($params['offset']) || (int) $params['offset'] <= 0) ? 0 : $params['offset'];

            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }


        $query = $queryBuilder->getQuery();
        $data = $query->getResult();

        if (!$ignoreLimit)
        {
            $count = $this->count($params);
            return array('data' => $data, 'count' => $count);
        }
        return array('data' => $data, 'count' => 0);*/
    }

    public function count($params)
    {
        $queryBuilder = $this->getQuery($this->createQueryBuilder('p')->select('COUNT(p.id)'), $params);
        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return $count;
    }

    private function getQuery($queryBuilder, array $params)
    {
        $expr = $queryBuilder->expr();

        $classMetaInfo = $this->getClassMetadata();

        $newParams = array();
        foreach ($params as $key => $value) {
            $realFieldName = lcfirst(Inflector::classify($key));
            $newParams[$realFieldName] = $value;
        }

        $orderBy = 'DESC';
        if (isset($newParams['orderBy'])) {
            $lowerOrderBy = strtolower($newParams['orderBy']);
            if (in_array($lowerOrderBy, array('asc', 'desc'))) {
                $orderBy = strtoupper($lowerOrderBy);
            }
        }

        foreach ($newParams as $key => $value) {
            switch ($key) {
                case 'name':
                    if (!empty($value)) {
                        $queryBuilder
                            ->andWhere($expr->like('p.name', ':name'))
                            ->setParameter('name', '%'.$value.'%');;
                    }
                    break;

                case 'type':
                    if (!empty($value)) {
                        $queryBuilder
                            ->andWhere('p.type = :type')
                            ->setParameter('type', $value);
                    }
                    break;

                case 'sortBy':
                    if (isset($newParams['ignoreSort'])) {
                        break;
                    }
                    if (!empty($value)) {
                        $columns = explode(',', $value);
                        // remove empty columns and dupliate columns
                        $columns = array_unique(array_filter($columns));
                        if (!empty($columns)) {
                            foreach ($columns as $column) {
                                $columnName = lcfirst(Inflector::classify($column));
                                if ($classMetaInfo->hasField($columnName)) {
                                    $queryBuilder->addOrderBy("p.$columnName", $orderBy);
                                }
                            }
                        }
                    }
                    break;

                case 'id':
                    if (!empty($value)) {
                        $queryBuilder->andWhere($expr->in('p.id', $value));
                    }
                    break;

                case 'hasIssue':
                    if (!empty($value)) {
                        $queryBuilder->join('p.issues', 'i')
                        ->andWhere('i.status = :status')
                        ->setParameter('status', Issue::STATUS_PUBLISHED);
                    }
                    break;

                case 'catalogId':
                    if (!empty($value)) {
                        $queryBuilder->join('p.catalogsPublications', 'cp')
                        ->andWhere('cp.catalogId = :catalog_id')
                        ->setParameter('catalog_id', $value);
                    }
                    break;

                case 'cpstatus':
                    if (isset($newParams['catalogId']) && $value == 1) {
                        $queryBuilder->andWhere('cp.status = 1');
                    }
                    break;

                case 'categoryId':
                    if (!empty($value)) {
                        $queryBuilder
                        ->join('p.publicationsCategories', 'pc')
                        ->andWhere(
                            $expr->in('pc.categoryId', $value)
                        );
                    }
                    break;

                case 'contentRating':
                    if (!empty($value)) {
                        $expContentRating = explode('-', $value);
                        $count = count($expContentRating);
                        if ($count == 1) {
                            $queryBuilder->andWhere($expr->in('p.contentRating', $value));
                        } else if ($count == 2) {
                            $queryBuilder->andWhere(
                                $expr->between(
                                    'p.contentRating', $expContentRating[0], $expContentRating[1]
                                )
                            );
                        }
                    }
                    break;

                case 'locale':
                case 'language':
                case 'countryCode':
                case 'publisherId':
                case 'remoteIdentifier':
                    if (!empty($value)) {
                        if ($value == 'null') {
                            $queryBuilder->andWhere("p.$key is NULL or p.$key = ''");
                        } else {
                            $queryBuilder->andWhere(
                                $expr->eq("p.$key", ":$key")
                            )->setParameter("$key", $value);
                        }
                    }
                    break;

                case 'status':
                    if (is_numeric($value)) {
                        $queryBuilder->andWhere(
                            $expr->in("p.$key", $value)
                        );
                    }
                    break;
                case 'sort':
                    if (isset($newParams['ignoreSort'])) {
                        break;
                    }
                    $columns = explode(',', $newParams['sort']);
                    // remove empty columns and dupliate columns
                    $columns = array_unique(array_filter($columns));
                    if (!empty($columns)) {
                        foreach ($columns as $column) {
                            $tmpColumn = explode('-', $column);
                            if (2 == count($tmpColumn) && ('' == $tmpColumn[0])) {
                                $column = $tmpColumn[1];
                                $orderBy = 'desc';
                            } else {
                                $orderBy = 'asc';
                            }
                            $columnName = lcfirst(Inflector::classify($column));
                            if ($classMetaInfo->hasField($columnName)) {
                                $queryBuilder->addOrderBy('p.'.$columnName, $orderBy);
                            }
                        }
                    } else {
                        $queryBuilder->addOrderBy('p.id', $orderBy);
                    }
                    break;
                case 'sortByListId':
                    if (!empty($newParams['id']) && (int) $newParams['sortByListId'] == 1) {
                        $queryBuilder->orderBy('field');
                    }
                    break;
                default:
                    break;
            }
        }
        return $queryBuilder;
    }

    public function getListNotInTheList($list = null)
    {
        $em = $this->getEntityManager();
        if ($list == null) {
            $query = $em->createQuery(
                "SELECT p.id, p.name FROM AM\CatalogService\Domain\Publication\Publication p"
            );

            return $data = $query->getResult();
        }

        $query = $em->createQuery(
            "SELECT p.id, p.name
             FROM
                AM\CatalogService\Domain\Publication\Publication p
             WHERE p.id NOT IN ($list)"
        );

        return $data = $query->getResult();
    }

    public function getListEmptyIssue()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT p.id, p.name
             FROM AM\CatalogService\Domain\Publication\Publication p
             WHERE NOT EXISTS (
                SELECT i
                FROM AM\CatalogService\Domain\Issue\Issue i
                WHERE i.publication = p.id
             )
            "
        );

        return $data = $query->getResult();
    }

    public function getByIds($list = null)
    {
        $em = $this->getEntityManager();
        if ($list == null) {
            $query = $em->createQuery(
                "SELECT p FROM AM\CatalogService\Domain\Publication\Publication p"
            );

            return $data = $query->getResult();
        }

        $query = $em->createQuery(
            "SELECT p, FIELD(p.id, $list) as HIDDEN field
              FROM
                AM\CatalogService\Domain\Publication\Publication p
             WHERE p.id IN ($list) ORDER BY field"
        );

        return $data = $query->getResult();
    }
}