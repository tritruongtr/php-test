<?php

namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;

use AM\CatalogService\Domain\Catalog\Catalog;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Zenith\CoreService\PaginationBundle\Pagination;

class CatalogController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get Categories from Catalog
     * <br>
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   resourceDescription="Operations on catalogs.",
     *   description = "Get all categories from catalog",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     * @QueryParam(name="content_rating", description="Rating of the content of this publication.", nullable=true)
     * @QueryParam(name="language",  description="ISO 639-2 alpha-3 Language code.", nullable=true)
     * @QueryParam(name="locale",  description="Locale Example: eng-GB", nullable=true)
     * @QueryParam(name="project_id", requirements = "\d+", description="id of project", nullable=true)
     * @QueryParam(name="sort", description="id of project", nullable=true)
     * @QueryParam(name="category_set_id", description="category set id", nullable=true)
     * @QueryParam(name="limit", requirements="\d+", description="limit of category list", nullable=true)
     * @QueryParam(name="offset", requirements="\d+", description="offset of category list", nullable=true)
     *
     * @param int $id id of catalog
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @author Huong Le <huong.le@audiencemedia.com>
     * @return array
     * @Get("/{id}/categories", name = "categories", options = {"method_prefix" = false})
     */
    public function getCategoryAction($id, ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $params['catalog_id'] = $id;
        $categoryRepo = $this->get('catalog.category.repository');
        $result = $categoryRepo->getCategory($params);
        $data = array(
            'categories'=> $result['data'],
            'language' => $params['language'],
            'locale' => $params['locale']
        );
        $categories = $this->get('catalog.category_translation.repository')->getFilteredCategories($data);
        return ['status' => true, 'data' => $categories, 'pagination' => $result['pagination']];
    }

    /**
     * Get catalog list
     * <br>
     *
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   description = "Get all catalogs",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     * @QueryParam(
     *     name="id",
     *     requirements="\d+",
     *     description="id of catalog",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     description="limit of catalog list",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     description="offset of catalog list",
     *     nullable=true
     * )
     * @QueryParam(name="status",
     *      requirements="\d+",
     *      description="status of catalog list",
     *      nullable=true
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     * @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "Ignore limit"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by group"
     * )
     * @QueryParam (
     *     name = "name",
     *     nullable = true,
     *     description = "Filter list of catalog by name"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $thisRepo = $this->get('catalog.catalog.repository');
        $ignoreLimit = isset($params['ignore_limit']) && ($params['ignore_limit'] == 1);

        $catalogs = $thisRepo->all($params, $ignoreLimit, Catalog::getResponseGroups('list'));
        $thisRepo->appendData($catalogs['data'], 'appendNoOfPublications', Catalog::getResponseGroups('list'));

        return ['status' => true, 'data' => $catalogs['data'], 'pagination' => $catalogs['pagination']];
    }

    /**
     * Create catalog
     * <br>
     *
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   description = "create catalog",
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Catalog"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of Catalog"
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "remote identifier of Catalog"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Status of Catalog: active, inactive, etc"
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @Post("", name = "create", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request param request to create new catalog
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $catalog = new Catalog();
        $catalog->setData($data);

        $errors = $this->container->get('validator')->validate($catalog);

        if ($errors->count() > 0) {
            throw new BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }
        $thisRepo = $this->get('catalog.catalog.repository');
        $thisRepo->addCatalog($catalog);
        $catalog = $thisRepo->getFinalResultByJMSGroup($catalog, 'view');
        return ['status' => true, 'data' => $catalog];
    }


    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   resourceDescription="Operations on catalogs.",
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update catalog
     * <br>
     *
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   description = "update catalog",
     *    requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Name of the Catalog"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of Catalog"
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "remote identifier of Catalog"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Status of Catalog: active, inactive, etc"
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     * @param int $id id of catalog that is needed to update
     * @param Request $request param request to update new catalog
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function putAction($id, Request $request)
    {
        $thisRepo = $this->get('catalog.catalog.repository');
        $params = $request->request->all();
        $params['id'] = $id != null ? (int) $id : 0;

        $catalog = $thisRepo->find($params['id']);
        if (!$catalog instanceof Catalog) {
            throw new NotFoundHttpException(
                'catalog.catalog.not_found'
            );
        }

        $catalog->setData($params);

        $errors = $this->container->get('validator')->validate($catalog);
        if ($errors->count() > 0) {
            throw new BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $thisRepo->updateCatalog($catalog);

        return ['status' => true, 'data' => []];
    }

    /**
     * Delete catalog
     * <br>
     *
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   description = "delete catalog",
     *    requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     * @param int $id id of catalog that is needed to delete
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function deleteAction($id)
    {
        $newsstandServiceUrl = $this->container->getParameter('newsstand_service_url');
        $newsstandListUrl = $this->container->getParameter('newsstand_service_url.get_newstand_list');
        $params['id'] = $id;
        $params['newsstandUrl'] = $newsstandServiceUrl.$newsstandListUrl;
        $this->get('catalog.catalog.repository')->deleteCatalog($params);

        return ['status' => true, 'data' => []];
    }

    /**
     * Get catalog detail by id
     * <br>
     *
     * @ApiDoc(
     *   section = "Catalog",
     *   resource = true,
     *   description = "Get catalog detail by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the catalog can not be found"
     *   }
     * )
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     * @QueryParam(name="content_rating", description="Rating of the content of this publication.", nullable=true)
     * @QueryParam(name="language",  description="ISO 639-2 alpha-3 Language code.", nullable=true)
     * @QueryParam(name="locale",  description="Locale Example: ‘eng-GB’.", nullable=true)
     * @QueryParam(name="project_id", requirements = "\d+", description="id of project", nullable=true)
     * @param int $id id of catalog that is needed to get
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getAction($id,ParamFetcherInterface $paramFetcher)
    {
        $params = array(
            'content_rating' => $paramFetcher->get('content_rating'),
            'language' => $paramFetcher->get('language'),
            'locale' => $paramFetcher->get('locale'),
            'project_id' => $paramFetcher->get('project_id'),
            'catalog_id' => $id
        );

        $thisRepo = $this->get('catalog.catalog.repository');
        $catalog = $thisRepo->find($id);

        if (!$catalog instanceof Catalog) {
            throw new NotFoundHttpException(
                'catalog.catalog.not_found'
            );
        }

        $total = $this->get('catalog.publication.repository')->count($params);
        $catalog->set('noOfPublications', $total);

        $serializer = SerializerBuilder::create()->build();
        $catalogJson = $serializer->serialize(
            $catalog,
            'json',
            SerializationContext::create()->setGroups(
                ['view']
            )->setSerializeNull(true)
        );
        $catalogResult = json_decode($catalogJson, true);

        return ['status' => true, 'data' => $catalogResult];
    }

    /**
     * Updating catalog publications
     * @Annotations\View(statuscode = 200,serializerGroups={"list"})
     * @Put("/{id}/publications")
     * @param int $id id of catalog that is needed to update
     * @param Request $request param request to update catalog publications
     * @author Phuc Nguyen <phuc.nguyen@audiencemedia.com>
     * @return array
     */
    public function putPublicationsAction($id, Request $request)
    {
        $catalogHandler = $this->get('catalog.catalog_publication.handler');
        $result = $catalogHandler->putPublications($id, $request);
        return ['status' => true, 'data' => $result];
    }
}
