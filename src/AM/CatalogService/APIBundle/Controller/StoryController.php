<?php
/**
 * @author Huong Le <huong.le@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

// FOSRestBundle stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Routing\ClassResourceInterface;

// Zinio stuff
use AM\CatalogService\Domain\Issue\Issue;

class StoryController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get list of story by site & story IDs pairs
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "id",
     *     nullable = true,
     *     description = "list site and story IDs pair"
     * )
     * @QueryParam(
     *     name = "extra_info",
     *     nullable = true,
     *     description = "Determining data return with site and story IDs pair or not"
     * )
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $siteStoryPairs = $paramFetch->get('id');
        $extraInfo = $paramFetch->get('extra_info');
        $siteStoryPairs = explode(',', $siteStoryPairs);
        $redis = $this->get('snc_redis.cache');

        $redisKeys = [];
        foreach ($siteStoryPairs as $siteStoryPair) {
            $redisKeys[] = "nsd_v1_$siteStoryPair";
        }

        $result = $redis->MGET(...$redisKeys);

        $stories = [];
        foreach ($result as $k => $story) {
            $siteStoryPair = $siteStoryPairs[$k];
            if (!empty($story)) {
                $story = json_decode($story, true);
                if (empty($story['layout'])) {
                    $story['layout'] = json_decode('{}');
                }

                if ($extraInfo === 'true') {
                    $stories[$siteStoryPair] = $story;
                } else {
                    $stories[] = $story;
                }

                continue;
            }

            // get from API
            list($siteID, $storyID) = explode('_', $siteStoryPair);
            $story = $this->get('catalog.legacy.repository')
                ->setUp($siteID)
                ->getIssueStory($storyID)
            ;

            if (empty($story) || !isset($story['issue'])) {
                if ($extraInfo === 'true') {
                    $stories[$siteStoryPair] = ['Error' => 'CMS return empty'];
                }
                continue;
            }

            $legacyIssueID = $story['issue']['id'];
            $legacyPubId = $story['issue']['publication']['id'];
            $issueRepo = $this->get('catalog.issue.repository');
            $issueList = $issueRepo->getListByQueryParams(
                [
                    'legacy_issue_id' => $legacyIssueID,
                    'publication_remote_id' => $legacyPubId
                ],
                true,
                Issue::getResponseGroups('list')
            );

            if (empty($issueList['data'])) {
                if ($extraInfo === 'true') {
                    $stories[$siteStoryPair] = ['Error' => "Issue could not be found with legacy_issue_id = $legacyIssueID and publication_remote_id = $legacyPubId"];
                }

                continue;
            }

            $issue = $issueList['data'][0];
            $story['issue']['publish_date'] = date(DATE_ISO8601, $story['issue']['published_date']);
            $story['issue']['cover_date'] = date(DATE_ISO8601, $story['issue']['cover_date']);
            $story['issue']['catalog_issue_id'] = $issue['id'];
            $story['issue']['cover_image'] = $issue['cover_image'];
            $story['issue']['publication']['catalog_publication_id'] = $issue['publication_id'];
            $story['issue']['publication']['site_id'] = $siteID;
            unset($story['issue']['published_date']);

            if (empty($story['layout'])) {
                $story['layout'] = json_decode('{}');
            }

            $json = json_encode($story);
            $redis->SET($redisKeys[$k], $json);

            if ($extraInfo === 'true') {
                $stories[$siteStoryPair] = $story;
            } else {
                $stories[] = $story;
            }
        }

        return ['status' => true, 'data' => $stories];
    }
}
