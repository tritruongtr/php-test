<?php

namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;
use Symfony\Component\DependencyInjection\Exception as DIException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

// FOSRestBundle Stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Routing\ClassResourceInterface;

// Audience Media stuff
use AM\CatalogService\Domain\CategorySet\CategorySet;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Zenith\CoreService\Pagination\Pagination;
use Zenith\CoreService\Sluggable\Sluggable;

/**
 * @author Ca Pham <ca.pham@audiencemedia.com>
 */
class CategorySetController extends FOSRestController implements ClassResourceInterface
{
	/**
     * List CategorySet
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "List CategorySet",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     * @QueryParam (
     *     name = "id",
     *     nullable = true,
     *     description = "ID of categoryset"
     * )
     * @QueryParam (
     *     name = "name",
     *     nullable = true,
     *     description = "Name of categoryset"
     * )
     * @QueryParam (
     *     name = "type",
     *     nullable = true,
     *     description = "Type of categoryset"
     * )
     *  @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "0 or 1"
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort multi field"
     * )
     * @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "Ignore limit"
     * )
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @return array
     */
	public function cgetAction(ParamFetch $paramFetch)
	{
		$params = $paramFetch->all();
        $thisRepo = $this->get('catalog.category_set.repository');
        $ignoreLimit = isset($params['ignore_limit']) && ($params['ignore_limit'] == 1);
        $categorySets = $thisRepo->findList($params, $ignoreLimit);
        $categorySets = $thisRepo->getFinalResultByJMSGroup($categorySets, 'list');

        if ($ignoreLimit) {
            return ['status' => true, 'data' => $categorySets];
        }

        $totalRows = $thisRepo->count($params);
        $pagination = new Pagination($totalRows, $params['offset'], $params['limit']);
        return ['status' => true, 'data' => $categorySets, 'pagination' => $pagination->getResult()];
	}

	/**
     * Create new a CategorySet
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "Create new a CategorySet",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Bad Request",
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  Request $request
     * @return array
     *
     * @Post("", name = "register", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $categorySet = new CategorySet($data);
        $errors = $this->container->get('validator')->validate($categorySet);

        if ($errors->count() > 0) {
            throw new BadRequestHttpException(
                $this->get("jms_serializer")->serialize($errors, 'json')
            );
        }

        $categorySetRepo = $this->get('catalog.category_set.repository');
        $categorySetRepo->create($categorySet);

        $rs = $categorySetRepo->getFinalResultByJMSGroup($categorySet, 'view');
        return ['status' => true, 'data' => $rs];
    }

    /**
     * Get categorySet By Id
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "Get categorySet By Id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "CategorySet Id"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Not found",
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  bigint $id ID of categorySet
     * @return array
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     */
    public function getAction($id)
    {
        $thisRepo = $this->get('catalog.category_set.repository');
        $categorySet = $thisRepo->find($id);
        if (null == $categorySet) {
            throw new NotFoundHttpException('catalog.category_set.not_found');
        }
        $categorySet = $thisRepo->getFinalResultByJMSGroup($categorySet, 'view');

        return ['status' => true, 'data' => $categorySet];
    }

    /**
     * Update CategorySet By Id
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "Update CategorySet By Id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "CategorySet Id"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     404 = "Not found",
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  Request $request HTTP Foundation Request object
     * @param  int     $id      An unsigned INT to reprensent categorySet's id
     * @return array
     *
     * @Put("/{id}", name = "update", options = {"method_prefix" = false})
     */
    public function putAction(Request $request, $id)
    {
        $thisRepo = $this->get('catalog.category_set.repository');
        $categorySet = $thisRepo->find($id);
        if (null == $categorySet) {
            throw new NotFoundHttpException('catalog.category_set.not_found');
        }
        $data = $request->request->all();

        // $data['modified_at'] = new \DateTime();
        $categorySet->setData($data);
        $errors = $this->container->get('validator')->validate($categorySet);
        if (count($errors) > 0) {
            throw new BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors,'json')
            );
        }
        $thisRepo->update($categorySet);
        $categorySet = $thisRepo->getFinalResultByJMSGroup($categorySet, 'view');

        return ['status' => true, 'data' => $categorySet];
    }

    /**
     * Remove CategorySet by id
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "Remove CategorySet by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "CategorySet Id"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     404 = "Not found",
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param int $id An unsigned INT to represent categorySet's id
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $thisRepo = $this->get('catalog.category_set.repository');
        $categorySet = $thisRepo->find($id);

        if (null == $categorySet) {
            throw new NotFoundHttpException(
                'catalog.categorySet.not_found'
            );
        }

        // Check on Project and Category ???

        $thisRepo->delete($categorySet);

        return ['status' => true];
    }

    /**
     * List of categories under that set Id
     * @ApiDoc(
     *   section = "CategorySet",
     *   resource = true,
     *   resourceDescription="Operations on CategorySet.",
     *   description = "List of categories under that set Id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "CategorySet Id"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @Get("/{id}/categories", name = "categories", options = {"method_prefix" = false})
     */
    public function getCategoriesAction($id)
    {
    	$categoryRepo = $this->get('catalog.category.repository');
    	$categories = $categoryRepo->findBy(array('categorySetId'=>$id));
        $rs = $categoryRepo->getFinalResultByJMSGroup($categories, 'category');

    	return ['status' => true, 'data'=>$rs];
    }
}