<?php

namespace AM\CatalogService\APIBundle\Controller;

use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Get;

use Zenith\CoreService\PaginationBundle\Pagination;

class PublicationCategoryController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @Get("", name = "list", options = {"method_prefix" = false})
     *
     * @QueryParam(name="limit", requirements="\d+", description="limit of catalog list", nullable=true)
     * @QueryParam(name="offset", requirements="\d+", description="offset of catalog list", nullable=true)
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @return array
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $offset = $params['offset'];
        $limit = $params['limit'];
        $thisRepo = $this->get('catalog.publication_category.repository');
        list($data, $total) = $thisRepo->getList($offset, $limit);
        $pagination = new Pagination($total, $offset, $limit);
        return ['status' => true, 'data' => $data, 'pagination' => $pagination->getResult()];
    }


}
