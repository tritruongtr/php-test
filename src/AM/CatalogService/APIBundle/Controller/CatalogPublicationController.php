<?php

namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;


// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Zenith\CoreService\PaginationBundle\Pagination;

class CatalogPublicationController extends FOSRestController implements ClassResourceInterface
{
    
    /**
     * Get catalog publication
     * <br>
     * @ApiDoc(
     *   section = "CatalogPublication",
     *   resource = true,
     *   description = "Get catalog publication",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     * @Get("", name = "list", options = {"method_prefix" = false})
     * @QueryParam(name="catalog_id", requirements="\d+", description="id of catalog", nullable=true)
     * @QueryParam(name="publication_id", requirements="\d+", description="id of catalog", nullable=true)
     * @QueryParam(name="limit", requirements="\d+", description="limit of catalog list", nullable=true)
     * @QueryParam(name="offset", requirements="\d+", description="offset of catalog list", nullable=true)
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getCatalogPublicationsAction(ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $thisRepo = $this->get('catalog.catalog_publication.repository');
        $result = $thisRepo->findCatalogPublicationList($params);
        $data = $thisRepo->getFinalResultByJMSGroup($result['data'], 'list');
    
        return ['status' => true, 'data' => $data, 'pagination' => $result['pagination']];
    }
    

}
