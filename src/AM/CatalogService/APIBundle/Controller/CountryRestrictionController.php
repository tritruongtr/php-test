<?php

namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpKernel\Exception as HTTPException;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use AM\CatalogService\Domain\CountryRestriction\CountryRestriction;
use AM\CatalogService\Domain\Publication\Publication;

class CountryRestrictionController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @Get("", name = "list", options = {"method_prefix" = false})
     *
     * @QueryParam(
     *     name = "id",
     *     nullable = true,
     *     description = "list publication reading modes id"
     * )
     * @QueryParam(
     *     name = "object_id",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "publication id"
     * )
     * @QueryParam(
     *     name = "object_type",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "publication id"
     * )
     * @QueryParam(
     *     name = "country_code",
     *     nullable = true,
     *     description = "has two values: mobile | tablet"
     * )
     * @QueryParam(
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by one or multi columns. syntax: sort=col1,col2,..."
     * )
     * @QueryParam(
     *     name = "ignore_limit",
     *     requirements="\d+",
     *     default="0",
     *     nullable = true,
     *     description = "ignore limit"
     * )
     * @QueryParam(
     *     name = "offset",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam(
     *     name = "limit",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "An unsigned INT to represent limit of records"
     * )
     *
     * @param  ParamFetch $paramFetch
     * @return array
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $thisRepo = $this->get('catalog.country_restriction.repository');
        $countryRestriction = $thisRepo->getListByQueryParams($params, CountryRestriction::getResponseGroups('list'));

        return ['status' => true, 'data' => $countryRestriction['data'], 'pagination' => $countryRestriction['pagination']];
    }

    /**
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     *
     * @param  int        $id
     * @param  ParamFetch $paramFetch
     * @return array
     */
    public function getAction($id, ParamFetch $paramFetch)
    {
        $data = $paramFetch->all();
        $data['id'] = !empty($id) ? (int) $id : 0;
        $thisRepo = $this->get('catalog.country_restriction.repository');

        $countryRestriction = $thisRepo->getListByQueryParams($data, CountryRestriction::getResponseGroups('view'));

        if (empty($countryRestriction['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.country_restriction.not_found'
            );
        }

        return ['status' => true, 'data' => $countryRestriction['data'][0]];
    }

    /**
     * @Post("", name = "create", options = {"method_prefix" = false})
     * @View(statuscode = 201)
     *
     * @param  Request $request
     * @return array
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();

        $countryRestriction = new CountryRestriction($data);
        $errors = $this->get('validator')->validate($countryRestriction);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $thisRepo = $this->get('catalog.country_restriction.repository');
        $thisRepo->create($countryRestriction);

        $countryRestriction = $thisRepo->getFinalResultByJMSGroup($countryRestriction, 'view');
        return ['status' => true, 'data' => $countryRestriction];
    }

    /**
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     *
     * @param  int     $id
     * @param  Request $request
     * @return array
     */
    public function putAction($id, Request $request)
    {
        $data = $request->request->all();
        $id = !empty($id) ? (int) $id : 0;

        $thisRepo = $this->get('catalog.country_restriction.repository');
        $countryRestriction = $thisRepo->find($id);

        if (!$countryRestriction instanceof CountryRestriction) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.country_restriction.not_found'
            );
        }

        unset($data['created_by']);
        $countryRestriction->setData($data);

        $errors = $this->container->get('validator')->validate($countryRestriction);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $thisRepo->update($countryRestriction);
        $result = $thisRepo->getFinalResultByJMSGroup($countryRestriction, 'view');

        return ['status' => true, 'data' => $result];
    }

    /**
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     *
     * @param  int   $id
     * @return array
     */
    public function deleteAction($id)
    {
        $id = !empty($id) ? (int) $id : 0;
        $thisRepo = $this->get('catalog.country_restriction.repository');
        $countryRestriction = $thisRepo->find($id);

        if (!$countryRestriction instanceof CountryRestriction) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.country_restriction.not_found'
            );
        }

        $thisRepo->delete($countryRestriction);
        return ['status' => true, 'data' => []];
    }
}
