<?php

/**
 * @author Huong Le <huong.le@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception as HTTPException;

// FOSRestBundle Stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Routing\ClassResourceInterface;

// Nelmio ApiDocBundle stuff
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

// Audience Media stuff
use AM\CatalogService\Domain\Partner\Partner;
use Zenith\CoreService\Pagination\Pagination;

class PartnerController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Create new partner
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Partner",
     *   resource = true,
     *   description = "Create new partner",
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Partner"
     *     },
     *     {
     *       "name" = "parent_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Parent Partner this Partner belongs to"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Partner’s status"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Common\APIResponse",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *	   400 = "Returned when do not pass the validation",
     *     409 = "Returned when duplicate name in same level"
     *   }
     * )
     *
     * @param Request $request HTTP Foundation Request object
     *
     * @Post("", name = "create", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $partner = new Partner($data);
        $errors = $this->container->get('validator')->validate(
            $partner, null, ['client']
        );

        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        } else {
            $errors = $this->container->get('validator')->validate(
                $partner, null, ['server']
            );
            if (count($errors) > 0) {
                throw new HTTPException\ConflictHttpException(
                    $this->get('jms_serializer')->serialize($errors, 'json')
                );
            }
        }

        $parentId = $partner->get('parentId');

        if ($parentId != 0) {
            $parentPartner = $this->get('catalog.partner.repository')->find($parentId);
            if (!$parentPartner instanceof Partner) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.partner.parent_id.not_found'
                );
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($partner);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Update partner
     * /partner/update/{id}
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Partner",
     *   resource = true,
     *   description = "Update partner",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of partner"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Partner"
     *     },
     *     {
     *       "name" = "parent_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Parent Partner this Partner belongs to"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Partner’s status"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Category\Category",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the partner can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent partner's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("", name = "edit", options = {"method_prefix" = false})
     */
    public function putAction($id, Request $request)
    {
        $partner = $this->get('catalog.partner.repository')->find($id);

        if (!$partner instanceof Partner) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.partner.not_found'
            );
        }

        $data = $request->request->all();
        $partner->setData($data);
        $errors = $this->container->get('validator')->validate(
            $partner, null, ['client']
        );

        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $parentId = $partner->get('parentId');
        if ($parentId != 0) {
            $parentPartner = $this->get('catalog.partner.repository')->find($parentId);
            if (!$parentPartner instanceof Partner) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.partner.parent_id.not_found'
                );
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($partner);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Remove specific partner by id
     * /categories/delete/{id}
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Partner",
     *   resource = true,
     *   description = "Remove specific partner by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of partner"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Common\APIResponse",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the partner can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent partner's id
     *
     * @Delete("", name = "edit", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $partner = $this->get('catalog.partner.repository')->find($id);

        if (!$partner instanceof Partner) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.partner.not_found'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($partner);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Get Partner By Id
     * @ApiDoc(
     *   section = "Partner",
     *   resource = true,
     *   description = "Get Partner By Id",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @param  bigint $id ID of topic
     * @return array
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     */
    public function getAction($id)
    {
        $partner = $this->get('catalog.partner.repository')->find($id);

        if (!$partner instanceof Partner) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.partner.not_found'
            );
        }

		return ['status' => true, 'data' => $partner];
    }

    /**
     * Get list of partners (optional)
     * @ApiDoc(
     *   section = "Partner",
     *   resource = true,
     *   description = "Get Partner List",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * @QueryParam (
     *     name = "parent_id",
     *     nullable = true,
     *     description = "Parent id of Partner"
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "0 or 1"
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();

        $thisRepo = $this->get('catalog.partner.repository');
        $partners = $thisRepo->getListByQueryParams($params);
        $data = $thisRepo->getFinalResultByJMSGroup($partners['data'], 'list');

        $totalRows = $partners['count'];
        $pagination = new Pagination($totalRows, $params['offset'], $params['limit']);
        return ['status' => true, 'data' => $data, 'pagination' => $pagination->getResult()];
    }


	/**
     * Get Partner
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Partner",
     *  description="Get Partner by Partner ID",
     *
     * )
     *
     * @Get("/{id}")
     * @author Cuong Nguyen <cuong.nguyen@audiencemedia.com>
     */
    public function getPartnerAction($id)
    {
        $partner = $this->get('DTPartnerRepository')->find(
            array(
                'id' => $id,
            )
        );
        if (null === $partner) {
            $errorKeyName = "Catalog.Partner.NotFound";
            throw new HTTPException\NotFoundHttpException(
                $errorKeyName
            );
        }

		return ['status' => true, 'data' => $partner];
    }
}
