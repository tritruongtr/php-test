<?php

/**
 * @author Huong Le <huong.le@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

use AM\CatalogService\Domain\Catalog\Catalog;
use AM\CatalogService\Domain\CategoryTranslation\CategoryTranslation;
use AM\CatalogService\Domain\Category\Category;
use FOS\RestBundle\Controller\Annotations;

// FOSRestBundle Stuff
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Routing\ClassResourceInterface;

// Nelmio ApiDocBundle stuff
use JMS\Serializer\SerializationContext;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

// Audience Media stuff
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zenith\CoreService\Pagination\Pagination;
use Zenith\CoreService\Sluggable\Sluggable;

class CategoryController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get list of categories by parent category id (optional)
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Get list of categories by parent category id (optional)",
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Category\Category",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @QueryParam (
     *     name = "project_id",
     *     requirements = "^[0-9]+$",
     *     description = "project_id"
     * )
     * @QueryParam (
     *     name = "language",
     *     requirements = "^[a-z]{3}$",
     *     description = "language of category"
     * )
     * @QueryParam (
     *     name = "parent_id",
     *     requirements = "^[0-9]+$",
     *     description = "parent category id"
     * )
     * @QueryParam (
     *     name = "remote_identifier",
     *     nullable = true,
     *     description = "Remote identifier"
     * )
     * @QueryParam (
     *     name = "depth",
     *     requirements = "^[0-9]+$",
     *     description = "Depth of category"
     * )
     * @QueryParam (
     *     name = "limit",
     *     requirements = "\d+",
     *     description = "A integer number to represents limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     requirements = "\d+",
     *     description = "An integer number to represents offset [i] of records"
     * )
     * @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "Ignore limit"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by group"
     * )
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetcher)
    {
        $params = $paramFetcher->all();
        $ignoreLimit = isset($params['ignore_limit']) && ($params['ignore_limit'] == 1);
        $thisRepo = $this->get('catalog.category.repository');
        $categories = $thisRepo->all($params);

        if ($ignoreLimit) {
            return ['status' => true, 'data' => $categories['data']];
        }

        $totalRows = $categories['count'];
        $pagination = new Pagination($totalRows, $params['offset'], $params['limit']);
        return ['status' => true, 'data' => $categories['data'], 'pagination' => $pagination->getResult()];
    }

    /**
     * Get category detail
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Get specific category by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "category id"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Category\Category",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category can not be found"
     *   }
     * )
     *
     * @param    int $id An unsigned INT to represent category's id
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     */
    public function getAction($id)
    {
        $thisRepo = $this->get('catalog.category.repository');
        $category = $thisRepo->find($id);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }
        $category = $thisRepo->formatDetailCategory($category);
        return ['status' => true, 'data' => $category];
    }

    /**
     * Create new category
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Create new category",
     *   parameters = {
     *     {
     *       "name" = "project_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of project the Categories to fetch belongs to"
     *     },
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Category"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of category"
     *     },
     *      {
     *       "name" = "image",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Main image of category"
     *     },
     *     {
     *       "name" = "slug",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Short slug for this category, intended to be used for URLs"
     *     },
     *     {
     *       "name" = "parent_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Parent Category this Category belongs to"
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Unique string to identify other representations of this Category"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "status of this Category"
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Common\APIResponse",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @param Request $request HTTP Foundation Request object
     * @Post("", name = "create", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        if ($data['parent_id'] == null) {
            $data['parent_id'] = 0;
        }
        if ((!isset($data['slug']) || empty($data['slug'])) && !empty($data['name'])) {
            $data['name'] = Sluggable::convertSpecialCharacter($data['name']);
            $data['slug'] = Sluggable::urlize($data['name'], '_');
        }
        $data['slug'] = Sluggable::urlize($data['slug'], '_');
        $category = new Category($data);
        $errors = $this->container->get('validator')->validate(
            $category, null, ['client', 'server']
        );

        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $parentId = $category->get('ParentId');

        $thisRepo = $this->get('catalog.category.repository');
        if ($parentId != 0) {
            $parentCategory = $thisRepo->find($parentId);
            if (!$parentCategory instanceof Category) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.category.parent_category_id.not_found'
                );
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return ['status' => true, 'data' => $category];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   resourceDescription="Operations on category.",
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update category
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Update category",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of category"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "project_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of project the Categories to fetch belongs to"
     *     },
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Category"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of category"
     *     },
     *     {
     *       "name" = "image",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Main image of category"
     *     },
     *     {
     *       "name" = "parent_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Parent Category this Category belongs to"
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Unique string to identify other representations of this Category"
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Returned when the category can not be found"
     *   }
     * )
     *
     * @param int $id An unsigned INT to represent category's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     */
    public function putAction($id, Request $request)
    {
        $thisRepo = $this->get('catalog.category.repository');
        $category = $thisRepo->find($id);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }
        $oldParentId = $category->getParentCategoryId();
        $data = $request->request->all();

        if (isset($data['slug']) && empty($data['slug'])) {
            $data['name'] = Sluggable::convertSpecialCharacter($data['name']);
            $data['slug'] = Sluggable::urlize($data['name'], '_');
        }
        $data['slug'] = Sluggable::urlize($data['slug'], '_');

        $category->setData($data);
        $errors = $this->container->get('validator')->validate(
        // $category, null, ['client']
            $category, null, ['client', 'server']
        );

        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $id = $category->getId();
        $parentId = $category->getParentCategoryId();
        if ($parentId != 0) {
            $parentCategory = $thisRepo->find($parentId);
            if (!$parentCategory instanceof Category) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.category.parent_category_id.not_found'
                );
            }

            $hierarchical = $thisRepo->getHierarchical(true);
            $categoriesChild = $thisRepo->getChild($hierarchical, $id, 50);
            if (array_key_exists($parentId, $categoriesChild)) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.category.parent_category_id.not_allow'
                );
            }

            if ($oldParentId != $parentId) {
                //restriction of moving categories IF there are publications assigned to it or its children
                $publCateRepo = $this->get('catalog.publication_category.repository');
                $childrent = $thisRepo->getListChild($id);
                $pubCate = $publCateRepo->getPublicationCategory($id);
                $arr = [];
                foreach ($childrent as $child) {
                    foreach ($pubCate as $pc) {
                        $arr[] = $child . '-' . $pc['publication_id'];
                        $publicationCategory = $publCateRepo->findOneBy([
                            'categoryId' => $child,
                            'publicationId' => $pc['publication_id'],
                        ]);
                        if (!empty($publicationCategory)) {
                            throw new HTTPException\BadRequestHttpException(
                                'catalog.category.not_allow_change_parent'
                            );
                        }
                    }
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        $category = $thisRepo->formatDetailCategory($category);
        return ['status' => true, 'data' => $category];
    }

    /**
     * Remove specific category by id
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Remove specific category by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of category"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category can not be found"
     *   }
     * )
     *
     * @param int $id An unsigned INT to represent category's id
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $category = $this->get('catalog.category.repository')->find($id);

        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Get list of categories translation by category id
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Get list of categories translation by category id",
     *   output = {
     *     "class" = "AM\CatalogService\Domain\CategoryTranslation\CategoryTranslation",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @QueryParam (
     *     name = "locale",
     *     requirements = "^[a-zA-Z]{1}[a-zA-Z-_]{0,9}$",
     *     description = "locale of category"
     * )
     * @QueryParam (
     *     name = "limit",
     *     requirements = "\d+",
     *     description = "A integer number to represents limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     requirements = "\d+",
     *     description = "An integer number to represents offset [i] of records"
     * )
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @Get("/{categoryId}/translations", name = "list_translation", options = {"method_prefix" = false})
     */
    public function cgetTranslationAction($categoryId, ParamFetch $paramFetcher)
    {
        $locale = $paramFetcher->get('locale');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');
        $condition = [];

        if ($categoryId != null) {
            $condition['categoryId'] = (int)$categoryId;
        }

        if ($locale != null) {
            $condition['locale'] = $locale;
        }

        if ($limit == null || (int)$limit <= 0) {
            $limit = 10;
        }

        if ($offset == null || (int)$offset <= 0) {
            $offset = 0;
        }

        $translateRepo = $this->get('catalog.category_translation.repository');
        $all = $categoryTranslation = $translateRepo->findBy(
            $condition,
            ['id' => 'desc']
        );
        $count = count($all);
        $categoryTranslation = $translateRepo->findBy(
            $condition,
            ['id' => 'desc'],
            $limit,
            $offset
        );
        $categoryTranslation = $translateRepo->getFinalResultByJMSGroup($categoryTranslation, 'list');
        $totalRows = $count;
        $pagination = new Pagination($totalRows, $offset, $limit);
        return ['status' => true, 'data' => $categoryTranslation, 'pagination' => $pagination->getResult()];
    }

    /**
     * Get category translation detail
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Get specific category translation by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "category translation id"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\CategoryTranslation\CategoryTranslation",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category translation can not be found"
     *   }
     * )
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @param    int $id An unsigned INT to represent translation category's id
     *
     * @Get("/{categoryId}/translations/{id}", name = "view_translation", options = {"method_prefix" = false})
     */
    public function getTranslationAction($categoryId, $id)
    {
        $translateRepo = $this->get('catalog.category_translation.repository');
        $categoryTranslation = $translateRepo->find($id);
        if (!$categoryTranslation instanceof CategoryTranslation) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category_translation.not_found'
            );
        }
        $categoryTranslation = $translateRepo->getFinalResultByJMSGroup($categoryTranslation, 'view');
        return ['status' => true, 'data' => $categoryTranslation];
    }

    /**
     * Create new category translation
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Create new category translation",
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Category translation"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of category translation"
     *     },
     *     {
     *       "name" = "language",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Language code of the translation to use."
     *     },
     *     {
     *       "name" = "locale",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Locale this publication uses."
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Common\APIResponse",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Post("/{categoryId}/translations", name = "create_translation", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postTranslationAction($categoryId, Request $request)
    {
        $data = $request->request->all();
        $category = $this->get('catalog.category.repository')->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'Catalog.Category.CategoryId.NotFound'
            );
        }

        $categoryTranslation = new CategoryTranslation($data);
        $categoryTranslation->set('category', $category);
        $categoryTranslation->set('category_id', $category->get('id'));
        $errors = $this->container->get('validator')->validate(
            $categoryTranslation, null, ['client', 'server']
        );
        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($categoryTranslation);
        $em->flush();

        return ['status' => true, 'data' => $categoryTranslation];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   resourceDescription="Operations on category.",
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @param    int $id An unsigned INT to represent translation category's id
     * @Options("/{categoryId}/translations/{id}", name = "options_translation", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsTranslationAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update category translation
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Update category translation",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of category translation"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "category_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of Category"
     *     },
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Category translation"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of category translation"
     *     },
     *     {
     *       "name" = "language",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Language code of the translation to use."
     *     },
     *     {
     *       "name" = "locale",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Locale this publication uses."
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\CategoryTranslation\CategoryTranslation",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Returned when the category translation can not be found"
     *   }
     * )
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @param int $id An unsigned INT to represent category translation's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{categoryId}/translations/{id}", name = "edit_translation", options = {"method_prefix" = false})
     */
    public function putTranslationAction($categoryId, $id, Request $request)
    {
        $categoryTranslation = $this->get('catalog.category_translation.repository')->find($id);
        if (!$categoryTranslation instanceof CategoryTranslation) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category_translation.not_found'
            );
        }

        $data = $request->request->all();
        $categoryTranslation->setData($data);
        $categoryTranslation->set('categoryId', $categoryId);
        $errors = $this->container->get('validator')->validate(
            $categoryTranslation, null, ['client', 'server']
        );
        if (count($errors) > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $category = $this->get('catalog.category.repository')->find($categoryId);

        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($categoryTranslation);
        $em->flush();

        return ['status' => true, 'data' => $categoryTranslation];
    }

    /**
     * Remove specific category translation by id
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Category",
     *   resource = true,
     *   description = "Remove specific category translation by id",
     *   requirements = {
     *      {
     *       "name" = "categoryId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of category"
     *     },
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Translation Id"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category can not be found"
     *   }
     * )
     *
     * @param    int $categoryId An unsigned INT to represent category's id
     * @param int $id An unsigned INT to represent category's id
     *
     * @Delete("/{categoryId}/translations/{id}", name = "delete_translation", options = {"method_prefix" = false})
     */
    public function deleteTranslationAction($categoryId, $id)
    {
        $categoryTranslation = $this->get('catalog.category_translation.repository')->find($id);
        if (!$categoryTranslation instanceof CategoryTranslation) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category_translation.not_found'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($categoryTranslation);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Get Nesstand Categories from Catalog
     * Can.Ly feedback not use this api
     * <br>
     * @Get("/catalog/{id}/{contentRating}/{language}/{locale}/{projectId}", name = "_list_catalog", options = {"method_prefix" = false})
     * @param int $id id of catalog that is needed to get
     * @param string $contentRating content_rating of newsstand
     * @param string $language language of newsstand
     * @param string $locale locale of newsstand
     * @param string $project_id project_id of newsstand
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getNewsstandCategoriesAction($id, $contentRating, $language, $locale, $projectId)
    {
        $catalogRepo = $this->get('catalog.catalog.repository');
        $thisRepo = $this->get('catalog.category.repository');
        $contentRating = explode('-', $contentRating);
        if (count($contentRating) != 2) {
            throw new NotFoundHttpException(
                'catalog.category.content_rating.format'
            );
        }
        $params = array(
            'content_rating' => $contentRating,
            'language' => $language,
            'locale' => $locale,
            'project_id' => $projectId,
            'catalog_id' => $id,
        );

        $catalog = $catalogRepo->find($id);

        if (!$catalog instanceof Catalog) {
            throw new NotFoundHttpException(
                'catalog.catalog.not_found'
            );
        }

        $categories = $thisRepo->getCatalogCategories($catalog, $params);

        $serializer = SerializerBuilder::create()->build();
        $categoriesJson = $serializer->serialize(
            $categories,
            'json',
            SerializationContext::create()->setGroups(
                ['category']
            )->setSerializeNull(true)
        );
        $categories = json_decode($categoriesJson, true);
        $data = array(
            'categories' => $categories,
            'language' => $params['language'],
            'locale' => $params['locale'],
        );

        $categories = $this->get('catalog.category_translation.repository')->getFilteredCategories($data);

        return ['status' => true, 'data' => $categories];
    }

    /**
     * Assign publication.
     * @param  int $categoryId
     * @param  array $publicationIds
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @return array
     *
     * @Post("/{categoryId}/", name = "assign_publication", options = {"method_prefix" = false})
     */
    public function postAssignPublicationAction($categoryId, Request $request)
    {
        $data = $request->request->all();
        $publicationIds = explode(',', $data['publication_ids']);
        $thisRepo = $this->get('catalog.category.repository');
        $publicationRepo = $this->get('catalog.publication.repository');
        $publicationCategoryRepo = $this->get('catalog.publication_category.repository');

        $category = $thisRepo->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }

        if (count($publicationIds) > 0) {
            foreach ($publicationIds as $publicationId) {
                $publication = $publicationRepo->find($publicationId);
                if (!$publication instanceof Publication) {
                    throw new HTTPException\NotFoundHttpException(
                        'catalog.publication.not_found'
                    );
                }
                $publicationCategory = $publicationCategoryRepo->findOneBy([
                    'categoryId' => $categoryId,
                    'publicationId' => $publicationId,
                ]);

                if (empty($publicationCategory)) {
                    $publicationCategory = new PublicationCategory();
                    $publicationCategory->setCategory($category);
                    $publicationCategory->setPublication($publication);

                    $hierarchical = $thisRepo->getHierarchical();
                    $rootParentCategoryId = $thisRepo->getRootParentCategoryId(
                        $hierarchical, $categoryId
                    );
                    $publicationCategory->setRootParentCategoryId($rootParentCategoryId);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($publicationCategory);
                    $em->flush();
                }
            }
        }

        return ['status' => true, 'data' => []];
    }

    /**
     * Assign multiple publication to category.
     * @Post("/{categoryId}/publications", name = "assigning_publication_category", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postPublicationToCategoryAction($categoryId, Request $request)
    {
        $data = $request->request->all();
        $id = !empty($categoryId) ? (int)$categoryId : 0;
        $publicationHandler = $this->get('catalog.category.handler');
        $result = $publicationHandler->postMultiplePublicationToCategory($id, $data);
        return ['status' => true, 'data' => $result];
    }

    /**
     * Assign multiple publication to category.
     * @Delete("/{categoryId}/publications", name = "un_assigning_publication_category", options = {"method_prefix" = false})
     */
    public function deletePublicationToCategoryAction($categoryId, Request $request)
    {
        $data = $request->request->all();
        $id = !empty($categoryId) ? (int)$categoryId : 0;
        
        $publicationHandler = $this->get('catalog.category.handler');
        $publicationHandler->deleteMultiplePublicationToCategory($id, $data);
        return ['status' => true, 'data' => []];
    }
}
