<?php
/**
 * @author Ca Pham <ca.pham@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;

// FOSRestBundle stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Routing\ClassResourceInterface;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

// Nelmio ApiDocBundle stuff
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

// Audience Media stuff
use AM\CatalogService\Domain\Publisher\Publisher;
use AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata;
use Zenith\CoreService\PaginationBundle\Pagination;

class PublisherMetadataController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Create new publisher metadata
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   description = "Create new publisher metadata",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when do not pass the validation",
     *     404 = "Returned when the publisher metadata can not be created"
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     * @Post("", name = "create", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request HTTP Foundation Request object
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $publisherMetadata = new PublisherMetadata($data);
        $errors = $this->get('validator')->validate($publisherMetadata);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $publisherId = $publisherMetadata->get('publisher_id');
        $keyName = $publisherMetadata->get('key_name');

        $publisher = $this->get('catalog.publisher.repository')->find($publisherId);

        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $publisherMetadataRepo = $this->get('catalog.publisher_metadata.repository');
        $metadata = $publisherMetadataRepo->findOneByPublisherIdKey($publisherId, $keyName);

        if ($metadata instanceof PublisherMetadata) {
            throw new HTTPException\ConflictHttpException(
                'catalog.publisher.metadata_key.exist'
            );
        }

        $keyValue = serialize([$publisherMetadata->getKeyValue(false)]);
        $publisherMetadata->set('key_value', $keyValue);
        $publisherMetadata->set('publisher', $publisher);

        $em = $this->getDoctrine()->getManager();
        $em->persist($publisherMetadata);
        $em->flush();

        $publisherMetadata = $publisherMetadataRepo->getFinalResultByJMSGroup($publisherMetadata, 'view');

        return ['status' => true, 'data' => $publisherMetadata];
    }

    /**
     * Get list of publisher metadata by publisher id or key_name
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   description = "Get list of publisher metadata by publisher id or key_name",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * @QueryParam (
     *     name = "publisher_id",
     *     nullable = true
     * )
     * @QueryParam (
     *     name = "key_name",
     *     nullable = true,
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     *
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $thisRepo = $this->get('catalog.publisher_metadata.repository');
        $publisherMetadata = $thisRepo->getListByQueryParams($params);
        $data = $thisRepo->getFinalResultByJMSGroup($publisherMetadata['data'], 'list');

        $totalRows = $publisherMetadata['count'];
        $pagination = new Pagination($totalRows, $params['offset'], $params['limit']);
        return ['status' => true, 'data' => $data, 'pagination' => $pagination->getResult()];
    }

    /**
     * Get specific metadata by publisher metadata id
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   description = "Get specific metadata by publisher metadata id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when publisher metadata id not found"
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     */
    public function getAction($id)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $metadata = $metaRepo->find($id);
        if ($metadata instanceof PublisherMetadata) {
            $metadata = $metaRepo->getFinalResultByJMSGroup($metadata, 'view');

            return ['status' => true, 'data' => $metadata];
        }

        throw new HTTPException\NotFoundHttpException(
            'catalog.publisher.metadata.not_exist'
        );
    }

    /**
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   resourceDescription="Operations on catalogs.",
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ca Pham <ca.pham@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update publisher metadata
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   description = "Update publisher metadata",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when do not pass the validation",
     *     404 = "Returned when publisher metadata id not found"
     *   }
     * )
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     */
    public function putAction($id, Request $request)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $updateMetadata = $metaRepo->find($id);
        if (!$updateMetadata instanceof PublisherMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.metadata.not_exist'
            );
        }
        $metadata = $metaRepo->find($id);
        $oldKeyValue = $metadata->getKeyValue(false);
        $data = $request->request->all();
        $updateMetadata->setData($data);

        $errors = $this->get('validator')->validate($updateMetadata);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $keyName = $updateMetadata->get('key_name');
        $publisherId = $updateMetadata->get('publisher_id');

        $publisher = $this->get('catalog.publisher.repository')->find($publisherId);
        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $targetMetadata = $metaRepo->findOneByPublisherIdKey($publisherId, $keyName);
        if ($targetMetadata instanceof PublisherMetadata &&
            $targetMetadata->get('id') != $id
        ) {
            throw new HTTPException\ConflictHttpException(
                'catalog.publisher.metadata_key.exist'
            );
        }

        if ($oldKeyValue != $updateMetadata->getKeyValue(false)) {
            $keyValue = serialize([$updateMetadata->getKeyValue(false)]);
            $updateMetadata->set('key_value', $keyValue);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($updateMetadata);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Remove specific publisher metadata by id
     * @ApiDoc(
     *   section = "PublisherMetadata",
     *   resource = true,
     *   description = "Remove specific publisher metadata by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when publisher metadata id not found"
     *   }
     * )
     * @author Ca Pham <ca.pham@audiencemedia.com>
     *
     *
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $metadata = $metaRepo->find($id);
        if (!$metadata instanceof PublisherMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.metadata.not_exist'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($metadata);
        $em->flush();

        return ['status' => true, 'data' => []];
    }
}
