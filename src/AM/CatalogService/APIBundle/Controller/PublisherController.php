<?php
/**
 * @author Can Ly <can.ly@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;

// FOSRestBundle stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Routing\ClassResourceInterface;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

// Audience Media stuff
use AM\CatalogService\Domain\Publisher\Publisher;
use AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata;
use Zenith\CoreService\PaginationBundle\Pagination;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AM\CatalogService\Domain\Catalog\Catalog;
use Zenith\CoreService\Sluggable\Sluggable;

class PublisherController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get list of publishers by country code (optional)
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @QueryParam (
     *     name = "name",
     *     nullable = true,
     *     description = "List of publishers filtered by name"
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "0 or 1"
     * )
     * @QueryParam (
     *     name = "country_code",
     *     nullable = true,
     *     description = "ISO 3166-1 alpha-2 country code"
     * )
     * @QueryParam (
     *     name = "remote_identifier",
     *     nullable = true,
     *     description = "Remote identifier of publisher"
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $thisRepo = $this->get('catalog.publisher.repository');
        $publishers = $thisRepo->getListByQueryParams($params, false, Publisher::getResponseGroups('publisher_list'));

        return ['status' => true, 'data' => $publishers['data'], 'pagination' => $publishers['pagination']];
    }

    /**
     * Create new publisher
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @Post("", name = "create", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201,serializerGroups={"view"})
     *
     * @param Request $request HTTP Foundation Request object
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        // Auto generate slug.
        if (isset($data['name'])) {
            $data['slug'] = Sluggable::urlize($data['name']);
        }

        if (isset($data['created_at'])) {
            try {
                $data['created_at'] = new \DateTime($data['created_at'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.publisher.created_at.invalid'
                );
            }
        }

        $publisher = new Publisher();
        $publisher->setFieldValues($data, Publisher::$allowedCreatingFields);
        $errors = $this->container->get('validator')->validate($publisher);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($publisher);
        $em->flush();

        return ['status' => true, 'data' => $publisher];
    }

    /**
     * Get specific publisher by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher's id
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     * @Annotations\View(serializerGroups={"view"})
     */
    public function getAction($id)
    {
        $thisRepo = $this->get('catalog.publisher.repository');
        $publisher = $thisRepo->find($id);

        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        return ['status' => true, 'data' => $publisher];
    }

    /**
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update publisher
     *
     * @param int     $id      An unsigned INT to represent publisher's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     * @Annotations\View(serializerGroups={"view"})
     */
    public function putAction($id, Request $request)
    {
        $publisher = $this->get('catalog.publisher.repository')->find($id);
        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $data = $request->request->all();

        if (isset($data['modified_at'])) {
            try {
                $data['modified_at'] = new \DateTime($data['modified_at'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.publisher.modified_at.invalid'
                );
            }
        }

        $publisher->setFieldValues($data, Publisher::$allowedUpdatingFields);
        $errors = $this->get('validator')->validate($publisher);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($publisher);
        $em->flush();

        return ['status' => true, 'data' => $publisher];
    }

    /**
     * Remove specific publisher by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher's id
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $publisher = $this->get('catalog.publisher.repository')->find($id);

        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $noOfPublications = $publisher->getNoOfPublications();
        if ($noOfPublications > 0) {
            throw new HTTPException\ConflictHttpException(
                'catalog.publisher.publication.exist'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($publisher);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Create new publisher metadata
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @Post("/metadata", name = "create_metadata", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request HTTP Foundation Request object
     */
    public function postMetadataAction(Request $request)
    {
        $data = $request->request->all();
        $publisherMetadata = new PublisherMetadata($data);
        $errors = $this->get('validator')->validate($publisherMetadata);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $publisherId = $publisherMetadata->get('publisher_id');
        $keyName = $publisherMetadata->get('key_name');

        $publisher = $this->get('catalog.publisher.repository')->find($publisherId);
        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $metadata = $this->get('catalog.publisher_metadata.repository')
        ->findOneByPublisherIdKey($publisherId, $keyName);

        if ($metadata instanceof PublisherMetadata) {
            throw new HTTPException\ConflictHttpException(
                'catalog.publisher.metadata_key.exist'
            );
        }

        $keyValue = serialize([$publisherMetadata->getKeyValue(false)]);
        $publisherMetadata->set('key_value', $keyValue);
        $publisherMetadata->set('publisher', $publisher);

        $em = $this->getDoctrine()->getManager();
        $em->persist($publisherMetadata);
        $em->flush();

        $location = $this->get('router')->generate(
            'api_1_catalog_publisher_view_metadata',
            ['id' => $publisherMetadata->get('id')]
        );

        return ['status' => true, 'data' => ['location' => $location]];
    }

    /**
     * Get list of publisher metadata by publisher id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher's id
     *
     * @Get("/{id}/metadata", name = "list_metadata", options = {"method_prefix" = false})
     */
    public function cgetMetadataAction($id)
    {
        $thisRepo = $this->get('catalog.publisher.repository');
        $publisher = $thisRepo->find($id);
        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $metadata = $publisher->get('metadata');
        $metadata = $thisRepo->getFinalResultByJMSGroup($metadata, 'view');

        return ['status' => true, 'data' => $metadata];
    }

    /**
     * Get specific metadata by publisher metadata id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     *
     * @Get("/metadata/{id}", name = "view_metadata", options = {"method_prefix" = false})
     */
    public function getMetadataAction($id)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $metadata = $metaRepo->find($id);
        if ($metadata instanceof PublisherMetadata) {
            $metadata = $metaRepo->getFinalResultByJMSGroup($metadata, 'view');

            return ['status' => true, 'data' => $metadata];
        }

        throw new HTTPException\NotFoundHttpException(
            'catalog.publisher.metadata.not_exist'
        );
    }

    /**
     * @Options("/metadata/{id}", name = "options_metadata", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsMetadataAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update publisher metadata
     *
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/metadata/{id}", name = "edit_metadata", options = {"method_prefix" = false})
     */
    public function putMetadataAction($id, Request $request)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $updateMetadata = $metaRepo->find($id);
        if (!$updateMetadata instanceof PublisherMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.metadata.not_exist'
            );
        }
        $metadata = $metaRepo->find($id);
        $oldKeyValue = $metadata->getKeyValue(false);
        $data = $request->request->all();
        $updateMetadata->setData($data);

        $errors = $this->get('validator')->validate($updateMetadata);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $keyName = $updateMetadata->get('key_name');
        $publisherId = $updateMetadata->get('publisher_id');

        $publisher = $this->get('catalog.publisher.repository')->find($publisherId);
        if (!$publisher instanceof Publisher) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.not_found'
            );
        }

        $targetMetadata = $metaRepo->findOneByPublisherIdKey($publisherId, $keyName);
        if ($targetMetadata instanceof PublisherMetadata &&
            $targetMetadata->get('id') != $id
        ) {
            throw new HTTPException\ConflictHttpException(
                'catalog.publisher.metadata_key.exist'
            );
        }

        if ($oldKeyValue != $updateMetadata->getKeyValue(false)) {
            $keyValue = serialize([$updateMetadata->getKeyValue(false)]);
            $updateMetadata->set('key_value', $keyValue);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($updateMetadata);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Remove specific publisher metadata by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent publisher metadata's id
     *
     * @Delete("/metadata/{id}", name = "delete_metadata", options = {"method_prefix" = false})
     */
    public function deleteMetadataAction($id)
    {
        $metaRepo = $this->get('catalog.publisher_metadata.repository');
        $metadata = $metaRepo->find($id);
        if (!$metadata instanceof PublisherMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publisher.metadata.not_exist'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($metadata);
        $em->flush();

        return ['status' => true, 'data' => []];
    }


    /**
     * get Publishers from Catalog
     * <br>
     *
     * @Get("/catalog/{id}/{contentRating}", name = "_list_catalog", options = {"method_prefix" = false})
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "0 or 1"
     * )
     * @param int $id id of catalog that is needed to get
     * @param string $contentRating content_rating of newsstand
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getNewsstandPublishersAction($id,$contentRating, ParamFetch $paramFetch)
    {
        $catalogRepo = $this->get('catalog.catalog.repository');
        $thisRepo = $this->get('catalog.publisher.repository');
        $contentRating = explode('-',$contentRating);
        if(count($contentRating) !=2) {
            throw new NotFoundHttpException(
                'catalog.publisher.content_rating.format'
                );
        }
        $params = array(
            'content_rating' => $contentRating,
            'catalog_id' => $id,
            'status' => $paramFetch->get('status')
        );


        $catalog = $catalogRepo->find($id);

        if (!$catalog instanceof Catalog) {
            throw new NotFoundHttpException(
                'catalog.catalog.not_found'
                );
        }

        $publishers = $thisRepo->getCatalogPublishers($catalog,$params);

        $serializer = SerializerBuilder::create()->build();
        $publishersJson = $serializer->serialize(
            $publishers,
            'json',
            SerializationContext::create()->setGroups(
                ['newsstand']
                )->setSerializeNull(true)
            );
        $publishers = json_decode($publishersJson, true);


        return ['status' => true, 'data' => $publishers];
    }
}
