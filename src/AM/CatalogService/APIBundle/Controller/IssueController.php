<?php
/**
 * @author Can Ly <can.ly@audiencemedia.com>
 */
namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception as HTTPException;
use Symfony\Component\DependencyInjection\Exception as DIException;

// FOSRestBundle stuff
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Routing\ClassResourceInterface;

// Nelmio ApiDocBundle stuff
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

// JMS stuff
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

// Audience Media stuff
use AM\CatalogService\Domain\Issue\Issue;
use AM\CatalogService\Domain\IssueMetadata\IssueMetadata;
use AM\CatalogService\Domain\Publication\Publication;
use Zenith\CoreService\Pagination\Pagination;
use Zenith\CoreService\Sluggable\Sluggable;
use Zenith\CoreService\RestClientBundle\JsonResponse;

class IssueController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get list of issues by filters (optional)
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc (
     *   section = "Issue",
     *   resource = true,
     *   description = "Get list of issues by filters (optional)",
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Issue\Issue",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"list"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @QueryParam (
     *     name = "id",
     *     nullable = true,
     *     description = "list issue id"
     * )
     * @QueryParam (
     *     name = "only_id",
     *     nullable = true,
     *     description = "only_id"
     * )
     * @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "ignore limit"
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "A unsigned INT to represent status of issue"
     * )
     * @QueryParam (
     *     name = "type",
     *     nullable = true,
     *     description = "A unsigned INT to represent type of issue"
     * )
     * @QueryParam (
     *     name = "publication_id",
     *     nullable = true,
     *     description = "Id of publication that issue belongs to"
     * )
     * @QueryParam (
     *     name = "legacy_issue_id",
     *     nullable = true,
     *     description = "Legacy issue id"
     * )
     * @QueryParam (
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of newsstand that issue belongs to"
     * )
     * @QueryParam (
     *     name = "category_id",
     *     nullable = true,
     *     description = "Id of category that issue belongs to"
     * )
     * @QueryParam (
     *     name = "root_category_id",
     *     nullable = true,
     *     description = "Id of root category that issue belongs to"
     * )
     * @QueryParam (
     *     name = "language",
     *     nullable = true,
     *     description = "language code the issues should have (based on the Publication)."
     * )
     * @QueryParam (
     *     name = "locale",
     *     nullable = true,
     *     description = "Locale code the issues should have as default locale (based on the Publication)."
     * )
     * @QueryParam (
     *     name = "country_code",
     *     nullable = true,
     *     description = "Country code these issues belong to (based on the Publication)."
     * )
     * @QueryParam (
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of issues  should have (based on the Publication)."
     * )
     * @QueryParam (
     *     name = "issue_remote_id",
     *     nullable = true,
     *     description = "Remote identifier of issue"
     * )
     * @QueryParam (
     *     name = "publication_remote_id",
     *     nullable = true,
     *     description = "Remote identifier of publication"
     * )
     * @QueryParam (
     *     name = "sort_by_list_id",
     *     nullable = true,
     *     description = "Sort list result by list issue id"
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "An unsigned INT to represent limit of records"
     * )
     * @QueryParam (
     *     name = "from_date",
     *     nullable = true,
     *     description = "An unsigned INT to represent from date"
     * )
     * @QueryParam (
     *     name = "to_date",
     *     nullable = true,
     *     description = "An unsigned INT to represent to date"
     * )
     * @QueryParam (
     *     name = "date_type",
     *     nullable = true,
     *     description = "Type of date, by default is cover_date."
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * ) description = "ISO-3 Country code."
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     * @Post("/get_list", name = "lists", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch, Request $request)
    {
        $params = $paramFetch->all();
        $data = $request->request->all();
        $params = array_merge($params, $data);
        $thisRepo = $this->get('catalog.issue.repository');
        $ignoreLimit = (isset($params["ignore_limit"]) && $params["ignore_limit"]=='true') ? true : false;
        $onlyId = (isset($params["only_id"]) && $params["only_id"]=='true') ? true : false;
        $group = ($onlyId) ? Issue::getResponseGroups('showonlyid') : Issue::getResponseGroups('list');
        $issues = $thisRepo->getListByQueryParams($params, $ignoreLimit, $group);
        if (!$onlyId) {
            $thisRepo->appendItems(
                $issues['data'], [['publication', 'search']]
            );
        }

        return ['status' => true, 'data' => $issues['data'], 'pagination' => $issues['pagination']];

        /* below codes should not be used, we need to remove JMS Serializer */
        $data = $thisRepo->getFinalResultByJMSGroup($issues['data'], 'list');

        $values = implode(',', array_column($data, 'publication_id'));
        if (empty($values)) {
            return ['status' => true, 'data' => $data, 'pagination' => $issues['pagination']];
        }
        $pubRepo = $this->get('catalog.publication.repository');
        $temp = $pubRepo->getListByQueryParams(array('id' => $values), true);
        $publications = $pubRepo->getFinalResultByJMSGroup($temp['data'], 'search');
        $issueTemp = array();
        foreach($data as $issue) {
            $issue['publication'] = $publications[array_search($issue['publication_id'], array_column($publications, 'id'))];
            $issueTemp[] = $issue;
        }

        return ['status' => true, 'data' => $issueTemp, 'pagination' => $issues['pagination']];
    }

    /**
     * Create new issue
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     * @param Request $request HTTP Foundation Request object
     * @Post("", name = "create", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        // temporary hard code for modified_by, will change after passed the validation
        $data['modified_by'] = 1;

        if (isset($data['cover_date'])) {
            try {
                $data['cover_date'] = new \DateTime($data['cover_date'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'Cover date is invalid'
                );
            }
        }

        if (isset($data['publish_date'])) {
            try {
                $data['publish_date'] = new \DateTime($data['publish_date'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'Publish date is invalid'
                );
            }
        }

        $issue = new Issue($data);
        $errors = $this->get('validator')->validate($issue);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $publicationId = $issue->get('publication_id');
        $pubRepo = $this->get('catalog.publication.repository');
        $publication = $pubRepo->find($publicationId);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.publication.not_exist'
            );
        }

        // Auto generate slug.
        if (isset($data['name'])) {
            $data['slug'] = Sluggable::urlize($publication->get('name') . '_' . $data['name']);
        }

        $isPublish = $issue->get('status') == Issue::STATUS_PUBLISHED;
        $isUnpublishType = $issue->get('type') < Issue::TYPE_STANDARD;

        if ($isPublish && $isUnpublishType) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.issue.type_publish.invalid'
            );
        }

        $issue->set('publication', $publication);
        $issue->set('modified_by', $data['created_by']);

        $thisRepo = $this->get('catalog.issue.repository');
        $thisRepo->create($issue);

        $price = isset($data['default_price']) ? $data['default_price'] : null;
        $currency = isset($data['default_currency']) ? $data['default_currency'] : null;
        $pubId = $publication->get('id');
        $issueId = $issue->get('id');
        $pubName = $publication->get('name');
        $issName = $issue->get('name');
        $productName = "$pubName $issName";
        $productDescription = empty($issue->get('description')) ? $publication->get('description') : $issue->get('description');

        $issueStatus = (int) $issue->get('status');

        try {
            $resque = $this->get('zenith.queue.resque');
            $resqueHost = $this->container->getParameter('resque_host');
            $resquePort = $this->container->getParameter('resque_port');
            $resque->setBackend($resqueHost, $resquePort);
            $resque->enqueue(
                'generate_product',
                'AM\CommerceService\APIBundle\Job\GenerateProductJob',
                [
                    'rrp' => $price,
                    'rrp_currency_code' => $currency,
                    'publication_id' => $pubId,
                    'issue_id' => $issueId,
                    'issue_status' => $issueStatus,
                    'name' => $productName,
                    'publish_date' => ($issue->get('publishDate') instanceof \DateTime) ? $issue->get('publishDate')->format('Y-m-d H:i:s') : null,
                    'description' => $productDescription,
                    'type' => 1 // Single Issue
                ]
            );

            $resque->enqueue(
                'zenith_index',
                'AM\SearchService\APIBundle\Job\ZenithIndexJob',
                [
                    'id' => $pubId,
                    'type' => 'publications'
                ]
            );
        } catch (\Exception $e) {
            // log plz
        }

        $issues = $thisRepo->getListByQueryParams(['id' => $issueId], true, Issue::getResponseGroups('view'));
        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        return ['status' => true, 'data' => $issues['data'][0]];
    }

    /**
     * Get specific issue by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get specific issue by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Issue\Issue",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"view"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @QueryParam(
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of newsstand that issue belongs to"
     * )
     * @QueryParam(
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of issues  should have (based on the Publication)."
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "A unsigned INT to represent status of issue"
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     */
    public function getAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $thisRepo = $this->get('catalog.issue.repository');
        $params = $paramFetch->all();
        $params['id'] = $id;
        try {
            $issues = $thisRepo->getListByQueryParams($params, true, Issue::getResponseGroups('view'));
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'],
            [['publication', 'database'], ['metadata', 'list']]
        );

        $issue = $issues['data'][0];

        $customDesignPackPubIDs = $this->container->getParameter('custom_design_pack_publication_id');
        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];
        if (in_array($issue['publication']['id'], $customDesignPackPubIDs)) {
            $issueContent = $this->get('catalog.legacy.repository')
                ->setUp($siteID)
                ->getIssue($legacyIssueID)
            ;
        } else {
            $issueContent = array();
            $keyWord = array('name', 'description', 'issue_no', 'volume_no');
            foreach ($issue as $key => $value) {
                if ($key == 'id') {
                    $issueContent[$key] = (string) $legacyIssueID;
                }

                if (in_array($key, $keyWord)) {
                    $issueContent[$key] = $value;
                }

                if ($key == 'preview') {
                    $issueContent['previewable'] = (boolean)$value;
                }

                if ($key == 'publish_date') {
                    $issueContent['published_date'] =  (string) $value->getTimestamp();
                }

                if ($key == 'cover_date') {
                    $issueContent[$key] =  (string) $value->getTimestamp();
                }
            }

            $issueContent['channel'] = 'tablet';
            $defaultDesignPackUrl = $this->container->getParameter('design_pack_reader_version_2_url');
            $issueContent['designs'] = array($thisRepo->getDesignPack($defaultDesignPackUrl));
            $issueContent['full_pdf'] = $thisRepo->getLinearPDF($siteID, $legacyIssueID, $issue['has_pdf']);

        }

        $issue['issue_content'] = $issueContent;

        return ['status' => true, 'data' => $issue];
    }

    /**
     * Get section list of a specific issue
     *
     * @author Ha Do <ha.do@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get Sections list of a specific issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/sections", name = "view_sections", options = {"method_prefix" = false})
     */
    public function getSectionsAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');
        try {
            $result = $thisRepo->getCMSInfo($id);
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($result)) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $CMSInfo = $result[0];

        $sectionRepo = $this->get('catalog.section.repository');
        $siteID = $CMSInfo['site_id'];
        $legacyIssueID = $CMSInfo['legacy_issue_id'];
        $sections = $sectionRepo->config($siteID, $legacyIssueID)->getSectionList($id);

        return ['status' => true, 'data' => $sections];
    }

    /**
     * Get pdf/svg page list of a specific issue
     *
     * @author Ha Do <ha.do@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get pages list of a specific issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @QueryParam(
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of newsstand that issue belongs to"
     * )
     * @QueryParam(
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of issues  should have (based on the Publication)."
     * )
     * @QueryParam(
     *     name = "publication_allow_pdf",
     *     nullable = true,
     *     description = "Determining publication allow xml or not"
     * )
     * @QueryParam(
     *     name = "issue_has_pdf",
     *     nullable = true,
     *     description = "Determining issue has xml or not"
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/pages", name = "view_pages", options = {"method_prefix" = false})
     */
    public function getPagesAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');

        $params = $paramFetch->all();
        $params['id'] = $id;

        $group = Issue::getResponseGroups('view');
        $issues = $thisRepo->getListByQueryParams($params, /* $ignoreLimit = */ true, $group);

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        $issue = $issues['data'][0];
        if ($params['publication_allow_pdf'] == 1 && $issue['publication']['allow_pdf'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not.allow_pdf'
            );
        }

        if ($params['issue_has_pdf'] == 1 && $issue['has_pdf'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.has_not_pdf'
            );
        }

        $pageRepo = $this->get('catalog.page.repository');
        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];
        $pages = $pageRepo->config($siteID, $legacyIssueID)->getPageList($id);

        return ['status' => true, 'data' => $pages];
    }

    /**
     * Get story list of a specific issue
     *
     * @author Ha Do <ha.do@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get pages list of a specific issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @QueryParam(
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of newsstand that issue belongs to"
     * )
     * @QueryParam(
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of issues  should have (based on the Publication)."
     * )
     * @QueryParam(
     *     name = "publication_allow_xml",
     *     nullable = true,
     *     description = "Determining publication allow xml or not"
     * )
     * @QueryParam(
     *     name = "issue_has_xml",
     *     nullable = true,
     *     description = "Determining issue has xml or not"
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/stories", name = "view_stories", options = {"method_prefix" = false})
     */
    public function getStoriesAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');

        $params = $paramFetch->all();
        $params['id'] = $id;

        $group = Issue::getResponseGroups('view');
        $issues = $thisRepo->getListByQueryParams($params, /* $ignoreLimit = */ true, $group);

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        $issue = $issues['data'][0];
        if ($params['publication_allow_xml'] == 1 && $issue['publication']['allow_xml'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not.allow_xml'
            );
        }

        if ($params['issue_has_xml'] == 1 && $issue['has_xml'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.has_not_xml'
            );
        }

        $storyRepo = $this->get('catalog.story.repository');
        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];
        $stories = $storyRepo->config($siteID, $legacyIssueID)->getStoryList($id);

        $jsonResponse = new JsonResponse(['status' => true, 'data' => $stories]);
        return $jsonResponse->get();

        //return ['status' => true, 'data' => $stories];
    }

    /**
     * Get story detail of a specific story for an issue
     *
     * @author Ha Do <ha.do@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get pages list of a specific issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     },
     *    {
     *       "name" = "storyid",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of story"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/stories/{storyid}", name = "view_single_story", options = {"method_prefix" = false})
     */
    public function getSingleStoryAction($id, $storyid, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');
        try {
            $result = $thisRepo->getCMSInfo($id);
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($result)) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $CMSInfo = $result[0];
        $siteID = $CMSInfo['site_id'];
        $legacyIssueID = $CMSInfo['legacy_issue_id'];

        $storyRepo = $this->get('catalog.story.repository');
        $story = $storyRepo->config($siteID, $legacyIssueID)->getStory($id, $storyid);

        return ['status' => true, 'data' => $story];
    }

    /**
     * Get advert list of a specific issue
     *
     * @author Ha Do <ha.do@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get advert list of a specific issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/ads", name = "view_ads", options = {"method_prefix" = false})
     */
    public function getAdvertsAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');
        $result = $thisRepo->getCMSInfo($id);

        if (empty($result)) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $CMSInfo = $result[0];

        $adverts = $this->get('catalog.legacy.repository')
                      ->setUp($CMSInfo['site_id'])
                      ->getIssueAdverts($CMSInfo['legacy_issue_id']);

        return ['status' => true, 'data' => $adverts];

        $issue = $this->get('catalog.issue.repository')->find($id);
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   resourceDescription="Operations on catalogs.",
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update issue
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Update issue",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "publication_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of publication"
     *     },
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of issue"
     *     },
     *     {
     *       "name" = "internal_name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Internal name of issue"
     *     },
     *     {
     *       "name" = "issn",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISSN Number of issue"
     *     },
     *      {
     *       "name" = "slug",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "slug of issue"
     *     },
     *      {
     *       "name" = "code",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "code of issue"
     *     },
     *     {
     *       "name" = "sequence_no",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Sequence Number of issue"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of issue"
     *     },
     *     {
     *       "name" = "cover_image",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Path to cover image url"
     *     },
     *     {
     *       "name" = "cover_date",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Date that is intended to be the label on the Magazine"
     *     },
     *     {
     *       "name" = "publish_date",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Date that the issue was actually completed and made available to the public"
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Remote identifier"
     *     },
     *     {
     *       "name" = "legacy_issue_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the default issue in the CMS siteaccess"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "0: New, 1: Published, 2: Ready, 3: Archived, 4:Discarded, 5:Retracted, 6:Importing, 500: Error"
     *     },
     *     {
     *       "name" = "modified_by",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "User ID (from Identity Service) of user who last updated this record."
     *     },
     *     {
     *       "name" = "file_path",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Path to the Issue XML zip file"
     *     },
     *     {
     *       "name" = "type",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "0: Unknown, 1: Current, 2: Regular, 3: Back, 4: Special"
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when do not pass the validation",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     */
    public function putAction($id, Request $request)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $issue = $thisRepo->find($id);

        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $oldStatus = $issue->get('status');

        // $oldCreated = $issue->get('created');
        $oldCreatedBy = $issue->get('created_by');
        $oldPublicationId = $issue->get('publication_id');
        $issue->set('modified_by', '');

        $data = $request->request->all();

        if (isset($data['cover_date'])) {
            try {
                $data['cover_date'] = new \DateTime($data['cover_date'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'Cover date is invalid'
                );
            }
        }

        if (isset($data['publish_date'])) {
            try {
                $data['publish_date'] = new \DateTime($data['publish_date'], new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new HTTPException\BadRequestHttpException(
                    'Publish date date is invalid'
                );
            }
        }

        // code property shouldn't be changeable.
        if (isset($data['code'])) {
            unset($data['code']);
        }
        // If edit slug, should check before update.
        if (isset($data['slug'])) {
            $data['slug'] = Sluggable::urlize($data['slug']);
        }

        $issue->setData($data);
        $errors = $this->get('validator')->validate($issue);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $isPublish = $issue->get('status') == Issue::STATUS_PUBLISHED;
        $isUnpublishType = $issue->get('type') < Issue::TYPE_STANDARD;

        if ($isPublish && $isUnpublishType) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.issue.type_publish.invalid'
            );
        }

        $newPublicationId = $issue->get('publication_id');
        $pubRepo = $this->get('catalog.publication.repository');
        $publication = $pubRepo->find($newPublicationId);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.publication.not_exist'
            );
        }

        if ($oldPublicationId != $newPublicationId) {
            $issue->set('publication', $publication);
        }

        $issue->set('created_by', $oldCreatedBy);

        $em = $this->getDoctrine()->getManager();
        $em->persist($issue);
        $em->flush();

        $newStatus = $issue->get('status');
        if ($oldStatus != $newStatus && $newStatus == Issue::STATUS_PUBLISHED) {
            try {
                $resque = $this->get('zenith.queue.resque');
                $resqueHost = $this->container->getParameter('resque_host');
                $resquePort = $this->container->getParameter('resque_port');
                $resque->setBackend($resqueHost, $resquePort);
                $resque->enqueue(
                    'generate_offer',
                    'AM\CommerceService\APIBundle\Job\GenerateOfferJob',
                    [
                        'issue_id' => $id
                    ]
                );

/*
                $this->get('old_sound_rabbit_mq.publish_issue_producer')
                    ->publish(json_encode([
                        'id' => $id,
                        'publication_id' => $publication->get('id'),
                        'publish_date' => $issue->get('publishDate')->format(\DateTime::ISO8601)
                    ]), 'publish_issue')
                ;
*/
                $resque->enqueue(
                    'zenith_index',
                    'AM\SearchService\APIBundle\Job\ZenithIndexJob',
                    [
                        'id' => $newPublicationId,
                        'type' => 'publications'
                    ]
                );
            } catch (\Exception $e) {
                // log plz
            }
        }

        // produce update_issue message
        try {
            $this
                ->get('old_sound_rabbit_mq.update_issue_producer')
                ->publish(json_encode([
                    'id' => $id,
                    'publication_id' => $publication->get('id'),
                    'publish_date' => $issue->get('publishDate')->format(\DateTime::ISO8601),
                    'is_published' => ($oldStatus != $newStatus && $newStatus == Issue::STATUS_PUBLISHED) ? true : false
                ]), 'update_issue')
            ;
        } catch(\Exception $exc) {
            $this->get('monolog.logger.catalog.rabbitmq')->error('[UpdateIssueEvent] Error: ' . $exc->getMessage());
        }

        $thisRepo = $this->get('catalog.issue.repository');
        $issues = $thisRepo->getListByQueryParams(['id' => $id], true, Issue::getResponseGroups('database'));
        $thisRepo::bigint2Int(['id', 'publication_id', 'created_by', 'modified_by'], $issues['data'][0]);
        //$data = $thisRepo->getFinalResultByJMSGroup($issue, 'database');

        return ['status' => true, 'data' => $issues['data'][0]];
    }

    /**
     * Remove specific issue by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Remove specific issue by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $issue = $thisRepo->find($id);

        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $publicationID = $issue->get('publication_id');
        $thisRepo->delete($issue);

        try {
            $resque = $this->get('zenith.queue.resque');
            $resqueHost = $this->container->getParameter('resque_host');
            $resquePort = $this->container->getParameter('resque_port');
            $resque->setBackend($resqueHost, $resquePort);

            $resque->enqueue(
                'zenith_index',
                'AM\SearchService\APIBundle\Job\ZenithIndexJob',
                [
                    'id' => $publicationID,
                    'type' => 'publications'
                ]
            );
        } catch (\Exception $e) {
            // log plz
        }

        return ['status' => true, 'data' => []];
    }

    /**
     * Create new issue metadata
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Create new issue metadata",
     *   parameters = {
     *     {
     *       "name" = "issue_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the issue"
     *     },
     *     {
     *       "name" = "key_name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Key of metadata"
     *     },
     *     {
     *       "name" = "key_value",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Value of metadata"
     *     }
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when do not pass the validation",
     *     409 = "Returned when duplicated key name"
     *   }
     * )
     *
     * @param Request $request HTTP Foundation Request object
     *
     * @Post("/{issueId}/metadata", name = "create_metadata", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     */
    public function postMetadataAction($issueId, Request $request)
    {
        $data = $request->request->all();
        $data['issue_id'] = $issueId;
        $issueMetadata = new IssueMetadata($data);
        $errors = $this->get('validator')->validate($issueMetadata);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $keyName = $issueMetadata->get('key_name');

        $thisRepo = $this->get('catalog.issue.repository');

        $issue = $thisRepo->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $metadataRepo = $this->get('catalog.issue_metadata.repository');
        $metadata = $metadataRepo->findOneByIssueIdKey($issueId, $keyName);

        if ($metadata instanceof IssueMetadata) {
            throw new HTTPException\ConflictHttpException(
                'catalog.issue.metadata.exist'
            );
        }

        $keyValue = serialize([$issueMetadata->getKeyValue(false)]);
        $issueMetadata->set('key_value', $keyValue);
        $issueMetadata->set('issue', $issue);

        $em = $this->getDoctrine()->getManager();
        $em->persist($issueMetadata);
        $em->flush();

        $result = $metadataRepo->getListByQueryParams(['id' => $issueMetadata->get('id')], false, IssueMetadata::getResponseGroups('view'));

        return ['status' => true, 'data' => $result['data'][0]];
    }

    /**
     * Get list of issue metadata by issue id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get list of issue metadata by issue id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\IssueMetadata\IssueMetadata",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"view"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{issueId}/metadata", name = "list_metadata", options = {"method_prefix" = false})
     */
    public function cgetMetadataAction($issueId)
    {
        $issue = $this->get('catalog.issue.repository')->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $metadataRepo = $this->get('catalog.issue_metadata.repository');
        $result = $metadataRepo->getListByQueryParams(['issue_id' => $issueId], true, IssueMetadata::getResponseGroups('view'));

        return ['status' => true, 'data' => $result];
    }

    /**
     * Get specific metadata by issue metadata id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get specific metadata by issue metadata id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue metadata"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\IssueMetadata\IssueMetadata",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"view"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue metadata can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue metadata's id
     *
     * @Get("/{issueId}/metadata/{id}", name = "view_metadata", options = {"method_prefix" = false})
     */
    public function getMetadataAction($issueId, $id)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $issue = $this->get('catalog.issue.repository')->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $metadataRepo = $this->get('catalog.issue_metadata.repository');
        try {
            $result = $metadataRepo->getListByQueryParams(['id' => $id], true, IssueMetadata::getResponseGroups('view'));
            if (empty($result['data'])) {
                $result = new \stdClass();
            } else {
                $result = $result['data'][0];
            }
        } catch (\Exception $e) {
            $result = new \stdClass();
        }

        return ['status' => true, 'data' => $result];
    }

    /**
     * Return a list REST resources
     * <br>
     *
     *
     * @Options("/{issuesId}/metadata/{id}", name = "options_metadata", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsMetadataAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update issue metadata
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Update issue metadata",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue metadata"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "key_name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Key of metadata"
     *     },
     *     {
     *       "name" = "key_value",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Value of metadata"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue metadata's id
     * @param Request $request HTTP Foundation Request object
     *
     * @Put("/{issueId}/metadata/{id}", name = "edit_metadata", options = {"method_prefix" = false})
     */
    public function putMetadataAction($issueId, $id, Request $request)
    {
        $issue = $this->get('catalog.issue.repository')->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $metaRepo = $this->get('catalog.issue_metadata.repository');
        $metadata = $metaRepo->findOneBy(['id' => $id, 'issue' => $issueId]);
        if (!$metadata instanceof IssueMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.metadata.not_exist'
            );
        }

        $oldKeyValue = $metadata->getKeyValue(/* $unserialize = */ false);
        $data = $request->request->all();
        $metadata->setData($data);

        $errors = $this->get('validator')->validate($metadata);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $keyName = $metadata->get('key_name');
        $issueId = $metadata->get('issue_id');

        $issue = $this->get('catalog.issue.repository')->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $updateMetadata = $metaRepo->findOneByIssueIdKey($issueId, $keyName);
        if ($updateMetadata instanceof IssueMetadata &&
            $updateMetadata->get('id') != $id
        ) {
            throw new HTTPException\ConflictHttpException(
                'catalog.issue.metadata_key.exist'
            );
        }

        if ($oldKeyValue != $metadata->getKeyValue(/* $unserialize = */ false)) {
            $keyValue = serialize([$metadata->getKeyValue(/* $unserialize = */ false)]);
            $metadata->set('key_value', $keyValue);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($metadata);
        $em->flush();

        $result = $metaRepo->getListByQueryParams(['id' => $id], false, IssueMetadata::getResponseGroups('view'));

        return ['status' => true, 'data' => $result['data'][0]];
    }

    /**
     * Remove specific issue metadata by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Remove specific issue metadata by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue metadata can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent issue metadata's id
     *
     * @Delete("/{issueId}/metadata/{id}", name = "delete_metadata", options = {"method_prefix" = false})
     */
    public function deleteMetadataAction($issueId, $id)
    {
        $issue = $this->get('catalog.issue.repository')->find($issueId);
        if (!$issue instanceof Issue) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $metaRepo = $this->get('catalog.issue_metadata.repository');
        $metadata = $metaRepo->findOneBy(['id' => $id, 'issue' => $issueId]);
        if (!$metadata instanceof IssueMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.metadata.not_exist'
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($metadata);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Get list of issues with empty remote id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @Get("/list/empty_remote_id", name = "list_empty_remote_id", options = {"method_prefix" = false})
     */
    public function cgetEmptyRemoteIdAction(ParamFetch $paramFetch)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $data = $thisRepo->getListEmptyRemoteId();
        return ['status' => true, 'data' => $data];
    }

    /**
     * Get list of issues doesn't belong to any categories
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "project_id",
     *     nullable = true,
     *     description = "Id of project"
     * )
     *
     * @Get("/list/empty_category", name = "list_empty_category", options = {"method_prefix" = false})
     */
    public function cgetEmptyCategoryAction(ParamFetch $paramFetch)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $params = $paramFetch->all();
        $projectId = (isset($params['project_id']) && !empty($params['project_id'])) ? $params['project_id'] : null;
        $data = $thisRepo->getListUnassignCategory($projectId);
        return ['status' => true, 'data' => $data];
    }

    /**
     * Get list of issues not in list of issue id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "id",
     *     nullable = true,
     *     description = "list issue id"
     * )
     *
     * @Get("/list/not_in_list_id", name = "list_not_in_list_id", options = {"method_prefix" = false})
     */
    public function cgetNotInListIdAction(ParamFetch $paramFetch)
    {
        $thisRepo = $this->get('catalog.issue.repository');
        $params = $paramFetch->all();
        $listId = (isset($params['id']) && !empty($params['id'])) ? $params['id'] : null;
        $data = $thisRepo->getListNotInTheList($listId);
        return ['status' => true, 'data' => $data];
    }

    /**
     * Get pdf/svg page list of a specific issue
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "publication_allow_pdf",
     *     nullable = true,
     *     description = "Determining publication allow xml or not"
     * )
     * @QueryParam(
     *     name = "issue_has_pdf",
     *     nullable = true,
     *     description = "Determining issue has xml or not"
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/content/pages", name = "view_content_pages", options = {"method_prefix" = false})
     */
    public function getContentPagesAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');

        $params = $paramFetch->all();
        $params['id'] = $id;

        $group = Issue::getResponseGroups('view');
        $issues = $thisRepo->getListByQueryParams($params, /* $ignoreLimit = */ true, $group);

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        $issue = $issues['data'][0];
        if ($params['publication_allow_pdf'] == 1 && $issue['publication']['allow_pdf'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not.allow_pdf'
            );
        }

        if ($params['issue_has_pdf'] == 1 && $issue['has_pdf'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.has_not_pdf'
            );
        }

        $pageRepo = $this->get('catalog.page.repository');
        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];
        $pages = $pageRepo->config($siteID, $legacyIssueID)->getPageList($id);

        return ['status' => true, 'data' => $pages];
    }

    /**
     * Get story list of a specific issue
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "publication_allow_xml",
     *     nullable = true,
     *     description = "Determining publication allow xml or not"
     * )
     * @QueryParam(
     *     name = "issue_has_xml",
     *     nullable = true,
     *     description = "Determining issue has xml or not"
     * )
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/content/stories", name = "view_content_stories", options = {"method_prefix" = false})
     */
    public function getContentStoriesAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $this->get('catalog.issue.repository');

        $params = $paramFetch->all();
        $params['id'] = $id;

        $group = Issue::getResponseGroups('view');
        $issues = $thisRepo->getListByQueryParams($params, /* $ignoreLimit = */ true, $group);

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        $issue = $issues['data'][0];
        if ($params['publication_allow_xml'] == 1 && $issue['publication']['allow_xml'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not.allow_xml'
            );
        }

        if ($params['issue_has_xml'] == 1 && $issue['has_xml'] != 1) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.has_not_xml'
            );
        }

        $storyRepo = $this->get('catalog.story.repository');
        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];
        $stories = $storyRepo->config($siteID, $legacyIssueID)->getStoryList($id);

        return ['status' => true, 'data' => $stories];
    }

    /**
     * Get specific issue by id
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @param int     $id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/content", name = "view_content", options = {"method_prefix" = false})
     */
    public function getContentAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $thisRepo = $this->get('catalog.issue.repository');
        $params = $paramFetch->all();
        $params['id'] = $id;

        try {
            $issues = $thisRepo->getListByQueryParams($params, true, Issue::getResponseGroups('view'));
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'],
            [['publication', 'database'], ['metadata', 'list']]
        );

        $issue = $issues['data'][0];
        $customDesignPackPubIDs = $this->container->getParameter('custom_design_pack_publication_id');

        if (in_array($issue['publication']['id'], $customDesignPackPubIDs)) {
            $issueContent = $this->get('catalog.legacy.repository')
                ->setUp($issue['publication']['site_id'])
                ->getIssue($issue['legacy_issue_id']);
        } else {
            $issueContent = array();
            $keyWord = array('name', 'description', 'issue_no', 'volume_no');
            foreach ($issue as $key => $value) {
                if ($key == 'id') {
                    $issueContent[$key] = (string) $issue['legacy_issue_id'];
                }

                if (in_array($key, $keyWord)) {
                    $issueContent[$key] = $value;
                }

                if ($key == 'preview') {
                    $issueContent['previewable'] = (boolean)$value;
                }

                if ($key == 'publish_date') {
                    $issueContent['published_date'] =  (string) $value->getTimestamp();
                }

                if ($key == 'cover_date') {
                    $issueContent[$key] =  (string) $value->getTimestamp();
                }
            }

            $issueContent['channel'] = 'tablet';
            $defaultDesignPackUrl = $this->container->getParameter('design_pack_reader_version_2_url');
            $issueContent['designs'] = array($thisRepo->getDesignPack($defaultDesignPackUrl));
        }
        $issue['issue_content'] = $issueContent;

        //filter content
        $data = [];
        if (isset($issue['issue_content']) && empty($issue['issue_content'])) {
            $issue['issue_content'] = new \stdClass();
        }
        if (!$issue['issue_content'] instanceof \stdClass) {
            $data['designs'] = $issue["issue_content"]["designs"];
        } else {
            $data['designs'] = null;
        }

        $siteID = $issue['publication']['site_id'];
        $legacyIssueID = $issue['legacy_issue_id'];

        // Logic for xml
        if ($issue['publication']['allow_xml']) {
            if ($issue['has_xml']) {
                $storyRepo = $this->get('catalog.story.repository');
                $stories = $storyRepo->config($siteID, $legacyIssueID)->getStoryList($id);
                $data['stories'] = $stories;
            } else {
                $data['stories'] = [
                    'status' => false,
                    'error' => [
                        'message' => $this->container->get('translator')->trans('catalog.issue.has_not_xml', array(), 'messages')
                    ]
                ];
            }
        } else {
            $data['stories'] = [
                'status' => false,
                'error' => [
                    'message' => $this->container->get('translator')->trans('catalog.issue.has_not_xml', array(), 'messages')
                ]
            ];
        }

        // Logic for pdf
        if ($issue['publication']['allow_pdf']) {
            if ($issue['has_pdf']) {
                $pageRepo = $this->get('catalog.page.repository');
                $pages = $pageRepo->config($siteID, $legacyIssueID)->getPageList($id);
                $data['pages'] = $pages;
            } else {
                $data['pages'] = [
                    'status' => false,
                    'error' => [
                        'message' => $this->container->get('translator')->trans('catalog.issue.has_not_pdf', array(), 'messages')
                    ]
                ];
            }
        } else {
            $data['pages'] = [
                'status' => false,
                'error' => [
                    'message' => $this->container->get('translator')->trans('catalog.issue.has_not_pdf', array(), 'messages')
                ]
            ];
        }

        return ['status' => true, 'data' => $data];
    }

    /**
     * Get design packs of issue
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @Get("/{id}/content/design_packs", name = "view_content_design_packs", options = {"method_prefix" = false})
     */
    public function getContentDesignPacksAction($id, ParamFetch $paramFetch)
    {
        if ($id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $thisRepo = $this->get('catalog.issue.repository');
        $params = $paramFetch->all();
        $params['id'] = $id;

        try {
            $issues = $thisRepo->getListByQueryParams($params, true, Issue::getResponseGroups('view'));
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'],
            [['publication', 'database'], ['metadata', 'list']]
        );

        $issue = $issues['data'][0];

        $customDesignPackPubIDs = $this->container->getParameter('custom_design_pack_publication_id');

        $result = array();
        if (in_array($issue['publication']['id'], $customDesignPackPubIDs)) {
            $issueContent = $this->get('catalog.legacy.repository')
                ->setUp($issue['publication']['site_id'])
                ->getIssue($issue['legacy_issue_id']);

            $result = $issueContent['designs'];
        } else {
            $defaultDesignPackUrl = $this->container->getParameter('design_pack_reader_version_2_url');
            $thisRepo = $thisRepo = $this->get('catalog.issue.repository');

            $result[] = $thisRepo->getDesignPack($defaultDesignPackUrl);
        }

        return ['status' => true, 'data' => $result];
    }

    /**
     * Get list of issues by category and return list issue follow rule at
     * https://audiencemedia.jira.com/wiki/display/ZEN/Rules+for+content+handlers
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "catalog_id",
     *     description = "catalog id"
     * )
     * @QueryParam(
     *     name = "category_id",
     *     description = "list category id"
     * )
     * @QueryParam(
     *     name = "content_rating",
     *     description = "Content rating the list of publications should have"
     * )
     * @Get("/list/my_interest", name = "list_my_interest", options = {"method_prefix" = false})
     */
    public function cgetMyInterestsAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $listCategory = isset($params['category_id']) ? explode(',', $params['category_id']) : null;
        $thisRepo = $this->get('catalog.issue.repository');
        $result = [];

        foreach ($listCategory as $value) {
            $listIssue = $thisRepo->getListMyInterests($value, $params['catalog_id'], $params['content_rating']);
            $result[$value] = $listIssue;
        }
        return ['status' => true, 'data' => $result];
    }

    /**
     * Get publication sort by issue is new arrival
     * each publication has only a recent issue
     *
     * https://audiencemedia.jira.com/wiki/display/ZEN/Rules+for+content+handlers
     *
     * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "catalog_id",
     *     description = "Catalog id"
     * )
     * @QueryParam(
     *     name = "content_rating",
     *     description = "Content rating the list of publications should have"
     * )
     * @QueryParam(
     *     name = "category_id",
     *     description = "Category id"
     * )
     * @QueryParam(
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     * @QueryParam(
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "Ignore pagination"
     * )
     * @QueryParam(
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam(
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam(
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     *
     * @Get("/list/new_issues", name = "list_new_issue", options = {"method_prefix" = false})
     */
    public function cgetNewIssueAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $thisRepo = $this->get('catalog.issue.repository');

        $listIssue = $thisRepo->getNewIssueList($params, Issue::getResponseGroups('list'));

        return ['status' => true, 'data' => $listIssue['data'], 'pagination' => $listIssue['pagination']];
    }
}
