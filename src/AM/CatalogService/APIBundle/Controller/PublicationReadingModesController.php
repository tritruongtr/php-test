<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpKernel\Exception as HTTPException;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use AM\CatalogService\Domain\PublicationReadingModes\PublicationReadingModes;
use AM\CatalogService\Domain\Publication\Publication;

class PublicationReadingModesController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get list publication reading modes
     * @Get("", name = "list", options = {"method_prefix" = false})
     *
     * @QueryParam (
     *     name = "id",
     *     nullable = true,
     *     description = "list publication reading modes id"
     * )
     * @QueryParam (
     *     name = "publication_id",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "publication id"
     * )
     * @QueryParam (
     *     name = "channel",
     *     nullable = true,
     *     description = "has two values: mobile | tablet"
     * )
     * @QueryParam (
     *     name = "status",
     *     requirements="\d+",
     *     default="1",
     *     nullable = true,
     *     description = "status of item"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by one or multi columns. syntax: sort=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "ignore_limit",
     *     requirements="\d+",
     *     default="0",
     *     nullable = true,
     *     description = "ignore limit"
     * )
     * @QueryParam (
     *     name = "offset",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "limit",
     *     requirements="\d+",
     *     nullable = true,
     *     description = "An unsigned INT to represent limit of records"
     * )
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $thisRepo = $this->get('catalog.publication_reading_modes.repository');
        $publicationReadingModes = $thisRepo->getListByQueryParams($params, PublicationReadingModes::getResponseGroups('list'));

        return ['status' => true, 'data' => $publicationReadingModes['data'], 'pagination' => $publicationReadingModes['pagination']];
    }

    /**
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     *
     * @QueryParam (
     *     name = "status",
     *     requirements="\d+",
     *     default="1",
     *     nullable = true,
     *     description = "status of item"
     * )
     */
    public function getAction($id, ParamFetch $paramFetch)
    {
        $data = $paramFetch->all();
        $data['id'] = !empty($id) ? (int) $id : 0;
        $thisRepo = $this->get('catalog.publication_reading_modes.repository');

        $publicationReadingModes = $thisRepo->getListByQueryParams($data, PublicationReadingModes::getResponseGroups('view'));

        if (empty($publicationReadingModes['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_reading_modes.not_found'
            );
        }

        return ['status' => true, 'data' => $publicationReadingModes['data'][0]];
    }

    /**
     * Create new item
     * @Post("", name = "create", options = {"method_prefix" = false})
     * @View(statuscode = 201)
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $data['modified_by'] = isset($data['created_by']) ? $data['created_by'] : null;

        $publicationReadingModes = new PublicationReadingModes($data);
        $errors = $this->get('validator')->validate($publicationReadingModes);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $publicationId = $publicationReadingModes->get('publication_id');
        $pubRepo = $this->get('catalog.publication.repository');
        $publication = $pubRepo->find($publicationId);
        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }
        $publicationReadingModes->set('publication', $publication);
        
        $thisRepo = $this->get('catalog.publication_reading_modes.repository');
        $thisRepo->create($publicationReadingModes);

        $publicationReadingModes = $thisRepo->getFinalResultByJMSGroup($publicationReadingModes, 'view');
        return ['status' => true, 'data' => $publicationReadingModes];
    }

    /**
     * Update item
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     */
    public function putAction($id, Request $request)
    {
        $data = $request->request->all();
        $id = !empty($id) ? (int) $id : 0;

        $thisRepo = $this->get('catalog.publication_reading_modes.repository');
        $publicationReadingModes = $thisRepo->find($id);

        if (!$publicationReadingModes instanceof PublicationReadingModes) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_reading_modes.not_found'
            );
        }

        //prepare data
        unset($data['created_by']);
        $publicationReadingModes->set('modified_by', '');
        $oldPublicationId = $publicationReadingModes->get('publication_id');
        $publicationReadingModes->setData($data);

        $newPublicationId = $publicationReadingModes->get('publication_id');
        $pubRepo = $this->get('catalog.publication.repository');
        $publication = $pubRepo->find($newPublicationId);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }
        if ($oldPublicationId != $newPublicationId) {
            $publicationReadingModes->set('publication', $publication);
        }
        
        //check validation
        $errors = $this->container->get('validator')->validate($publicationReadingModes);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }
        
        //update item
        $thisRepo->update($publicationReadingModes);
        $result = $thisRepo->getFinalResultByJMSGroup($publicationReadingModes, 'view');
        return ['status' => true, 'data' => $result];
    }

    /**
     *  Delete item
     *  @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     */
    public function deleteAction($id)
    {
        $id = !empty($id) ? (int) $id : 0;
        $thisRepo = $this->get('catalog.publication_reading_modes.repository');
        $publicationReadingModes = $thisRepo->find($id);

        if (!$publicationReadingModes instanceof PublicationReadingModes) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_reading_modes.not_found'
            );
        }

        $thisRepo->delete($publicationReadingModes);
        return ['status' => true, 'data' => []];
    }
}
