<?php

namespace AM\CatalogService\APIBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Zenith\CoreService\Swagger\SwaggerLoader;
use Zenith\CoreService\Swagger\FileReader\YamlReader as SwaggerYamlReader;

class SwaggerController extends FOSRestController
{
    /**
     * Get swagger documentation in json format
     * @Get("/swagger")
     * @author Tin Nguyen <tin.nguyen@audiencemedia.com>
     */
    public function getSwaggerAction(Request $request)
    {
        $key = $request->query->get('key', '');
        $removeExamples = in_array(strtolower($request->query->get('remove_examples', '')), ['true', 'y', '1']) ? true : false;
        $swaggerConfigs = $this->container->getParameter('swagger');

        if ($swaggerConfigs['enable'] !== true || $key !== $swaggerConfigs['key']) {
            throw new AccessDeniedHttpException("Access denied");
        }

        $swaggerDir = $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . $swaggerConfigs['path'];
        $swaggerLoader = new SwaggerLoader(new SwaggerYamlReader());
        $swaggerLoader->scanDir($swaggerDir)->combineContents();
        $errors = $swaggerLoader->getErrors();

        if(0 !== count($errors)) {
            // for debugging, use $swaggerLoader->getErrorsAsArrayOfStrings();
            throw new \Exception('Could not get the swagger json.');
        }

        // Overwrite host, schemes if necessary
        if($swaggerLoader->getResults()) {
            if(isset($swaggerConfigs['host']) && $swaggerConfigs['host']) {
                $swaggerLoader->setField('host', $swaggerConfigs['host']);
            }

            if(isset($swaggerConfigs['schemes']) && $swaggerConfigs['schemes']) {
                $swaggerLoader->setField('schemes', $swaggerConfigs['schemes']);
            }
        }

        // remove examples in responses
        if($removeExamples) $swaggerLoader->removeExamplesFromResponses(true);

        $response = new JsonResponse();
        $response->setContent(json_encode($swaggerLoader->sortFields()->getResults()));

        return $response;
    }
}
