<?php
namespace AM\CatalogService\APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HTTPException;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetch;
use FOS\RestBundle\Routing\ClassResourceInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


// Audience Media stuff
use Zenith\CoreService\PaginationBundle\Pagination;
use Zenith\CoreService\Sluggable\Sluggable;
use AM\CatalogService\Domain\CatalogPublication\CatalogPublication;
use AM\CatalogService\Domain\Catalog\Catalog;
use AM\CatalogService\Domain\Category\Category;
use AM\CatalogService\Domain\Issue\Issue;
use AM\CatalogService\Domain\PublicationCategory\PublicationCategory;
use AM\CatalogService\Domain\PublicationCountryModifier\PublicationCountryModifier;
use AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata;
use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Publisher\Publisher;

class PublicationController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Create Publication
     * <br>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "create publication",
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Name of the Publication"
     *     },
     *     {
     *       "name" = "internal_name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Display name of this publication in English language used internally"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Optional. Description of this publication."
     *     },
     *     {
     *       "name" = "logo",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "URL to publication logo image file"
     *     },
     *     {
     *       "name" = "country_code",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO 3166-1 alpha-2 Country code."
     *     },
     *     {
     *       "name" = "language_code",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO 639-2 alpha-3 Language code."
     *     },
     *     {
     *       "name" = "locale_code",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Locale this publication uses. Example: eng-GB"
     *     },
     *     {
     *       "name" = "publisher_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the Publisher from the publishers table that the publication belongs to."
     *     },
     *     {
     *       "name" = "content_rating",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Rating of the content of this publication."
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Unique string to identify other representations of this publication ."
     *     },
     *     {
     *       "name" = "created_by",
     *       "dataType" = "datetime",
     *       "required" = true,
     *       "description" = "Timestamp the record of this object was added to the database."
     *     },
     *     {
     *       "name" = "publication_site_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID to the AM Console Site instance that represents this Publication"
     *     },
     *     {
     *       "name" = "default_price",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Default or base price for this publication."
     *     },
     *     {
     *       "name" = "default_currency",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO-4217 currency code of the default currency this newsstand uses."
     *     },
     *     {
     *       "name" = "back_issue_threshold",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Number of days (or months) ago that we can consider an Regular Issue as a Back Issue"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Numeric representation of a publication’s status -- whether it be available or unavailable."
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   output = {
     *        "Status" = "Status of creating action",
     *        "ErrorCode" = "Error Code of creating action",
     *        "ErrorMessage" = "Error Code of creating action"
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @Post("", name = "create", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request param request to create new catalog
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function postAction(Request $request)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $data = $request->request->all();
        $data['modified_by'] = isset($data['created_by']) ? $data['created_by'] : null;
        if (!isset($data['back_issue_threshold']) || $data['back_issue_threshold'] == null) {
            $data['back_issue_threshold'] = 180;
        }

        if (isset($data['slug']) && !empty($data['slug'])) {
            if (!preg_match('/^[a-zA-Z0-9-]*$/', $data['slug'])) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.publication.slug.format'
                );
            }
        } elseif (isset($data['name'])) {
            $data['slug'] = Sluggable::urlize($data['name']);
        }

        $publication = new Publication($data);
        if (isset($data['publisher_id'])) {
            $publisher = $this->get('catalog.publisher.repository')->find($data['publisher_id']);
            if (!$publisher instanceof Publisher) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.publisher.not_found'
                );
            }
            $publication->setPublisher($publisher);
        }

        $errors = $this->container->get('validator')->validate($publication);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $thisRepo->addPublication($publication);

        // Can Ly <can.ly@audiencemedia.com> June 23, 2015
        // generating products after created publication
        // ***** Must do this in resque *****
        $price = isset($data['default_price']) ? $data['default_price'] : null;
        $currency = isset($data['default_currency']) ? $data['default_currency'] : null;
        $pubId = $publication->get('id');
        $frequency = $publication->get('frequency');

        try {
            $resque = $this->get('zenith.queue.resque');
            $resqueHost = $this->container->getParameter('resque_host');
            $resquePort = $this->container->getParameter('resque_port');
            $resque->setBackend($resqueHost, $resquePort);
            if ($frequency) {
                $resque->enqueue(
                    'generate_product',
                    'AM\CommerceService\APIBundle\Job\GenerateProductJob',
                    [
                        'rrp' => $price,
                        'rrp_currency_code' => $currency,
                        'publication_id' => $pubId,
                        'frequency' => $frequency,
                        'type' => 3, // Time base issue subscription
                    ]
                );
            }

            $noOfIssues = $publication->get('no_of_issues');
            if ($noOfIssues != null && $noOfIssues > 0) {
                $resque->enqueue(
                    'generate_product',
                    'AM\CommerceService\APIBundle\Job\GenerateProductJob',
                    [
                        'rrp' => $price,
                        'rrp_currency_code' => $currency,
                        'publication_id' => $pubId,
                        'type' => 2, // Count base issue subscription
                        'no_of_issues' => $noOfIssues,
                    ]
                );
            }
        } catch (\Exception $e) {
            // log plz
        }

        $publication = $thisRepo->getFinalResultByJMSGroup($publication, 'view');

        return ['status' => true, 'data' => $publication];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Options("/{id}", name = "options", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update Publication
     * <br>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "update publication",
     *     requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Name of the Publication"
     *     },
     *     {
     *       "name" = "internal_name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Display name of this publication in English language used internally"
     *     },
     *     {
     *       "name" = "description",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Description of this publication"
     *     },
     *     {
     *       "name" = "logo",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "URL to publication logo image file"
     *     },
     *     {
     *       "name" = "country_code",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISO-3 Country code."
     *     },
     *     {
     *       "name" = "language_code",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISO 639-2 Language code."
     *     },
     *     {
     *       "name" = "locale_code",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Locale this publication uses. Example: eng-GB"
     *     },
     *     {
     *       "name" = "publisher_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the Publisher from the publishers table that the publication belongs to."
     *     },
     *     {
     *       "name" = "content_rating",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Rating of the content of this publication."
     *     },
     *     {
     *       "name" = "remote_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Unique string to identify other representations of this publication ."
     *     },
     *     {
     *       "name" = "modified_by",
     *       "dataType" = "datetime",
     *       "required" = false,
     *       "description" = "User ID (from Identity Service) of user who last updated this record."
     *     },
     *     {
     *       "name" = "publication_site_id",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "ID to the AM Console Site instance that represents this Publication"
     *     },
     *     {
     *       "name" = "default_price",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Default or base price for this publication."
     *     },
     *     {
     *       "name" = "default_currency",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISO-4217 currency code of the default currency this newsstand uses."
     *     },
     *     {
     *       "name" = "back_issue_threshold",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Number of days (or months) ago that we can consider an Regular Issue as a Back Issue"
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Numeric representation of a publication’s status -- whether it be available or unavailable."
     *     },
     *     {
     *       "name" = "legacy_identifier",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Data previous"
     *     }
     *   },
     *   output = {
     *        "Status" = "Status of creating action",
     *        "ErrorCode" = "Error Code of creating action",
     *        "ErrorMessage" = "Error Code of creating action"
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Put("/{id}", name = "edit", options = {"method_prefix" = false})
     * @param Request $request param request to create new catalog
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function putAction($id, Request $request)
    {
        $params = $request->request->all();
        $params['id'] = $id != null ? (int) $id : 0;

        $thisRepo = $this->get('catalog.publication.repository');
        $publication = $thisRepo->find($params['id']);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        // If edit slug, should check before update.
        if (isset($params['slug'])) {
            $params['slug'] = Sluggable::urlize($params['slug']);
        }

        $publication->setData($params);

        if (isset($params['publisher_id'])) {
            $publisher = $this->get('catalog.publisher.repository')->find($params['publisher_id']);
            if (!$publisher instanceof Publisher) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.publisher.not_found'
                );
            }
            $publication->setPublisher($publisher);
        }

        $errors = $this->container->get('validator')->validate($publication);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $thisRepo->updatePublication($publication);

        $publication = $thisRepo->getFinalResultByJMSGroup($publication, 'view');

        return ['status' => true, 'data' => $publication];
    }

    /**
     * Get list of publications (optional)
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc (
     *   section = "Publication",
     *   resource = true,
     *   description = "Get list of publications by country code (optional)",
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Publication\Publication",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"list"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @QueryParam (
     *     name = "name",
     *     nullable = true,
     *     description = "list publication filter by name"
     * )
     * @QueryParam (
     *     name = "type",
     *     nullable = true,
     *     description = "list publication filter by type"
     * )
     * @QueryParam (
     *     name = "id",
     *     nullable = true,
     *     description = "list publication id"
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     strict=true,
     *     requirements="\d+",
     *     description = "A unsigned INT to represent status of publication"
     * )
     * @QueryParam (
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of catalog that publication belongs to"
     * )
     * @QueryParam (
     *     name = "category_id",
     *     nullable = true,
     *     description = "Id of category that publication belongs to"
     * )
     * @QueryParam (
     *     name = "publisher_id",
     *     nullable = true,
     *     description = "Id of publisher that publication belongs to"
     * )
     * @QueryParam (
     *     name = "locale",
     *     nullable = true,
     *     description = "Locale this publication uses. Example: ‘eng-GB’."
     * )
     * @QueryParam (
     *     name = "country_code",
     *     nullable = true,
     *     description = "ISO-3 Country code."
     * )
     * @QueryParam (
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of publications should have"
     * )
     * @QueryParam (
     *     name = "remote_identifier",
     *     nullable = true,
     *     description = "Remote identifier string of publication"
     * )
     * @QueryParam (
     *     name = "hasIssue",
     *     nullable = true,
     *     description = "hasIssue"
     * )
     * @QueryParam (
     *     name = "cpstatus",
     *     nullable = true,
     *     description = "status of catalog publications"
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     *  @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "get all publications ignore limit"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "sort_by",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort_by=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "order_by",
     *     nullable = true,
     *     description = "DESC or ASC, default = DESC"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "ignore_sort",
     *     nullable = true,
     *     description = "Skip the sort function"
     * )
     * @QueryParam (
     *     name = "include",
     *     nullable = true,
     *     description = "Include more parameters"
     * )
     * @QueryParam (
     *     name = "sort_by_list_id",
     *     nullable = true,
     *     description = "Sort list result by list publication id"
     * )
     *
     * @Get("", name = "list", options = {"method_prefix" = false})
     */
    public function cgetAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $ignoreLimit = isset($params['ignore_limit']) && $params['ignore_limit'] == true ? true : false;
        $thisRepo = $this->get('catalog.publication.repository');
        $group = $ignoreLimit === true ? 'ignore_limit' : 'list';


        $publications = $thisRepo->getListByQueryParams($params, $ignoreLimit, Publication::getResponseGroups($group));
        if ($group == 'ignore_limit') {
            foreach ($publications['data'] as &$publication) {
                $thisRepo::bigint2Int(['id'], $publication);
            }

            return ['status' => true, 'data' => $publications['data'], 'pagination' => $publications['pagination']];
        }

        $paramAppends = [
            ['publisher', 'publication_list'],
            ['latest_issue', 'list']
        ];
        if (isset($params['include']) && $params['include'] == 'categories') {
            array_push($paramAppends, ['categories', 'list']);
        }

        $thisRepo->appendItems($publications['data'], $paramAppends);

        return ['status' => true, 'data' => $publications['data'], 'pagination' => $publications['pagination']];
    }

    /**
     * Get list of issues by publication_id
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Get list of issues by publication_id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @QueryParam (
     *     name = "publication_id",
     *     nullable = true,
     *     description = "Id of publication that issue belongs to"
     * )
     * @param int     $id      An unsigned INT to represent publication's id
     * @Get("/{id}/issues", name = "issue_list", options = {"method_prefix" = false})
     */
    public function cgetIssueAction($id, ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $params['publication_id'] = $id;
        $thisRepo = $this->get('catalog.issue.repository');
        $ignoreLimit = (isset($params["ignore_limit"]) && $params["ignore_limit"]=='true') ? true : false;
        $issues = $thisRepo->getListByQueryParams($params, $ignoreLimit, Issue::getResponseGroups('list'));
        $thisRepo->appendItems(
            $issues['data'], [['publication', 'search']]
        );

        return ['status' => true, 'data' => $issues['data'], 'pagination' => $issues['pagination']];

        /* below codes should not be used, we need to remove JMS Serializer */
        $data = $thisRepo->getFinalResultByJMSGroup($issues['data'], 'list');

        $values = implode(',', array_column($data, 'publication_id'));
        if (empty($values)) {
            return ['status' => true, 'data' => $data, 'pagination' => $issues['pagination']];
        }
        $pubRepo = $this->get('catalog.publication.repository');
        $temp = $pubRepo->getListByQueryParams(array('id' => $values), true);
        $publications = $pubRepo->getFinalResultByJMSGroup($temp['data'], 'search');
        $issueTemp = array();
        foreach($data as $issue) {
            $issue['publication'] = $publications[array_search($issue['publication_id'], array_column($publications, 'id'))];
            $issueTemp[] = $issue;
        }

        return ['status' => true, 'data' => $issueTemp, 'pagination' => $issues['pagination']];
    }

    /**
     * Get specific issue by id
     *
     * @author Huong Le <huong.le@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Issue",
     *   resource = true,
     *   description = "Get specific issue by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of issue"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Issue\Issue",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"view"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the issue can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent publication's id
     * @param int     $issue_id      An unsigned INT to represent issue's id
     *
     * @Get("/{id}/issues/{issue_id}", name = "issue_view", options = {"method_prefix" = false})
     */
    public function getIssueAction($id, $issue_id, ParamFetch $paramFetch)
    {
        if ($issue_id === '0') {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo = $thisRepo = $this->get('catalog.issue.repository');
        try {
            $issues = $thisRepo->getListByQueryParams(['id' => $issue_id], true, Issue::getResponseGroups('view'));
        } catch (\Exception $e) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        if (empty($issues['data'])) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.issue.not_exist'
            );
        }

        $thisRepo->appendItems(
            $issues['data'], [['publication', 'database'], ['metadata', 'list']]
        );

        $issue = $issues['data'][0];
        if ($id != $issue['publication']['id']) {
            return ['status' => false, 'message' => 'This issue (' . $issue_id . ') not belong to this publication (' . $id . ')'];
        }
        $issueContent = $this->get('catalog.legacy.repository')
                             ->setUp($issue['publication']['site_id'])
                             ->getIssue($issue['legacy_issue_id']);
        $issue['issue_content'] = $issueContent;

        return ['status' => true, 'data' => $issue];
    }

    /**
     * Delete Publication
     * <br>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "delete publication",
     *    requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   output = {
     *       "Status" = "Status of deleting action",
     *       "ErrorCode" = "Error Code of deleting action",
     *       "ErrorMessage" = "Error Code of deleting action"
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Delete("/{id}", name = "delete", options = {"method_prefix" = false})
     * @param int $id id of publication that is needed to delete
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function deleteAction($id)
    {
        $params['id'] = $id != null ? (int) $id : 0;
        $this->get('catalog.publication.repository')->deletePublication($params);

        return ['status' => true, 'data' => []];

    }

    /**
     * Get specific publication by id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Get specific publication by id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Publication\Publication",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"view"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent publication's id
     *
     * @Get("/{id}", name = "view", options = {"method_prefix" = false})
     *
     * @QueryParam (
     *     name = "channel",
     *     nullable = true,
     *     description = "filter reading modes, has two values: mobile | tablet"
     * )
     */
    public function getAction($id, ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $params['id'] = $id;

        $thisRepo = $this->get('catalog.publication.repository');
        $publication = $thisRepo->find($id);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $publication->set('publisher', $publication->getPublisher('database'));

        $categories = $publication->getCategories();
        if (count($categories) > 0) {
            $categories = $this->get('catalog.category.repository')
                ->reformArray($categories);
            $publication->set('categories', $categories);
        }

        $publication = $thisRepo->getFinalResultByJMSGroup($publication, 'view');

        $issue = $this->get('catalog.issue.repository')->findOneBy(
            ['publicationId' => $id, 'status' => 1],
            ['coverDate' => 'DESC']
        );
        $latestIssue = $this->get('catalog.issue.repository')->getFinalResultByJMSGroup($issue, 'list');
        $publication['latest_issue'] = $latestIssue;

        //list reading modes
        $thisRepo->appendReadingModes($publication, $params);

        return ['status' => true, 'data' => $publication];
    }

    /**
     * Get latest issue by publication id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Get latest issue by publication id",
     *   requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\Issue\Issue",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     },
     *     "groups" = {"list"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication can not be found"
     *   }
     * )
     *
     * @param int     $id      An unsigned INT to represent publication's id
     *
     * @Get("/{id}/latest_issue", name = "latest_issue", options = {"method_prefix" = false})
     */
    public function getLatestIssueAction($id)
    {
        $publication = $this->get('catalog.publication.repository')->find($id);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $latestIssue = $publication->getLatestIssue();

        return ['status' => true, 'data' => $latestIssue];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{publicationId}/assign/catalog/{catalogId}", name = "options_assign_catalog", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAssignCatalogAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Assign publication to catalog
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $publicationId  An unsigned INT to represent publication's id
     * @param int     $catalogId      An unsigned INT to represent catalog's id
     * @param Request $request        HTTP Foundation Request object
     *
     * @Post("/{publicationId}/assign/catalog/{catalogId}", name = "assign_catalog", options = {"method_prefix" = false})
     */
    public function postAssignCatalogAction($publicationId, $catalogId, Request $request)
    {
        $params = $request->request->all();
        $params['publication_id'] = $publicationId;
        $params['catalog_id'] = $catalogId;
        $params['status'] = 1;
        // temporary hard code for modified_by, will change after passed the validation
        $params['modified_by'] = 1;
        $cp = new CatalogPublication($params);

        $errors = $this->get('validator')->validate($cp);
        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $cp->set('modified_by', $params['created_by']);

        $thisRepo = $this->get('catalog.publication.repository');
        $publication = $thisRepo->find($publicationId);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $catalog = $this->get('catalog.catalog.repository')->find($catalogId);
        if (!$catalog instanceof Catalog) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.catalog.not_found'
            );
        }

        $cpRepo = $this->get('catalog.catalog_publication.repository');
        $catalogPublication = $cpRepo->findOneBy([
            'catalogId' => $catalogId,
            'publicationId' => $publicationId,
        ]);

        if (empty($catalogPublication)) {
            $cp->setCatalog($catalog);
            $cp->setPublication($publication);

            $em = $this->getDoctrine()->getManager();

            $em->persist($cp);
            $em->flush();
        } elseif ($catalogPublication->get('status') == CatalogPublication::STATUS_DISABLED) {
            $catalogPublication->set('status', CatalogPublication::STATUS_ENABLED);
            $catalogPublication->set('created_by', $cp->get('created_by'));
            $catalogPublication->set('modified_by', $cp->get('created_by'));
            $cpRepo->update($catalogPublication);
        }

        return ['status' => true, 'data' => []];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{publicationId}/update_status/catalog/{catalogId}", name = "options_update_status_catalog", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsStatusCatalogAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Reset status for publication which was belonged catalog
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Reset status for publication which was belonged catalog",
     *   requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *     {
     *       "name" = "catalogId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "0: Unassign, 1: Assign"
     *     },
     *     {
     *       "name" = "modified_by",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "User ID (from Identity Service) of user who last updated this record."
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication or catalog can not be found",
     *     400 = "Returned when can not pass the validation"
     *   }
     * )
     *
     * @param int     $publicationId  An unsigned INT to represent publication's id
     * @param int     $catalogId      An unsigned INT to represent catalog's id
     * @param Request $request        HTTP Foundation Request object
     *
     * @Put("/{publicationId}/update_status/catalog/{catalogId}", name = "update_status_catalog", options = {"method_prefix" = false})
     */
    public function putUpdateStatusCatalogAction($publicationId, $catalogId, Request $request)
    {
        $thisRepo = $this->get('catalog.publication.repository');

        $publication = $thisRepo->find($publicationId);
        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $catalog = $this->get('catalog.catalog.repository')->find($catalogId);
        if (!$catalog instanceof Catalog) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.catalog.not_found'
            );
        }

        $cp = $this->get('catalog.catalog_publication.repository')->findOneBy([
            'catalogId' => $catalogId,
            'publicationId' => $publicationId,
        ]);
        if (!$cp instanceof CatalogPublication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_assign'
            );
        }

        $oldCreatedBy = $cp->get('created_by');
        $cp->set('modified_by', '');
        $params = $request->request->all();
        $params['publication_id'] = $publicationId;
        $params['catalog_id'] = $catalogId;
        $cp->setData($params);
        $errors = $this->get('validator')->validate($cp);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $cp->set('created_by', $oldCreatedBy);

        $em = $this->getDoctrine()->getManager();
        $em->persist($cp);
        $em->flush();

        return ['status' => true, 'data' => []];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     * @Options("/{publicationId}/unassign/catalog/{catalogId}", name = "options_unassign_catalog", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsUnassignCatalogAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Unassign publication from catalog
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Unassign publication from catalog",
     *   requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *     {
     *       "name" = "catalogId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication or catalog can not be found"
     *   }
     * )
     * @Annotations\View(statuscode = 200, serializerGroups={"view"})
     * @param int  $publicationId  An unsigned INT to represent publication's id
     * @param int  $catalogId      An unsigned INT to represent catalog's id
     * @Delete("/{publicationId}/unassign/catalog/{catalogId}", name = "unassign_catalog", options = {"method_prefix" = false})
     */
    public function deleteUnassignCatalogAction($publicationId, $catalogId, Request $request)
    {
        $catalogHandler = $this->get('catalog.catalog_publication.handler');
        $catalogPublication = $catalogHandler->unAssignCatalogPublication($publicationId, $catalogId, $request);
        return ['status' => true, 'data' => $catalogPublication];
    }

    /**
     * Support CMS call before call PUT service
     * <br>
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Support CMS call before call PUT service",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Options("/{publicationId}/assign/category/{categoryId}", name = "options_assign_category", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsAssignCategoryAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Assign publication to category
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Assign publication to category",
     *   requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *     {
     *       "name" = "categoryId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication or category can not be found"
     *   }
     * )
     *
     * @param int  $publicationId  An unsigned INT to represent publication's id
     * @param int  $categoryId     An unsigned INT to represent category's id
     *
     * @POST("/{publicationId}/assign/category/{categoryId}", name = "assign_category", options = {"method_prefix" = false})
     */
    public function putAssignCategoryAction($publicationId, $categoryId)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $categoryRepo = $this->get('catalog.category.repository');
        $publCateRepo = $this->get('catalog.publication_category.repository');

        $publication = $thisRepo->find($publicationId);
        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $category = $categoryRepo->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.category.not_found'
            );
        }

        $publicationCategory = $publCateRepo->findOneBy([
            'categoryId' => $categoryId,
            'publicationId' => $publicationId,
        ]);

        if (empty($publicationCategory)) {
            $publicationCategory = new PublicationCategory();
            $publicationCategory->setCategory($category);
            $publicationCategory->setPublication($publication);

            $hierarchical = $categoryRepo->getHierarchical();
            $rootParentCategoryId = $categoryRepo->getRootParentCategoryId(
                $hierarchical, $categoryId
            );
            $publicationCategory->setRootParentCategoryId($rootParentCategoryId);

            $em = $this->getDoctrine()->getManager();
            $em->persist($publicationCategory);
            $em->flush();
        }
        //set publication to parent category
        $parentCategory = $categoryRepo->getListParent($categoryId);
        foreach ($parentCategory as $item) {
            $category = $categoryRepo->find($item);
            if (!$category instanceof Category) {
                $logger = $this->get('logger');
                $logger->error('category ' . $item . ' not found');
            } else {
                $publicationCategory = new PublicationCategory();
                $publicationCategory->setCategory($category);
                $publicationCategory->setPublication($publication);

                $hierarchical = $categoryRepo->getHierarchical();
                $rootParentCategoryId = $categoryRepo->getRootParentCategoryId(
                    $hierarchical, $categoryId
                );
                $publicationCategory->setRootParentCategoryId($rootParentCategoryId);
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($publicationCategory);
                    $em->flush();
                } catch (DBALException $ex) {
                    $logger = $this->get('logger');
                    $logger->error($ex->getMessage());
                }
            }
        }

        return ['status' => true, 'data' => []];
    }

    /**
     * Return a list REST resources
     * <br>
     *
     *
     * @Options("/{publicationId}/unassign/category/{categoryId}", name = "options_unassign_category", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsUnassignCategoryAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Unassign publication from category
     *
     * @ApiDoc(
     *   section = "Publication",
     *   resource = true,
     *   description = "Unassign publication from category",
     *   requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *     {
     *       "name" = "categoryId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of catalog"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication or category can not be found"
     *   }
     * )
     *
     * @param int     $publicationId  An unsigned INT to represent publication's id
     * @param int     $categoryId     An unsigned INT to represent category's id
     *
     * @DELETE("/{publicationId}/unassign/category/{categoryId}", name = "unassign_category", options = {"method_prefix" = false})
     */
    public function putUnassignCategoryAction($publicationId, $categoryId)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $categoryRepo = $this->get('catalog.category.repository');
        $publCateRepo = $this->get('catalog.publication_category.repository');
        $publication = $thisRepo->find($publicationId);

        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $category = $categoryRepo->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.category.not_found'
            );
        }

        $publicationCategory = $publCateRepo->findOneBy([
            'categoryId' => $categoryId,
            'publicationId' => $publicationId,
        ]);

        if (!empty($publicationCategory)) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($publicationCategory);
            $em->flush();
        }
        // unassign publication to childrent category
        $children = $categoryRepo->getListChild($categoryId);
        foreach ($children as $item) {
            $publicationCategory = $publCateRepo->findOneBy([
                'categoryId' => $item,
                'publicationId' => $publicationId,
            ]);

            if (!empty($publicationCategory)) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($publicationCategory);
                    $em->flush();
                } catch (DBALException $ex) {
                    $logger = $this->get('logger');
                    $logger->error($ex->getMessage());
                }
            }
        }

        return ['status' => true, 'data' => []];
    }

    /**
     * Create Publication Metadata
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationMetadata",
     *   resource = true,
     *   description = "create publication metadata",
     *   parameters = {
     *     {
     *       "name" = "publication_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the Publication from the publications table that the publication_metadata belongs to."
     *     },
     *     {
     *       "name" = "key_name",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "key name of metadata"
     *     },
     *     {
     *       "name" = "key_value",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "Key value of metadata"
     *     }
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @Post("/{publicationId}/metadata", name = "create_metadata", options = {"method_prefix" = false})
     *
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request param request to create new pulication metadata
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function postMetadataAction(Request $request)
    {
        $data = $request->request->all();
        if (isset($data['key_value']) && !empty($data['key_value'])) {
            $data['key_value'] = serialize([$data['key_value']]);
        }

        $publicationMetadata = new PublicationMetadata($data);
        if (isset($data['publication_id'])) {
            $publication = $this->get('catalog.publication.repository')->find($data['publication_id']);

            if (!$publication instanceof Publication) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }
            $publicationMetadata->setPublication($publication);
        }

        $errors = $this->container->get('validator')->validate($publicationMetadata);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $this->get('catalog.publication_metadata.repository')->addPublicationMetadata($publicationMetadata);

        $serializer = SerializerBuilder::create()->build();
        $publicationMetadataJson = $serializer->serialize(
            $publicationMetadata,
            'json',
            SerializationContext::create()->setGroups(
                ['view']
            )->setSerializeNull(true)
        );
        $publicationMetadata = json_decode($publicationMetadataJson, true);

        return ['status' => true, 'data' => $publicationMetadata];
    }

    /**
     * View publication metadata
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationMetadata",
     *   resource = true,
     *   description = "view publication metadata",
     *    requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *    {
     *       "name" = "keyName",
     *       "dataType" = "string",
     *       "description" = "key name of publication"
     *     }
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication metadata can not be found"
     *   }
     * )
     *
     * @Get("/{publicationId}/metadata/{id}", name = "view_metadata", options = {"method_prefix" = false})
     * @param int $publicationId id of publication metadata that is needed to get
     * @param string $keyName key name of publication metadata that is needed to get
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getMetadataAction($id)
    {
        $metaRepo = $this->get('catalog.publication_metadata.repository');
        $publicationMetadata = $metaRepo->find($id);

        if (!$publicationMetadata instanceof PublicationMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_metadata.not_found'
            );
        }

        $serializer = SerializerBuilder::create()->build();
        $publicationMetadataJson = $serializer->serialize(
            $publicationMetadata,
            'json',
            SerializationContext::create()->setGroups(
                ['view']
            )
        );
        $publicationMetadata = json_decode($publicationMetadataJson, true);

        return ['status' => true, 'data' => $publicationMetadata];
    }

    /**
     * Return a list REST resources
     * <br>
     *
     *
     * @Options("/{publicationId}/metadata/{id}", name = "options_metadata", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsMetadataAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update Publication Metadata
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationMetadata",
     *   resource = true,
     *   description = "update publication metadata",
     *     requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication metadata"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "key_name",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Key name of publication metadata"
     *     },
     *     {
     *       "name" = "key_value",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "Key value of publication metadata"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Put("/{publicationId}/metadata/{id}", name = "edit_metadata", options = {"method_prefix" = false})
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function putMetadataAction($id, Request $request)
    {
        $params = $request->request->all();
        $params['id'] = $id != null ? (int) $id : 0;
        if (isset($params['key_value']) && !empty($params['key_value'])) {
            $params['key_value'] = serialize([$params['key_value']]);
        }
        $metaRepo = $this->get('catalog.publication_metadata.repository');
        $publicationMetadata = $metaRepo->find($params['id']);

        if (!$publicationMetadata instanceof PublicationMetadata) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_metadata.not_found'
            );
        }

        $publicationMetadata->setData($params);
        $errors = $this->container->get('validator')->validate($publicationMetadata);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $metaRepo->updatePublicationMetadata($publicationMetadata);

        return ['status' => true, 'data' => []];
    }

    /**
     * Delete Publication Metadata
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationMetadata",
     *   resource = true,
     *   description = "delete publication metadata",
     *    requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication metadata"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Delete("/{publicationId}/metadata/{id}", name = "delete_metadata", options = {"method_prefix" = false})
     * @param int $id id of publication metadata that is needed to delete
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function deleteMetadataAction($id)
    {
        $params['id'] = $id != null ? (int) $id : 0;
        $this->get('catalog.publication_metadata.repository')->deletePublicationMetadata($params);

        return ['status' => true, 'data' => []];

    }

    /**
     * Get publication metadata list
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationMetadata",
     *   resource = true,
     *   description = "Get all metadata follow publication",
     *   output = {
     *    "class" = "AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata",
     *    "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not successful (Exception error)"
     *   }
     * )
     *
     * @Get("/{publicationId}/metadata", name = "list_metadata", options = {"method_prefix" = false})
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function cgetMetadataAction($publicationId)
    {
        $result = $this->get('catalog.publication_metadata.repository')->findPublicationMetadataList($publicationId);

        return ['status' => true, 'data' => $result['data']];
    }

    /**
     * Create Publication Country Modifier
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationCountryModifier",
     *   resource = true,
     *   description = "create publication country modifier",
     *   parameters = {
     *     {
     *       "name" = "publication_id",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "ID of the Publication from the publications table that the metadata record belongs to."
     *     },
     *     {
     *       "name" = "country_code",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO-2 Country code that this publication is available in."
     *     },
     *     {
     *       "name" = "price",
     *       "dataType" = "float",
     *       "required" = true,
     *       "description" = "Default or base price for this publication."
     *     },
     *     {
     *       "name" = "currency_code",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO-4217 currency code of the default currency this newsstand uses."
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = true,
     *       "description" = "Numeric representation of status whether the modifier is in effect or not"
     *     }
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @Post("/country_modifier", name = "create_country_modifier", options = {"method_prefix" = false})
     * @Annotations\View(statuscode = 201)
     *
     * @param Request $request param request to create new pulication coutry modifier
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function postCountryModifierAction(Request $request)
    {
        $data = $request->request->all();

        $countryMod = new PublicationCountryModifier($data);
        if (isset($data['publication_id'])) {
            $publication = $this->get('catalog.publication.repository')->find($data['publication_id']);

            if (!$publication instanceof Publication) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }
            $countryMod->setPublication($publication);
        }
        $errors = $this->container->get('validator')->validate($countryMod);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $this->get('catalog.publication_country_modifier.repository')->addPublicationCountryModifier($countryMod);

        $serializer = SerializerBuilder::create()->build();
        $publicationCountryModifierJson = $serializer->serialize(
            $countryMod,
            'json',
            SerializationContext::create()->setGroups(
                ['view']
            )->setSerializeNull(true)
        );
        $publicationCountryModifier = json_decode($publicationCountryModifierJson, true);

        return ['status' => true, 'data' => $publicationCountryModifier];
    }

    /**
     * View publication country modifier
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationCountryModifier",
     *   resource = true,
     *   description = "view publication country modifier",
     *    requirements = {
     *     {
     *       "name" = "publicationId",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication"
     *     },
     *     {
     *       "name" = "countryCode",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO-2 Country code that this publication is available in."
     *     },
     *     {
     *       "name" = "currency",
     *       "dataType" = "string",
     *       "required" = true,
     *       "description" = "ISO-4217 currency code of the default currency this newsstand uses."
     *     },
     *   },
     *   output = {
     *     "class" = "AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the publication metadata can not be found"
     *   }
     * )
     *
     * @Get("/{publicationId}/country_modifier/{countryCode}/{currency}", name = "view_country_modifier", options = {"method_prefix" = false})
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function getCountryModifierAction($publicationId, $countryCode, $currency)
    {
        $countryModRepo = $this->get('catalog.publication_country_modifier.repository');
        $countryMod = $countryModRepo->findOneBy(
            [
                'publicationId' => $publicationId,
                'countryCode' => $countryCode,
                'currencyCode' => $currency,
            ]
        );

        if (!$countryMod instanceof PublicationCountryModifier) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_country_modifier.not_found'
            );
        }

        $serializer = SerializerBuilder::create()->build();
        $countryModJson = $serializer->serialize(
            $countryMod,
            'json',
            SerializationContext::create()->setGroups(
                ['view']
            )
        );
        $result = json_decode($countryModJson, true);

        return ['status' => true, 'data' => $result];
    }

    /**
     * Return a list REST resources
     * <br>
     *
     *
     * @Options("/country_modifier/{id}", name = "options_country_modifier", options = {"method_prefix" = false})
     * @author Ha Do <ha.do@audiencemedia.com>
     * @return array
     */
    public function optionsCountryModifierAction()
    {
        return ['status' => true, 'data' => []];
    }

    /**
     * Update Publication Country Modifier
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationCountryModifier",
     *   resource = true,
     *   description = "update publication country modifier",
     *     requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication country modifier"
     *     }
     *   },
     *   parameters = {
     *     {
     *       "name" = "country_code",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISO-2 Country code that this publication is available in."
     *     },
     *     {
     *       "name" = "price",
     *       "dataType" = "float",
     *       "required" = false,
     *       "description" = "Default or base price for this publication."
     *     },
     *     {
     *       "name" = "currency",
     *       "dataType" = "string",
     *       "required" = false,
     *       "description" = "ISO-4217 currency code of the default currency this newsstand uses."
     *     },
     *     {
     *       "name" = "status",
     *       "dataType" = "integer",
     *       "required" = false,
     *       "description" = "Numeric representation of status whether the modifier is in effect or not"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Put("/country_modifier/{id}", name = "edit_country_modifier", options = {"method_prefix" = false})
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function putCountryModifierAction($id, Request $request)
    {
        $params = $request->request->all();
        $params['id'] = $id != null ? (int) $id : 0;

        $countryModRepo = $this->get('catalog.publication_country_modifier.repository');
        $countryMod = $countryModRepo->find($params['id']);

        if (!$countryMod instanceof PublicationCountryModifier) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication_country_modifier.not_found'
            );
        }

        $countryMod->setData($params);
        $errors = $this->container->get('validator')->validate($countryMod);

        if ($errors->count() > 0) {
            throw new HTTPException\BadRequestHttpException(
                $this->get('jms_serializer')->serialize($errors, 'json')
            );
        }

        $countryModRepo->updatePublicationCountryModifier($countryMod);

        return ['status' => true, 'data' => []];
    }

    /**
     * Delete Publication Country Modifier
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationCountryModifier",
     *   resource = true,
     *   description = "delete publication country modifier",
     *    requirements = {
     *     {
     *       "name" = "id",
     *       "dataType" = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Id of publication country modifier"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "Not found"
     *   }
     * )
     *
     * @Delete("/country_modifier/{id}", name = "delete_country_modifier", options = {"method_prefix" = false})
     * @param int $id id of publication metadata that is needed to delete
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function deleteCountryModifierAction($id)
    {
        $params['id'] = $id != null ? (int) $id : 0;
        $this->get('catalog.publication_country_modifier.repository')->deletePublicationCountryModifier($params);

        return ['status' => true, 'data' => []];
    }

    /**
     * Get publication country modifier list
     * <br>
     *
     * @ApiDoc(
     *   section = "PublicationCountryModifier",
     *   resource = true,
     *   description = "Get all country modifier follow publication",
     *   output = {
     *    "class" = "AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata",
     *    "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not successful (Exception error)"
     *   }
     * )
     *
     * @Get("/{publicationId}/country_modifier", name = "list_country_modifier", options = {"method_prefix" = false})
     * @author Quang Vo <quang.vo@audiencemedia.com>
     * @return array
     */
    public function cgetCountryModifierAction($publicationId)
    {
        $result = $this->get('catalog.publication_country_modifier.repository')->findPublicationCountryModifierList($publicationId);

        return ['status' => true, 'data' => $result['data']];
    }

    /**
     * Get assigned publication to catalog
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $publicationId  An unsigned INT to represent publication's id
     * @param int     $catalogId      An unsigned INT to represent catalog's id
     *
     * @Get("/{publicationId}/assign/catalog/{catalogId}", name = "get_assign_catalog", options = {"method_prefix" = false})
     */
    public function getAssignedPubCatalogAction($publicationId, $catalogId)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $catalogPub = $this->get('catalog.catalog_publication.repository')->findOneBy([
            'catalogId' => $catalogId,
            'publicationId' => $publicationId,
            'status' => CatalogPublication::STATUS_ENABLED
        ]);

        if (empty($catalogPub)) {
            return ['status' => true, 'data' => []];
        }

        $catalog = $thisRepo->getFinalResultByJMSGroup($catalogPub->get('catalog'), 'list');
        $publication = $thisRepo->getFinalResultByJMSGroup($catalogPub->get('publication'), 'list');

        return ['status' => true, 'data' => [
            'catalog' => $catalog,
            'publication' => $publication,
        ]];
    }

    /**
     * list assigned publications to catalog
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @param int     $publicationId  An unsigned INT to represent publication's id
     *
     * @Get("/{publicationId}/assign/catalog", name = "list_assign_catalog", options = {"method_prefix" = false})
     */
    public function cgetAssignedPubCatalogAction($publicationId)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $catalogPubs = $this->get('catalog.catalog_publication.repository')->findBy([
            'publicationId' => $publicationId,
            'status' => CatalogPublication::STATUS_ENABLED
        ]);

        if (empty($catalogPubs)) {
            return ['status' => true, 'data' => []];
        }

        $catalogs = [];
        foreach ($catalogPubs as $catalogPub) {
            $catalogs[] = $thisRepo->getFinalResultByJMSGroup($catalogPub->get('catalog'), 'list');
            if (!isset($publication)) {
                $publication = $thisRepo->getFinalResultByJMSGroup($catalogPub->get('publication'), 'list');
            }
        }

        return ['status' => true, 'data' => [
            'catalogs' => $catalogs,
            'publication' => $publication,
        ]];
    }

    /**
     * Get list of publications not in list of publication id
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @QueryParam(
     *     name = "id",
     *     nullable = true,
     *     description = "list publication id"
     * )
     *
     * @Get("/list/not_in_list_id", name = "list_not_in_list_id", options = {"method_prefix" = false})
     */
    public function cgetNotInListIdAction(ParamFetch $paramFetch)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $params = $paramFetch->all();
        $listId = (isset($params['id']) && !empty($params['id'])) ? $params['id'] : null;
        $data = $thisRepo->getListNotInTheList($listId);

        return ['status' => true, 'data' => $data];
    }

    /**
     * Get list of publications don't have issue
     *
     * @author Can Ly <can.ly@audiencemedia.com>
     *
     * @Get("/list/empty_issue", name = "list_empty_issue", options = {"method_prefix" = false})
     */
    public function cgetEmptyIssueAction(ParamFetch $paramFetch)
    {
        $thisRepo = $this->get('catalog.publication.repository');
        $data = $thisRepo->getListEmptyIssue();

        return ['status' => true, 'data' => $data];
    }

    /**
     * Get list publication random
     *
     * @author Gia Hoang Nguyen giahoang.nguyen@audiencemedia.com
     *
     * @QueryParam (
     *     name = "id",
     *     nullable = true,
     *     description = "list publication id"
     * )
     * @QueryParam (
     *     name = "status",
     *     nullable = true,
     *     description = "A unsigned INT to represent status of publication"
     * )
     * @QueryParam (
     *     name = "catalog_id",
     *     nullable = true,
     *     description = "Id of catalog that publication belongs to"
     * )
     * @QueryParam (
     *     name = "category_id",
     *     nullable = true,
     *     description = "Id of category that publication belongs to"
     * )
     * @QueryParam (
     *     name = "content_rating",
     *     nullable = true,
     *     description = "Content rating the list of publications should have"
     * )
     * @QueryParam (
     *     name = "hasIssue",
     *     nullable = true,
     *     description = "hasIssue"
     * )
     * @QueryParam (
     *     name = "limit",
     *     nullable = true,
     *     description = "A unsigned INT to represent limit of records"
     * )
     *  @QueryParam (
     *     name = "ignore_limit",
     *     nullable = true,
     *     description = "get all publications ignore limit"
     * )
     * @QueryParam (
     *     name = "offset",
     *     nullable = true,
     *     description = "An unsigned INT to represent offset [i] of records"
     * )
     * @QueryParam (
     *     name = "sort",
     *     nullable = true,
     *     description = "Sort by 1 or multi columns. syntax: sort=col1,col2,..."
     * )
     * @QueryParam (
     *     name = "ignore_sort",
     *     nullable = true,
     *     description = "Skip the sort function"
     * )
     *
     * @GET("/list/random", name = "list_random", options = {"method_prefix" = false})
     */
    public function cgetListRandomAction(ParamFetch $paramFetch)
    {
        $params = $paramFetch->all();
        $ignoreLimit = isset($params['ignore_limit']) && $params['ignore_limit'] == true ? true : false;
        $thisRepo = $this->get('catalog.publication.repository');
        $group = $ignoreLimit === true ? 'ignore_limit' : 'list';

        $publications = $thisRepo->getListByQueryParams($params, $ignoreLimit, Publication::getResponseGroups($group));
        $limit = $params['limit'];

        //count page with limit is 10
        $params['limit'] = 10;
        $pageCount = intval($publications['pagination']['count'] /  $params['limit']) + 1;
        $result = null;
        $pageStore = array();

        //get random publication by page and get total record equal variable limit
        while (count($result) < $limit) {
            $page = rand(0, $pageCount);
            $params['offset'] = ($page - 1) * $params['limit'];
            if (in_array($page, $pageStore)) {
                continue;
            }
            array_push($pageStore, $page);

            $rows = $thisRepo->getListByQueryParams($params, $ignoreLimit, Publication::getResponseGroups($group));
            $result = ($result == null ? $rows['data'] : array_merge($result, $rows['data']));
        }

        if ($group == 'ignore_limit') {
            foreach ($result as &$publication) {
                $thisRepo::bigint2Int(['id'], $publication);
            }
            return ['status' => true, 'data' => $result, 'pagination' => $publications['pagination']];
        }

        $thisRepo->appendItems(
            $result,
            [
                ['publisher', 'publication_list'],
                ['latest_issue', 'list']
            ]
        );

        return ['status' => true, 'data' => $result, 'pagination' => $publications['pagination']];
    }

    /**
     * Put  multiple Country Restrictions
     *
     * @author Gia Hoang Nguyen giahoang.nguyen@audiencemedia.com
     *
     * @PUT("/{publicationId}/country_restrictions", name = "country_restrictions", options = {"method_prefix" = false})
     */
    public function countryRestrictionsAction($publicationId, Request $request)
    {
        $data = $request->request->all();
        $id = !empty($publicationId) ? (int) $publicationId : 0;
        $thisRepo = $this->get('catalog.publication.repository');

        $publication = $thisRepo->find($id);
        if (!$publication instanceof Publication) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.publication.not_found'
            );
        }

        $publicationHandler = $this->get('catalog.publication.handler');
        $result = $publicationHandler->putMultipleCountryRestriction($id, $data);
        return ['status' => true, 'data' => $result];
    }
}
