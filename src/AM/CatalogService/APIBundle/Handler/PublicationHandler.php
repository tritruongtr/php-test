<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\APIBundle\Handler;

use Symfony\Component\HttpKernel\Exception as HTTPException;
use AM\CatalogService\Domain\CountryRestriction\CountryRestriction;

class PublicationHandler 
{
    private $validator;
    private $jmsSerializer;
    private $em;
    private $countryRestrictionRepo;

    /**
     * PublicationHandler constructor.
     * @param $validator
     * @param $em
     * @param $publicationRepo
     * @param $countryRestrictionRepo
     */
    public function __construct($jmsSerializer, $validator, $em, $countryRestrictionRepo)
    {
        $this->jmsSerializer = $jmsSerializer;
        $this->validator = $validator;
        $this->em = $em;
        $this->countryRestrictionRepo = $countryRestrictionRepo;
    }

    /**
     * @param $id
     * @param $data
     * @return array
     */
    public function putMultipleCountryRestriction($id, $data)
    {
        $object = array(
            'object_type' => CountryRestriction::PUBLICATION,
            'object_id'=> $id
        );

        //multi parameter
        if (empty($data)) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.publication.country_code.not_blank'
            );
        }

        //check array json
        if (!isset($data[0])) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.publication.country_restriction.format'
            );
        }

        $result = $this->insertData($data, $object);
        return $result;

    }

    /**
     * @param $data
     * @param $object
     */
    private function insertData($data, $object)
    {
        //validation
        $insertData = array();
        $result = array();
        $countries = array();

        foreach ($data as $key => $value) {
            if (count($value) > 1 || !isset($value['country_code'])) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.publication.allow_country_code'
                );
            }

            $object['country_code'] = $value['country_code'];
            $countries[] = $value['country_code'];
            $countryRestriction = $this->countryRestrictionRepo->findOneBy(
                array(
                    'objectType' => $object['object_type'],
                    'objectId' => $object['object_id'],
                    'countryCode' => $value['country_code']
                )
            );

            if (!empty($countryRestriction)) {
                $result[] = $countryRestriction;
                continue;
            }
            ${"countryRestriction_" . $key} = new CountryRestriction($object);

            $errors = $this->validator->validate(${"countryRestriction_" . $key});
            if ($errors->count() > 0) {
                throw new HTTPException\BadRequestHttpException(
                    $this->jmsSerializer->serialize($errors, 'json')
                );
            }

            $insertData[] = ${"countryRestriction_" . $key};
        }

        if (count(array_unique($countries)) < count($countries)) {
            // Array has duplicates
            throw new HTTPException\BadRequestHttpException(
                'catalog.publication.duplicates_country_code'
            );
        }

        if (!empty($insertData)) {
            $this->countryRestrictionRepo->insertMulti($insertData);
        }

        $result = $this->countryRestrictionRepo->getFinalResultByJMSGroup(array_merge($result, $insertData), 'view');

        return $result;
    }
}