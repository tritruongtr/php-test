<?php
/**
 * @author Phuc Nguyen <phuc.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\APIBundle\Handler;

use AM\CatalogService\Domain\Catalog\Catalog;
use AM\CatalogService\Domain\CatalogPublication\CatalogPublication;
use AM\CatalogService\Domain\Publication\Publication;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints;

class CatalogPublicationHandler
{
    private $catalogRepo;
    private $publicationRepo;
    private $catalogPublicationRepo;
    private $validator;
    private $serializer;
    private $em;

    /**
     * CatalogPublicationHandler constructor.
     * @param $catalogRepo
     * @param $publicationRepo
     * @param $catalogPublicationRepo
     * @param $serializer
     * @param $validator
     * @param $em
     */
    public function __construct($catalogRepo, $publicationRepo, $catalogPublicationRepo, $serializer, $validator, $em)
    {
        $this->catalogRepo = $catalogRepo;
        $this->publicationRepo = $publicationRepo;
        $this->catalogPublicationRepo = $catalogPublicationRepo;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->em = $em;
    }

    public function putPublications($catalogId, $request)
    {
        $catalog = $this->checkExistCatalog($catalogId);
        $catalogPublication = $this->validatePublicationParams($catalog, $request);
        $result = $this->insertOrUpdate($catalogPublication);
        return $result;
    }

    protected function checkExistCatalog($catalogId)
    {
        $catalog = $this->findCatalog($catalogId);
        if (!$catalog instanceof Catalog) {
            throw new NotFoundHttpException('catalog.catalog.not_found');
        }
        return $catalog;
    }

    protected function findCatalog($catalogId)
    {
        return $this->catalogRepo->find($catalogId);
    }

    protected function checkExistPublication($publicationId)
    {
        $publication = $this->findPublication($publicationId);
        if (!$publication instanceof Publication) {
            throw new NotFoundHttpException('catalog.publication.not_found');
        }
        return $publication;
    }

    protected function findPublication($publicationId)
    {
        return $this->publicationRepo->find($publicationId);
    }

    protected function validatePublicationParams($catalog, $request)
    {
        //validate publication params
        $catalogPublications = $this->parsePublicationRequest($request);
        //validate unique publicationId
        $this->validateUniqueKey($catalogPublications);
        //validate publications object
        $catalogPublications = $this->validateCatalogPublications($catalogPublications);
        //validate publication entities
        $catalogPublications = $this->setCatalogPublicationEntity($catalog, $catalogPublications);
        return $catalogPublications;
    }

    public function parsePublicationRequest($request)
    {
        $params = $request->request->all();
        $publications = null;
        if (isset($params[0])) {
            foreach ($params as $key => $item) {
                $publications[] = array(
                    "publicationId" => isset($item['publication_id']) ? $item['publication_id'] : null,
                    "remarks" => isset($item['remarks']) ? $item['remarks'] : null,
                    "status" => isset($item['status']) ? $item['status'] : CatalogPublication::STATUS_ENABLED,
                    "createdBy" => isset($item['created_by']) ? $item['created_by'] : null,
                );
                $publications[$key]['modifiedBy'] = isset($item['modified_by']) ? $item['modified_by'] : $publications[$key]['createdBy'];
            }
        } else {
            throw new BadRequestHttpException('catalog.catalog_publication.publication.not_multiple');
        }
        return $publications;
    }

    protected function validateUniqueKey($publications, $uniqueKey = array("publicationId"))
    {
        if (empty($publications)) {
            return null;
        }
        for ($i = 0; $i < count($publications); $i++) {
            foreach ($publications[$i] as $key => $val) {
                if (in_array($key, $uniqueKey)) {
                    $uniqueArray[$i][$key] = $val;
                }
            }
        }
        $uniqueInArray = array_unique($uniqueArray, SORT_REGULAR);
        if (count($uniqueInArray) < count($uniqueArray)) {
            throw new BadRequestHttpException('catalog.catalog_publication.publication.duplicated');
        }
        return true;
    }

    protected function validateCatalogPublications($catalogPublications)
    {
        $constraint = new Constraints\Collection(
            array(
                'publicationId' => array(
                    new Constraints\NotBlank(array('message' => 'catalog.catalog_publication.publication_id.not_blank')),
                    new Constraints\Regex(array(
                        'pattern' => '/^[1-9]\d*$/',
                        'message' => 'catalog.catalog_publication.publication_id.not_number'
                    ))
                ),
                'remarks' => array(
                    new Constraints\Length(array(
                        'max' => 255,
                        'maxMessage' => 'catalog.catalog_publication.remarks.length'
                    ))
                ),
                'createdBy' => array(
                    //new Constraints\NotBlank(array('message' => 'catalog.publication.created_by.not_blank')),
                    new Constraints\Regex(array(
                        'pattern' => '/^[0-9]\d*$/',
                        'message' => 'catalog.publication.created_by.format'
                    ))
                ),
                'modifiedBy' => array(
                    //new Constraints\NotBlank(array('message' => 'catalog.publication.modified_by.not_blank')),
                    new Constraints\Regex(array(
                        'pattern' => '/^[0-9]\d*$/',
                        'message' => 'catalog.publication.modified_by.format'
                    ))
                ),
                'status' => array(
                    new Constraints\Regex(array(
                        'pattern' => '/^(0|1)$/',
                        'message' => 'catalog.publication.assign_status.format'
                    )),
                ),

            )
        );

        $publicationIds = array();
        foreach ($catalogPublications as $catalogPublication) {
            $errors = $this->validator->validate($catalogPublication, $constraint);
            if ($errors->count() > 0) {
                throw new BadRequestHttpException($this->serializer->serialize($errors, 'json'));
            }
            $publicationIds[] = $catalogPublication['publicationId'];
        }
        $publications = $this->findPublicationByIds($publicationIds);
        //validate publication id found in array
        $this->comparePublicationInCatalogPublication($publications, $catalogPublications);
        foreach ($catalogPublications as $key => $catalogPublication) {
            $catalogPublications[$key]['publication'] = $publications[$key];
        }
        return $catalogPublications;
    }

    protected function findPublicationByIds($publicationIds)
    {
        $publicationIds = implode(",", $publicationIds);
        return $this->publicationRepo->getByIds($publicationIds);
    }

    protected function comparePublicationInCatalogPublication($publications, $catalogPublications)
    {
        if (count($catalogPublications) != count($publications)) {
            throw new NotFoundHttpException('catalog.catalog_publication.catalog.array.not_found');
        }
        return true;
    }

    public function setCatalogPublicationEntity(Catalog $catalog, $params)
    {
        $catalogPublications = [];
        foreach ($params as $param) {
            $catalogPublication = new CatalogPublication($param);
            $catalogPublication->setCatalog($catalog);
            $catalogPublications[] = $catalogPublication;
        }

        return $catalogPublications;
    }

    protected function findOneByCond(array $cond)
    {
        return $this->catalogPublicationRepo->findOneBy($cond);
    }

    protected function checkExistOneByCond(array $cond)
    {
        $catalogPublication = $this->findOneByCond($cond);
        if (!$catalogPublication instanceof CatalogPublication) {
            throw new NotFoundHttpException('catalog.catalog_publication.not_found');
        }
        return $catalogPublication;
    }

    private function insertOrUpdate($catalogPublications)
    {
        //start transaction
        $connection = $this->em->getConnection();
        $connection->beginTransaction();

        //$batchSize = 20;
        try {
            for ($i = 1; $i <= count($catalogPublications); ++$i) {
                $catalogPublication = $catalogPublications[$i - 1];
                $currentCatalogPublication = $this->findOneByCond(
                    array(
                        "catalog" => $catalogPublication->getCatalog(),
                        "publication" => $catalogPublication->getPublication()
                    )
                );
                /// get createdBy, modifiedBy
                $createdBy = $catalogPublication->get('createdBy');
                $modifiedBy = $catalogPublication->get('modifiedBy');

                if ($currentCatalogPublication instanceof CatalogPublication) { //update
                    $currentCatalogPublication->set('status', $catalogPublication->get('status'));
                    $currentCatalogPublication->set('remarks', $catalogPublication->get('remarks'));
                    if ($createdBy !== null) {
                        $currentCatalogPublication->set('createdBy', $createdBy);
                    }
                    if ($modifiedBy !== null) {
                        $currentCatalogPublication->set('modifiedBy', $modifiedBy);
                    }
                    $catalogPublications[$i - 1] = $currentCatalogPublication;
                    $this->em->persist($catalogPublications[$i - 1]);
                } else {
                    if ($createdBy == null) {
                        $catalogPublication->set('createdBy', CatalogPublication::DEFAULT_CREATED_BY);
                    }
                    if ($modifiedBy == null) {
                        $catalogPublication->set('modifiedBy', CatalogPublication::DEFAULT_MODIFIED_BY);
                    }
                    $this->em->persist($catalogPublication);
                }

                //if (($i % $batchSize) === 0) {
                    //$this->em->flush();
                    //$this->em->clear(); // Detaches all objects from Doctrine!
                //}
            }
            $this->em->flush(); //Persist objects that did not make up an entire batch
            $this->em->clear(); // Detaches all objects from Doctrine!
            $connection->commit();
        } catch (DBALException $ex) {
            $connection->rollBack();
            throw new BadRequestHttpException('entity.create.database.error');
        }
        return $catalogPublications;
    }

    protected function validate(CatalogPublication $catalogPublication)
    {
        $errors = $this->validator->validate($catalogPublication);
        if ($errors->count() > 0) {
            throw new BadRequestHttpException($this->serializer->serialize($errors, 'json'));
        }
        return true;
    }

    public function unAssignCatalogPublication($publicationId, $catalogId, $request)
    {
        $catalog = $this->checkExistCatalog($catalogId);
        $publication = $this->checkExistPublication($publicationId);
        $catalogPublication = $this->checkExistOneByCond(
            array(
                "catalog" => $catalog,
                "publication" => $publication
            )
        );
        //request params
        $params = $request->request->all();
        $modifiedBy = isset($params['modified_by']) ? $params['modified_by'] : null;
        $remarks = isset($params['remarks']) ? $params['remarks'] : null;
        if ($modifiedBy != null) {
            $catalogPublication->set('modifiedBy', $modifiedBy);
        }
        if ($remarks != null) {
            $catalogPublication->set('remarks', $remarks);
        }
        $catalogPublication->set('status', CatalogPublication::STATUS_DISABLED);
        //validate entity
        $this->validate($catalogPublication);
        //update
        $this->catalogPublicationRepo->update($catalogPublication);
        return $catalogPublication;
    }
}