<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\APIBundle\Handler;

use Symfony\Component\HttpKernel\Exception as HTTPException;
use AM\CatalogService\Domain\CountryRestriction\CountryRestriction;
use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Category\Category;
use AM\CatalogService\Domain\PublicationCategory\PublicationCategory;

class CategoryHandler
{
    private $validator;
    private $jmsSerializer;
    private $em;
    private $categoryRepo;
    private $publicationRepo;
    private $publicationCategoryRepo;

    /**
     * CategoryHandler constructor.
     * @param $jmsSerializer
     * @param $validator
     * @param $em
     * @param $categoryRepo
     * @param $publicationRepo
     * @param $publicationCategoryRepo
     */
    public function __construct($jmsSerializer, $validator, $em, $categoryRepo, $publicationRepo, $publicationCategoryRepo)
    {
        $this->jmsSerializer = $jmsSerializer;
        $this->validator = $validator;
        $this->em = $em;
        $this->categoryRepo = $categoryRepo;
        $this->publicationRepo = $publicationRepo;
        $this->publicationCategoryRepo = $publicationCategoryRepo;
    }

    /**
     * @param $categoryId
     * @param $data
     * @return array
     */
    public function postMultiplePublicationToCategory($categoryId, $data)
    {
        $insertData = array();
        $publicationIdList = array();
        $result = array();
        
        //check array json
        if (!isset($data[0])) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.category.publication_id.is_required'
            );
        }

        $category = $this->categoryRepo->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.not_found'
            );
        }
        
        foreach ($data as $key => $value) {
            if (!isset($value['publication_id'])) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.category.publication_id.is_required'
                );
            }

            $publicationId = $value['publication_id'];
            $publicationIdList[] = $publicationId;

            $publication = $this->publicationRepo->find($publicationId);
            if (!$publication instanceof Publication) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }
            
            $publicationCategory = $this->publicationCategoryRepo->findOneBy(
                [
                    'categoryId' => $categoryId,
                    'publicationId' => $publicationId
                ]
            );

            if (!empty($publicationCategory)) {
                $result[] = $publicationCategory;
                continue;
            }
            
            //insert
            $publicationCategory = new PublicationCategory();
            $publicationCategory->setCategory($category);
            $publicationCategory->setPublication($publication);
            $publicationCategory->setPublicationId($publicationId);
            $publicationCategory->setCategoryId($categoryId);

            $hierarchical = $this->categoryRepo->getHierarchical();
            $rootParentCategoryId = $this->categoryRepo->getRootParentCategoryId(
                $hierarchical, $categoryId
            );
            $publicationCategory->setRootParentCategoryId($rootParentCategoryId);

            $errors = $this->validator->validate($publicationCategory);
            if ($errors->count() > 0) {
                throw new HTTPException\BadRequestHttpException(
                    $this->jmsSerializer->serialize($errors, 'json')
                );
            }

            $insertData[] = $publicationCategory;
            $result[] = $publicationCategory;

            //assign => parent catalog should follow
            $parentCategory = $this->assignParentCategory($categoryId, $publication);
            if (!empty($parentCategory)) {
                $insertData = array_merge($insertData, $parentCategory);
            }
        }

        if (count(array_unique($publicationIdList)) < count($publicationIdList)) {
            // Array has duplicates
            throw new HTTPException\BadRequestHttpException(
                'catalog.category.duplicates_publication_id'
            );
        }

        if (!empty($insertData)) {
            $this->publicationCategoryRepo->insertMultiple($insertData);
        }

        $result = $this->publicationCategoryRepo->getFinalResultByJMSGroup($result, 'view');

        return $result;
    }

    public function assignParentCategory($categoryId, $publication)
    {
        $data = array();
        $parent = $this->categoryRepo->getListParent($categoryId);

        foreach ($parent as $item) {
            $category = $this->categoryRepo->find($item);
            if (!$category instanceof Category) {
                continue;
                //log plz
            }

            $publicationCategory = $this->publicationCategoryRepo->findOneBy(
                array(
                    'categoryId' => $item,
                    'publicationId' => (int)$publication->get('id')
                )
            );

            if (!empty($publicationCategory)) {
                continue;
            }
            
            //insert
            $publicationCategory = new PublicationCategory();
            $publicationCategory->setCategory($category);
            $publicationCategory->setPublication($publication);
            $hierarchical = $this->categoryRepo->getHierarchical();
            $rootParentCategoryId = $this->categoryRepo->getRootParentCategoryId(
                $hierarchical, $categoryId
            );
            $publicationCategory->setRootParentCategoryId($rootParentCategoryId);

            $data[] = $publicationCategory;
        }

        return $data;
    }

    public function deleteMultiplePublicationToCategory($categoryId, $data)
    {
        $deleteData = array();

        //check array json
        if (!isset($data[0])) {
            throw new HTTPException\BadRequestHttpException(
                'catalog.category.publication_id.is_required'
            );
        }

        $category = $this->categoryRepo->find($categoryId);
        if (!$category instanceof Category) {
            throw new HTTPException\NotFoundHttpException(
                'catalog.category.category_id.not_found'
            );
        }

        foreach ($data as $key => $value) {
            if (!isset($value['publication_id'])) {
                throw new HTTPException\BadRequestHttpException(
                    'catalog.category.publication_id.is_required'
                );
            }

            $publicationId = $value['publication_id'];

            $publication = $this->publicationRepo->find($publicationId);
            if (!$publication instanceof Publication) {
                throw new HTTPException\NotFoundHttpException(
                    'catalog.publication.not_found'
                );
            }

            $publicationCategory = $this->publicationCategoryRepo->findOneBy(
                [
                    'categoryId' => $categoryId,
                    'publicationId' => $publicationId
                ]
            );

            if (!empty($publicationCategory)) {
                $deleteData[] = $publicationCategory;
            }

            //un-assign => children catalog should follow
            $childCategory = $this->unAssignChildCategory($categoryId, $publicationId);
            if (!empty($childCategory)) {
                $deleteData = array_merge($deleteData, $childCategory);
            }
        }

        if (!empty($deleteData)) {
            $this->publicationCategoryRepo->deleteMultiple($deleteData);
        }
    }

    public function unAssignChildCategory($categoryId, $publicationId)
    {
        $data = array();
        $children = $this->categoryRepo->getListChild($categoryId);
        foreach ($children as $item) {
            $publicationCategory = $this->publicationCategoryRepo->findOneBy(
                [
                    'categoryId' => $item,
                    'publicationId' => $publicationId
                ]
            );

            if (!empty($publicationCategory)) {
                $data[] = $publicationCategory;
            }
        }
        return $data;
    }
}