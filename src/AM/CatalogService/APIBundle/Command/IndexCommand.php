<?php

namespace AM\CatalogService\APIBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zenith\CoreService\RestClient\RestClient;

use AM\CatalogService\Domain\Publication\Publication;

class IndexCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName ( 'elastic:index' )->setDescription ( 'Index list objects' )->addArgument ( 'name', InputArgument::OPTIONAL )->addArgument ( 'page', InputArgument::OPTIONAL )->addArgument ( 'limit', InputArgument::OPTIONAL );
    }
    public function indexData($url, $method, $params = array()) {
        $restClient = new RestClient ();
        $options = array (
                'content-type' => 'application/json',
                'body' => json_encode ( $params )
        );
        $response = $restClient->run ( $method, $url, $options );

        $httpStatus = $response->getStatusCode ();

        if ($httpStatus != 200 && $httpStatus != 201) {

            return false;
        }

        $response = json_decode ( $response->getBody (), true );
        return $response;
    }
    public function index($data, $output, $url) {
        $output->writeln ( "Object Total: " . count ( $data ) . "\n" );
        foreach ( $data as $item ) {
            $result = $this->indexData ( $url, 'post', $item );
            if ($result == false) {
                $output->writeln ( "Indexing object_id: " . $item ['id'] . " was failed\n" );
            } else {
                $output->writeln ( "Indexed object_id: " . $item ['id'] . "\n" );
            }
        }
    }
    public function getData($url, $params = array()) {
        $restClient = new RestClient ();
        $options = array (
                'content-type' => 'application/json',
                'query' => $params
        );
        $response = $restClient->run ( 'get', $url, $options );

        $httpStatus = $response->getStatusCode ();

        if ($httpStatus != 200 && $httpStatus != 201) {

            return false;
        }

        $response = json_decode ( $response->getBody (), true );
        if (! isset ( $response ['data'] )) {
            return array ();
        }
        return $response ['data'];
    }
    protected function execute(InputInterface $input, OutputInterface $output) {
        $name = $input->getArgument ( 'name' );
        $page = $input->getArgument ( 'page' );
        $limit = $input->getArgument ( 'limit' );
        if (empty ( $name )) {
            $output->writeln ( "Please choose input type to index\n" );
            exit ();
        }

        if (empty ( $page )) {
            $page = 1;
        }

        if (empty ( $limit )) {
            $limit = 100;
        }

        $container = $this->getContainer ();

        $output->writeln ( "Begin index.....\n" );

        $type = $name;
        $data = array ();
        $offset = ($page - 1) * $limit;
        $params = array (
                'offset' => $offset,
                'limit' => $limit,
                'page' => $page
        );
        switch ($type) {
            case 'issue' :
                $issueRepo = $container->get ( 'catalog.issue.repository' );

                $issues = $issueRepo->getListByQueryParams ( $params );
                $data = $issueRepo->getFinalResultByJMSGroup ( $issues ['data'], 'list,list_search' );
                $url = $container->getParameter ( 'url_search_issue_create' );

                $this->index ( $data, $output, $url );

                break;
            case 'publication' :
                $publicationRepo = $container->get('catalog.publication.repository');
                /* old code */
                $publications = $publicationRepo->getListByQueryParams($params);
                $data = $publicationRepo->getFinalResultByJMSGroup($publications['data'], 'list');
                $url = $container->getParameter ('url_search_publication_create');
                $this->index($data, $output, $url);
                break;
                /* new code */
                $publications = $publicationRepo->getListByQueryParams(
                    $params, false, Publication::getResponseGroups('list')
                );

                $publicationRepo->appendItems(
                    $publications['data'],
                    [
                        ['publisher', 'publication_list'],
                        ['latest_issue', 'list']
                    ]
                );
                $data = $publications['data'];

                if (!empty($data)) {
                    foreach ($data as & $publication) {
                        if (!is_object($publication['latest_issue'])) {
                            static::convertDateTimeObjectToStr(['cover_date', 'publish_date', 'created_at', 'modified_at'], $publication['latest_issue']);
                        }

                        static::convertDateTimeObjectToStr(['created_at', 'modified_at'], $publication);
                    }
                }

                $url = $container->getParameter ( 'url_search_publication_create' );
                $this->index($data, $output, $url);
                break;
            case 'catalog' :
                $catalogRepo = $container->get ( 'catalog.catalog.repository' );
                $catalogs = $catalogRepo->findCatalogList ( $params );
                $data = $catalogRepo->getFinalResultByJMSGroup ( $catalogs ['data'], 'list' );
                $url = $container->getParameter ( 'url_search_catalog_create' );
                $this->index ( $data, $output, $url );
                break;
            case 'project' :
                $urlData = $container->getParameter ( 'url_data_project' );
                $data = $this->getData ( $urlData, $params );
                $url = $container->getParameter ( 'url_search_project_create' );
                $this->index ( $data, $output, $url );
                break;
            case 'newsstand' :
                $urlData = $container->getParameter ( 'url_data_newsstand' );
                $data = $this->getData ( $urlData, $params );
                $url = $container->getParameter ( 'url_search_newsstand_create' );
                $this->index ( $data, $output, $url );
                break;
            case 'publisher' :
                $publisherRepo = $container->get ( 'catalog.publisher.repository' );
                $publishers = $publisherRepo->getListByQueryParams ( $params );
                $data = $publisherRepo->getFinalResultByJMSGroup ( $publishers ['data'], 'list' );
                $url = $container->getParameter ( 'url_search_publisher_create' );
                $this->index ( $data, $output, $url );
                break;
            case 'category' :
                $urlData = $container->getParameter ( 'url_data_category' );
                $data = $this->getData ( $urlData, $params );
                $url = $container->getParameter ( 'url_search_category_create' );
                $this->index ( $data, $output, $url );
                break;
            default :
                $output->writeln ( "Type is not found.....\n" );
                exit ();
                break;
        }

        $output->writeln ( "Finished index.....\n" );
    }

    /**
     * Convert mysql datetime to php datetime object in UTC format
     *
     * @param  array  $properties List of properties will be converted
     * @param  array  $data       Data will be converted
     *
     * @return void
     */
    public static function convertDateTimeObjectToStr(array $properties, array & $data = [])
    {
        if (empty($properties)) {
            return;
        }

        foreach ($properties as $property) {
            if (isset($data[$property]) && $data[$property] instanceof \DateTime) {
                $data[$property] = $data[$property]->format(\DateTime::ISO8601);;
            }
        }
    }
}