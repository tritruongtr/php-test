<?php
namespace AM\CatalogService\CommandBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Issue\Issue;

class SyncCommand extends ContainerAwareCommand
{
	protected $output;
	protected function configure()
    {
        $this
            ->setName('catalog:sync')
            ->setDescription('Sync content from Console and WSA')
			->addArgument(
                'source',
                InputArgument::REQUIRED,
                'Which source you want to sync? i.e: console, wsa'
            )
        ;
    }

	protected function execute(InputInterface $input, OutputInterface $output)
    {
		$source = $input->getArgument('source');
		$this->output = $output;

		if ($source == 'console') {
			$this->syncConsole();
		} else {
			$this->syncWSA();
		}
	}

	protected function syncConsole()
	{
		$container = $this->getContainer();
		$em = $container->get('doctrine')->getManager();

		//Sync Publication Legacy Site ID
		$this->output->writeln("Start sync publications from console");
        $publications = $container->get('catalog.publication.repository')
							      ->findAll();

		foreach ($publications as $publication) {
			$this->output->writeln("Sync publication ".$publication->get('remoteIdentifier'));
	        $pub = $container->get('catalog.legacy.repository')
	                         ->getPublication($publication->get('remoteIdentifier'));

			if (isset($pub['site_id'])) {
				$publication->set('site_id', (int) $pub['site_id']);
		        $em->persist($publication);
		        $em->flush();

				//Sync AMConsoleIssue
				$this->output->writeln("Start sync issues from console");
				$issues = $container->get('catalog.legacy.repository')
									->setUp((int)$pub['site_id'])
									->getIssues();
				if ($issues) {
					$date = new \DateTime('now', new \DateTimeZone('UTC'));
					foreach ($issues as $issue) {
				        $newIssue = $container->get('catalog.issue.repository')
											   ->findOneBy(array('legacyIssueId' => $issue['default_issue'], 'publicationId' => $publication->get('id')));
						if (!$newIssue instanceof Issue) {
                            $this->output->writeln("Add new issue: ".$issue['issue_name']);
							$newIssue = new Issue(array(
								'name' => $issue['issue_name'],
								'internalName' => $issue['issue_name'],
								'coverDate' => $date->setTimestamp($issue['cover_date']),
								'publishDate' => $date->setTimestamp($issue['published_date']),
								'status' => Issue::STATUS_PUBLISHED,
								'type' => ($issue['is_special']) ? Issue::TYPE_SPECIAL : Issue::TYPE_STANDARD,
								'description' => $issue['detail'],
								'coverImage' => $issue['cover_image']['portrait'],
								'remoteIdentifier' => $issue['default_issue'],
								'legacyIssueId' => $issue['default_issue'],
								'createdBy' => 14,
								'modifiedBy' => 14,
								'preview' => (isset($issue['has_previews'])) ? (int) $issue['has_previews'] : 0,
								'file_path' => 'none'
							));
					        $newIssue->set('publication', $publication);
					        $em->persist($newIssue);
					        $em->flush();
						} else {
                            $this->output->writeln("Update issue: ".$newIssue->get('id'));
						    $newIssue->set('remoteIdentifier', $issue['default_issue']);
                            $newIssue->set('legacyIssueId', $issue['default_issue']);
					        $em->persist($newIssue);
					        $em->flush();
						}
					}
				}
			}
        }
	}

	protected function syncWSA()
	{

	}
}
?>