<?php
namespace AM\CatalogService\CommandBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Intl\Intl;

use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\Publisher\Publisher;
use AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata;
use AM\CatalogService\Domain\PublicationCategory\PublicationCategory;
use AM\CatalogService\Domain\Category\Category;
use AM\CatalogService\Domain\CatalogPublication\CatalogPublication;
use AM\CatalogService\Domain\Catalog\Catalog;

class ImportCommand extends ContainerAwareCommand
{
	protected $limit = 200;
	
	protected function configure()
    {
        $this
            ->setName('catalog:import')
            ->setDescription('Import entity from CSV file')
			->addArgument(
                'entity',
                InputArgument::REQUIRED,
                'Which entity you want to import? i.e: publication, issue'
            )
            ->addOption(
                'src',
				null,
                InputOption::VALUE_REQUIRED,
                'Please enter location of the CSV file.'
            )
        ;
    }
	
	protected function execute(InputInterface $input, OutputInterface $output)
    {
        $csvFile = $input->getOption('src');
		$pathInfo = pathinfo($csvFile);
		if ($pathInfo['extension'] != 'csv') {
			$output->writeln("Import failed. $csvFile is not CSV.", 'ERROR');
			return 1;
		} else {
			if (( $handle = fopen($csvFile, "r")) !== FALSE ) {
				
				$entity = $input->getArgument('entity');
			
				switch ($entity) {
				    case 'publication':
				        $this->importPublication($handle, $output);
				        break;
				    case 'all':
						$this->importAll($handle, $output);
				}
				
			    fclose($handle);
				
				return 0;
			} else {
				$output->writeln("Import failed. $csvFile is not readable.", 'ERROR');
				return 1;
			}
		}
    }
	
	protected function importPublication($csvFile, OutputInterface $output)
	{
		$output->writeln("Updating publication from file ".$csvFile);
		$row = 1;
		$totalPublication = 0;
		$container = $this->getContainer();
		$em = $container->get('doctrine')->getManager();
		$countries = Intl::getRegionBundle()->getCountryNames();
		
	    while (($data = fgetcsv($csvFile, 0, ",")) !== FALSE ) {
			
			//$output->writeln("Import index: $row");
	        if ( $row == 1 )
	        {
	            $row++;
	            continue;
	        }
			$output->writeln("Update Publication: $data[0]");
			if (trim($data[0]) == null) {
				$output->writeln("Updating publication failed. $data[1] is invalid.", 'ERROR');
				continue;
			}
			$countryCode = (array_search(trim($data[4]), $countries)) ? array_search(trim($data[4]), $countries) : "US";
			
			//Update publication
	        $publication = $container->get('catalog.publication.repository')
								     ->findOneBy(array('remoteIdentifier' => trim($data[0])));
			
			if ($publication instanceof Publication) {
				/*
				$publication->set('name' , trim($data[2]));
				$publication->set('internalName' , trim($data[1]));
				$publication->set('countryCode' , $countryCode);
				$publication->set('languageCode' , 'eng'); //TODO: set language
				$publication->set('localeCode' , 'eng-US'); //TODO: set locale
				$publication->set('contentRating' , 15) ;
				$publication->set('createdBy' , 14);
				$publication->set('modifiedBy' , 14);
				$publication->set('description' , trim($data[3]));
				$publication->set('logo' , trim($data[8]));
				
		        $em->persist($publication);
		        $em->flush();*/
				$publicationId = $publication->get('id');
				
				//Assign Category
		        $categoryRepo = $container->get('catalog.category.repository');
		        $publCateRepo = $container->get('catalog.publication_category.repository');
				$categoryId = $data[9];

		        $category = $categoryRepo->find($categoryId);
				//die(print_r($category));
		        if (empty($category)) {
		            $output->writeln("Category $categoryId is invalid.", 'ERROR');
		        } else {
			        $publicationCategory = $publCateRepo->findOneBy([
			            'categoryId' => $categoryId,
			            'publicationId' => $publicationId
			        ]);
					
			        if (empty($publicationCategory)) {
			            $publicationCategory = new PublicationCategory();
			            $publicationCategory->setCategory($category);
			            $publicationCategory->setPublication($publication);

			            $hierarchical = $categoryRepo->getHierarchical();
			            $rootParentCategoryId = $categoryRepo->getRootParentCategoryId(
			                $hierarchical, $categoryId
			            );
			            $publicationCategory->setRootParentCategoryId($rootParentCategoryId);

			            $em->persist($publicationCategory);
			            $em->flush();
			        }
		        }
				
				//Assign Catalog
		        $catalogId = $data[10];
		        $params['status'] = 1;
		        $params['modified_by'] = 14;
				
		        $catalog = $container->get('catalog.catalog.repository')->find($catalogId);
		        if (!$catalog instanceof Catalog) {
		             $output->writeln("Catalog $catalogId is invalid.", 'ERROR');
		        } else {
			        $catalogPublication = $container->get('catalog.catalog_publication.repository')->findOneBy([
			            'catalogId' => $catalogId,
			            'publicationId' => $publicationId
			        ]);
						
			        if (!$catalogPublication instanceof CatalogPublication) {
				        $catalogPublication = new CatalogPublication(array(
				        	'publication_id' => $publicationId,
							'catalog_id' => $catalogId,
							'status' => 1,
							'modified_by' => 14,
							'created_by' => 14
				        ));
			            $catalogPublication->setCatalog($catalog);
			            $catalogPublication->setPublication($publication);
			            $em->persist($catalogPublication);
			            $em->flush();
			        }
		        }
			}

	        $row++;
	    }
		$output->writeln("Update success.", 'SUCCESS');
	}
	
	protected function importAll($csvFile, OutputInterface $output)
	{
		$output->writeln("Importing publication from file ".$csvFile);
		$row = 1;
		$totalPublisher = 0;
		$totalPublication = 0;
		$container = $this->getContainer();
		$em = $container->get('doctrine')->getManager();
		$countries = Intl::getRegionBundle()->getCountryNames();
		
	    while (($data = fgetcsv($csvFile, 0, ",")) !== FALSE ) {
			
			$output->writeln("Import index: $row");
	        if ( $row == 1 )
	        {
	            $row++;
	            continue;
	        }
			
			if (trim($data[1]) == null) {
				$output->writeln("Import publisher failed. $data[3] is invalid.", 'ERROR');
				continue;
			}
			$countryCode = (array_search(trim($data[4]), $countries)) ? array_search(trim($data[4]), $countries) : "US";
			
			//Import Publisher
	        $publisher = $container->get('catalog.publisher.repository')
								   ->findOneBy(array('remoteIdentifier' => trim($data[3])));
			
	        if (!$publisher instanceof Publisher) {
				$publisher = new Publisher(array(
					'name' => trim($data[1]),
					'internalName' => trim($data[1]),
					'remoteIdentifier' => trim($data[3]),
					'countryCode' => $countryCode,
					'status' => 1
				));
				
				$errors = $container->get('validator')->validate($publisher);
				if ($errors->count() > 0) {
					$output->writeln("Import publisher failed. $data[3] is invalid.", 'ERROR');
					continue;
				}
		        $em->persist($publisher);
		        $em->flush();
				$totalPublisher++;
			}
			
			//Import Publisher Metadata
	        $publisherCurrency = $container->get('catalog.publisher_metadata.repository')
								   ->findOneByPublisherIdKey($publisher->get('id'), 'currency');
			
			if (!$publisherCurrency instanceof PublisherMetadata) {
				$publisherCurrency = new PublisherMetadata(array(
					'publisherId' => $publisher->get('id'),
					'keyName' => 'currency',
					'keyValue' => trim($data[10])
				));
				
				$errors = $container->get('validator')->validate($publisherCurrency);
				if ($errors->count() > 0) {
					$output->writeln("Import publisher currency failed. $data[10] is invalid.", 'WARNING');
				} else {
			        $keyValue = serialize([$publisherCurrency->getKeyValue(false)]);
			        $publisherCurrency->set('key_value', $keyValue);
			        $publisherCurrency->set('publisher', $publisher);
					
			        $em->persist($publisherCurrency);
			        $em->flush();
				}
			}
			
			//Import publication
	        $publication = $container->get('catalog.publication.repository')
								     ->findOneBy(array('remoteIdentifier' => trim($data[2])));
			
			if (!$publication instanceof Publication) {
				$publication = new Publication(array(
					'publisherId' => $publisher->get('id'),
					'name' => trim($data[5]),
					'frequency' => trim($data[6]),
					'internalName' => trim($data[0]),
					'remoteIdentifier' => trim($data[2]),
					'countryCode' => $countryCode,
					'languageCode' => 'eng', //TODO: set language
					'localeCode' => 'eng-US', //TODO: set locale
					'type' => (trim($data[9])=='Book') ? 2 : 1,
					'noOfIssues' => (trim($data[7])) ? trim($data[7]) : 999,
					'contentRating' => 15, 
					'createdBy' => 14,
					'modifiedBy' => 14,
					'siteId' => 1,
					'legacyIdentifier' => 1,
					'status' => 1
				));
				
				$errors = $container->get('validator')->validate($publication);
				if ($errors->count() > 0) {
					$output->writeln("Import publication failed. $data[2] is invalid.", 'ERROR');
					continue;
				}
				$publication->setPublisher($publisher);
		        $em->persist($publication);
		        $em->flush();
				$totalPublication++;
			}

	        $row++;
	    }
		$output->writeln("Import success with $totalPublisher publishers and $totalPublication publication", 'SUCCESS');
	}
}
?>