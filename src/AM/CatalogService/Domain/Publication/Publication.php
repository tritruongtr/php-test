<?php
namespace AM\CatalogService\Domain\Publication;

use AM\CatalogService\Domain\Publisher\Publisher;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Zenith\CoreService\ContentRating\ContentRating;
use Zenith\CoreService\Intl\Country;
use Zenith\CoreService\Intl\Language;
use Zenith\CoreService\Intl\Locale;
use Doctrine\Common\Collections\Criteria;

/**
 * Publication
 *
 * @author Can Ly <can.ly@audiencemedia.com>
 *
 * @ORM\Table(name="publications", options={"engine"="InnoDB"})
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublicationRepository")
 */
class Publication
{
    const PUBLICATION_TYPE_MAGAZINE = 1;
    const PUBLICATION_TYPE_NEWSPAPER = 2;
    const PUBLICATION_TYPE_BOOK = 3;
    const PUBLICATION_TYPE_TEXTBOOK = 4;
    const PUBLICATION_TYPE_JOURNAL = 5;

    private static $responseGroups = [
        'view' => ['id','name','frequency','legacyIdentifier','internalName','description','slug','code','countryCode','languageCode','localeCode','publisherId','publisher','contentRating','remoteIdentifier','createdAt','modifiedAt','createdBy','modifiedBy','siteId','status','type','noOfIssues','logo','allowXml','allowPdf','latestIssue','countryAvailability','categories','issues','publicationsMetadata', 'latinName', 'tagline', 'parentId', 'seoKeywords', 'issn', 'circulationType', 'binding', 'watermark', 'allowPrinting', 'allowIntegratedFulfilment', 'fulfilmentHouseId', 'fulfilmentCode'],
        'list' => ['id','name','frequency','legacyIdentifier','internalName','description','slug','code','countryCode','languageCode','localeCode','publisherId','publisher','contentRating','remoteIdentifier','createdAt','modifiedAt','createdBy','modifiedBy','siteId','status','type','noOfIssues','logo','allowXml','allowPdf','latestIssue', 'latinName', 'tagline', 'parentId', 'seoKeywords', 'issn', 'circulationType', 'binding', 'watermark', 'allowPrinting', 'allowIntegratedFulfilment', 'fulfilmentHouseId', 'fulfilmentCode'],
        'database' => ['id','name','frequency','legacyIdentifier','internalName','description','countryCode','languageCode','localeCode','publisherId','publisher','contentRating','remoteIdentifier','createdAt','modifiedAt','createdBy','modifiedBy','siteId','status','type','noOfIssues','logo','allowXml','allowPdf', 'latinName', 'tagline', 'parentId', 'seoKeywords', 'issn', 'circulationType', 'binding', 'watermark', 'allowPrinting', 'allowIntegratedFulfilment', 'fulfilmentHouseId', 'fulfilmentCode'],
        'search' => ['id','name','publisher','remoteIdentifier','legacyIdentifier','siteId','allowXml','allowPdf'],
        'ignore_limit' => ['id','name','remoteIdentifier','siteId','legacyIdentifier'],
        'publication' => ['slug','code'],
        'Default' => ['publicationsCategories']
    ];

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database", "search", "ignore_limit"}
     * )
     * @Type("integer")
     * @ORM\Column(
     *     name = "id",
     *     type = "bigint",
     *     options={"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database", "search", "ignore_limit"}
     * )
     *
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 255,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.name.not_blank")
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.publication.name.min_message",
     *     maxMessage = "catalog.publication.name.max_message"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "frequency",
     *     type = "string",
     *     length = 100,
     *     nullable = true,
     *     options = {"default":"unknown"}
     * )
     *
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.publication.frequency.max_message"
     * )
     */
    private $frequency = 'unknown';

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "legacy_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.publication.legacy_identifier.max_message"
     * )
     */
    private $legacyIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "internal_name",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.publication.internal_name.max_message"
     * )
     */
    private $internalName;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "description",
     *     type = "text",
     *     nullable = true
     * )
     */
    private $description;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.publication.slug.length",
     *     maxMessage = "catalog.publication.slug.length"
     * )
     * @Groups(
     *     {"view", "list", "publication"}
     * )
     */
    private $slug;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.publication.code.length"
     * )
     * @Groups(
     *     {"view", "list", "publication"}
     * )
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "country_code",
     *     type = "string",
     *     length = 2,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.country_code.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[A-Z]{2}$/",
     *     htmlPattern = "^[A-Z]{2}$",
     *     message = "catalog.publication.country_code.format"
     * )
     * @Type("array")
     * @Accessor(getter="getCountry")
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @SerializedName("country")
     *
     */
    private $countryCode;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "language_code",
     *     type = "string",
     *     length = 2,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.language.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[a-z]{2}$/",
     *     htmlPattern = "^[a-z]{2}",
     *     message = "catalog.publication.language.format"
     * )
     *
     * @Type("array")
     * @Accessor(getter="getLanguage")
     * @SerializedName("language")
     */
    private $languageCode;

    /**
     * @var string
     *
     * @Groups(
     *     {"list", "view", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "locale_code",
     *     type = "string",
     *     length = 10,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.locale.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z]{1}[a-zA-Z-_]{0,9}$/",
     *     htmlPattern = "^[a-zA-Z]{1}[a-zA-Z-_]{0,9}$",
     *     message = "catalog.publication.locale.format"
     * )
     *
     * @Type("array")
     * @Accessor(getter="getLocale")
     * @SerializedName("locale")
     */
    private $localeCode;

    /**
     * @var integer
     *
     * @Groups(
     *     {"list", "view", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "publisher_id",
     *     type = "bigint",
     *     nullable = false,
     *     options={"unsigned":true}
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.publisher_id.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication.publisher_id.format"
     * )
     *
     * @Type("integer")
     */
    private $publisherId;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publisher\Publisher",
     *     inversedBy = "publications"
     * )
     *
     * @ORM\JoinColumn(
     *     name = "publisher_id",
     *     referencedColumnName = "id"
     * )
     *
     * @Accessor(
     *     getter = "getPublisher"
     * )
     *
     * @Groups(
     *     {"list", "view", "search", "database"}
     * )
     *
     */
    private $publisher;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "content_rating",
     *     type = "smallint",
     *     nullable = false,
     *     options={"unsigned":true}
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.content_rating.not_blank")
     * @Assert\Regex(
     *     pattern = "/^(1|10|15|20|30|40|50)$/",
     *     htmlPattern = "/^(1|10|15|20|30|40|50)$/",
     *     message = "catalog.publication.content_rating.format"
     * )
     *
     * @Type("integer")
     */
    private $contentRating;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database", "ignore_limit", "search"}
     * )
     *
     * @ORM\Column(
     *     name = "remote_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true,
     *     options={"default" : ""}
     * )
     *
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.publication.remote_identifier.max_message"
     * )
     *
     */
    private $remoteIdentifier = '';

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $createdAt;

    /**
     * @var \DateTime $modifiedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "created_by",
     *     type = "bigint",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.created_by.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication.created_by.format"
     * )
     *
     * @Type("integer")
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "modified_by",
     *     type = "bigint",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.modified_by.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication.modified_by.format"
     * )
     *
     * @Type("integer")
     */
    private $modifiedBy;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database", "search", "ignore_limit"}
     * )
     *
     * @ORM\Column(
     *     name = "site_id",
     *     type = "integer",
     *     nullable = true,
     *     options={"default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.site_id.format"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.site_id.format"
     * )
     *
     * @Type("integer")
     */
    private $siteId = 0;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "smallint",
     *     nullable = false,
     *    options={"unsigned":true}
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.status.not_blank")
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.publication.status.not_blank"
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @Type("integer")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "type",
     *     type = "smallint",
     *     nullable = false,
     *     options={"unsigned":true,"default":1}
     * )
     *
     * @Assert\NotBlank(message="catalog.publication.type.not_blank")
     *
     * @Assert\Regex(
     *     pattern = "/^(1|2|3|4|5)$/",
     *     htmlPattern = "^(1|2|3|4|5)$",
     *     message = "catalog.publication.type.not_blank"
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @Type("integer")
     */
    private $type = 1;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "no_of_issues",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.no_of_issues.format"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.no_of_issues.format"
     * )
     *
     * @Type("integer")
     */
    private $noOfIssues = 0;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "logo",
     *     type = "text",
     *     nullable = true
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $logo;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "allow_xml",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":1}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.allow_xml.format"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "catalog.publication.allow_xml.format"
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     */
    private $allowXml = 1;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "allow_pdf",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":1}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.allow_pdf.format"
     * )
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "catalog.publication.allow_pdf.format"
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     */
    private $allowPdf = 1;

    public function setPublisher(Publisher $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    public function getPublisher($groupName = 'list')
    {
        $serializer = SerializerBuilder::create()->build();
        $publisherJson = $serializer->serialize(
            $this->publisher,
            'json',
            SerializationContext::create()->setGroups(
                [$groupName]
            )
        );

        $publisherArr = json_decode($publisherJson, true);

        $this->publisher = $publisherArr;
        return $this->publisher;
    }

    /**
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @Accessor(
     *     getter = "getLatestIssue"
     * )
     * @SerializedName("latest_issue")
     *
     */
    private $latestIssue = null;

    /**
     * @var array
     *
     * @Accessor(
     *     getter = "getPlatformsAvailability"
     * )
     * @SerializedName("platforms_availability")
     * @Type("array")
     *
     */
    private $platformsAvailability;

    /**
     * @var array
     *
     * @Groups(
     *     {"view"}
     * )
     *
     * @Accessor(
     *     getter = "getCountryAvailability"
     * )
     * @SerializedName("country_availability")
     * @Type("array")
     *
     */
    private $countryAvailability;

    /**
     * @var array
     *
     * @Groups(
     *     {"view"}
     * )
     *
     * @Accessor(
     *     getter = "getCategories"
     * )
     * @SerializedName("categories")
     * @Type("array")
     *
     */
    private $categories;

    /**
     * @var array
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\Issue\Issue",
     *     mappedBy = "publication"
     * )
     * @ORM\OrderBy({"coverDate" = "ASC"})
     * @Accessor(
     *     getter = "getIssues"
     * )
     *
     * @Type("array")
     * @Groups({"view"})
     **/
    private $issues;

    /**
     * @Groups(
     *     {"Default"}
     * )
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\PublicationCategory\PublicationCategory",
     *     mappedBy = "publication"
     * )
     **/
    private $publicationsCategories;

    /**
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\CatalogPublication\CatalogPublication",
     *     mappedBy = "publication"
     * )
     **/
    private $catalogsPublications;

    /**
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\PublicationMetadata\PublicationMetadata",
     *     mappedBy = "publication"
     * )
     *
     * @Accessor(
     *     getter = "getMetadata"
     * )
     *
     * @Type("array")
     * @SerializedName("metadata")
     * @Groups({"view"})
     **/
    private $publicationsMetadata;

    /**
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\PublicationCountryModifier\PublicationCountryModifier",
     *     mappedBy = "publication"
     * )
     **/
    private $publicationCountryModifier;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "latin_name",
     *     type = "string",
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "catalog.publication.latin_name.length"
     * )
     *
     */
    private $latinName;

    /**
     * @var string
     *
     * @Groups(
     *    {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "tagline",
     *     type = "string",
     *     length = 2000,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 2000,
     *      maxMessage = "catalog.publication.tagline.length"
     * )
     */
    private $tagline;

    /**
     * @var integer
     *
     * @Groups(
     *    {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "parent_id",
     *     type = "integer",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.parent_id.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.parent_id.not_integer"
     * )
     */
    private $parentId = 0;

    /**
     * @var text
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "seo_keywords",
     *     type = "text",
     *     length = 65535,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.seo_keywords.length"
     * )
     */
    private $seoKeywords;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "issn",
     *     type = "string",
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "catalog.publication.issn.length"
     * )
     *
     */
    private $issn;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "circulation_type",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.circulation_type.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.circulation_type.not_integer"
     * )
     *
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.circulation_type.range"
     * )
     *
     */
    private $circulationType = 0;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "binding",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default" : 0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.binding.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.binding.not_integer"
     * )
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.binding.range"
     * )
     *
     * @Type("integer")
     */

    private $binding = 0;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "watermark",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.watermark.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.watermark.not_integer"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.watermark.range"
     * )
     */
    private $watermark = 0;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "allow_printing",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.allow_printing.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.allow_printing.not_integer"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.allow_printing.range"
     * )
     */
    private $allowPrinting = 0;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "allow_integrated_fulfilment",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.publication.allow_integrated_fulfilment.not_integer"
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.allow_integrated_fulfilment.not_integer"
     * )
     *
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.publication.allow_integrated_fulfilment.range"
     * )
     */
    private $allowIntegratedFulfilment = 0;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "fulfilment_house_id",
     *     type = "string",
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "catalog.publication.fulfilment_house_id.length"
     * )
     *
     */
    private $fulfilmentHouseId;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "fulfilment_code",
     *     type = "string",
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "catalog.publication.fulfilment_code.length"
     * )
     *
     */
    private $fulfilmentCode;

    public function __construct(array $data)
    {
        $this->publicationsCategories = new ArrayCollection();
        $this->catalogs = new ArrayCollection();
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));

        if (is_string($value) && ($realFieldName == 'createdAt' || $realFieldName == 'modifiedAt')) {
            try {
                $value = new \DateTime($value, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new \Symfony\Component\Validator\Exception\InvalidArgumentException (
                    "common.datetime.invalid"
                );
            }
        }

        $this->$realFieldName = $value;
    }

    public function getCategories()
    {
        if (count($this->categories) > 0) {
            return $this->categories;
        }
        $categories = [];
        if (count($this->publicationsCategories) > 0) {
            foreach ($this->publicationsCategories as $pc) {
                $categories[] = $pc->getCategory();
            }
        }
        $this->categories = $categories;
        return $this->categories;
    }

    public function getIssues($groupName = 'list')
    {
        if (empty($this->issues)) {
            $this->issues = [];
            return $this->issues;
        }

        $issues = $this->issues;
        $criteria = Criteria::create()->where(Criteria::expr()->eq("status", 1));
        $issues = $issues->matching($criteria);

        $serializer = SerializerBuilder::create()->build();
        $issuesJson = $serializer->serialize(
            $issues,
            'json',
            SerializationContext::create()->setGroups(
                [$groupName]
            )
        );

        $issuesArr = json_decode($issuesJson, true);

        $this->issues = $issuesArr;
        return $this->issues;
    }

    public function getPublicationCategory()
    {
        return $this->publicationsCategories;
    }

    public function getCountry()
    {
        return Country::instance()->getInfoByCode($this->countryCode);
    }

    public function getLanguage()
    {
        $instance = Language::instance();
        if (method_exists($instance,'getLangInfoByCode')) {
            return $instance->getLangInfoByCode($this->languageCode);
        }
        return null;
    }

    public function getLocale()
    {
        return Locale::instance()->getInfoByCode($this->localeCode);
    }

    public function getContentRating()
    {
        return ContentRating::instance()->getNameByNumber($this->contentRating);
    }

    public function getLatestIssue()
    {
        if (empty($this->issues)) {
            $this->issues = [];
            return $this->issues;
        }

        $issues = $this->issues;
        if (!empty($issues)) {
            $criteria = Criteria::create()->where(Criteria::expr()->eq("status", 1));
            $issues = $issues->matching($criteria);
        }

        $serializer = SerializerBuilder::create()->build();
        $issuesJson = $serializer->serialize(
            $issues,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )->setSerializeNull(true)
        );

        $this->latestIssue = json_decode($issuesJson, true);

        if (is_array($this->latestIssue)) {
            $this->latestIssue = end($this->latestIssue);
        }

        if ($this->latestIssue === false) {
            $this->latestIssue = null;
        }

        return $this->latestIssue;
    }

    public function getCatalogPublication()
    {
        return $this->catalogsPublications;
    }

    public function getPlatformsAvailability()
    {
        $this->platformsAvailability = [];

        return $this->platformsAvailability;
    }

    public function getCountryAvailability()
    {
        $this->countryAvailability = [];

        return $this->countryAvailability;
    }

    public function getMetadata()
    {
        $serializer = SerializerBuilder::create()->build();
        $metadataJson = $serializer->serialize(
            $this->publicationsMetadata,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )
        );

        $metadataArr = json_decode($metadataJson, true);

        $this->publicationsMetadata = $metadataArr;
        return $this->publicationsMetadata;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}
