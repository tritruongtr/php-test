<?php

namespace AM\CatalogService\Domain\IssueMetadata;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;

use AM\CatalogService\Domain\Issue\Issue;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Issue Metadata
 *
 * @author Can Ly <can.ly@audiencemedia.com>
 *
 * @ORM\Table(
 *     name = "issue_metadata",
 *     options = {"engine" = "InnoDB"},
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint(
 *             name = "issue_meta_key",
 *             columns = {"issue_id", "key_name"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTIssueMetadataRepository")
 */
class IssueMetadata
{
    private static $responseGroups = [
        'view' => ['id','keyName','keyValue', 'issueId'],
        'list' => ['id','keyName','keyValue']
    ];

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "bigint",
     *     options= {"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "issue_id",
     *     type = "bigint",
     *     nullable = false,
     *     options= {"unsigned":true}
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.metadata_issue_id.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.issue.metadata_issue_id.integer"
     * )
     *
     * @Groups({"view"})
     */
    private $issueId;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Issue\Issue",
     *     inversedBy = "metadata"
     * )
     *
     * @ORM\JoinColumn(
     *     name = "issue_id",
     *     referencedColumnName = "id",
     *     onDelete = "CASCADE"
     * )
     */
    private $issue;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "key_name",
     *     type = "string",
     *     length = 100,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.metadata_key.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z]{1}[a-zA-Z0-9_]{0,99}$/",
     *     htmlPattern = "^[a-zA-Z]{1}[a-zA-Z0-9_]{0,99}$",
     *     message = "catalog.issue.metadata_key.alpha_numeric"
     * )
     *
     * @Groups({"view", "list"})
     */
    private $keyName;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "key_value",
     *     type = "text",
     *     nullable = false
     * )
     *
     * @Accessor(getter="getKeyValue", setter="setKeyValue")
     *
     * @Groups({"view", "list"})
     */
    private $keyValue;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function getKeyValue($unserialize = true)
    {
        if ($unserialize === false) {
            return $this->keyValue;
        }
        return unserialize($this->keyValue);
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}