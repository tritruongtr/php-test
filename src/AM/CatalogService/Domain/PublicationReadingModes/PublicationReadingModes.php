<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\Domain\PublicationReadingModes;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Util\Inflector;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;

use AM\CatalogService\Domain\Publication\Publication;

/**
 * @ORM\Table(name="publication_reading_modes")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublicationReadingModesRepository")
 */

class PublicationReadingModes
{
    private static $responseGroups = [
        'view' => ['id','publicationId','channel','mode','priority','status','createdAt','createdBy','modifiedAt','modifiedBy'],
        'list' => ['id','publicationId','channel','mode','priority','status','createdAt','createdBy','modifiedAt','modifiedBy']
    ];

    //note: @Group and @Type only use for POST method

    /**
     * @var integer
     * @ORM\Column(
     *     name="id",
     *     type="integer",
     *     options={"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups(
     *    {"view"}
     * )
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name="publication_id",
     *     type="bigint",
     *     nullable=false,
     *     options={"unsigned":true}
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.publication_id.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_reading_modes.publication_id.not_integer"
     * )
     *
     * @Type("integer")
     * @Groups(
     *    {"view"}
     * )
     */
    private $publicationId;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "channel",
     *     type = "string",
     *     nullable = false
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.channel.not_blank")
     * @Assert\Choice(choices = {"mobile", "tablet"}, message = "catalog.publication_reading_modes.channel.invalid_value")
     *
     * @Groups(
     *    {"view"}
     * )
     */
    private $channel;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "mode",
     *     type = "string",
     *     nullable = false
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.mode.not_blank")
     * @Assert\Choice(choices = {"text", "page"}, message = "catalog.publication_reading_modes.mode.invalid_value")
     *
     * @Groups(
     *    {"view"}
     * )
     */
    private $mode;

    /**
     * @var smallint
     *
     * @ORM\Column(
     *     name = "priority",
     *     type = "smallint",
     *     nullable = false,
     *     options={"unsigned":true, "default":1}
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.priority.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_reading_modes.priority.not_integer"
     * )
     * @Groups(
     *    {"view"}
     * )
     */
    private $priority = 1;

    /**
     * @var smallint
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "smallint",
     *     nullable = false,
     *     options={"unsigned":true}
     * )
     *
     * @Assert\NotBlank(message="catalog.publication_reading_modes.status.not_blank")
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.publication_reading_modes.status.invalid_value"
     * )
     * @Groups(
     *    {"view"}
     * )
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups(
     *    {"view"}
     * )
     */
    private $createdAt;
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     * @Groups(
     *    {"view"}
     * )
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "created_by",
     *     type = "bigint",
     *     nullable = false
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.created_by.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_reading_modes.created_by.not_integer"
     * )
     * @Type("integer")
     * @Groups(
     *    {"view"}
     * )
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "modified_by",
     *     type = "bigint",
     *     nullable = false
     * )
     * @Assert\NotBlank(message="catalog.publication_reading_modes.modified_by.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_reading_modes.modified_by.not_integer"
     * )
     * @Type("integer")
     * @Groups(
     *    {"view"}
     * )
     */
    private $modifiedBy;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     inversedBy = "publicationReadingModes"
     * )
     * @ORM\JoinColumn(
     *     name = "publication_id",
     *     referencedColumnName = "id"
     * )
     */
    private $publication;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}