<?php

namespace AM\CatalogService\Domain\Catalog;

use AM\CatalogService\Domain\Category\Category;
use AM\CatalogService\DomainBundle\Repository\DTCategoryRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Catalog
 *
 * @ORM\Table(name="catalogs")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTCatalogRepository")
 *
 * @UniqueEntity(
 *     fields={"name"},
 *     message="catalog.catalog.name.unique"
 * )
 *
 */
class Catalog
{
    private static $responseGroups = [
        'view' => ['id', 'name', 'description', 'remoteIdentifier', 'status', 'no_of_publications', 'legacyIdentifier', 'createdAt', 'modifiedAt'],
        'list' => ['id', 'name', 'description', 'remoteIdentifier', 'status', 'no_of_publications', 'legacyIdentifier', 'createdAt', 'modifiedAt']
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"view", "list"})
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Assert\NotBlank(
     *     message = "catalog.catalog.name.not_blank"
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.catalog.name.min_message",
     *     maxMessage = "catalog.catalog.name.max_message",
     * )
     *
     * @Groups({"view", "list"})
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Groups({"view", "list"})
     *
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="remote_identifier", type="string", length=100, nullable=true)
     *
     * @Assert\Length(
     *     min = 1,
     *     max = 100,
     *     minMessage = "catalog.catalog.remote_identifier.min_message",
     *     maxMessage = "catalog.catalog.remote_identifier.max_message",
     * )
     *
     * @Groups({"view", "list"})
     *
     */
    private $remoteIdentifier;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=true, options={"unsigned":true, "default": 1})
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "catalog.catalog.status.format"
     * )
     *
     * @Groups({"view", "list"})
     *
     */
    private $status = 1;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @ORM\Column(
     *     name = "legacy_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.catalog.legacy_identifier.max_message"
     * )
     */
    private $legacyIdentifier;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $createdAt;

    /**
     * @var \DateTime $modifiedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $modifiedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Catalog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Catalog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set remoteIdentifier
     *
     * @param string $remoteIdentifier
     * @return Catalog
     */
    public function setRemoteIdentifier($remoteIdentifier)
    {
        $this->remoteIdentifier = $remoteIdentifier;

        return $this;
    }

    /**
     * Get remoteIdentifier
     *
     * @return string
     */
    public function getRemoteIdentifier()
    {
        return $this->remoteIdentifier;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Catalog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @ORM\OneToMany(targetEntity="AM\CatalogService\Domain\CatalogPublication\CatalogPublication",  mappedBy="catalog")
     **/
    private $catalogsPublications;

    public function getCatalogPublication()
    {
        return $this->catalogsPublications;
    }

    public function __constructor() {}

    /**
     * @var integer
     *
     * @Accessor(
     *     getter = "getNoOfPublications"
     * )
     * @SerializedName("no_of_publications")
     * @Type("integer")
     *
     * @Groups({"view", "list"})
     */
    private $noOfPublications;

    /**
     * Get NoOfPublications
     * @return int
     * */
    public function getNoOfPublications()
    {
        return $this->noOfPublications;
    }

    /**
     * Set NoOfPublications
     * @return AM\CatalogService\Domain\Catalog\Catalog
     * */
    public function setNoOfPublications($noOfPublications)
    {
    	$this->$noOfPublications = $noOfPublications;
    	return $this;
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));

        if (is_string($value) && ($realFieldName == 'createdAt' || $realFieldName == 'modifiedAt')) {
            try {
                $value = new \DateTime($value, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new \Symfony\Component\Validator\Exception\InvalidArgumentException (
                    "common.datetime.invalid"
                );
            }
        }

        $this->$realFieldName = $value;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}

