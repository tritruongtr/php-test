<?php

namespace AM\CatalogService\Domain\PublisherMetadata;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;

use AM\CatalogService\Domain\Publisher\Publisher;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Publisher Metadata
 * 
 * @author Can Ly <can.ly@audiencemedia.com>
 *
 * @ORM\Table(
 *     name = "publisher_metadata", 
 *     options = {"engine" = "InnoDB"},
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint(
 *             name = "publisher_meta_key", 
 *             columns = {"publisher_id", "key_name"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublisherMetadataRepository")
 */
class PublisherMetadata
{
    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "id", 
     *     type = "integer",
     *     options = {"unsigned" = true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "publisher_id",
     *     type = "bigint",
     *     nullable = false,
     *     options= {"unsigned":true}
     * )
     * 
     * @Assert\NotBlank(
     *     message = "catalog.publisher.metadata_publisher_id.not_blank"
     * )
     * 
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publisher.metadata_publisher_id.integer"
     * )
     *
     * @Groups({"view"})
     */
    private $publisherId;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publisher\Publisher",
     *     inversedBy = "metadata"
     * )
     *
     * @ORM\JoinColumn(
     *     name = "publisher_id",
     *     referencedColumnName = "id",
     *     onDelete = "CASCADE"
     * )
     */
    private $publisher;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "key_name", 
     *     type = "string", 
     *     length = 100, 
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.publisher.metadata_key.not_blank"
     * )
     * 
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,99}$/",
     *     htmlPattern = "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,99}$",
     *     message = "catalog.publisher.metadata_key.alpha_numeric"
     * )
     *
     * @Groups({"view", "list"})
     */
    private $keyName;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "key_value", 
     *     type = "text", 
     *     nullable = false
     * )
     * 
     * @Accessor(getter="getKeyValue", setter="setKeyValue")
     * 
     * @Groups({"view", "list"})
     */
    private $keyValue;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function getKeyValue($unserialize = true)
    {
        if ($unserialize === false) {
            return $this->keyValue;
        }
        return unserialize($this->keyValue);
    }
}