<?php
namespace AM\CatalogService\Domain\Issue;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Annotation\MaxDepth;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Issue
 *
 * @author Can Ly <can.ly@audiencemedia.com>
 *
 * @ORM\Table(name="issues", options={"engine"="InnoDB"})
 * @UniqueEntity(
 *     fields={"code"},
 *     message="catalog.issue.code.unique"
 * )
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTIssueRepository")
 */
class Issue
{
    const ACTION_DOWNLOAD = 'DOWNLOAD';
    const ACTION_SUBSCRIBE = 'SUBSCRIBE';
    const ACTION_PREVIEW = 'PREVIEW';
    const ACTION_BUY = 'BUY';
    const ACTION_READ = 'READ';

    const TYPE_STANDARD = 0;
    const TYPE_STANDALONE = 1;
    const TYPE_SPECIAL = 2;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_SCHEDULED = 2;
    const STATUS_ARCHIVED = 3;
    const STATUS_DISCARDED = 4;
    const STATUS_RETRACTED = 5;
    const STATUS_IMPORTING = 6;
    const STATUS_ON_DECK = 7;

    const STATUS_ERROR = 500;

    private static $responseGroups = [
        'list' => ['id', 'publicationId', 'name', 'internalName', 'volumeNo', 'issueNo', 'sequenceNo', 'slug', 'code', 'coverImage', 'coverDate', 'publishDate', 'legacyIssueId', 'remoteIdentifier', 'legacyIdentifier', 'status', 'createdAt', 'modifiedAt', 'type', 'preview', 'hasXml', 'hasPdf', 'binding', 'fulfilmentCode', 'allowPrinting', 'watermark', 'coverPrice', 'coverCurrency'],
        'view' => ['id','publicationId','name','internalName','issn','volumeNo','issueNo','sequenceNo','description','slug','code','coverImage','coverDate','publishDate','remoteIdentifier','legacyIssueId', 'legacyIdentifier', 'status','createdAt','modifiedAt','createdBy','modifiedBy','filePath','type','metadata','publication','issueContent','preview','hasXml','hasPdf', 'binding', 'fulfilmentCode', 'allowPrinting', 'watermark', 'coverPrice', 'coverCurrency', 'noOfPages'],
        //database role similar view role
        'database' => ['id','publicationId','name','internalName','issn','volumeNo','issueNo','sequenceNo','description','coverImage','coverDate','publishDate','remoteIdentifier','legacyIssueId', 'legacyIdentifier', 'status','createdAt','modifiedAt','createdBy','modifiedBy','filePath','type','preview','hasXml','hasPdf', 'binding', 'fulfilmentCode', 'allowPrinting', 'watermark', 'coverPrice', 'coverCurrency', 'noOfPages'],
        'issue' => ['slug', 'code'],
        'search' => ['publication'],
        'showonlyid' => ['id']
    ];

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "bigint",
     *     options= {"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"view", "list", "database"})
     */
    private $id;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "publication_id",
     *     type = "bigint",
     *     nullable = false,
     *     options= {"unsigned":true}
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.publication_id.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.issue.publication_id.integer"
     * )
     *
     * @Groups({"list", "view", "database"})
     */
    private $publicationId;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 255,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.name.not_blank"
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.issue.name.length",
     *     maxMessage = "catalog.issue.name.length"
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "internal_name",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.issue.internal_name.length"
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $internalName;

     /**
     * @var string
     *
     * @ORM\Column(
     *     name = "issn",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Groups({"view", "database"})
     */
    private $issn;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "volume_no",
     *     type = "string",
     *     length = 16,
     *     nullable = true
     * )
     *
     * @Groups({"view", "database"})
     */
    private $volumeNo;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "issue_no",
     *     type = "string",
     *     length = 16,
     *     nullable = true
     * )
     *
     * @Groups({"view", "database"})
     */
    private $issueNo;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "sequence_no",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Groups({"view", "database"})
     */
    private $sequenceNo;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "description",
     *     type = "text",
     *     nullable = true
     * )
     *
     * @Groups({"view", "database"})
     */
    private $description;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     *
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9-]*$/",
     *     htmlPattern = "^[a-zA-Z0-9-]*$",
     *     message = "catalog.issue.slug.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.issue.slug.length",
     *     maxMessage = "catalog.issue.slug.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "issue"}
     * )
     */
    private $slug;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true,
     *     unique=true
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9-]*$/",
     *     htmlPattern = "^[a-zA-Z0-9-]*$",
     *     message = "catalog.issue.code.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.issue.code.length",
     *     maxMessage = "catalog.issue.code.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "issue"}
     * )
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "cover_image",
     *     type = "text",
     *     nullable = true
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $coverImage;

    /**
     * @var \DateTime $coverDate
     *
     * @ORM\Column(name="cover_date", type="datetime", nullable=true)
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     */
    private $coverDate;

    /**
     * @var \DateTime $publishDate
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     */
    private $publishDate;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "remote_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.issue.remote_identifier.length"
     * )
     *
     * @Groups({"view", "database"})
     */
    private $remoteIdentifier;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "legacy_issue_id",
     *     type = "integer",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="catalog.issue.legacy_issue_id.integer"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.issue.legacy_issue_id.integer"
     * )
     *
     * @Groups({"list","view", "database"})
     */
    private $legacyIssueId = 0;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "integer",
     *     nullable = true,
     *     options = {
     *         "default" : 0,
     *         "unsigned" : true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.status.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1|2|3|4|5|6|7|500)$/",
     *     htmlPattern = "^(0|1|2|3|4|5|6|7|500)$",
     *     message = "catalog.issue.status.integer"
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $status;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $createdAt;

    /**
     * @var \DateTime $modifiedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $modifiedAt;

    /**
     * @var integer
     * @Type("integer")
     *
     * @Groups(
     *     {"view", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "created_by",
     *     type = "bigint",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.created_by.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.issue.created_by.integer"
     * )
     */
    private $createdBy;

    /**
     * @var integer
     * @Type("integer")
     *
     * @Groups(
     *     {"view", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "modified_by",
     *     type = "bigint",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.modified_by.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.issue.modified_by.integer"
     * )
     */
    private $modifiedBy;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "file_path",
     *     type = "text",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.file_path.not_blank"
     * )
     *
     * @Groups({"view", "database"})
     */
    private $filePath;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "type",
     *     type = "integer",
     *     nullable = false,
     *     options = {
     *         "default" : 1,
     *         "unsigned" : true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.type.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1|2|3|4)$/",
     *     htmlPattern = "^(0|1|2|3|4)$",
     *     message = "catalog.issue.type.integer"
     * )
     *
     * @Groups({"list", "view", "database"})
     */
    private $type;

    /**
     * Issue metadata
     *
     * @var array
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\IssueMetadata\IssueMetadata",
     *     mappedBy = "issue"
     * )
     *
     * @Accessor(
     *     getter = "getMetadata"
     * )
     *
     * @Type("array")
     * @MaxDepth(1)
     * @Groups({"view"})
     */
    private $metadata;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     inversedBy = "issues"
     * )
     *
     * @ORM\JoinColumn(
     *     name = "publication_id",
     *     referencedColumnName = "id"
     * )
     *
     * @Accessor(
     *     getter = "getPublication"
     * )
     * @MaxDepth(1)
     * @Groups({"view","search"})
     */
    private $publication;

    /**
     * Issue content
     *
     * @var array
     *
     * @Type("array")
     * @SerializedName("issue_content")
     *
     * @Groups({"view"})
     */
    private $issueContent;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "preview",
     *     type = "integer",
     *     nullable = true,
     *     options = {
     *         "default" : 0,
     *         "unsigned" : true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.issue.preview.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.issue.not_blank.integer"
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $preview;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "has_xml",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "newsstand.newsstand.has_xml.format",
     *     groups={"client"}
     * )
     *
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $hasXml = 0;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "has_pdf",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "newsstand.newsstand.has_pdf.format",
     *     groups={"client"}
     * )
     *
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $hasPdf = 0;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "legacy_identifier",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.issue.legacy_identifier.max_message"
     * )
     */
    private $legacyIdentifier;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "binding",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.issue.binding.not_integer"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 65535,
     *      maxMessage = "catalog.issue.binding.range"
     * )
     */
    private $binding = 0;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "fulfilment_code",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "catalog.issue.fulfilment_code.length"
     * )
     */
    private $fulfilmentCode;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "allow_printing",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.issue.allow_printing.format"
     * )
     */
    private $allowPrinting = 0;

    /**
     * @var smallint
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "watermark",
     *     type = "smallint",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.issue.watermark.format"
     * )
     */
    private $watermark = 0;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "cover_price",
     *     type = "integer",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.issue.cover_price.integer"
     * )
     */
    private $coverPrice = 0;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "cover_currency",
     *     type = "string",
     *     length = 3,
     *     nullable = true,
     *     options={"default":""}
     * )
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 3,
     *      maxMessage = "catalog.issue.cover_currency.length"
     * )
     */
    private $coverCurrency = '';

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "no_of_pages",
     *     type = "integer",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.issue.no_of_pages.integer"
     * )
     */
    private $noOfPages = 0;

    public function __construct(array $data)
    {
        $this->coverDate = new \DateTime("0000-00-00 00:00:00", new \DateTimeZone("UTC"));
        $this->publishDate = new \DateTime("0000-00-00 00:00:00", new \DateTimeZone("UTC"));

        $this->publications = new ArrayCollection();
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function getMetadata()
    {
        $serializer = SerializerBuilder::create()->build();
        $metadataJson = $serializer->serialize(
            $this->metadata,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )->setSerializeNull(true)
        );

        $metadataArr = json_decode($metadataJson, true);

        $this->metadata = $metadataArr;
        return $this->metadata;
    }

    public function getPublication($groupName = 'list')
    {
        $serializer = SerializerBuilder::create()->build();
        $publicationJson = $serializer->serialize(
            $this->publication,
            'json',
            SerializationContext::create()->setGroups(
                [$groupName]
            )->setSerializeNull(true)
        );

        $publicationArr = json_decode($publicationJson, true);

        $this->publication = $publicationArr;
        return $this->publication;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}