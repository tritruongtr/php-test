<?php

namespace AM\CatalogService\Domain\CategoryTranslation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Zenith\CoreService\Intl\Locale;
use Zenith\CoreService\Intl\Language;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category Translation
 *
 * @ORM\Table(name="category_translations")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTCategoryTranslationRepository")
 *
 * @UniqueEntity(
 *     fields={"category","locale"},
 *     message="catalog.category_translation.category_id_and_locale.unique",
 *     groups={"server"}
 * )
 */
class CategoryTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", options={"unsigned":true})
     * @Assert\NotBlank(message = "catalog.category_translation.category_id.not_blank",groups={"client"} )
     * @Assert\Regex(
     *     pattern = "/^[0-9]+$/",
     *     htmlPattern = "^[0-9]+$",
     *     message = "catalog.category_translation.category_id.not_interger",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $categoryId;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Category\Category",
     *     inversedBy = "translation"
     * )
     *
     * @ORM\JoinColumn(
     *     name = "category_id",
     *     referencedColumnName = "id",
     *     onDelete="CASCADE"
     * )
     *
     * @Accessor(
     *     getter = "getCategory"
     * )
     * @Type("array")
     *
     * @Groups(
     *     {"view"}
     * )
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category_translation.name.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.category_translation.name.length",
     *     maxMessage = "catalog.category_translation.name.length",
     *     groups={"client"}
     * )
     *
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "language_code",
     *     type = "string",
     *     length = 3,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.category_translation.language.not_blank", groups={"client"})
     * @Assert\Regex(
     *     pattern = "/^[a-z]{2}$/",
     *     htmlPattern = "^[a-z]{2}$",
     *     message = "catalog.category_translation.language.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 2,
     *     max = 2,
     *     minMessage = "catalog.category_translation.language.format",
     *     maxMessage = "catalog.category_translation.language.format",
     *     groups={"client"}
     * )
     */
    private $language;

    /**
     * @var array
     * @Accessor(getter="getLanguage")
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $languageCode;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "locale_code",
     *     type = "string",
     *     length = 10,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(message="catalog.category_translation.locale.not_blank", groups={"client"})
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z]{1}[a-zA-Z-_]{0,9}$/",
     *     htmlPattern = "^[a-zA-Z]{1}[a-zA-Z-_]{0,9}$",
     *     message = "catalog.category_translation.locale.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min =1,
     *     max = 10,
     *     minMessage = "catalog.category_translation.locale.format",
     *     maxMessage = "catalog.category_translation.locale.format",
     *     groups={"client"}
     * )
     */
    private $locale;

    /**
     * @var array
     * @Accessor(getter="getLocale")
     * @Groups(
     *     {"view", "list"}
     * )
     */
    private $localeCode;

    public function __construct(array $data)
    {
        $this->publications_categories = new \Doctrine\Common\Collections\ArrayCollection();
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function getCategory()
    {
        $serializer = SerializerBuilder::create()->build();
        $categoryJson = $serializer->serialize(
            $this->category,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )
        );

        $categoryArr = json_decode($categoryJson, true);

        $this->category = $categoryArr;
        return $this->category;
    }

    public function getLanguage()
    {
        $instance = Language::instance();
        if (method_exists($instance,'getLangInfoByCode')) {
            return $instance->getLangInfoByCode($this->language);
        }
        return null;
    }

    public function getLocale()
    {
        return Locale::instance()->getInfoByCode($this->locale);
    }
}
