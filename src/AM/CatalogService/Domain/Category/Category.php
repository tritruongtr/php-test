<?php

namespace AM\CatalogService\Domain\Category;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 *
 * @ORM\Table(
 *     name="categories",
 *     uniqueConstraints = {
 *        @ORM\UniqueConstraint(
 *          name = "unique_object",
 *          columns = {"name", "parent_id", "category_set_id"}
 *        )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTCategoryRepository")
 *
 * @UniqueEntity(
 *     fields={"name","parentId", "categorySetId"},
 *     message="catalog.category.name.unique",
 *     groups={"server"}
 * )
 * @UniqueEntity(
 *     fields={"slug","parentId", "categorySetId"},
 *     message="catalog.category.slug.unique",
 *     groups={"server"}
 * )
 */
class Category
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $id;

	/**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer")
     * @Assert\NotBlank( message = "catalog.category.project_id.not_blank", groups={"client"} )
     * @Assert\Regex(
     *     pattern = "/^[0-9]+$/",
     *     htmlPattern = "^[0-9]+$",
     *     message = "catalog.category.project_id.not_interger",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category.name.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.category.name.length",
     *     maxMessage = "catalog.category.name.length",
     *     groups={"client"}
     * )
     *
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", nullable=true)
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category.slug.not_blank", groups={"client"} )
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9-_]+$/",
     *     htmlPattern = "^[a-zA-Z0-9-_]+$",
     *     message = "catalog.category.slug.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.category.slug.length",
     *     maxMessage = "catalog.category.slug.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "category"}
     * )
     *
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category.parent_category_id.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 50,
     *     minMessage = "catalog.category.parent_category_id.length",
     *     maxMessage = "catalog.category.parent_category_id.length",
     *     groups={"client"}
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]+$/",
     *     htmlPattern = "^[0-9]+$",
     *     message = "catalog.category.parent_category_id.not_interger",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "category"}
     * )
     *
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="remote_identifier", type="string", length=100, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category.remote_identifier.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 100,
     *     minMessage = "catalog.category.remote_identifier.length",
     *     maxMessage = "catalog.category.remote_identifier.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "category"}
     * )
     *
     */
    private $remoteIdentifier;

    /**
     * @var array
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\CategoryTranslation\CategoryTranslation",
     *     mappedBy = "category"
     * )
     * @Accessor(
     *     getter = "getTranslation"
     * )
     *
     * @Type("array")
     *
     **/
    private $translation;

    /**
     * @var array
     *
     * @Accessor(
     *     getter = "getChildrens"
     * )
     *
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @Type("array")
     *
     **/
    private $children;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", options={"unsigned":true})
     * @Assert\NotBlank(message = "catalog.category.status.not_blank", groups={"client"})
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "catalog.category.status.format",
     *     groups={"client"}
     * )
     *
     * @Groups({"view", "list", "category"})
     *
     */
    private $status;

    /**
     *
     * @ORM\Column(
     *     name = "priority",
     *     type = "integer",
     *     nullable = false,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Groups({"view", "list", "database", "category"})
     */
    private $priority = 0;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "legacy_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.category.legacy_identifier.max_message",
     *     groups={"client"}
     * )
     */
    private $legacyIdentifier;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $createdAt;

    /**
     * @var \DateTime $modifiedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     *
     * @Groups({"view", "list", "database"})
     */
    private $modifiedAt;

    public function getChildrens()
    {
        if($this->repositoryCategory == null) {
            return array();
        }
        $params = array(
            'parentId' => $this->id
        );

        $childrens = $this->repositoryCategory->findBy($params);
        return $childrens;
    }

    private $repositoryCategory;

    public function setRepositoryCategory($respositoryCategory)
    {
        $this->repositoryCategory = $respositoryCategory;
    }



    public function __construct(array $data)
    {
    	$this->publications_categories = new \Doctrine\Common\Collections\ArrayCollection();
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));

        if (is_string($value) && ($realFieldName == 'createdAt' || $realFieldName == 'modifiedAt')) {
            try {
                $value = new \DateTime($value, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new \Symfony\Component\Validator\Exception\InvalidArgumentException (
                    "common.datetime.invalid"
                );
            }
        }

        $this->$realFieldName = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getParentCategoryId()
    {
        return $this->parentId;
    }

    public function getRemoteIdentifier()
    {
        return $this->remoteIdentifier;
    }

    /**
     * @ORM\OneToMany(
     *     targetEntity="AM\CatalogService\Domain\PublicationCategory\PublicationCategory",
     *     mappedBy="category"
     * )
     *
     **/
    private $publicationsCategories;

   /**
    * @var array
     *
     * @Type("array")
     *
     **/
    private $publications;

    public function setPublication($publications)
    {
        return $this->publications = $publications;
    }

    public function getTranslation()
    {
        $serializer = SerializerBuilder::create()->build();
        $translationJson = $serializer->serialize(
            $this->translation,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )
        );

        $translationArr = json_decode($translationJson, true);

        $this->translation = $translationArr;
        return $this->translation;
    }

    /**
     *
     * @ORM\Column(
     *     name = "category_set_id",
     *     type = "integer",
     *     nullable = true,
     *     options={"unsigned":true, "default":0}
     * )
     *
     * @Groups({"view", "list", "category"})
     */
    private $categorySetId = 0;

}
