<?php
namespace AM\CatalogService\Domain\CatalogPublication;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Util\Inflector;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;

/**
 * CatalogPublication
 *
 * @ORM\Table(
 *     name = "catalogs_publications"
 * )
 * @ORM\Entity(
 *     repositoryClass = "AM\CatalogService\DomainBundle\Repository\DTCatalogPublicationRepository"
 * )
 * @ExclusionPolicy("all")
 */
class CatalogPublication
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const DEFAULT_CREATED_BY = 0;
    const DEFAULT_MODIFIED_BY = 0;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "integer",
     *     options={"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list","view"})
     * @Expose()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "catalog_id",
     *     type = "integer",
     *     options={"unsigned":true}
     * )
     */
    private $catalogId;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "publication_id",
     *     type = "bigint",
     *     options={"unsigned":true}
     * )
     * @Type("integer")
     * @Groups({"list","view"})
     * @Expose()
     */
    private $publicationId;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "integer",
     *     nullable = true,
     *     options = {
     *         "default" = 0
     *     }
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.publication.assign_status.format"
     * )
     * @Groups({"list","view"})
     * @Expose()
     */
    private $status;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view", "list"})
     * @Expose()
     */
    private $createdAt;

    /**
     * @var \DateTime $modifiedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     *
     * @Groups({"view", "list"})
     * @Expose()
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @ORM\Column(
     *     name = "created_by",
     *     type = "bigint",
     *     nullable = true,
     *     options = {
     *         "default" = 0
     *     }
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.created_by.format"
     * )
     * @Type("integer")
     * @Groups({"list", "view"})
     * @Expose()
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @ORM\Column(
     *     name = "modified_by",
     *     type = "bigint",
     *     nullable = true,
     *     options = {
     *         "default" = 0
     *     }
     * )
     * @Assert\Regex(
     *     pattern = "/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     message = "catalog.publication.modified_by.format"
     * )
     * @Type("integer")
     * @Groups({"list", "view"})
     * @Expose()
     */
    private $modifiedBy;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Catalog\Catalog",
     *     inversedBy = "catalogsPublications"
     * )
     * @ORM\JoinColumn(
     *     name = "catalog_id",
     *     referencedColumnName = "id"
     * )
     * @Groups(
     *     {"list", "view"}
     * )
     * @Expose()
     */
    protected $catalog;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     inversedBy = "catalogsPublications"
     * )
     * @ORM\JoinColumn(
     *     name = "publication_id",
     *     referencedColumnName = "id"
     * )
     *
     */
    protected $publication;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.catalog_publication.remarks.max_length"
     * )
     *
     * @Groups(
     *     {"view", "list"}
     * )
     * @Expose()
     */
    private $remarks;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catalogId
     *
     * @param integer $catalogId
     * @return CatalogPublication
     */
    public function setCatalogId($catalogId)
    {
        $this->catalogId = $catalogId;

        return $this;
    }

    /**
     * Get catalogId
     *
     * @return integer
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }

    /**
     * Set publicationId
     *
     * @param integer $publicationId
     * @return CatalogPublication
     */
    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;

        return $this;
    }

    /**
     * Get publicationId
     *
     * @return integer
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    public function getPublication()
    {
        return $this->publication;
    }

    public function setPublication($publication)
    {
        $this->publication = $publication;
    }

    public function getCatalog()
    {
        return $this->catalog;
    }

    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));

        if (is_string($value) && ($realFieldName == 'createdAt' || $realFieldName == 'modifiedAt')) {
            try {
                $value = new \DateTime($value, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new \Symfony\Component\Validator\Exception\InvalidArgumentException (
                    "common.datetime.invalid"
                );
            }
        }

        $this->$realFieldName = $value;
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }
}
