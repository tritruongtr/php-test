<?php

namespace AM\CatalogService\Domain\CatalogPublication;

interface CatalogPublicationRepository
{
    public function findCatalogPublicationList(array $params, $ignore_limit = false);
    public function count(array $params);
    public function getFinalResultByJMSGroup($data, $JMSGroup);
    public function update(CatalogPublication $catalogPublication);
}