<?php

namespace AM\CatalogService\Domain\Publisher;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Zenith\CoreService\Intl\Country;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Publisher
 *
 * @author Can Ly <can.ly@audiencemedia.com>
 *
 * @ORM\Table(name="publishers", options={"engine"="InnoDB"})
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublisherRepository")
 * @UniqueEntity(
 *     fields={"code"},
 *     message="catalog.publisher.code.unique"
 * )
 */
class Publisher
{
    private static $responseGroups = [
        'view' => ['id','name','internalName','description','slug','code','logo','remoteIdentifier','country','status','publications','noOfPublications','metadata','internalNotes','legacyIdentifier','createdAt','createdBy','modifiedAt','modifiedBy'],
        'list' => ['id','name','internalName','description','slug','code','logo','remoteIdentifier','country','status','internalNotes','legacyIdentifier','createdAt','createdBy','modifiedAt','modifiedBy'],
        'publisher_list' => ['id','name','internalName','description','slug','code','logo','remoteIdentifier','countryCode','status','internalNotes','legacyIdentifier','createdAt','createdBy','modifiedAt','modifiedBy'],
        'issue_list' => ['id','name','internalName','description','slug','code','logo','remoteIdentifier','countryCode','status'],
        'publication_list' => ['id','name','internalName','description','slug','code','logo','remoteIdentifier','countryCode','status'],
        'database' => ['id','name','internalName','description','logo','remoteIdentifier','status'],
        'newsstand' => ['id','name','internalName','description','logo','remoteIdentifier','country','status','noOfPublications'],
        'publisher' => ['slug','code']
    ];

    public static $allowedCreatingFields = [
        'name', 'country_code', 'internal_name', 'description',
        'logo', 'remote_identifier', 'status', 'legacy_identifier',
        'internal_notes', 'code', 'created_at', 'created_by'
    ];

    public static $allowedUpdatingFields = [
        'name', 'country_code', 'internal_name', 'description',
        'logo', 'remote_identifier', 'status', 'legacy_identifier',
        'internal_notes', 'code', 'slug', 'modified_at', 'modified_by'
    ];

    /**
     * @var integer
     * @Type("string")
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "bigint",
     *     options= {"unsigned":true}
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 255,
     *     nullable = false
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.publisher.name.not_blank"
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.publisher.name.length",
     *     maxMessage = "catalog.publisher.name.length"
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "internal_name",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "catalog.publisher.internal_name.max_length"
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $internalName;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "description",
     *     type = "text",
     *     nullable = true
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $description;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9-]*$/",
     *     htmlPattern = "^[a-zA-Z0-9-]*$",
     *     message = "catalog.publisher.slug.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.publisher.slug.length",
     *     maxMessage = "catalog.publisher.slug.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "publisher"}
     * )
     */
    private $slug;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true,
     *     unique=true
     * )
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9-]*$/",
     *     htmlPattern = "^[a-zA-Z0-9-]*$",
     *     message = "catalog.publisher.code.format",
     *     groups={"client"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.publisher.code.length",
     *     maxMessage = "catalog.publisher.code.length",
     *     groups={"client"}
     * )
     * @Groups(
     *     {"view", "list", "publisher"}
     * )
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "logo",
     *     type = "text",
     *     nullable = true
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "remote_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.publisher.remote_identifier.max_length"
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $remoteIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "country_code",
     *     type = "string",
     *     length = 2,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.publisher.country_code.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[A-Z]{2}$/",
     *     htmlPattern = "^[A-Z]{2}$",
     *     message = "catalog.publisher.country_code.format"
     * )
     */
    private $countryCode;

    /**
     * Country object
     *
     * @var array
     *
     * @Groups(
     *     {"view", "list", "newsstand"}
     * )
     *
     * @Type("array")
     *
     * @Accessor(getter="getCountry")
     */
    private $country;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "smallint",
     *     nullable = true,
     *     options = {
     *         "default" = 0,
     *         "unsigned" = true
     *     }
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.publisher.status.format"
     * )
     *
     * @Groups({"view", "list", "database", "newsstand"})
     */
    private $status;

    /**
     * @var array
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     mappedBy = "publisher"
     * )
     *
     * @Accessor(
     *     getter = "getPublications"
     * )
     * @Type("array")
     * @Groups({"view"})
     *
     **/
    private $publications;

    /**
     * @var integer
     *
     * @Accessor(
     *     getter = "getNoOfPublications"
     * )
     * @SerializedName("no_of_publications")
     * @Type("integer")
     *
     * @Groups({"view", "newsstand"})
     */
    private $noOfPublications;

    /**
     * Publisher metadata
     *
     * @var array
     *
     * @ORM\OneToMany(
     *     targetEntity = "AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata",
     *     mappedBy = "publisher"
     * )
     *
     * @Accessor(
     *     getter = "getMetadata"
     * )
     *
     * @Type("array")
     *
     * @Groups({"view"})
     */
    private $metadata;

    /**
     * @var string
     *
     * @Groups(
     *     {"view", "list", "database"}
     * )
     *
     * @ORM\Column(
     *     name = "legacy_identifier",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "catalog.publisher.legacy_identifier.max_message"
     * )
     */
    private $legacyIdentifier;

    /**
     * @var string
     * @ORM\Column(name="internal_notes", type="text", nullable=true)
     * @Groups({"view", "list", "database"})
     */
    private $internalNotes;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({"view", "list", "database"})
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true)
     * @Groups({"view", "list", "database"})
     *
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups({"view", "list", "database"})
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", nullable=true)
     * @Groups({"view", "list", "database"})
     */
    private $modifiedBy;

    public function __construct(array $data = array())
    {
        $this->publications = new ArrayCollection();
        return $this->setData($data);
    }

    public function setFieldValues($data, array $allowedFields = array())
    {
        if($data) {
            foreach ($data as $field => $value) {
                if(in_array($field, $allowedFields)) {
                    $this->set($field, $value);
                }
            }
        }
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function getCountry()
    {
        return Country::instance()->getInfoByCode($this->countryCode);
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));

        if(property_exists($this, $realFieldName)) {
            if (is_string($value) && ($realFieldName == 'createdAt' || $realFieldName == 'modifiedAt')) {
                try {
                    $value = new \DateTime($value, new \DateTimeZone('UTC'));
                } catch (\Exception $e) {
                    throw new \Symfony\Component\Validator\Exception\InvalidArgumentException (
                        "common.datetime.invalid"
                    );
                }
            }

            $this->$realFieldName = $value;
        }
    }

    public function getPublications()
    {
        return [];
    }

    public function getMetadata()
    {
        $serializer = SerializerBuilder::create()->build();
        $metadataJson = $serializer->serialize(
            $this->metadata,
            'json',
            SerializationContext::create()->setGroups(
                ['list']
            )
        );

        $metadataArr = json_decode($metadataJson, true);

        $this->metadata = $metadataArr;
        return $this->metadata;
    }

    public function getNoOfPublications()
    {
        if (empty($this->publications)) {
            $this->noOfPublications = 0;
            return $this->noOfPublications;
        }

        $this->noOfPublications = count($this->publications);
        return $this->noOfPublications;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }
}
