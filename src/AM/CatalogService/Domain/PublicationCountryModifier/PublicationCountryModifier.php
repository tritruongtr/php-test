<?php

namespace AM\CatalogService\Domain\PublicationCountryModifier;

use Doctrine\ORM\Mapping as ORM;
use AM\CatalogService\Domain\Publication\Publication;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

use Zenith\CoreService\Intl\Country;
use Zenith\CoreService\Intl\Currency;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PublicationCountryModifier
 *
 * @ORM\Table(name="publication_country_modifiers")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublicationCountryModifierRepository")
 * @UniqueEntity(
 *     fields={"publicationId","countryCode","currencyCode"},
 *     message="catalog.publication_country_modifier.publication_id_and_country_code_and_currency.unique"
 * )
 */
class PublicationCountryModifier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_id", type="bigint", nullable = false, options={"unsigned":true})
     *
     * @Assert\NotBlank(message="catalog.publication_country_modifier.publication_id.not_blank")
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_country_modifier.publication_id.format"
     * )
     *
     * @Groups({"view", "list"})
     * @Type("integer")
     *
     */
    private $publicationId;

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     inversedBy = "publicationCountryModifier"
     * )
     * @ORM\JoinColumn(
     *     name = "publication_id",
     *     referencedColumnName = "id",
     *     onDelete="cascade"
     * )
     *
     */
    private $publication;

    public function setPublication(Publication $publication)
    {
        $this->publication =$publication;
        return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="country_code",
     *     type="string",
     *     length=2,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(message="catalog.publication_country_modifier.country_code.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[A-Z]{2}$/",
     *     htmlPattern = "^[A-Z]{2}$",
     *     message = "catalog.publication_country_modifier.country_code.format"
     * )
     * 
     * @Type("array")
     * @Accessor(getter="getCountry")
     * 
     * @SerializedName("country")
     *
     * @Groups({"view", "list"})
     *
     */
    private $countryCode;
   

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable = false)
     *
     * @Assert\NotBlank(message="catalog.publication_country_modifier.price.not_blank")
     * @Assert\Regex(
     *     pattern = "/^\d+(\.\d+)?$/",
     *     htmlPattern = "^\d+(\.\d+)?$",
     *     message = "catalog.publication_country_modifier.price.format"
     * )
     *
     * @Groups({"view", "list"})
     * @Type("float")
     *
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="currency_code",
     *     type="string",
     *     length=3,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(message="catalog.publication_country_modifier.currency.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[A-Z]{3}$/",
     *     htmlPattern = "^[A-Z]{3}$",
     *     message = "catalog.publication_country_modifier.currency.format"
     * )
     *
     * @Type("array")
     * @Accessor(getter="getCurrency")
     * 
     * @SerializedName("currency")
     *
     * @Groups({"view", "list"})
     *
     */
    private $currencyCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer" , nullable = false)
     *
     * * @Assert\NotBlank(message="catalog.publication_country_modifier.status.not_blank")
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1)$/",
     *     htmlPattern = "^(0|1)$",
     *     message = "catalog.publication_country_modifier.status.not_blank"
     * )
     *
     *
     * @Type("integer")
     *
     * @Groups({"view", "list"})
     *
     */
    private $status;

    /**
     * Country object
     *
     * @var array
     *
     * @Groups(
     *     {"view", "list"}
     * )
     *
     * @Type("array")
     *
     * @Accessor(getter="getCountry")
     */
    private $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publicationId
     *
     * @param integer $publicationId
     * @return PublicationCountryModifier
     */
    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;

        return $this;
    }

    /**
     * Get publicationId
     *
     * @return integer
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return PublicationCountryModifier
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return PublicationCountryModifier
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return PublicationCountryModifier
     */
    public function setCurrency($currency)
    {
        $this->currencyCode = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return Country::instance()->getInfoByCode($this->currencyCode);
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PublicationCountryModifier
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function getCountry()
    {
        return Country::instance()->getInfoByCode($this->countryCode);
    }
}
