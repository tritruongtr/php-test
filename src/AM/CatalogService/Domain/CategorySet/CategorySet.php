<?php

namespace AM\CatalogService\Domain\CategorySet;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CategorySet
 *
 * @ORM\Table(name="category_sets")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTCategorySetRepository")
 *
 */
class CategorySet
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.category_set.name.not_blank" )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.category_set.name.length",
     *     maxMessage = "catalog.category_set.name.length",
     *     groups={"client"}
     * )
     *
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups(
     *     {"view", "list", "category"}
     * )
     */
    private $description;

    /**
     * 1 = standard, 2 = custom
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", nullable=true))
     *
     * @Assert\NotBlank(message="catalog.category_set.type.not_blank")
     * @Assert\Regex(
     *     pattern = "/^(\d){1}$/",
     *     message = "catalog.category_set.type"
     * )
     * @Assert\Choice(
     *     choices = {1, 2},
     *     message = "catalog.category_set.type"
     * )
     * @Groups({"view", "list", "category"})
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", options={"default":0})
     *
     * @Assert\NotBlank(message="catalog.category_set.status.not_blank")
     * @Assert\Regex(
     *     pattern = "/^(\d){1}$/",
     *     message = "catalog.category_set.status"
     * )
     * @Assert\Choice(
     *     choices = {0, 1},
     *     message = "catalog.category_set.status"
     * )
     * @Groups({"view", "list", "category"})
     */
    private $status;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }
}
