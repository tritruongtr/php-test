<?php

namespace AM\CatalogService\Domain\PublicationMetadata;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AM\CatalogService\Domain\Publication\Publication;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PublicationMetadata
 *
 * @ORM\Table(name="publications_metadata")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublicationMetadataRepository")
 * @UniqueEntity(
 *     fields={"publicationId","keyName"},
 *     message="catalog.publication_metadata.publication_id_and_key_name.unique"
 * )
 */
class PublicationMetadata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer",  options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_id", type="bigint", nullable=false,  options={"unsigned":true})
     * @Groups({"view", "list"})
     * @Assert\NotBlank(message="catalog.publication_metadata.publication_id.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.publication_metadata.publication_id.format"
     * )
     */
    private $publicationId;

    /**
     * @var string
     *
     * @ORM\Column(name="key_name", type="string", length=100, nullable=false)
     *
     * @Assert\NotBlank(
     *     message = "catalog.publication_metadata.key_name.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,99}$/",
     *     htmlPattern = "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,99}$",
     *     message = "catalog.publication_metadata.metadata_key.alpha_numeric"
     * )
     *
     * @Groups({"view", "list"})
     *
     */
    private $keyName;

    /**
     * @var string
     *
     *
     *
     * @ORM\Column(name="key_value", type="text", nullable=true)
     * @Groups({"view", "list"})
     * @Accessor(getter="getKeyValue")
     *
     */
    private $keyValue;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publicationId
     *
     * @param integer $publicationId
     * @return PublicationMetadata
     */
    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;

        return $this;
    }

    /**
     * Get publicationId
     *
     * @return integer
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    /**
     * Set keyName
     *
     * @param string $keyName
     * @return PublicationMetadata
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;

        return $this;
    }

    /**
     * Get keyName
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set keyValue
     *
     * @param string $keyValue
     * @return PublicationMetadata
     */
    public function setKeyValue($keyValue)
    {
        $this->keyValue = $keyValue;

        return $this;
    }

    /**
     * Get keyValue
     *
     * @return string
     */
    public function getKeyValue()
    {
        if($this->keyValue!=null && !empty($this->keyValue)) {
            return unserialize($this->keyValue);
        }
        return $this->keyValue;
    }

    /**
     * @ORM\ManyToOne(
     *     targetEntity = "AM\CatalogService\Domain\Publication\Publication",
     *     inversedBy = "publicationsMetadata"
     * )
     * @ORM\JoinColumn(
     *     name = "publication_id",
     *     referencedColumnName = "id",
     *     onDelete="cascade"
     * )
     *
     */
    private $publication;

    public function setPublication(Publication $publication)
    {
        $this->publication =$publication;
        return $this;
    }

    /**
     * @var integer
     *
     * @Accessor(
     *     getter = "getPublisherId"
     * )
     * @SerializedName("publisher_id")
     * @Type("integer")
     *
     *
     */
    private $publisherId;

    public function getPublisherId()
    {
        return $this->publication->get('publisherId');
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }

    public function __construct(array $data)
    {
        return $this->setData($data);
    }



}
