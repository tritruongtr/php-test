<?php

namespace AM\CatalogService\Domain\PublicationCategory;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Util\Inflector;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * PublicationCategory
 *
 * @ORM\Table(
 *     name="publications_categories",
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint(
 *             name = "unique_object",
 *             columns = {"root_parent_category_id", "category_id", "publication_id"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPublicationCategoryRepository")
 * 
 * @UniqueEntity(
 *     fields={"rootParentCategoryId", "categoryId", "publicationId"},
 *     message="catalog.publication_category.object.unique"
 * )
 */

class PublicationCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="root_parent_category_id", type="integer", options={"unsigned":true})
     * @Groups({"view", "list"})
     */
    private $rootParentCategoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", options={"unsigned":true})
     * @Groups({"view", "list"})
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_id", type="bigint", options={"unsigned":true})
     * @Groups({"view", "list"})
     */
    private $publicationId;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups({"view", "list"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AM\CatalogService\Domain\Publication\Publication", inversedBy="publicationsCategories")
     * @ORM\JoinColumn(name="publication_id", referencedColumnName="id")
     */
    protected $publication;

    /**
     * @ORM\ManyToOne(targetEntity="AM\CatalogService\Domain\Category\Category", inversedBy="publicationsCategories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rootParentCategoryId
     *
     * @param integer $rootParentCategoryId
     * @return PublicationCategory
     */
    public function setRootParentCategoryId($rootParentCategoryId)
    {
        $this->rootParentCategoryId = $rootParentCategoryId;

        return $this;
    }

    /**
     * Get rootParentCategoryId
     *
     * @return integer 
     */
    public function getRootParentCategoryId()
    {
        return $this->rootParentCategoryId;
    }
    
    public function getPublication()
    {
    	return $this->publication;
    }

    public function setPublication($publication)
    {
        $this->publication = $publication;
    }
    
    public function getCategory()
    {
    	return $this->category;
    }
    
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    /**
     * @param int $publicationId
     */
    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }
}
