<?php

namespace AM\CatalogService\Domain\Partner;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Util\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Partner
 *
 * @ORM\Table(name="partners")
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTPartnerRepository")
 *
 * @UniqueEntity(
 *     fields={"name","parentId"},
 *     message="catalog.partner.name.unique",
 *     groups={"server"}
 * )
 *
 */
class Partner
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"view", "list", "database"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.partner.name.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 50,
     *     minMessage = "catalog.partner.name.length",
     *     maxMessage = "catalog.partner.name.length",
     *     groups={"client"}
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $name;


    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     *
     * @Assert\NotBlank( message = "catalog.partner.parent_id.not_blank", groups={"client"} )
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.partner.parent_id.length",
     *     maxMessage = "catalog.partner.parent_id.length",
     *     groups={"client"}
     * )
     * @Assert\Regex(pattern="/\d+/", message = "catalog.partner.parent_id.not_interger", groups={"client"})
     *
     * @Groups({"view", "list", "database"})
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(
     *     name = "status",
     *     type = "integer",
     *     nullable = false
     * )
     *
     * @Assert\NotBlank( message = "catalog.partner.status.not_blank", groups={"client"} )
     * @Assert\Regex(
     *     pattern = "/^(0|1){1}$/",
     *     htmlPattern = "^(0|1){1}$",
     *     message = "catalog.partner.status.format",
     *	   groups={"client"}
     * )
     *
     * @Groups({"view", "list", "database"})
     */
    private $status;

    public function __construct(array $data)
    {
    	return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = lcfirst(Inflector::classify($fieldName));
        $this->$realFieldName = $value;
    }
}
