<?php

namespace AM\CatalogService\Domain\CountryRestriction;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use Zenith\CoreService\Intl\Country;

/**
 * @ORM\Table(
 *     name="country_restrictions",
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint(
 *             name = "unique_object",
 *             columns = {"object_type", "object_id", "country_code"}
 *         )
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="AM\CatalogService\DomainBundle\Repository\DTCountryRestrictionRepository")
 * @UniqueEntity(
 *     fields={"objectType", "objectId", "countryCode"},
 *     message="catalog.country_restriction.object.unique"
 * )
 */
class CountryRestriction
{
    const  UNKNOWN = 0;
    const  STORY = 1;
    const  ISSUE = 2;
    const  PUBLICATION = 3;
    
    private static $responseGroups = [
        'view' => ['id','objectType','objectId','countryCode','remarks','createdAt'],
        'list' => ['id','objectType','objectId','countryCode','remarks','createdAt'],
    ];

    /**
     * @var integer
     *
     * @Type("string")
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"view"})
     */
    private $id;

    /**
     * @var integer
     * @Type("integer")
     *
     * @ORM\Column(
     *     name = "object_type",
     *     type = "smallint",
     *     nullable = true,
     *     options = {
     *         "default" : 0,
     *         "unsigned" : true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.country_restriction.object_type.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^(0|1|2|3)$/",
     *     htmlPattern = "^(0|1|2|3)$",
     *     message = "catalog.country_restriction.object_type.invalid"
     * )
     *
     * @Groups({"view"})
     */
    private $objectType;

    /**
     * @var integer
     * @Type("string")
     *
     * @ORM\Column(
     *     name = "object_id",
     *     type = "bigint",
     *     nullable = true,
     *     options = {
     *         "default" : 0,
     *         "unsigned" : true
     *     }
     * )
     *
     * @Assert\NotBlank(
     *     message = "catalog.country_restriction.object_id.not_blank"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9]\d*$/",
     *     htmlPattern = "^[1-9]\d*$",
     *     message = "catalog.country_restriction.object_id.invalid"
     * )
     *
     * @Groups({"view"})
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "country_code",
     *     type = "string",
     *     length = 2,
     *     nullable = false,
     *     options = {
     *         "fixed": true
     *     }
     * )
     *
     * @Assert\NotBlank(message="catalog.country_restriction.country_code.not_blank")
     * @Assert\Regex(
     *     pattern = "/^[A-Z]{2}$/",
     *     htmlPattern = "^[A-Z]{2}$",
     *     message = "catalog.country_restriction.country_code.invalid"
     * )
     *
     * @Accessor(getter="getCountry")
     * @Groups({"view"})
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name = "remarks",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     *
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "catalog.country_restriction.remarks.length",
     *     maxMessage = "catalog.country_restriction.remarks.length"
     * )
     *
     * @Groups({"view"})
     */
    private $remarks;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"view"})
     */
    private $createdAt;

    public function __construct(array $data)
    {
        return $this->setData($data);
    }

    public function setData($data)
    {
        if (!empty($data)) {
            foreach ($data as $fieldName => $value) {
                $this->set($fieldName, $value);
            }
        }

        return $this;
    }

    public function get($fieldName)
    {
        $realFieldName = Inflector::camelize($fieldName);
        return $this->$realFieldName;
    }

    public function set($fieldName, $value)
    {
        $realFieldName = Inflector::camelize($fieldName);

        if (is_string($value) && $realFieldName == 'createdAt') {
            try {
                $value = new \DateTime($value, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                throw new InvalidArgumentException(
                    'catalog.country_restriction.created_at.invalid'
                );
            }
        }

        $this->$realFieldName = $value;
    }

    public static function getResponseGroups($group)
    {
        if (!isset(static::$responseGroups[$group])) {
            throw new \Exception("Unrecognized group $group");
        }
        return static::$responseGroups[$group];
    }

    public function getCountry()
    {
        return Country::instance()->getInfoByCode($this->countryCode);
    }
}
