<?php
namespace AM\CatalogService\Tests\FT\Publisher;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class PublisherCreateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publisher/PublisherCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublisherCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/publishers?access_token=test',
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider successParameterProvider
     */
    public function testPublisherCreateSuccess($parameters)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/publishers?access_token=test',
            $parameters
        );
        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];

        $this->assertEquals('legacy_identifier', $data['legacy_identifier']);
    }

    public function successParameterProvider()
    {
        return [
            'success' => [
                [
                    'name' => 'Publisher1',
                    'country_code' => 'US',
                    'legacy_identifier' => 'legacy_identifier'
                ]
            ]
        ];
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'length_of_legacy_identifier_too_long' => [
                [
                    'name' => 'length_of_legacy_identifier_too_long',
                    'country_code' => 'US',
                    //length = 101
                    'legacy_identifier' => 'qNf2djPxb5DL1E18JCjspSsJhSlELqCKjUJzUblVLsw1739eNO18gdY9r0o0YJ1FLiBXRavLK3oHgt1k3o1TBrqJzNVwjx2WWIObRfsdfsdf',
                ],
                400
            ]
        ];
    }
}