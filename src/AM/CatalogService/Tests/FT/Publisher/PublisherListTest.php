<?php

namespace AM\CatalogService\Tests\FT\Publisher;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class PublisherListTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publisher/PublisherList.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider queryStringProvider
     */
    public function testPublisherList($queryString, $limit = null, $checker = null)
    {
        if (!isset($queryString['access_token'])) {
            $queryString['access_token'] = 'test';
        }

        if ($limit != null && is_int($limit)) {
            $queryString['limit'] = $limit;
        }

        static::$client->request('GET', '/catalog/v1/publishers', $queryString);

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $data = $content['data'];

        if (isset($queryString['limit'])) {
            //test limit
            $this->assertLessThanOrEqual($limit, count($data));
        }

        if ($checker != null) {
            if (!method_exists($this, $checker)) {
                throw  new \Exception("$checker method does not exits in class ".get_class($this));
            }

            $this->$checker($data, $queryString);
        } else {
            $copyQueryString = $queryString;
            unset($copyQueryString['access_token'], $copyQueryString['limit']);

            if (!empty($copyQueryString)) {
                foreach ($copyQueryString as $key => $value) {
                    foreach ($data as $publisher) {
                        $this->assertEquals($value, $publisher[$key]);
                    }
                }
            }
        }
    }

    public function idIsString($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertInternalType('string', $value['id']);
        }
    }

    public function countryCodeIsObject($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertInternalType('array', $value['country']);
            $this->assertEquals(3, count($value['country']));
        }
    }

    public function hasKey($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertArrayHasKey('id', $value);
            $this->assertArrayHasKey('name', $value);
            $this->assertArrayHasKey('internal_name', $value);
            $this->assertArrayHasKey('description', $value);
            $this->assertArrayHasKey('slug', $value);
            $this->assertArrayHasKey('code', $value);
            $this->assertArrayHasKey('logo', $value);
            $this->assertArrayHasKey('remote_identifier', $value);
            $this->assertArrayHasKey('status', $value);
            $this->assertArrayHasKey('country', $value);
        }
    }

    public function filterByCountryCode($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertSame($queryString['country_code'], $value['country']['code']);
        }
    }

    public function filterByRemoteIdentifier($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertSame($queryString['remote_identifier'], $value['remote_identifier']);
        }
    }

    public function filterByStatus($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertEquals($queryString['status'], $value['status']);
        }
    }

    public function filterByNotPassStatus($data, $queryString)
    {
        foreach ($data as $value) {
            $this->assertEquals(1, $value['status']);
        }
    }

    public function filterByPage($data, $queryString)
    {
        $this->assertEquals(10, count($data));
    }

    public function queryStringProvider()
    {
        return array(
            'default' => array(
                [], 10
            ),
            'idIsString' => array(
                [], 1, 'idIsString'
            ),
            'countryCodeIsObject' => array(
                [], 1, 'countryCodeIsObject'
            ),
            'hasKey' => array(
                [], 1, 'hasKey'
            ),
            'filterByCountryCode' => array(
                ['country_code' => 'US'], 2, 'filterByCountryCode'
            ),
            'filterByRemoteIdentifier' => array(
                ['remote_identifier' => 'ezpublisher00'], 2, 'filterByRemoteIdentifier'
            ),
            'filterByStatus' => array(
                ['status' => 1], 5, 'filterByStatus'
            ),
            'filterByNotPassStatus' => array(
                [], 10, 'filterByNotPassStatus'
            ),
            'filterByPage' => array(
                ['offset' => 2], 0, 'filterByPage'
            )
        );
    }
}