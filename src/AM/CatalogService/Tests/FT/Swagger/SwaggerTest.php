<?php
namespace AM\CatalogService\Tests\FT\Swagger;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Tin Nguyen <tin.nguyen@audiencemedia.com>
 */
class SwaggerTest extends WebTestCase
{
    /**
     * @dataProvider successfulRequestProvider
     */
    public function testSwagger($method, $uri, $checkEmptyExamples)
    {
        $client = static::createClient();
        $client->request($method, $uri);
        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('swagger', $content);
        $this->assertArrayHasKey('paths', $content);

        if($checkEmptyExamples) {
            $this->assertEquals(true, $this->hasNoExamplesInResponses(isset($content['paths']) ? $content['paths'] : []));
        }
    }

    /**
     * @dataProvider failureRequestProvider
     */
    public function testFailure($method, $uri)
    {
        $client = static::createClient();
        $client->request($method, $uri);
        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEquals(200, $client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('error', $content);
        $this->assertArrayNotHasKey('swagger', $content);
    }

    private function hasNoExamplesInResponses($content)
    {
        $hasExamples = false;

        $walkRecursiveFindExamples = function ($content, $deep = 0, $forceToLoop = false) use (&$walkRecursiveFindExamples, &$hasExamples) {
            if(!$hasExamples && $deep <= 4) {
                foreach ($content as $k => $v) {
                    if(is_array($v)) {
                        if(4 == $deep && 'examples' === $k) {
                            $hasExamples = true;
                        } elseif($deep < 2) {
                            $walkRecursiveFindExamples($v, ($deep + 1));
                        } elseif($forceToLoop || (2 == $deep && 'responses' === $k)) {
                            $walkRecursiveFindExamples($v, ($deep + 1), true);
                        }
                    }
                }
            }
        };
        $walkRecursiveFindExamples($content);

        return !$hasExamples;
    }

    public function successfulRequestProvider()
    {
        return array(
            array('GET', '/catalog/v1/swagger?key=08d80623f02ec16f3ba84af4f2480eb0', false),
            array('GET', '/catalog/v1/swagger?key=08d80623f02ec16f3ba84af4f2480eb0&remove_examples=1', true),
            array('GET', '/catalog/v1/swagger?key=08d80623f02ec16f3ba84af4f2480eb0&remove_examples=true', true),
            array('GET', '/catalog/v1/swagger?key=08d80623f02ec16f3ba84af4f2480eb0&remove_examples=y', true),
        );
    } 

    public function failureRequestProvider()
    {
        return array(
            array('GET', '/catalog/v1/swagger'),
            array('GET', '/catalog/v1/swagger?key=123'),
        );
    } 
}