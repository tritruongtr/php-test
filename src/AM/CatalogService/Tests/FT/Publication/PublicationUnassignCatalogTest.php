<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Can Ly <can.ly@audiencemedia.com>
 */
class PublicationUnassignCatalogTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationUnassignCatalog.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationUnassignCatalogFailure($pubId, $catalogId, $statusCode, $parameter = [])
    {
        static::$client->request(
            'DELETE',
            "/catalog/v1/publications/$pubId/unassign/catalog/$catalogId?access_token=test",
            $parameter
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationUpdateAssignedCatalogStatusSuccess()
    {
        $catalogPublication = ["remarks" => "note here"];

        static::$client->request(
            'DELETE',
            '/catalog/v1/publications/1/unassign/catalog/1?access_token=test',
            $catalogPublication
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $em = static::$container->get('doctrine')->getEntityManager();
        $query = $em->createQuery(
            "SELECT cp
             FROM AM\CatalogService\Domain\CatalogPublication\CatalogPublication cp
             WHERE cp.publication = 1
             AND cp.catalog = 1
            "
        );
        $cp = $query->getResult();
        $this->assertSame($catalogPublication['remarks'], $cp[0]->get('remarks'));
        $this->assertSame(0, $cp[0]->get('status'));

        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $data = $content['data'];
        $this->assertEquals(0, $data['status']);
        $this->assertEquals($catalogPublication['remarks'], $data['remarks']);

    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                'asd', 'asd', 404
            ],
            'not_found_publication_1' => [
                'asd', 1, 404
            ],
            'not_found_publication_2' => [
                2, 1, 404
            ],
            'not_found_catalog_1' => [
                1, 'ads', 404
            ],
            'not_found_catalog_2' => [
                1, 2, 404
            ],
            'maximum_character_remarks_1' => [
                1, 1, 400,
                ['remarks' => "Tha;sldk !@#4 , ' ~ ,./ )(*^ sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;slda;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;256"]
            ],
            'invalid_modified_by_1' => [
                1, 1, 400, ['modified_by' => "Tha;"]
            ]
        ];
    }
}