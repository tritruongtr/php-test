<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublicationCountryRestrictionUpdateTest extends WebTestCase
{
    const API_ENDPOINT = '/catalog/v1/publications/%s/country_restrictions?access_token=test';
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationCountryRestrictionUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function failureParameterProvider()
    {
        return [
            'not_found_publication_100' => [
                100,
                [],
                404
            ],
            'null_country_code' => [
                1,
                [],
                400
            ],
            'not_allow_object_type_1' => [
                1,
                ["country_code" => "ZZ", "object_type" => 3],
                400
            ],
            'not_allow_object_type_2' => [
                1,
                [["country_code" => "VN", "object_type" => 3], ["country_code" => "US", "object_type" => 3]],
                400
            ],
            'not_allow_object_type_3' => [
                1,
                [["country_code" => "TH"], ["country_code" => "US", "object_type" => 3]],
                400
            ],
            'not_allow_object_type_4' => [
                1,
                [["country_code" => "TH", "object_type" => 3], ["country_code" => "US"]],
                400
            ],
            'not_allow_remarks_1' => [
                1,
                ["country_code" => "TH", "remarks" => "abcs"],
                400
            ],
            'not_allow_remarks_2' => [
                1,
                [["country_code" => "TH", "remarks" => "abcs"], ["country_code" => "US"]],
                400
            ],
            'not_allow_remarks_3' => [
                1,
                [["country_code" => "TH"], ["country_code" => "US", "remarks" => "abcs"]],
                400
            ],
            'not_allow_remarks_4' => [
                1,
                [["country_code" => "TH", "remarks" => "abcs"], ["country_code" => "US", "remarks" => "abcs"]],
                400
            ],
            'not_allow_object_id_1' => [
                1,
                ["country_code" => "US", "object_id" => 1],
                400
            ],
            'not_allow_object_id_2' => [
                1,
                [["country_code" => "TH"], ["country_code" => "US", "object_id" => 1]],
                400
            ],
            'not_allow_object_id_3' => [
                1,
                [["country_code" => "TH", "object_id" => 1], ["country_code" => "US"]],
                400
            ],
            'not_allow_object_id_4' => [
                1,
                [["country_code" => "TH", "object_id" => 1], ["country_code" => "US", "object_id" => 1]],
                400
            ],
            'not_allow_single_country_1' => [
                1,
                ["country_code" => "TH"],
                400
            ]
        ];
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationCountryRestrictionUpdateFailure($id, $parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, $id),
            $parameters
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationCountryRestrictionUpdateNotFound()
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, 10)
        );

        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }


    public function testPublicationCountryRestrictionMultiUpdateSuccess()
    {
        $countryRestrictions = array(
            [
                'country_code' => 'US'
            ],
            [
                'country_code' => 'TH'
            ],
            [
                'country_code' => 'VN'
            ]
        );

        $resultCountry =
            [
                [
                    "country_code" => array(
                        "format" => "ISO 3166-1 alpha-2",
                        "name" => "United States",
                        "code" => "US"
                    )
                ],
                [
                    "country_code" => array(
                        "format" => "ISO 3166-1 alpha-2",
                        "name" => "Thailand",
                        "code" => "TH"
                    )
                ],
                [
                    "country_code" => array(
                        "format" => "ISO 3166-1 alpha-2",
                        "name" => "Vietnam",
                        "code" => "VN"
                    )
                ],
            ];

        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, 1),
            $countryRestrictions
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        foreach ($data as $key => $value) {
            $this->assertEquals(
                1,
                $value['object_id']
            );
            $this->assertEquals(
                3,
                $value['object_type']
            );
            $this->assertEquals(
                $resultCountry[$key]['country_code']['format'],
                $value['country_code']['format']
            );
            $this->assertEquals(
                $resultCountry[$key]['country_code']['name'],
                $value['country_code']['name']
            );
            $this->assertEquals(
                $resultCountry[$key]['country_code']['code'],
                $value['country_code']['code']
            );
            $this->assertLessThanOrEqual(
                time(),
                strtotime($value['created_at'])
            );
        }
    }
}