<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationAssignCatalogTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationAssignCatalog.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationAssignCatalogFailure($pubId, $catalogId, $parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            "/catalog/v1/publications/$pubId/assign/catalog/$catalogId?access_token=test",
            $parameters
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationAssignCatalogSuccess()
    {
        for ($i = 0; $i < 3; $i++) {
            static::$client->request(
                'POST',
                '/catalog/v1/publications/1/assign/catalog/1?access_token=test',
                [
                    'created_by' => 14
                ]
            );

            $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        }

        $em = static::$container->get('doctrine')->getEntityManager();
        $query = $em->createQuery(
            "SELECT cp
             FROM AM\CatalogService\Domain\CatalogPublication\CatalogPublication cp
             WHERE cp.publication = 1
             AND cp.catalog = 1
             AND cp.status = 1
            "
        );
        $result = $query->getResult();
        $this->assertSame(1, count($result));
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                'asd', 'asd', [], 400
            ],
            'invalid_created_by' => [
                'asd', 'asd', ['created_by' => 'asdf'], 400
            ],
            'not_found_publication_1' => [
                'asd', 1, ['created_by' => 14], 404
            ],
            'not_found_publication_2' => [
                2, 1, ['created_by' => 14], 404
            ],
            'not_found_catalog_1' => [
                1, 'ads', ['created_by' => 14], 404
            ],
            'not_found_catalog_2' => [
                1, 2, ['created_by' => 14], 404
            ],
        ];
    }
}