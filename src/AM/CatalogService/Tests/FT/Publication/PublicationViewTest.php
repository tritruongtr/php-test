<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationViewTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationView.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider NotExistIdProvider
     */
    public function testPublicationViewFailure($id, $statusCode)
    {
        static::$client->request(
            'GET',
            "/catalog/v1/publications/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationViewSuccess()
    {
        static::$client->request(
            'GET',
            '/catalog/v1/publications/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
        $this->assertSame((int) $data['publisher_id'], $data['publisher_id']);
        $this->assertEquals('pub1', $data['name']);
        $this->assertSame((int) $data['id'], $data['id']);

        if (isset($data['created_by'])) {
            $this->assertSame((int) $data['created_by'], $data['created_by']);
        }

        if (isset($data['modified_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }

        $this->assertArrayHasKey('issues', $data);
        $issues = $data['issues'];
        $this->assertSame(3, count($issues));

        $latestCoverDate = 0;
        foreach ($issues as $issue) {
            if (strtotime($issue['cover_date']) >= $latestCoverDate) {
                $latestCoverDate = strtotime($issue['cover_date']);
                $expectLatestIssue = $issue;
            }
            $this->assertSame(1, $issue['publication_id']);
            $this->assertSame(1, $issue['status']);
        }

        $this->assertArrayHasKey('latest_issue', $data);
        $actualLatestIssue = $data['latest_issue'];
        $this->assertSame($expectLatestIssue['id'], $actualLatestIssue['id']);
        $this->assertSame($expectLatestIssue['publication_id'], $actualLatestIssue['publication_id']);
        $this->assertSame(1, $actualLatestIssue['status']);
    }

    public function NotExistIdProvider()
    {
        return [
            'publication_not_found_1' => [
                'asd', 404
            ],
            'publication_not_found_2' => [
                2, 404
            ]
        ];
    }
}