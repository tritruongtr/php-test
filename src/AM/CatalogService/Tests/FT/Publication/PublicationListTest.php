<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationListTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationList.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider queryStringProvider
     */
    public function testPublicationList($queryString, $limit = null, $checker = null)
    {
        if (!isset($queryString['access_token'])) {
            $queryString['access_token'] = 'test';
        }

        if ($limit !== null && is_int($limit)) {
            $queryString['limit'] = $limit;
        }

        static::$client->request(
            'GET',
            '/catalog/v1/publications',
            $queryString
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $data = $content['data'];

        if (isset($queryString['limit'])) {
            $this->assertLessThanOrEqual($limit, count($data));
        }

        if ($checker !== null) {
            if (!method_exists($this, $checker)) {
                throw new \Exception("$checker method does not exist in class " . get_class($this));
            }

            $this->$checker($data, $queryString);
        } else {
            $copiedQueryString = $queryString;
            unset($copiedQueryString['access_token'], $copiedQueryString['limit']);

            if (!empty($copiedQueryString)) {
                foreach ($copiedQueryString as $key => $value) {
                    foreach ($data as $publication) {
                        $this->assertEquals(
                            $value,
                            $publication[$key]
                        );
                    }
                }
            }
        }
    }

    public function filterByPublisher($data, $queryString)
    {
        $publisherId = $queryString['publisher_id'];
        foreach ($data as $publication) {
            $this->assertSame($publisherId, $publication['publisher_id']);
            $this->assertSame(1, $publication['status']);
        }
    }

    public function filterByDefaultStatus($data, $queryString)
    {
        foreach ($data as $publication) {
            $this->assertSame(1, $publication['status']);
        }
    }

    public function filterByContentRatingRange($data, $queryString)
    {
        $expectRange = explode('-', $queryString['content_rating']);
        list($fromContentRating, $toContentRating) = $expectRange;

        foreach ($data as $publication) {
            $actualContentRating = $publication['content_rating'];
            $this->assertGreaterThanOrEqual($fromContentRating, $actualContentRating);
            $this->assertLessThanOrEqual($toContentRating, $actualContentRating);
            $this->assertSame(1, $publication['status']);
        }
    }

    public function filterByListOfId($data, $queryString)
    {
        $expectIDs = explode(',', $queryString['id']);

        foreach ($data as $publication) {
            $this->assertContains($publication['id'], $expectIDs);
            $this->assertSame(1, $publication['status']);
        }
    }

    public function queryStringProvider()
    {
        return [
            'default' => [
                [],
                101,
                'filterByDefaultStatus'
            ],
            'limit_12_items' => [
                [],
                12
            ],
            'filter_by_publisher_2' => [
                ['publisher_id' => 2],
                10,
                'filterByPublisher'
            ],
            'filter_by_status' => [
                ['status' => 0]
            ],
            'filter_by_specific_content_rating' => [
                ['content_rating' => 50]
            ],
            'filter_by_content_rating_range' => [
                ['content_rating' => '15-30'],
                10,
                'filterByContentRatingRange'
            ],
            'filter_by_list_id' => [
                ['id' => '3,6,7,9,10'],
                10,
                'filterByListOfId'
            ]
        ];
    }
}