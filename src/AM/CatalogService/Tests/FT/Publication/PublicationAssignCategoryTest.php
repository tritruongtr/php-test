<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationAssignCategoryTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationAssignCategory.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationAssignCategoryFailure($pubId, $categoryId, $statusCode)
    {
        static::$client->request(
            'POST',
            "/catalog/v1/publications/$pubId/assign/category/$categoryId?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationAssignCategorySuccess()
    {
        for ($i = 0; $i < 3; $i++) {
            static::$client->request(
                'POST',
                '/catalog/v1/publications/1/assign/category/1?access_token=test'
            );

            $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        }

        $em = static::$container->get('doctrine')->getEntityManager();
        $query = $em->createQuery(
            "SELECT pc
             FROM AM\CatalogService\Domain\PublicationCategory\PublicationCategory pc
             WHERE pc.publication = 1
             AND pc.category = 1
            "
        );
        $result = $query->getResult();
        $this->assertSame(1, count($result));
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                'asd', 'asd', 404
            ],
            'not_found_publication_1' => [
                'asd', 1, 404
            ],
            'not_found_publication_2' => [
                2, 1, 404
            ],
            'not_found_category_1' => [
                1, 'ads', 404
            ],
            'not_found_category_2' => [
                1, 2, 404
            ]
        ];
    }
}