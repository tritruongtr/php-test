<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationDeleteTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationDelete.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureIdProvider
     */
    public function testPublicationDeleteFailure($id, $statusCode)
    {
        static::$client->request(
            'DELETE',
            "/catalog/v1/publications/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationDeleteSuccess()
    {
        static::$client->request(
            'DELETE',
            '/catalog/v1/publications/2?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $currentIssue = $content['data'];

        static::$client->request(
            'GET',
            '/catalog/v1/publications/2?access_token=test'
        );

        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('error', $content);
    }

    public function failureIdProvider()
    {
        return [
            'publication_not_found_1' => [
                'asd', 404
            ],
            'publication_not_found_2' => [
                101, 404
            ],
            'issue_exists' => [
                1, 400
            ]
        ];
    }
}