<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationUpdateAssignedCatalogStatusTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationUpdateAssignedCatalogStatus.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationUpdateAssignedCatalogStatusFailure($pubId, $catalogId, $parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            "/catalog/v1/publications/$pubId/update_status/catalog/$catalogId?access_token=test",
            $parameters
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationUpdateAssignedCatalogStatusSuccess()
    {
        static::$client->request(
            'PUT',
            '/catalog/v1/publications/1/update_status/catalog/1?access_token=test',
            [
                'modified_by' => 100,
                'status' => 0
            ]
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());

        $em = static::$container->get('doctrine')->getEntityManager();
        $query = $em->createQuery(
            "SELECT cp
             FROM AM\CatalogService\Domain\CatalogPublication\CatalogPublication cp
             WHERE cp.publication = 1
             AND cp.catalog = 1
            "
        );
        $cp = $query->getSingleResult();
        $this->assertSame(100, (int) $cp->get('modified_by'));
        $this->assertSame(0, (int) $cp->get('status'));
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                'asd', 'asd', [], 404
            ],
            'not_found_publication_1' => [
                'asd', 1, [], 404
            ],
            'not_found_publication_2' => [
                3, 1, [], 404
            ],
            'not_found_catalog_1' => [
                1, 'ads', [], 404
            ],
            'not_found_catalog_2' => [
                1, 2, [], 404
            ],
            'not_assign_publication_to_catalog' => [
                2, 1, [], 404
            ],
            'missing_modified_by' => [
                1, 1, [], 400
            ],
            'invalid_status' => [
                1, 1, ['modified_by' => 104, 'status' => 2], 400
            ]
        ];
    }
}