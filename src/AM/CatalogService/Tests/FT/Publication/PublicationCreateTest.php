<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationCreateTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/publications?access_token=test';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationCreateSuccess()
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            [
                'name' => 'xxx',
                'frequency' => 'daily',
                'legacy_identifier' => 'legacy',
                'country_code' => 'US',
                'language_code' => 'us',
                'locale_code' => 'eng',
                'publisher_id' => 1,
                'content_rating' => 15,
                'remote_identifier' => 'remote identifier',
                'created_by' => 14,
                'site_id' => 1,
                'status' => 1,
                'type' => 3,
                'no_of_issues' => 134,
                'slug' => 'asdf',
                'code' => 'asdfasdf',
                'allow_xml' => 0,
                'allow_pdf' => 0
            ]
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(1, $data['publisher_id']);
        $this->assertEquals('xxx', $data['name']);
        $this->assertLessThanOrEqual(time(), strtotime($data['created_at']));
        $this->assertEquals($data['created_at'], $data['modified_at']);
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'not_exist_publisher_1' => [
                [
                    'publisher_id' => 'asdf'
                ],
                404
            ],
            'not_exist_publisher_2' => [
                [
                    'publisher_id' => 14
                ],
                404
            ],
            'input_all_required_fields' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'xxx',
                    'legacy_identifier' => 'xxx',
                    'country_code' => 'xxx',
                    'language_code' => 'xxx',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 'xxx',
                    'remote_identifier' => 'xxx',
                    'created_by' => 'xxx',
                    'site_id' => 'xxx',
                    'status' => 'xxx',
                    'type' => 'xxx',
                    'no_of_issues' => 'xxx'
                ],
                400
            ],
            'length_of_name_too_long' => [
                [
                    'name' => 'adsjhfkajsdhfklasjdflaksdjflkasdjfaklsdjflaksjflk;asjflk;ajsdl;fjaskl;djf;lasdjfkl;asjdkl;fjasd;kljf;lasdjf;klasjlfjaskdljflk;asdjfkl;asdjf;kljasdl;kfjas;lkdjfkl;asdjfk;lasjdkfl;jasdkl;fjklas;djfkl;asdjfl;ajsdklf;jasdklfjaskl;djfkl;asdjfkl;asjdfkl;ajsdklfjask',
                    'frequency' => 'xxx',
                    'legacy_identifier' => 'xxx',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'length_of_frequency_too_long' => [
                [
                    'name' => 'xxx',
                    'frequency' => '123456789123451231424',
                    'legacy_identifier' => 'xxx',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'length_of_legacy_identifier_too_long' => [
                [
                    'name' => 'length_of_legacy_identifier_too_long',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'kjsdhfklajsdhflkasjhfklasjhdfklajsdhakjsdhfkajshdfkljahsdlfkhasdklfhaskdfaksjdhfkasljhdfklashdlfhasdkjlfhkjla',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 3,
                    'no_of_issues' => 134,
                    'slug' => 'asdf',
                    'code' => 'asdfasdf',
                    'allow_xml' => 0,
                    'allow_pdf' => 0
                ],
                400
            ],
            'length_of_internal_name_too_long' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'abc',
                    'internal_name' => 'adsjhfkajsdhfklasjdflaksdjflkasdjfaklsdjflaksjflk;asjflk;ajsdl;fjaskl;djf;lasdjfkl;asjdkl;fjasd;kljf;lasdjf;klasjlfjaskdljflk;asdjfkl;asdjf;kljasdl;kfjas;lkdjfkl;asdjfk;lasjdkfl;jasdkl;fjklas;djfkl;asdjfl;ajsdklf;jasdklfjaskl;djfkl;asdjfkl;asjdfkl;ajsdklfjask',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_country_code' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'ABC',
                    'language_code' => 'us',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_language_code' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'US',
                    'locale_code' => 'xxx',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_locale_code' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => '123abc',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_content_rating' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 100,
                    'remote_identifier' => 'xxx',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'length_of_remote_identifier_too_long' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'akjsdhfklajsdhflkasjhfklasjhdfklajsdhakjsdhfkajshdfkljahsdlfkhasdklfhaskdfaksjdhfkasljhdfklashdlfhasdkjlfhkjla',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_created_by' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 'xxx',
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_site_id' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 14,
                    'site_id' => 'xxx',
                    'status' => 1,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_status' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 2,
                    'type' => 1,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_type' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 4,
                    'no_of_issues' => 123
                ],
                400
            ],
            'invalid_no_of_issues' => [
                [
                    'name' => 'xxx',
                    'frequency' => 'daily',
                    'legacy_identifier' => 'legacy',
                    'country_code' => 'US',
                    'language_code' => 'us',
                    'locale_code' => 'eng',
                    'publisher_id' => 1,
                    'content_rating' => 15,
                    'remote_identifier' => 'remote identifier',
                    'created_by' => 14,
                    'site_id' => 1,
                    'status' => 1,
                    'type' => 3,
                    'no_of_issues' => 'xxx'
                ],
                400
            ]
        ];
    }
}