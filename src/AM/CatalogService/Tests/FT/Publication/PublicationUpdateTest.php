<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;

    const API_ENDPOINT = '/catalog/v1/publications/%s?access_token=test';
    private static $apiEndPoint;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testPublicationUpdateFailure($id, $parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, $id),
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationUpdateSuccess()
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, 1),
            [
                'name' => 'pub updated',
                'frequency' => 'weekly',
                'modified_by' => 20,
                'legacy_identifier' => 'update_legacy'
            ]
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(1, $data['publisher_id']);
        $this->assertEquals('pub updated', $data['name']);
        $this->assertEquals('weekly', $data['frequency']);
        $this->assertLessThanOrEqual(time(), strtotime($data['modified_at']));
        $this->assertEquals(20, $data['modified_by']);
        $this->assertEquals('update_legacy', $data['legacy_identifier']);
    }

    public function failureParameterProvider()
    {
        return [
            'not_exist_publication_1' => [
                'asdf',
                [],
                404
            ],
            'not_exist_publication_2' => [
                2,
                [],
                404
            ],
            'not_exist_publisher_2' => [
                1,
                [
                    'publisher_id' => 2
                ],
                404
            ],
            'not_exist_publisher_2' => [
                1,
                [
                    'publisher_id' => 2
                ],
                404
            ],
            'input_all_required_fields' => [
                1,
                [
                    'modified_by' => 'xxx'
                ],
                400
            ],
            'length_of_name_too_long' => [
                1,
                [
                    'name' => 'adsjhfkajsdhfklasjdflaksdjflkasdjfaklsdjflaksjflk;asjflk;ajsdl;fjaskl;djf;lasdjfkl;asjdkl;fjasd;kljf;lasdjf;klasjlfjaskdljflk;asdjfkl;asdjf;kljasdl;kfjas;lkdjfkl;asdjfk;lasjdkfl;jasdkl;fjklas;djfkl;asdjfl;ajsdklf;jasdklfjaskl;djfkl;asdjfkl;asjdfkl;ajsdklfjask',
                    'modified_by' => 14
                ],
                400
            ],
            'length_of_frequency_too_long' => [
                1,
                [
                    'modified_by' => 14,
                    'frequency' => '12345678912345asdfasdf'
                ],
                400
            ],
            'length_of_legacy_identifier_too_long' => [
                1,
                [
                    'legacy_identifier' => 'adsjhfkajsdhfklasjdflaksdjflkasdjfaklsdjflaksjflk;asjflk;ajsdl;fjaskl;djf;lasdjfkl;asjdkl;fjasd;kljf;lasdjf;klasjlfjaskdljflk;asdjfkl;asdjf;kljasdl;kfjas;lkdjfkl;asdjfk;lasjdkfl;jasdkl;fjklas;djfkl;asdjfl;ajsdklf;jasdklfjaskl;djfkl;asdjfkl;asjdfkl;ajsdklfjask',
                    'modified_by' => 14
                ],
                400
            ],
            'length_of_internal_name_too_long' => [
                1,
                [
                    'internal_name' => 'adsjhfkajsdhfklasjdflaksdjflkasdjfaklsdjflaksjflk;asjflk;ajsdl;fjaskl;djf;lasdjfkl;asjdkl;fjasd;kljf;lasdjf;klasjlfjaskdljflk;asdjfkl;asdjf;kljasdl;kfjas;lkdjfkl;asdjfk;lasjdkfl;jasdkl;fjklas;djfkl;asdjfl;ajsdklf;jasdklfjaskl;djfkl;asdjfkl;asjdfkl;ajsdklfjask',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_country_code' => [
                1,
                [
                    'country_code' => 'ABC',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_language_code' => [
                1,
                [
                    'language_code' => 'US',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_locale_code' => [
                1,
                [
                    'locale_code' => '123abc',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_content_rating' => [
                1,
                [
                    'content_rating' => 100,
                    'modified_by' => 14
                ],
                400
            ],
            'length_of_remote_identifier_too_long' => [
                1,
                [
                    'remote_identifier' => 'akjsdhfklajsdhflkasjhfklasjhdfklajsdhakjsdhfkajshdfkljahsdlfkhasdklfhaskdfaksjdhfkasljhdfklashdlfhasdkjlfhkjla',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_created_by' => [
                1,
                [
                    'created_by' => 'xxx',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_site_id' => [
                1,
                [
                    'site_id' => 'xxx',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_status' => [
                1,
                [
                    'status' => 2,
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_type' => [
                1,
                [
                    'type' => 4,
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_no_of_issues' => [
                1,
                [
                    'no_of_issues' => 'xxx',
                    'modified_by' => 14
                ],
                400
            ]
        ];
    }
}