<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class PublicationGetLatestIssueTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Publication/PublicationGetLatestIssue.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider NotExistIdProvider
     */
    public function testPublicationGetLatestIssueFailure($id, $statusCode)
    {
        static::$client->request(
            'GET',
            "/catalog/v1/publications/$id/latest_issue?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testPublicationGetLatestIssueSuccess()
    {
        $em = static::$container->get('doctrine')->getEntityManager();
        $query = $em->createQuery(
            "SELECT i
             FROM AM\CatalogService\Domain\Issue\Issue i
             JOIN AM\CatalogService\Domain\Publication\Publication p
             WHERE p.id = 1
             AND i.status = 1
             ORDER BY i.id DESC
            "
        );
        $query->setMaxResults(1);
        $expectLatestIssue = $query->getSingleResult();

        static::$client->request(
            'GET',
            '/catalog/v1/publications/1/latest_issue?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $actualLatestIssue = $content['data'];
        $this->assertSame((int) $expectLatestIssue->get('id'), $actualLatestIssue['id']);
    }

    public function NotExistIdProvider()
    {
        return [
            'publication_not_found_1' => [
                'asd', 404
            ],
            'publication_not_found_2' => [
                2, 404
            ]
        ];
    }
}