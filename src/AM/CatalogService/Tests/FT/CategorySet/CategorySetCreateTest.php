<?php
namespace AM\CatalogService\Tests\FT\CategorySet;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Ca Pham <ca.pham@audiencemedia.com>
*/
class CategorySetCreateTest extends WebTestCase
{
	public static $container;
	public static $client;

	public static function setUpBeforeClass()
	{
		self::bootKernel();
		static::$container = static::$kernel->getContainer();

		// $files = [__DIR__ . '/../../DataFixtures/ORM/CategorySet/CategorySetCreate.yml'];
		// $manager = static::$container->get('h4cc_alice_fixtures.manager');
		// $objects = $manager->loadFiles($files, 'yaml');
		// $manager->persist($objects, true);

		static::$client = static::createClient();
	}

	/**
	 * @dataProvider successParameterProvider
	 */
	public function testCategorySetCreateSuccess($parameters)
	{
		static::$client->request('POST', '/catalog/v1/category_sets?access_token=test', $parameters);

		$this->assertEquals(201, static::$client->getResponse()->getStatusCode());
	}

	public function successParameterProvider()
	{
		return [
			'success' => [
				[
					'name' => 'name test',
					'description' => 'description test',
					'type' => 1,
					'status' => 1
				]
			]
		];
	}
}