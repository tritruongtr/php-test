<?php

namespace AM\CatalogService\Tests\FT\CategorySet;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Ca Pham <ca.pham@audiencemedia.com>
 */
class CategorySetListTest extends WebTestCase
{
	public static $container;
	public static $client;

	public static function setUpBeforeClass()
	{
		self::bootKernel();
		static::$container = static::$kernel->getContainer();

		$files = [__DIR__ . '/../../DataFixtures/ORM/CategorySet/CategorySet.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
	}

	/**
	 * @dataProvider queryStringProvider
	 */
	public function testCategorySetList($queryString, $limit = null)
	{
		if (!isset($queryString['access_token'])) {
            $queryString['access_token'] = 'test';
        }

        if ($limit !== null && is_int($limit)) {
            $queryString['limit'] = $limit;
        }

        static::$client->request(
        	'GET',
        	'/catalog/v1/category_sets',
        	$queryString
        );
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
	}

	public function queryStringProvider()
	{
		return [
			'default' => [
				[]
			],
		];
	}
}