<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\Tests\FT\PublicationReadingModes;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublicationReadingModesTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/publication_reading_modes';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/PublicationReadingModes/PublicationReadingModesCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testPublicationCreateSuccess()
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            [
                'publication_id' => '1',
                'channel' => 'mobile',
                'mode' => 'text',
                'priority' => '1',
                'status' => '1',
                'created_by' => '1',
            ]
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
    }

    /**
     * @dataProvider dataValidation
     */
    public function testValidationCreate($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function dataValidation()
    {
        $data = array();

        $data['default'] = array([], 400);
        $data['invalid_publication_id'] = array([
            'publication_id' => 'xxx',
            'channel' => 'mobile',
            'mode' => 'text',
            'priority' => '1',
            'status' => '1',
            'created_by' => '1',
        ], 400);

        $data['invalid_chanel'] = array([
            'publication_id' => '1',
            'channel' => 'xxx',
            'mode' => 'text',
            'priority' => '1',
            'status' => '1',
            'created_by' => '1',
        ], 400);

        $data['invalid_mode'] = array([
            'publication_id' => '1',
            'channel' => 'mobile',
            'mode' => 'xxx',
            'priority' => '1',
            'status' => '1',
            'created_by' => '1',
        ], 400);

        $data['invalid_priority'] = array([
            'publication_id' => '1',
            'channel' => 'mobile',
            'mode' => 'text',
            'priority' => 'xxx',
            'status' => '1',
            'created_by' => '1',
        ], 400);

        $data['invalid_status'] = array([
            'publication_id' => '1',
            'channel' => 'mobile',
            'mode' => 'text',
            'priority' => '1',
            'status' => 'xxx',
            'created_by' => '1',
        ], 400);

        $data['invalid_created_by'] = array([
            'publication_id' => '1',
            'channel' => 'mobile',
            'mode' => 'text',
            'priority' => '1',
            'status' => '1',
            'created_by' => 'xxx',
        ], 400);
        return $data;
    }
}