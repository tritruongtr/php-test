<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\Tests\FT\PublicationReadingModes;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublicationReadingModesDeleteTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/publication_reading_modes';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/PublicationReadingModes/PublicationReadingModesDelete.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testPublicationDeleteSuccess()
    {
        static::$client->request(
            'DELETE',
            static::API_ENDPOINT.'/1',
            []
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider dataValidation
     */
    public function testValidationDelete($parameters, $statusCode)
    {
        static::$client->request(
            'DELETE',
            static::API_ENDPOINT.'/'.$parameters,
            []
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function dataValidation()
    {
        $data = array();

        $data['invalid_id'] = array('abc', 404);
        $data['invalid_exist_id'] = array(100, 404);
        return $data;
    }
}