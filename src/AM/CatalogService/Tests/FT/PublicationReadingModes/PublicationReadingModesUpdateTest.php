<?php
/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */

namespace AM\CatalogService\Tests\FT\PublicationReadingModes;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublicationReadingModesUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/publication_reading_modes';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/PublicationReadingModes/PublicationReadingModesUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testPublicationUpdateSuccess()
    {
        static::$client->request(
            'PUT',
            static::API_ENDPOINT.'/1',
            [
                'publication_id' => '1',
                'channel' => 'tablet',
                'mode' => 'text',
                'priority' => '1',
                'status' => '1',
                'modified_by' => '2',
            ]
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $this->assertEquals($content['data']['channel'], 'tablet');
    }

    /**
     * @dataProvider dataValidation
     */
    public function testValidationUpdate($parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            static::API_ENDPOINT.'/1',
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function dataValidation()
    {
        $data = array();

        $data['invalid_publication_id'] = array([
            'publication_id' => 'xxx',
            'modified_by' => '2'
        ], 404);

        $data['invalid_chanel'] = array([
            'channel' => 'xxx',
            'modified_by' => '2'
        ], 400);

        $data['invalid_mode'] = array([
            'mode' => 'xxx',
            'modified_by' => '2'
        ], 400);

        $data['invalid_priority'] = array([
            'priority' => 'xxx',
            'modified_by' => '2'
        ], 400);

        $data['invalid_status_id'] = array([
            'status' => 'xxx',
            'modified_by' => '2'
        ], 400);

        $data['invalid_modified_by'] = array([
            'modified_by' => 'xxx'
        ], 400);
        return $data;
    }
}