<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryRestrictionUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/country_restrictions';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CountryRestriction/CountryRestrictionUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCountryRestrictionUpdateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            static::API_ENDPOINT . '/1',
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testCountryRestrictionUpdateNotFound()
    {
        static::$client->request(
            'PUT',
            static::API_ENDPOINT . '/10',
            []
        );
        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }

    public function testCountryRestrictionUpdateSuccess()
    {
        static::$client->request(
            'PUT',
            static::API_ENDPOINT . '/1',
            [
                'country_code' => 'SG',
                'object_type' => 3,
                'object_id' => 3
            ]
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame('3', $data['object_id']);
        $this->assertSame(3, $data['object_type']);
        $this->assertSame('SG', $data['country']['code']);
    }

    public function failureParameterProvider()
    {
        return [
            'invalid_country_code' => [
                [
                    'country_code' => 'USA',
                    'object_type' => 3,
                    'object_id' => 3
                ],
                400
            ],
            'invalid_object_type' => [
                [
                    'country_code' => 'US',
                    'object_type' => 4,
                    'object_id' => 3
                ],
                400
            ],
            'invalid_object_id' => [
                [
                    'country_code' => 'US',
                    'object_type' => 3,
                    'object_id' => 0
                ],
                400
            ],
            'duplicated_object' => [
                [
                    'country_code' => 'US',
                    'object_type' => 2,
                    'object_id' => 2
                ],
                400
            ]
        ];
    }
}