<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryRestrictionDeleteTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/country_restrictions';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CountryRestriction/CountryRestrictionDelete.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testCountryRestrictionUpdateNotFound()
    {
        static::$client->request(
            'DELETE',
            static::API_ENDPOINT . '/10'
        );
        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }

    public function testCountryRestrictionDeleteSuccess()
    {
        static::$client->request(
            'DELETE',
            static::API_ENDPOINT . '/1'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(empty($data), true);

        static::$client->request(
            'GET',
            static::API_ENDPOINT . '/1'
        );

        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }
}