<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryRestrictionViewTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/country_restrictions';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CountryRestriction/CountryRestrictionView.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testCountryRestrictionViewNotFound()
    {
        static::$client->request(
            'DELETE',
            static::API_ENDPOINT . '/10'
        );
        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }

    public function testCountryRestrictionViewFound()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '/1'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame($data['id'] === '1', true);
        $this->assertSame($data['object_id'] === '1', true);
        $this->assertSame($data['object_type'] === 1, true);
        $this->assertSame($data['remarks'] === null, true);
        $this->assertSame(is_array($data['country']), true);
        $this->assertSame(isset($data['country']['format']), true);
        $this->assertSame(isset($data['country']['name']), true);
        $this->assertSame(isset($data['country']['code']), true);
    }
}