<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryRestrictionCreateTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/country_restrictions';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CountryRestriction/CountryRestrictionCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCountryRestrictionCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testCountryRestrictionCreateSuccess()
    {
        static::$client->request(
            'POST',
            static::API_ENDPOINT,
            [
                'country_code' => 'VN',
                'object_type' => 2,
                'object_id' => 2
            ]
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame('2', $data['object_id']);
        $this->assertSame(2, $data['object_type']);
        $this->assertSame('VN', $data['country']['code']);
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'input_all_required_fields' => [
                [
                    'country_code' => 'xxx',
                    'object_type' => 'xxx',
                    'object_id' => 'xxx'
                ],
                400
            ],
            'invalid_country_code' => [
                [
                    'country_code' => 'USA',
                    'object_type' => 3,
                    'object_id' => 3
                ],
                400
            ],
            'invalid_object_type' => [
                [
                    'country_code' => 'US',
                    'object_type' => 4,
                    'object_id' => 3
                ],
                400
            ],
            'invalid_object_id' => [
                [
                    'country_code' => 'US',
                    'object_type' => 3,
                    'object_id' => 0
                ],
                400
            ],
            'duplicated_object' => [
                [
                    'country_code' => 'US',
                    'object_type' => 1,
                    'object_id' => 1
                ],
                400
            ]
        ];
    }
}