<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryRestrictionListTest extends WebTestCase
{
    public static $container;
    public static $client;
    const API_ENDPOINT = '/catalog/v1/country_restrictions';

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CountryRestriction/CountryRestrictionList.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function testCountryRestrictionListNotFound()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?object_type=444'
        );
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(empty($data), true);
    }

    public function testCountryRestrictionListFound()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(count($data) == 10, true);
        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);
        }
    }

    public function testCountryRestrictionLimitResponse()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?limit=2'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(count($data) == 2, true);
        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);
        }
    }

    public function testCountryRestrictionLimitSortByIdDESCResponse()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?limit=2&sort=-id'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(count($data) == 2, true);
        $max = 1000000;
        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);

            $this->assertSame($countryRestriction['id'] < $max, true);
            $max = $countryRestriction['id'];
        }
    }

    public function testCountryRestrictionFilterByObjectType()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?object_type=1'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);

            $this->assertSame($countryRestriction['object_type'] === 1, true);
        }
    }

    public function testCountryRestrictionFilterByObjectId()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?object_id=1'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);

            $this->assertSame($countryRestriction['object_id'] === '1', true);
        }
    }

    public function testCountryRestrictionFilterByCountryCode()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?country_code=VN'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);

            $this->assertSame($countryRestriction['country']['code'] === 'VN', true);
        }
    }

    public function testCountryRestrictionFilterById()
    {
        static::$client->request(
            'GET',
            static::API_ENDPOINT . '?id=2'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertSame(count($data) == 1, true);

        foreach ($data as $countryRestriction) {
            $this->assertSame(isset($countryRestriction['id']), true);
            $this->assertSame(is_string($countryRestriction['id']), true);
            $this->assertSame(isset($countryRestriction['object_id']), true);
            $this->assertSame(is_string($countryRestriction['object_id']), true);
            $this->assertSame(isset($countryRestriction['object_type']), true);
            $this->assertSame(is_int($countryRestriction['object_type']), true);
            $this->assertSame(array_key_exists('remarks', $countryRestriction), true);
            $this->assertSame(is_array($countryRestriction['country']), true);
            $this->assertSame(isset($countryRestriction['country']['format']), true);
            $this->assertSame(isset($countryRestriction['country']['name']), true);
            $this->assertSame(isset($countryRestriction['country']['code']), true);

            $this->assertSame($countryRestriction['id'] === '2', true);
        }
    }
}