<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueListTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueList.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider queryStringProvider
     */
    public function testIssueList($queryString, $limit = null, $checker = null)
    {
        if (!isset($queryString['access_token'])) {
            $queryString['access_token'] = 'test';
        }

        if ($limit !== null && is_int($limit)) {
            $queryString['limit'] = $limit;
        }

        static::$client->request(
            'GET',
            '/catalog/v1/issues',
            $queryString
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $data = $content['data'];

        if (isset($queryString['limit'])) {
            $this->assertLessThanOrEqual($limit, count($data));
        }

        if ($checker !== null) {
            if (!method_exists($this, $checker)) {
                throw new \Exception("$checker method does not exist in class " . get_class($this));
            }

            $this->$checker($data, $queryString);
        } else {
            $copiedQueryString = $queryString;
            unset($copiedQueryString['access_token'], $copiedQueryString['limit']);
            foreach ($copiedQueryString as $key => $value) {
                foreach ($data as $issue) {
                    $this->assertEquals(
                        $value,
                        $issue[$key]
                    );
                }
            }
        }
    }

    public function filterByCoverDateRange($data, $queryString)
    {
        $fromDate = strtotime($queryString['from_date']);
        $toDate = strtotime($queryString['to_date']);
        foreach ($data as $issue) {
            $actualCoverDate = strtotime($issue['cover_date']);
            $this->assertGreaterThanOrEqual($fromDate, $actualCoverDate);
            $this->assertLessThanOrEqual($toDate, $actualCoverDate);
        }
    }

    public function filterByPublishDateRange($data, $queryString)
    {
        $fromDate = strtotime($queryString['from_date']);
        $toDate = strtotime($queryString['to_date']);
        foreach ($data as $issue) {
            $actualCoverDate = strtotime($issue['publish_date']);
            $this->assertGreaterThanOrEqual($fromDate, $actualCoverDate);
            $this->assertLessThanOrEqual($toDate, $actualCoverDate);
        }
    }

    public function filterByMultiPublication($data, $queryString)
    {
        $expectPubs = explode(',', $queryString['publication_id']);
        foreach ($data as $issue) {
            $actualPub = $issue['publication_id'];
            $this->assertContains($actualPub, $expectPubs);
        }
    }

    public function queryStringProvider()
    {
        return [
            'default' => [
                []
            ],
            'limit_12_items' => [
                [],
                12
            ],
            'filter_by_pub_13' => [
                ['publication_id' => 13]
            ],
            'filter_by_status_3' => [
                ['status' => 3]
            ],
            'filter_by_status_500' => [
                ['status' => 500]
            ],
            'filter_by_status_500_pub_14' => [
                [
                    'status' => 500,
                    'publication_id' => 14
                ]
            ],
            'filter_cover_date_range' => [
                [
                    'from_date' => '1986-10-13 18:06:02',
                    'to_date' => '1999-10-13 18:06:02'
                ],
                10,
                'filterByCoverDateRange'
            ],
            'filter_publish_date_range' => [
                [
                    'from_date' => '1986-10-13 18:06:02',
                    'to_date' => '1999-10-13 18:06:02',
                    'date_type' => 'publish_date'
                ],
                10,
                'filterByPublishDateRange'
            ],
            'filter_by_multi_pub' => [
                ['publication_id' => '1,2,3,4,5,6,7,8,9,10'],
                20,
                'filterByMultiPublication'
            ]
        ];
    }
}