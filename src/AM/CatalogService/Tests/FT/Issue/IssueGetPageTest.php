<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueGetPageTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueGetPage.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider NotExistIdProvider
     */
    public function testIssueViewFailure($id, $statusCode)
    {
        static::$client->request(
            'GET',
            "/catalog/v1/issues/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testIssueViewSuccess()
    {
        static::$client->request(
            'GET',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
        $this->assertSame((int) $data['publication_id'], $data['publication_id']);
        $this->assertEquals('issue1', $data['name']);
        $this->assertSame((int) $data['id'], $data['id']);

        if (isset($data['created_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }

        if (isset($data['modified_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }
    }

    public function NotExistIdProvider()
    {
        return [
            'issue_not_found_1' => [
                'asd', 404
            ],
            'issue_not_found_2' => [
                2, 404
            ]
        ];
    }
}