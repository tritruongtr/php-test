<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueDeleteTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueDelete.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider NotExistIdProvider
     */
    public function testIssueNotFound($id, $statusCode)
    {
        static::$client->request(
            'DELETE',
            "/catalog/v1/issues/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testIssueDeleteSuccess()
    {
        static::$client->request(
            'DELETE',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $currentIssue = $content['data'];

        static::$client->request(
            'GET',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('error', $content);
    }

    public function NotExistIdProvider()
    {
        return [
            'issue_not_found_1' => [
                'asd', 404
            ],
            'issue_not_found_2' => [
                2, 404
            ]
        ];
    }
}