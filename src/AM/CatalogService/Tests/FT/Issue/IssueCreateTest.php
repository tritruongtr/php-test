<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueCreateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testIssueCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/issues?access_token=test',
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider successParameterProvider
     */
    public function testIssueCreateSuccess($parameters)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/issues?access_token=test',
            $parameters
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
        $this->assertSame((int) $data['publication_id'], $data['publication_id']);
        $this->assertEquals('issue1', $data['name']);
        $this->assertSame((int) $data['id'], $data['id']);
        $this->assertEquals('legacy_identifier', $data['legacy_identifier']);

        if (isset($data['created_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }

        if (isset($data['modified_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }
    }

    public function successParameterProvider()
    {
        return [
            'success' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 0,
                    'preview' => 0,
                    'has_xml' => 0,
                    'has_pdf' => 0,
                    
                    'modified_by' => 14,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'legacy_identifier' => 'legacy_identifier',
                    'cover_price' => 1,
                    'cover_currency' => 'abc',
                    'no_of_pages' => 1
                ]
            ]
        ];
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'input_all_required_fields' => [
                [
                    'publication_id' => 'xxx',
                    'name' => 'xxx',
                    'status' => 'xxx',
                    'created_by' => 'xxx',
                    'modified_by' => 'xxx',
                    'file_path' => 'xxx',
                    'type' => 'xxx',
                    'preview' => 'xxx',
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 'xxx',
                    'has_pdf' => 'xxx'
                ],
                400
            ],
            'invalid_publication_id' => [
                [
                    'publication_id' => 'xxx',
                    'name' => 'issue1',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'length_of_name_too_long' => [
                [
                    'publication_id' => 1,
                    'name' => 'When using aaksdjfkdsaf;lksajfd;ljdsf;lajds;lfjdsa;lfkjdsa;lkdjf;lkdsajf;lksajf;lkajds;lkfjdsa;lkfjdsa;lkjf;ldsajf;lkjdsflk;sajdf;lkjdsf;lkjds;lkfjdsa;kfjsadf;ldsajf;lsadjf;lkdsajflk;dsajfkl;sajdkf;ljdsa;lkjfkdsa;ljfjkljdskf;ljdsajf;lkdsajf;lksajd;fathe HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'length_of_internal_name_too_long' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'internal_name' => 'When using aaksdjfkdsaf;lksajfd;ljdsf;lajds;lfjdsa;lfkjdsa;lkdjf;lkdsajf;lksajf;lkajds;lkfjdsa;lkfjdsa;lkjf;ldsajf;lkjdsflk;sajdf;lkjdsf;lkjds;lkfjdsa;kfjsadf;ldsajf;lsadjf;lkdsajflk;dsajfkl;sajdkf;ljdsa;lkjfkdsa;ljfjkljdskf;ljdsajf;lkdsajf;lksajd;fathe HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'length_of_remote_identifier_too_long' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    //length = 101
                    'remote_identifier' => '110nz2HnOWoDsURp6SyU4wDzKcLvyBRvF5NhD11wqOrlLupmeaSrGFQRDQn4oymL6npjKWrOe2NhqBKvXmpLe6IZsx7Uz8HCCl3Bi6f0',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'length_of_legacy_identifier_too_long' => [
                [
                    'publication_id' => 1,
                    'name' => 'length_of_legacy_identifier_too_long',
                    'remote_identifier' => 'xxx',
                    //length = 255
                    'legacy_identifier' => 'fff2djPxb5DL1E18JCjspSsJhSlELqCKjUJzUblVLsw17fff2djPxb5DL1E18JCjspSsJhSlELqCKjUJzUblVLsw1739eNO18gdY9r0o0YJ1FLiBXRavLK3oHgt1k3o1TBrqJzNVwjx2WWIObR39eNO18gdY9r0o0YJ1FLiBXRavLK3oHgt1k3o1TBrqJzNVwjx2WWIObRfff2djPxb5DL1E18JCjspSsJhSlELqCKjUJzUblVLsw1739eNO18gdY9r0o0YJ1FLiBXRavLK3oHgt1k3o1TBrqJzNVwjx2WWIObR',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'invalid_legacy_issue_id' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'legacy_issue_id' => 'asd',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'invalid_status' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 600,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'invalid_created_by' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 1,
                    'created_by' => 'asd',
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'invalid_type' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 7,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'input_all_required_fields_with_valid_value' => [
                [
                    'publication_id' => 100000,
                    'name' => 'issue1',
                    'status' => 1,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                404
            ],
            'cover_date_invalid' => [
                [
                    'publication_id' => 13,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'cover_date' => 'asd',
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'publish_date_invalid' => [
                [
                    'publication_id' => 13,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'modified_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 1,
                    'publish_date' => 'xxx',
                    'preview' => 0,
                    'slug' => 'xxx',
                    'code' => 'xxx',
                    'has_xml' => 0,
                    'has_pdf' => 0
                ],
                400
            ],
            'cover_price_not_integer' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 0,
                    'preview' => 0,
                    'has_xml' => 0,
                    'has_pdf' => 0,
                    'cover_price' => 'xxx',
                ],
                400
            ],
            'no_of_pages_not_integer' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 0,
                    'preview' => 0,
                    'has_xml' => 0,
                    'has_pdf' => 0,
                    'no_of_pages' => 'xxx',
                ],
                400
            ],
            'cover_currency_length' => [
                [
                    'publication_id' => 1,
                    'name' => 'issue1',
                    'status' => 3,
                    'created_by' => 14,
                    'file_path' => '/asdf',
                    'type' => 0,
                    'preview' => 0,
                    'has_xml' => 0,
                    'has_pdf' => 0,
                    'cover_currency' => 'xxxx',
                ],
                400
            ]

        ];
    }
}