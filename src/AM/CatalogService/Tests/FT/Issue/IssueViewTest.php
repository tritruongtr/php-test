<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueViewTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueView.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider notExistIdProvider
     */
    public function testIssueViewFailure($id, $statusCode)
    {
        static::$client->request(
            'GET',
            "/catalog/v1/issues/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testIssueViewSuccess()
    {
        static::$client->request(
            'GET',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
        $this->assertSame((int) $data['publication_id'], $data['publication_id']);
        $this->assertEquals('issue1', $data['name']);
        $this->assertSame((int) $data['id'], $data['id']);

        if (isset($data['created_by'])) {
            $this->assertSame((int) $data['created_by'], $data['created_by']);
        }

        if (isset($data['modified_by'])) {
            $this->assertSame((int) $data['modified_by'], $data['modified_by']);
        }
    }

    public function testViewIssueContent()
    {
        static::$client->request(
            'GET',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $data = $content['data'];
        $issueContent = $data['issue_content'];

        $this->assertArrayHasKey('id', $issueContent);
        $this->assertArrayHasKey('name', $issueContent);
        $this->assertArrayHasKey('cover_date', $issueContent);
        $this->assertArrayHasKey('published_date', $issueContent);
        $this->assertArrayHasKey('description', $issueContent);
        $this->assertArrayHasKey('designs', $issueContent);
        $this->assertArrayHasKey('channel', $issueContent);
        $this->assertArrayHasKey('previewable', $issueContent);
        $this->assertArrayHasKey('issue_no', $issueContent);
        $this->assertArrayHasKey('volume_no', $issueContent);


        $this->assertInternalType('array', $issueContent['designs']);
        foreach ($issueContent['designs'] as $key => $designs) {
            $this->assertInternalType('array', $designs);

            $this->assertArrayHasKey('id', $designs);
            $this->assertArrayHasKey('url', $designs);
            $this->assertArrayHasKey('version', $designs);
            $this->assertArrayHasKey('reader_version', $designs);
        }

        $this->assertInternalType('string', $issueContent['id']);

        //cover_date and published_date must be in unix timestamp (string)
        $this->assertInternalType('string', $issueContent['cover_date']);
        $this->assertTrue(is_numeric($issueContent['cover_date']));

        $this->assertInternalType('string', $issueContent['published_date']);
        $this->assertTrue(is_numeric($issueContent['published_date']));

        $this->assertEquals('tablet', $issueContent['channel']);
        $this->assertTrue(is_bool($issueContent['previewable']));

        if ($issueContent['issue_no'] != null) {
            $this->assertInternalType('string', $issueContent['issue_no']);
        }
        if ($issueContent['volume_no'] != null) {
            $this->assertInternalType('string', $issueContent['volume_no']);
        }
    }

    public function notExistIdProvider()
    {
        return [
            'issue_not_found_1' => [
                'asd', 404
            ],
            'issue_not_found_2' => [
                2, 404
            ]
        ];
    }
}