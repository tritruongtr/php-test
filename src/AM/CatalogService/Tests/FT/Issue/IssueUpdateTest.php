<?php
namespace AM\CatalogService\Tests\FT\Issue;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* @author Can Ly <can.ly@audiencemedia.com>
*/
class IssueUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Issue/IssueUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider NotExistIdProvider
     */
    public function testIssueNotFound($id, $statusCode)
    {
        static::$client->request(
            'PUT',
            "/catalog/v1/issues/$id?access_token=test"
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testIssueUpdateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            '/catalog/v1/issues/1?access_token=test',
            $parameters
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testIssueUpdateSuccess()
    {
        static::$client->request(
            'GET',
            '/catalog/v1/issues/1?access_token=test'
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $currentIssue = $content['data'];

        static::$client->request(
            'PUT',
            '/catalog/v1/issues/1?access_token=test',
            [
                'modified_by' => 14,
                'name' => 'issue_updated_1',
                'legacy_identifier' => 'legacy_identifier',
                'cover_price' => 1,
                'cover_currency' => 'abc',
                'no_of_pages' => 1
            ]
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $updatedIssue = $content['data'];

        $this->assertSame($currentIssue['created_by'], $updatedIssue['created_by']);
        $this->assertSame(14, $updatedIssue['modified_by']);
        $this->assertEquals('issue_updated_1', $updatedIssue['name']);
        $this->assertLessThanOrEqual(time(), strtotime($updatedIssue['modified_by']));
        $this->assertEquals('legacy_identifier', $updatedIssue['legacy_identifier']);
    }

    public function NotExistIdProvider()
    {
        return [
            'issue_not_found_1' => [
                'asd', 404
            ],
            'issue_not_found_2' => [
                2, 404
            ]
        ];
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'invalid_publication_id' => [
                [
                    'publication_id' => 'xxx',
                    'modified_by' => 14
                ],
                400
            ],
            'not_found_publication_id' => [
                [
                    'publication_id' => 4,
                    'modified_by' => 14
                ],
                404
            ],
            'length_of_name_too_long' => [
                [
                    'name' => 'When using aaksdjfkdsaf;lksajfd;ljdsf;lajds;lfjdsa;lfkjdsa;lkdjf;lkdsajf;lksajf;lkajds;lkfjdsa;lkfjdsa;lkjf;ldsajf;lkjdsflk;sajdf;lkjdsf;lkjds;lkfjdsa;kfjsadf;ldsajf;lsadjf;lkdsajflk;dsajfkl;sajdkf;ljdsa;lkjfkdsa;ljfjkljdskf;ljdsajf;lkdsajf;lksajd;fathe HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter',
                    'modified_by' => 14
                ],
                400
            ],
            'length_of_internal_name_too_long' => [
                [
                    'internal_name' => 'When using aaksdjfkdsaf;lksajfd;ljdsf;lajds;lfjdsa;lfkjdsa;lkdjf;lkdsajf;lksajf;lkajds;lkfjdsa;lkfjdsa;lkjf;ldsajf;lkjdsflk;sajdf;lkjdsf;lkjds;lkfjdsa;kfjsadf;ldsajf;lsadjf;lkdsajflk;dsajfkl;sajdkf;ljdsa;lkjfkdsa;ljfjkljdskf;ljdsajf;lkdsajf;lksajd;fathe HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter',
                    'modified_by' => 14
                ],
                400
            ],
            'length_of_remote_identifier_too_long' => [
                [
                    'remote_identifier' => 'askfdjaskdjfas;lkjflksdfjalskdjflksadjflksajdf;lkjdsaf;lkajds;lkfj;lkdsajf;lkjdsa;lkfjdsakfaalkdsjfksf',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_legacy_issue_id' => [
                [
                    'legacy_issue_id' => 'asd',
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_status' => [
                [
                    'status' => 600,
                    'modified_by' => 14
                ],
                400
            ],
            'invalid_modified_by' => [
                [
                    'modified_by' => 'asd'
                ],
                400
            ],
            'invalid_type' => [
                [
                    'modified_by' => 14,
                    'type' => 7
                ],
                400
            ],
            'cover_date_invalid' => [
                [
                    'modified_by' => 14,
                    'cover_date' => 'asd'
                ],
                400
            ],
            'publish_date_invalid' => [
                [
                    'modified_by' => 14,
                    'publish_date' => 'xxx'
                ],
                400
            ]
        ];
    }
}