<?php
namespace AM\CatalogService\Tests\FT\Category;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class AssigningCategoryTest extends WebTestCase
{
    public static $container;
    public static $client;
    public static $apiEndPoint = '/catalog/v1/categories/%s/publications';

    public function setUp()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();
        $files = [__DIR__ . '/../../DataFixtures/ORM/Category/AssigningCategory.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);
        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCategoryCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            sprintf(self::$apiEndPoint, 1),
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testCreateSuccess()
    {
        $parameters = [
            array('publication_id' => 1)
        ];

        static::$client->request(
            'POST',
            sprintf(self::$apiEndPoint, 1),
            $parameters
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];

        $this->assertEquals(0, $data[0]['root_parent_category_id']);
        $this->assertEquals(1, $data[0]['category_id']);
        $this->assertEquals(1, $data[0]['publication_id']);
    }

    public function testCreateSuccessWithParentCategory()
    {
        $parameters = [
                array('publication_id' => 2)
        ];

        static::$client->request(
            'POST',
            sprintf(self::$apiEndPoint, 3),
            $parameters
        );

        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];

        $this->assertEquals(2, $data[0]['root_parent_category_id']);
        $this->assertEquals(3, $data[0]['category_id']);
        $this->assertEquals(2, $data[0]['publication_id']);

        $em = static::$container->get('doctrine.orm.entity_manager');
        $publicationCategories = $em->getConnection()->fetchAll(
            'SELECT * FROM publications_categories pc where publication_id=2 and category_id=2'
        );
        $this->assertNotEmpty($publicationCategories);
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'invalid_publication_id' => [
                [
                     array('publication_id'=>5)
                ],
                404
            ],
            'miss_publication_id' => [
                [
                    array('test' => 5)
                ],
                400
            ]
        ];
    }
}