<?php
namespace AM\CatalogService\Tests\FT\Category;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class CategoryUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Category/CategoryUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider successParameterProvider
     */
    public function testCategoryUpdateSuccess($parameters)
    {
        static::$client->request(
            'PUT',
            '/catalog/v1/categories/1?access_token=test',
            $parameters
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        //present, method PUT's categories didn't return data
        $data = $content['data'];

        //$this->assertEquals('legacy_identifier', $data['legacy_identifier']);
    }

    public function successParameterProvider()
    {
        return [
            'success' => [
                [
                    'project_id' => 1,
                    'name' => 'category',
                    'slug' => 'zinio',
                    'parent_id' => 0,
                    'remote_identifier' => 1,
                    'status' => 1,
                    'legacy_identifier' => 'legacy_identifier'
                ]
            ]
        ];
    }

    /**
     * @dataProvider successParameterWithoutSlugProvider
     */
    public function testCategoryUpdateWithoutSlug($parameters)
    {
        static::$client->request(
            'PUT',
            '/catalog/v1/categories/1?access_token=test',
            $parameters
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
        $this->assertEquals('my_test_category', $data['slug']);
    }

    public function successParameterWithoutSlugProvider()
    {
        return [
            'success' => [
                [
                    'project_id' => 1,
                    'name' => 'My Test Category',
                    'slug' => '',
                    'parent_id' => 0,
                    'remote_identifier' => 1,
                    'status' => 1,
                    'legacy_identifier' => 'legacy_identifier'
                ]
            ]
        ];
    }
}