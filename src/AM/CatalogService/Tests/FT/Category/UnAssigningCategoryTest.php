<?php
namespace AM\CatalogService\Tests\FT\Category;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class UnAssigningCategoryTest extends WebTestCase
{
    public static $container;
    public static $client;
    public static $apiEndPoint = '/catalog/v1/categories/%s/publications';

    public function setUp()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();
        $files = [__DIR__ . '/../../DataFixtures/ORM/Category/UnAssigningCategory.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);
        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCategoryDeleteFailure($parameters, $statusCode)
    {
        static::$client->request(
            'DELETE',
            sprintf(self::$apiEndPoint, 1),
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testDeleteSuccess()
    {
        $parameters = [
            array('publication_id' => 1)
        ];

        static::$client->request(
            'DELETE',
            sprintf(self::$apiEndPoint, 1),
            $parameters
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
    }

    public function testDeleteSuccessWithChildCategory()
    {
        $parameters = [
                array('publication_id' => 2)
        ];

        static::$client->request(
            'DELETE',
            sprintf(self::$apiEndPoint, 2),
            $parameters
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());

        $em = static::$container->get('doctrine.orm.entity_manager');
        $publicationCategories = $em->getConnection()->fetchAll(
            'SELECT * FROM publications_categories pc where publication_id=2 and category_id=3'
        );
        $this->assertEmpty($publicationCategories);
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'invalid_publication_id' => [
                [
                     array('publication_id'=>5)
                ],
                404
            ],
            'miss_publication_id' => [
                [
                    array('test' => 5)
                ],
                400
            ]
        ];
    }
}