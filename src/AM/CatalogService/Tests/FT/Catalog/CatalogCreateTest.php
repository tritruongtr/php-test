<?php
namespace AM\CatalogService\Tests\FT\Catalog;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class CatalogCreateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Catalog/CatalogCreate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCatalogCreateFailure($parameters, $statusCode)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/catalogs?access_token=test',
            $parameters
        );
        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider successParameterProvider
     */
    public function testCategoryCreateSuccess($parameters)
    {
        static::$client->request(
            'POST',
            '/catalog/v1/catalogs?access_token=test',
            $parameters
        );
        $this->assertEquals(201, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];

        $this->assertEquals('legacy_identifier', $data['legacy_identifier']);
    }

    public function successParameterProvider()
    {
        return [
            'success' => [
                [
                    'name' => 'Catalog',
                    'legacy_identifier' => 'legacy_identifier'
                ]
            ]
        ];
    }

    public function failureParameterProvider()
    {
        return [
            'default' => [
                [], 400
            ],
            'length_of_legacy_identifier_too_long' => [
                [
                    'name' => 'length_of_legacy_identifier_too_long',
                    //length = 101
                    'legacy_identifier' => 'qNf2djPxb5DL1E18JCjspSsJhSlELqCKjUJzUblVLsw1739eNO18gdY9r0o0YJ1FLiBXRavLK3oHgt1k3o1TBrqJzNVwjx2WWIObRfsdfsdf',
                ],
                400
            ]
        ];
    }
}