<?php
namespace AM\CatalogService\Tests\FT\Catalog;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Gia Hoang Nguyen <giahoang.nguyen@audiencemedia.com>
 */
class CatalogUpdateTest extends WebTestCase
{
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/Catalog/CatalogUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }
    
    /**
     * @dataProvider successParameterProvider
     */
    public function testCatalogUpdateSuccess($parameters)
    {
        static::$client->request(
            'PUT',
            '/catalog/v1/catalogs/1?access_token=test',
            $parameters
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);
        //present, method PUT's catalog didn't return data
        $data = $content['data'];

        //$this->assertEquals('legacy_identifier', $data['legacy_identifier']);
    }

    public function successParameterProvider()
    {
        return [
            'success' => [
                [
                    'legacy_identifier' => 'legacy_identifier'
                ]
            ]
        ];
    }
}