<?php
namespace AM\CatalogService\Tests\FT\Publication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CatalogPublicationUpdateTest extends WebTestCase
{
    const API_ENDPOINT = '/catalog/v1/catalogs/%s/publications';
    public static $container;
    public static $client;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $files = [__DIR__ . '/../../DataFixtures/ORM/CatalogPublication/CatalogPublicationUpdate.yml'];
        $manager = static::$container->get('h4cc_alice_fixtures.manager');
        $objects = $manager->loadFiles($files, 'yaml');
        $manager->persist($objects, true);

        static::$client = static::createClient();
    }

    public function failureParameterProvider()
    {
        return [
            'not_found_catalog_100' => [
                100,
                [],
                404
            ],
            'not_multiple_catalog_publication_1' => [
                1,
                [],
                400
            ],
            'not_multiple_catalog_publication_2' => [
                1,
                ["publication_id" => 1,"created_by" => 12],
                400
            ],
            'null_publication_1' => [
                1,
                [["created_by" => 12]],
                400
            ],
            'null_publication_2' => [
                1,
                [
                    ["publication_id" => 1, "created_by" => 12],
                    ["created_by" => 1],
                ],
                400
            ],
            'null_publication_3' => [
                1,
                [
                    ["created_by" => 1],
                    ["publication_id" => 1, "created_by" => 12],
                ],
                400
            ],
            'duplicate_publication_1' => [
                1,
                [
                    ["publication_id" => 1, "created_by" => 16],
                    ["publication_id" => 1, "created_by" => 12]
                ],
                400
            ],
            'duplicate_publication_2' => [
                1,
                [
                    ["publication_id" => 1, "created_by" => 16],
                    ["publication_id" => 2, "created_by" => 14],
                    ["publication_id" => 1, "created_by" => 12]
                ],
                400
            ],
            'not_found_publication_1' => [
                1,
                [
                    ["publication_id" => 1, "created_by" => 16],
                    ["publication_id" => 2, "created_by" => 14],
                    ["publication_id" => 100, "created_by" => 12]
                ],
                404
            ],
            'not_number_publication_1' => [
                1,
                [
                    ["publication_id" => "asd", "created_by" => 16],
                    ["publication_id" => 1, "created_by" => 12]
                ],
                400
            ],
            'not_number_created_by_1' => [
                1,
                [
                    ["publication_id" => 2, "created_by" => "abs"],
                    ["publication_id" => 1, "created_by" => 12]
                ],
                400
            ],
            'out_range_status_1' => [
                1,
                [
                    ["publication_id" => 2, "created_by" => 12, "status" => 3],
                    ["publication_id" => 1, "created_by" => 12, "status" => 1]
                ],
                400
            ],
            'out_range_status_2' => [
                1,
                [
                    ["publication_id" => 2, "created_by" => 12, "status" => "asdas"],
                    ["publication_id" => 1, "created_by" => 12, "status" => "text"]
                ],
                400
            ],
            'maximum_character_remarks_1' => [
                1,
                [
                    ["publication_id" => 1, 'remarks' => "Tha;sldk !@#4 , ' ~ ,./ )(*^ sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;slda;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;ldfha;sldkfnas;256"]
                ],
                400
            ]
        ];
    }

    /**
     * @dataProvider failureParameterProvider
     */
    public function testCatalogPublicationUpdateFailure($id, $parameters, $statusCode)
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, $id),
            $parameters
        );

        $this->assertEquals($statusCode, static::$client->getResponse()->getStatusCode());
    }

    public function testCatalogPublicationUpdateNotFound()
    {
        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, 10)
        );

        $this->assertEquals(404, static::$client->getResponse()->getStatusCode());
    }


    public function testCatalogPublicationMultiUpdateSuccess()
    {
        $catalogPublication = array(
            ["publication_id" => 1, "created_by" => 16, "status" => 1],
            ["publication_id" => 2, "created_by" => 14,  "status" => 0],
        );

        static::$client->request(
            'PUT',
            sprintf(static::API_ENDPOINT, 1),
            $catalogPublication
        );

        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        foreach ($data as $key => $value) {
            $this->assertEquals(
                $catalogPublication[$key]['publication_id'],
                $value['publication_id']
            );
            $this->assertEquals(
                $catalogPublication[$key]['created_by'],
                $value['created_by']
            );
            $this->assertEquals(
                $catalogPublication[$key]['status'],
                $value['status']
            );

            $this->assertLessThanOrEqual(
                time(),
                strtotime($value['modified_by'])
            );
        }
    }
}