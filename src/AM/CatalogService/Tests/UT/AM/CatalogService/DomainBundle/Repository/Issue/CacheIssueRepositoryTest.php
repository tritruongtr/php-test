<?php

class CacheIssueRepositoryTest extends \PHPUnit_Framework_TestCase
{
    const TEST_CLASS = 'AM\CatalogService\DomainBundle\Repository\Issue\CacheIssueRepository';
    const REDIS_CLIENT_CLASS = 'Predis\Client';

    private $stubTestClass;
    private $stubRedisClient;

    public function setUp()
    {
        $this->stubTestClass = null;
        $this->stubRedisClient = null;
    }

    public function test_GetPublication_Method_When_TotalNotFound()
    {
        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock()
        ;

        $this->stubTestClass
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValue(false))
        ;

        $pagination = $this->stubTestClass->getPagination(10, 0);
        $this->assertSame($pagination, false);
    }

    public function test_GetPublication_Method_When_TotalFound()
    {
        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock()
        ;

        $total = 100;
        $limit = 10;
        $offset = 0;
        $page = ($offset / $limit) + 1;

        $this->stubTestClass
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValue(100))
        ;

        $pagination = $this->stubTestClass->getPagination($limit, $offset);
        $this->assertSame(is_array($pagination), true);
        $this->assertSame($pagination['count'] === $total, true);
        $this->assertSame($pagination['per_page'] === $limit, true);
        $this->assertSame($pagination['page'] === $page, true);
    }

    public function test_Get_Method_When_KeyNotFound()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->returnValue(null))
        ;

        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['getRedis'])
            ->getMock()
        ;

        $this->stubTestClass
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($this->stubRedisClient))
        ;

        $value = $this->stubTestClass->get('total');
        $this->assertSame($value, false);
    }

    public function test_Get_Method_When_KeyFound()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET'])
            ->getMock()
        ;

        $total = 10;
        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->returnValue($total))
        ;

        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['getRedis'])
            ->getMock()
        ;

        $this->stubTestClass
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($this->stubRedisClient))
        ;

        $value = $this->stubTestClass->get('total');
        $this->assertSame($value, $total);
    }

    public function test_Get_Method_When_Exception_Occurred()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET'])
            ->getMock()
        ;

        $total = 10;
        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->throwException(new \Exception))
        ;

        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['getRedis'])
            ->getMock()
        ;

        $this->stubTestClass
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($this->stubRedisClient))
        ;

        $value = $this->stubTestClass->get('total');
        $this->assertSame($value, false);
    }

    public function test_QueryStringWalk_Method_When_QueryString_isNull()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $queryString = $testClass->queryStringWalk();
        $this->assertSame($queryString, []);
    }

    public function test_QueryStringWalk_Method_When_ExcludeKeys_isNull()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $originalQueryString = ['status' => 1];
        $queryString = $testClass->queryStringWalk($originalQueryString);
        $this->assertSame($queryString, $originalQueryString);
    }

    public function test_QueryStringWalk_Method_After_ExcludeKeys()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $originalQueryString = ['status' => 1];
        $queryString = $testClass->queryStringWalk($originalQueryString, ['status']);
        $this->assertSame($queryString, []);
    }

    public function test_QueryStringToRedisKey_Method_Using_DefaultKey()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $originalQueryString = [];
        $redisKey = $testClass->queryStringToRedisKey($originalQueryString);
        $this->assertSame($redisKey, 'all');
    }

    public function test_QueryStringToRedisKey_Method_Using_QueryString()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $originalQueryString = ['status' => 1];
        $redisKey = $testClass->queryStringToRedisKey($originalQueryString);
        $this->assertSame($redisKey, 'status_1');
    }

    public function test_QueryStringToRedisKey_Method_Using_EmptyValueQueryString()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $originalQueryString = ['status' => null];
        $redisKey = $testClass->queryStringToRedisKey($originalQueryString);
        $this->assertSame($redisKey, 'all');
    }

    public function test_GetKeyName_Method_With_invalid_type()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        try {
            $testClass->getKeyName('invalid_type');
        } catch (\Exception $e) {
            $this->assertSame(get_class($e) === 'Exception', true);
        }
    }

    public function test_GetKeyName_Method_With_Total_Type()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $keyName = $testClass->getKeyName('total');
        $expectKeyName = sprintf($testClass::KEY_NAME_TOTAL, 'all');
        $this->assertSame($keyName, $expectKeyName);
    }

    public function test_GetKeyName_Method_With_Data_Type()
    {
        $reflectionClass = new \ReflectionClass(static::TEST_CLASS);
        $testClass = $reflectionClass->newInstanceWithoutConstructor();
        $keyName = $testClass->getKeyName('data');
        $expectKeyName = sprintf($testClass::KEY_NAME_DATA, 'all');
        $this->assertSame($keyName, $expectKeyName);
    }

    public function test_GetKeyName_Method_With_Total_Type_And_QueryString()
    {
        $this->mockRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $testClass = static::TEST_CLASS;
        $queryString = ['status' => 1];
        $testClassObject = new $testClass($this->mockRedisClient, $queryString, []);
        $keyName = $testClassObject->getKeyName('total');
        $expectKeyName = sprintf($testClass::KEY_NAME_TOTAL, 'status_1');
        $this->assertSame($keyName, $expectKeyName);
    }

    public function test_GetKeyName_Method_With_Data_Type_And_QueryString()
    {
        $this->mockRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $testClass = static::TEST_CLASS;
        $queryString = ['status' => 1];
        $testClassObject = new $testClass($this->mockRedisClient, $queryString, []);
        $keyName = $testClassObject->getKeyName('data');
        $expectKeyName = sprintf($testClass::KEY_NAME_DATA, 'status_1');
        $this->assertSame($keyName, $expectKeyName);
    }

    public function test_Create_Method_When_Exception_Occurred()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['SET'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('SET')
            ->will($this->throwException(new \Exception))
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->create(10, 'total');
        $this->assertSame($result, false);
    }

    public function test_Create_Method_When_Successfully()
    {
        $this->mockRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->mockRedisClient, $queryString, []);
        $result = $testClassObject->create(10, 'total');
        $this->assertSame($result, true);
    }

    public function test_UpdateTotalKey_Method_When_Exception_Occurred()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->throwException(new \Exception))
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->updateTotalKey(10);
        $this->assertSame($result, false);
    }

    public function test_UpdateTotalKey_Method_When_PreviousTotal_NotFound()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->returnValue(null))
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->updateTotalKey(10);
        $this->assertSame($result, false);
    }

    public function test_UpdateTotalKey_Method_When_PreviousTotal_Found()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET', 'SET', 'EXPIRE'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->returnValue(10))
        ;

        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['update'])
            ->getMock()
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->updateTotalKey(10);
        $this->assertSame($result, true);
    }

    public function test_UpdateTotalKey_Method_When_PreviousTotal_Found_But_Receive_Exception()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['GET', 'SET', 'EXPIRE'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('GET')
            ->will($this->returnValue(10))
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('SET')
            ->will($this->throwException(new \Exception))
        ;

        $this->stubTestClass = $this
            ->getMockBuilder(static::TEST_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['update'])
            ->getMock()
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->updateTotalKey(10);
        $this->assertSame($result, false);
    }

    public function test_GetQueryString_Method()
    {
        $this->mockRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->mockRedisClient, $queryString, []);
        $expectQueryString = $testClassObject->getQueryString();
        $this->assertSame($queryString, $expectQueryString);
    }

    public function test_Update_Method_When_Exception_Occurred()
    {
        $this->stubRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->setMethods(['SET'])
            ->getMock()
        ;

        $this->stubRedisClient
            ->expects($this->any())
            ->method('SET')
            ->will($this->throwException(new \Exception))
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->stubRedisClient, $queryString, []);
        $result = $testClassObject->update(10, 'total');
        $this->assertSame($result, false);
    }

    public function test_Update_Method_When_Successfully()
    {
        $this->mockRedisClient = $this
            ->getMockBuilder(static::REDIS_CLIENT_CLASS)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $queryString = ['status' => 1];
        $testClass = static::TEST_CLASS;
        $testClassObject = new $testClass($this->mockRedisClient, $queryString, []);
        $result = $testClassObject->update(10, 'total');
        $this->assertSame($result, true);
    }
}