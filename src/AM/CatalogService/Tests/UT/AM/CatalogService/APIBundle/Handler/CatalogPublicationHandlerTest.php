<?php

use AM\CatalogService\APIBundle\Handler\CatalogPublicationHandler;
use AM\CatalogService\Domain\Catalog\Catalog;
use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\CatalogPublication\CatalogPublication;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;
use Zenith\CoreService\Testing\TestDouble;

class CatalogPublicationHandlerTest extends TestDouble
{
    const CLASS_CATALOG_PUBLICATION_HANDLER = 'AM\CatalogService\APIBundle\Handler\CatalogPublicationHandler';
    const CLASS_CATALOG_ENTITY = 'AM\CatalogService\Domain\Catalog\Catalog';
    const CLASS_CATALOG_PUBLICATION_ENTITY = 'AM\CatalogService\Domain\CatalogPublication\CatalogPublication';
    const CLASS_PUBLICATION_ENTITY = 'AM\CatalogService\Domain\Publication\Publication';

    const CLASS_CATALOG_REPO = 'AM\CatalogService\DomainBundle\Repository\DTCatalogRepository';
    const CLASS_CATALOG_PUBLICATION_REPO = 'AM\CatalogService\DomainBundle\Repository\DTCatalogPublicationRepository';
    const CLASS_PUBLICATION_REPO = 'AM\CatalogService\DomainBundle\Repository\PublicationRepository';

    const CLASS_SERIALIZER = 'JMS\Serializer\Serializer';
    const CLASS_VALIDATOR = 'Symfony\Component\Validator\ValidatorInterface';
    const CLASS_DOCTRINE_EM = 'Doctrine\ORM\EntityManager';
    const CLASS_EM_CONNECTION = 'Doctrine\DBAL\Connection';
    const CLASS_EM_CONFIGURATION = 'Doctrine\DBAL\Configuration';
    const CLASS_EM_UNITOFWORK =    'Doctrine\ORM\UnitOfWork';


    const CLASS_CONSTRAINT_VIOLATION = 'Symfony\Component\Validator\ConstraintViolationListInterface';

    protected $catalogPublicationHandler;
    protected $catalogRepo;
    protected $publicationRepo;
    protected $catalogPublicationRepo;
    protected $serializer;
    protected $validator;

    protected $catalogEntity;
    protected $publicationEntity;
    protected $catalogPublicationEntity;
    protected $em;


    private function buildDummyCatalog(array $data = [], Catalog $catalog = null)
    {
        if (empty($data)) {
            return $this->catalogEntity;
        }

        if ($catalog instanceof Catalog) {
            $targetCatalog = $catalog;
        } else {
            $targetCatalog = $this->catalogEntity;
        }

        foreach ($data as $property => $value) {
            $targetCatalog->set($property, $value);
        }

        return $targetCatalog;
    }

    private function buildDummyPublication(array $data = [], Publication $publication = null)
    {
        if (empty($data)) {
            return $this->publicationEntity;
        }

        if ($publication instanceof Publication) {
            $targetPublication = $publication;
        } else {
            $targetPublication = $this->publicationEntity;
        }

        foreach ($data as $property => $value) {
            $targetPublication->set($property, $value);
        }

        return $targetPublication;
    }

    private function buildDummyCatalogPublication(array $data = [], CatalogPublication $catalogPublication = null)
    {
        if (empty($data)) {
            return $this->catalogPublicationEntity;
        }

        if ($catalogPublication instanceof CatalogPublication) {
            $targetCatalogPublication = $catalogPublication;
        } else {
            $targetCatalogPublication = $this->catalogPublicationEntity;
        }

        foreach ($data as $property => $value) {
            $targetCatalogPublication->set($property, $value);
        }

        return $targetCatalogPublication;
    }

    private function getPublicationRepoMock($method, $return) {
        $publicationRepo = $this->dummy(static::CLASS_PUBLICATION_REPO,[$method]);
        $publicationRepo
            ->expects($this->once())
            ->method($method)
            ->willReturn($return);
        return $publicationRepo;
    }

    private function setParams($params)
    {
        $request = new Request();
        foreach ($params as $key => $param) {
            $request->request->set($key, $param);
        }
        return $request;
    }

    public function setUp()
    {
        // simple instance objects
        $this->catalogPublicationHandler = $this->getInstance(static::CLASS_CATALOG_PUBLICATION_HANDLER);
        $this->catalogEntity = $this->getInstance(static::CLASS_CATALOG_ENTITY);
        $this->publicationEntity = $this->getInstance(static::CLASS_PUBLICATION_ENTITY);
        $this->catalogPublicationEntity = $this->getInstance(static::CLASS_CATALOG_PUBLICATION_ENTITY);

        // mock dummy objects
        $this->catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO);
        $this->catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO);
        $this->publicationRepo = $this->dummy(static::CLASS_PUBLICATION_REPO);
        $this->serializer = $this->dummy(static::CLASS_SERIALIZER);
        $this->validator = $this->dummy(static::CLASS_VALIDATOR);
        $this->em = $this->dummy(static::CLASS_DOCTRINE_EM);
    }

    private function getValidatorMock($returnValue = 0)
    {
        $error = $this->getMockForAbstractClass(static::CLASS_CONSTRAINT_VIOLATION);
        $error->method('count')->willReturn($returnValue);
        $validator = $this->getMockForAbstractClass(static::CLASS_VALIDATOR);
        $validator->method('validate')->willReturn($error);
        return $validator;
    }

    protected function getEmMock()
    {
        $connection = $this->dummy(static::CLASS_EM_CONNECTION);

        $emMock  = $this->dummy(static::CLASS_DOCTRINE_EM, ['getRepository', 'getClassMetadata', 'persist', 'commit', 'flush','beginTransaction']);
        /*$emMock->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue(new \AM\CatalogService\DomainBundle\Repository\DTCatalogPublicationRepository()));*/
        /*$emMock->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue((object)array('name' => 'aClass')));*/
        /*$emMock->expects($this->any())
            ->method('getConnection')
            ->will($this->returnValue(null));*/

        $this->invokeProperty($emMock,'conn', $connection);
        $unitOfWork = $this->dummy(static::CLASS_EM_UNITOFWORK);
        $this->invokeProperty($emMock,'unitOfWork', $unitOfWork);

        $emMock->expects($this->any())
            ->method('persist')
            ->will($this->returnValue(null));
        $emMock->expects($this->any())
            ->method('flush')
            ->will($this->returnValue(null));
        $emMock->expects($this->any())
            ->method('commit')
            ->will($this->returnValue(null));
        $emMock->expects($this->any())
            ->method('beginTransaction')
            ->will($this->returnValue(null));
        return $emMock;
    }

    public function testFindCatalog()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO, ['find']);

        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);

        $handler = new CatalogPublicationHandler($catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'findCatalog', [1]);
        $this->assertEquals($result, $catalog);
    }

    public function testCheckExistCatalog()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO, ['find']);

        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);

        $handler = new CatalogPublicationHandler($catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'checkExistCatalog', [1]);
        $this->assertEquals($result, $catalog);
    }

    public function testCheckExistCatalogNotFound()
    {
        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO, ['find']);

        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $handler = new CatalogPublicationHandler($catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->expectException(NotFoundHttpException::class);
        $result = $this->invokeMethod($handler, 'checkExistCatalog', [1]);
        $this->assertEquals(null, $result);
    }

    public function testCheckExistPublication()
    {
        $publication = 1;
        $publication = $this->buildDummyPublication(array('id' => $publication), $this->publicationEntity);

        $publicationRepo = $this->dummy(static::CLASS_PUBLICATION_REPO, ['find']);

        $publicationRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($publication);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'checkExistPublication', [1]);
        $this->assertEquals($result, $publication);
    }

    public function testCheckExistPublicationNotFound()
    {
        $publicationRepo = $this->dummy(static::CLASS_PUBLICATION_REPO, ['find']);

        $publicationRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->expectException(NotFoundHttpException::class);
        $this->invokeMethod($handler, 'checkExistPublication', [1]);
    }

    public function testFindOneByCond()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publicationId = 1000;
        $publication = $this->buildDummyPublication(array('id' => $publicationId), $this->publicationEntity);

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "catalog" => $catalog,
                "publication" => $publication
            ], $this->catalogPublicationEntity
        );

        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO, ['findOneBy']);

        $catalogPublicationRepo
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($catalogPublication);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'findOneByCond', [array(1,1000)]);
        $this->assertEquals($result, $catalogPublication);
    }

    public function testCheckExistOneByCond()
    {
        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO, ['findOneBy']);

        $catalogPublicationRepo
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->expectException(NotFoundHttpException::class);
        $this->invokeMethod($handler, 'checkExistOneByCond', [array(1,1000)]);
    }

    public function testCheckExistOneByCondNotFound()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publicationId = 1000;
        $publication = $this->buildDummyPublication(array('id' => $publicationId), $this->publicationEntity);

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "catalog" => $catalog,
                "publication" => $publication
            ], $this->catalogPublicationEntity
        );

        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO, ['findOneBy']);

        $catalogPublicationRepo
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($catalogPublication);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'checkExistOneByCond', [array(1,1000)]);
        $this->assertEquals($result, $catalogPublication);
    }

    public function testFindPublicationByIds()
    {

        $publicationIds = [1000,3057];
        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }

        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'findPublicationByIds', [array(1000,3057)]);
        $this->assertEquals($result, $publications);
    }

    public function testValidateCatalogPublicationsFailure()
    {

        $publications =
            [
                [
                    "publicationId" => "s",
                    "remarks" => "text",
                    "status" => 1
                ],
                [
                    "publicationId" => 1000,
                    "remarks" => "text1",
                    "status" => 1
                ],
            ];


        $error = $this->getMockForAbstractClass(static::CLASS_CONSTRAINT_VIOLATION);
        $error->method('count')->willReturn(1);
        $validator = $this->getMockForAbstractClass(static::CLASS_VALIDATOR);
        $validator->method('validate')->willReturn($error);

        $this->expectException(BadRequestHttpException::class);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $this->em);
        $this->invokeMethod($handler, 'validateCatalogPublications', [$publications]);
    }

    public function testComparePublicationInCatalogPublication()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publicationIds = [1000,3057];
        $publications = $catalogPublications = [];
        foreach ($publicationIds as $id) {
            $publication = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
            $catalogPublications[] = [
                'catalog' => $catalog,
                'publication'=> $publication
            ];
        }

            //add more catalog publicationns
        $catalogPublications[] = [
            'catalog' => $catalog,
            'publication'=> $catalogPublications[0]['publication']
        ];

        $this->expectException(NotFoundHttpException::class);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->invokeMethod($handler, 'comparePublicationInCatalogPublication', [$publications, $catalogPublications]);
    }

    public function testValidateCatalogPublications()
    {

        $params =
            [
                [
                    "publicationId" => 1000,
                    "remarks" => "text",
                    "status" => 1
                ],
                [
                    "publicationId" => 3057,
                    "remarks" => "text1",
                    "status" => 1
                ],
            ];

        $publicationIds = [1000,3057];
        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }

        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);

        $error = $this->getMockForAbstractClass(static::CLASS_CONSTRAINT_VIOLATION);
        $error->method('count')->willReturn(0);
        $validator = $this->getMockForAbstractClass(static::CLASS_VALIDATOR);
        $validator->method('validate')->willReturn($error);

        //$this->expectException(\Symfony\Component\HttpKernel\Exception\BadRequestHttpException::class);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $this->em);
        $this->invokeMethod($handler, 'validateCatalogPublications', [$params]);
    }

    public function testValidateUniqueKeyFailure()
    {
        $publicationIds =
            [
                [
                    "publicationId" => 1000
                ],
                [
                    "publicationId" => 1000
                ]
            ]
        ;
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->expectException(BadRequestHttpException::class);
        $this->invokeMethod($handler, 'validateUniqueKey', [$publicationIds]);

    }

    public function testValidateUniqueKey()
    {
        $publicationIds =
            [
                [
                    "publicationId" => 1000
                ],
                [
                    "publicationId" => 3057
                ]
            ]
        ;
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);

        $result = $this->invokeMethod($handler, 'validateUniqueKey', [$publicationIds]);
        $this->assertEquals(true, $result);
    }

    public function testValidateUniqueKeyIsNull()
    {
        $publicationIds = [];
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $result = $this->invokeMethod($handler, 'validateUniqueKey', [$publicationIds]);
        $this->assertEquals(null, $result);
    }

    public function testParsePublicationRequest()
    {
        $publications =
            [
                [
                    "publication_id" => 3057,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
                [
                    "publication_id" => 1000,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
            ];
        $request = $this->setParams($publications);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);

        $result = $handler->parsePublicationRequest($request);


        foreach ($publications as $key => $publication) {
            $this->assertEquals($publication['publication_id'], $result[$key]['publicationId']);
            $this->assertEquals($publication['remarks'], $result[$key]['remarks']);
            $this->assertEquals($publication['created_by'], $result[$key]['createdBy']);
            $this->assertEquals($publication['created_by'], $result[$key]['modifiedBy']);
            $this->assertEquals($publication['status'], $result[$key]['status']);
        }

    }

    public function testParsePublicationRequestFailure()
    {
        $publications = [];
        $request = $this->setParams($publications);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $this->validator, $this->em);
        $this->expectException(BadRequestHttpException::class);
        $handler->parsePublicationRequest($request);
    }

    public function testSetCatalogPublicationEntity()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publications =
            [
                [
                    "publicationId" => 3057,
                    "remarks" => "text",
                    "status" => 1,
                    "createdBy" => 12
                ],
                [
                    "publicationId" => 1000,
                    "remarks" => "text1",
                    "status" => 1,
                    "createdBy" => 12
                ],
            ];
        $catalogPublications = [];
        foreach ($publications as $publication) {
            $catalogPublication = $this->buildDummyCatalogPublication($publication, $this->catalogPublicationEntity);
            $catalogPublication->set('catalog', $catalog);
            $catalogPublications[] = $catalogPublication;
        }

        $error = $this->getMockForAbstractClass(static::CLASS_CONSTRAINT_VIOLATION);
        $error->method('count')->willReturn(0);
        $validator = $this->getMockForAbstractClass(static::CLASS_VALIDATOR);
        $validator->method('validate')->willReturn($error);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $this->em);
        $result = $handler->setCatalogPublicationEntity($catalog,$publications);
        foreach ($publications as $key => $publication) {
            $this->assertEquals($publication['publicationId'], $result[$key]->get('publicationId'));
            $this->assertEquals($publication['remarks'], $result[$key]->get('remarks'));
            $this->assertEquals($publication['createdBy'], $result[$key]->get('createdBy'));
            $this->assertEquals($publication['status'], $result[$key]->get('status'));
        }
    }

    public function testValidatePublicationParams()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);
        $publications =
            [
                [
                    "publication_id" => 3057,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
                [
                    "publication_id" => 1000,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
            ];
        $request = $this->setParams($publications);

        $validator = $this->getValidatorMock(0);
        $publicationIds = [1000,3057];
        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }
        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);

        $handler = new CatalogPublicationHandler($this->catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $this->em);
        $this->invokeMethod($handler, 'validatePublicationParams', [$catalog, $request]);
    }

    public function testPutPublications()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);
        //mock Catalog Repo
        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO,['find']);
        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);
        //mock validator
        $validator = $this->getValidatorMock(0);
        //mock publication
        $publicationIds = [1000,3057];
        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }
        //mock publication Repo
        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);
        //mock em
        $em = $this->getEmMock();

        $handler = new CatalogPublicationHandler($catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $em
        );

        $publications =
            [
                [
                    "publication_id" => 1000,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
                [
                    "publication_id" => 3057,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
            ];
        $request = $this->setParams($publications);
        $handler->putPublications($catalogId,$request);
    }

    public function testInsertOrUpdate()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);
        //mock Catalog Repo
        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO,['find']);
        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);
        //mock validator
        $validator = $this->getValidatorMock(0);
        //mock publication
        $publicationIds = [1000,3057];
        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }
        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);
        //mock em
        $em = $this->getEmMock();

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "publicationId" => 100,
                "remarks" => "text",
                "status" => 1,
                "createdBy" => 12
            ], $this->catalogPublicationEntity);


        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO,['findOneBy']);
        $catalogPublicationRepo->expects($this->any())->method('findOneBy')->willReturn($catalogPublication);

        $handler = new CatalogPublicationHandler($catalogRepo, $publicationRepo,
            $catalogPublicationRepo, $this->serializer, $validator, $em
        );

        $params =
            [
                [
                    "publication_id" => 1000,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
                [
                    "publication_id" => 3057,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ],
            ];
        $request = $this->setParams($params);
        $handler->putPublications($catalogId,$request);

    }

    public function testInsertOrUpdateWithBatch()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);
        //mock Catalog Repo
        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO,['find']);
        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);
        //mock validator
        $validator = $this->getValidatorMock(0);
        //mock publication
        $publicationIds = [1000];

        for($i = 0; $i < 20 ;$i++){
            $id = $i +1;
            array_push($publicationIds,$id);
        }

        $publications = [];
        foreach ($publicationIds as $id) {
            $publications[] = $this->buildDummyPublication(array('id' => $id), $this->publicationEntity);
        }
        //mock publication Repo
        $publicationRepo = $this->getPublicationRepoMock('getByIds', $publications);
        //mock em
        $em = $this->getEmMock();

        $handler = new CatalogPublicationHandler($catalogRepo, $publicationRepo,
            $this->catalogPublicationRepo, $this->serializer, $validator, $em
        );

        $publications =
            [
                [
                    "publication_id" => 1000,
                    "remarks" => "text",
                    "created_by" => 12,
                    "status" => 1
                ]
            ];

        for($i = 0; $i < 20 ;$i++){
            $id = $i +1;
            $publications[] = [
                "publication_id" => $id,
                "remarks" => "text",
                "created_by" => 12,
                "status" => 1
            ];
        }
        $request = $this->setParams($publications);
        $handler->putPublications($catalogId,$request);
    }

    public function testValidate()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publicationId = 1000;
        $publication = $this->buildDummyPublication(array('id' => $publicationId), $this->publicationEntity);

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "catalog" => $catalog,
                "publication" => $publication
            ], $this->catalogPublicationEntity
        );

        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO);

        //get validator mock
        $validator = $this->getValidatorMock(0);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $catalogPublicationRepo, $this->serializer, $validator, $this->em
        );
        $result = $this->invokeMethod($handler, 'validate', [$catalogPublication]);
        $this->assertEquals(true, $result);
    }

    public function testValidateFailure()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);

        $publicationId = 1000;
        $publication = $this->buildDummyPublication(array('id' => $publicationId), $this->publicationEntity);

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "catalog" => $catalog,
                "publication" => $publication
            ], $this->catalogPublicationEntity
        );

        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO);

        //get validator mock
        $validator = $this->getValidatorMock(1);
        $handler = new CatalogPublicationHandler($this->catalogRepo, $this->publicationRepo,
            $catalogPublicationRepo, $this->serializer, $validator, $this->em
        );
        $this->expectException(BadRequestHttpException::class);
        $this->invokeMethod($handler, 'validate', [$catalogPublication]);
    }

    public function testUnAssignCatalogPublication()
    {
        $catalogId = 1;
        $catalog = $this->buildDummyCatalog(array('id' => $catalogId), $this->catalogEntity);
        //mock Catalog Repo
        $catalogRepo = $this->dummy(static::CLASS_CATALOG_REPO,['find']);
        $catalogRepo
            ->expects($this->once())
            ->method('find')
            ->willReturn($catalog);
        //mock validator
        $validator = $this->getValidatorMock(0);
        //mock publication
        $publicationId = 1000;
        $publication = $this->buildDummyPublication(array('id' => $publicationId), $this->publicationEntity);
        $publicationRepo = $this->getPublicationRepoMock('find', $publication);

        $catalogPublication = $this->buildDummyCatalogPublication(
            [
                "catalog" => $catalog,
                "publication" => $publication
            ], $this->catalogPublicationEntity
        );
        $catalogPublicationRepo = $this->dummy(static::CLASS_CATALOG_PUBLICATION_REPO,['findOneBy','update']);
        $catalogPublicationRepo
            ->expects($this->once())
            ->method('update')
            ->willReturn(null);

        $catalogPublicationRepo
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($catalogPublication);

        $handler = new CatalogPublicationHandler($catalogRepo, $publicationRepo,
            $catalogPublicationRepo, $this->serializer, $validator, $this->em
        );

        $params = ["remarks" => "text", "modified_by" => 12];

        $request = $this->setParams($params);

        $handler->unAssignCatalogPublication($publicationId, $catalogId, $request);
    }
}