<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AM\CatalogService\Domain\Category\Category;
use AM\CatalogService\Domain\Publication\Publication;
use AM\CatalogService\Domain\PublicationCategory\PublicationCategory;

class CategoryHandlerTest extends KernelTestCase
{
    public static $container;
    public static $client;
    const CATALOG_MOCK_OBJECT = 'AM\NewsstandService\DomainBundle\Repository\DTCatalogRepository';
    const PUBLICATION_MOCK_OBJECT = 'AM\NewsstandService\DomainBundle\Repository\DTPublicationRepository';
    const PUBLICATION_CATEGORY_MOCK_OBJECT = 'AM\NewsstandService\DomainBundle\Repository\DTPublicationCategoryRepository';

    public function setUp()
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function testWrongDataAssignPublicationCategory()
    {
        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->postMultiplePublicationToCategory(1, array());
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testNotFoundCategoryAssignPublicationCategory()
    {
        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        static::$container->set('catalog.category.repository', $mockManager);

        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->postMultiplePublicationToCategory(1, array(array('publication_id'=>1)));
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function testInvalidPublicationAssignPublicationCategory()
    {
        $category = new Category($this->category());
 
        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($category));

        static::$container->set('catalog.category.repository', $mockManager);
        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->postMultiplePublicationToCategory(1, array(array('publication_xxx' => 1)));
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testNotFoundPublicationAssignPublicationCategory()
    {
        $category = new Category($this->category());

        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($category));

        $publicationMockManager = $this
            ->getMockBuilder(self::PUBLICATION_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $publicationMockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        static::$container->set('catalog.category.repository', $mockManager);
        static::$container->set('catalog.publication.repository', $publicationMockManager);
        
        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->postMultiplePublicationToCategory(1, array(array('publication_id' => 1)));

    }

    public function testFoundPublicationCategoryAssignPublicationCategory()
    {
        $category = new Category($this->category());

        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($category));

        $publication = new Publication(array());

        $publicationMockManager = $this
            ->getMockBuilder(self::PUBLICATION_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $publicationMockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($publication));



        $publicationCategoryMockManager = $this
            ->getMockBuilder(self::PUBLICATION_CATEGORY_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy', 'getFinalResultByJMSGroup'])
            ->getMock();

        $publicationCategoryMockManager->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($this->publicationCategory()));

        static::$container->set('catalog.category.repository', $mockManager);
        static::$container->set('catalog.publication.repository', $publicationMockManager);
        static::$container->set('catalog.publication_category.repository', $publicationCategoryMockManager);

        $categoryHandler = static::$container->get('catalog.category.handler');
        $result = $categoryHandler->postMultiplePublicationToCategory(1, array(array('publication_id' => 1)));

        $this->assertEmpty($result);
    }

    public function category()
    {
        return array('id' => 1,
            'project_id' => 1,
            'name' => 'test',
            'description' => 'test',
            'slug' => 'ccc',
            'parent_id' => 0,
            'remote_identifier' => 'xxx',
            'priority' => 1,
            'category_set_id' => 2,
            'status' => 1,
            'legacy_identifier' => 1);
    }

    public function publicationCategory()
    {
        $pubCategory =  new PublicationCategory();
        $pubCategory->setCategoryId(1);
        $pubCategory->setPublicationId(1);
        $pubCategory->setRootParentCategoryId(1);

        return $pubCategory;
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function testWrongDataUnAssignPublicationCategory()
    {
        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->deleteMultiplePublicationToCategory(1, array());
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testNotFoundCategoryUnAssignPublicationCategory()
    {
        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        static::$container->set('catalog.category.repository', $mockManager);

        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->deleteMultiplePublicationToCategory(1, array(array('publication_id' => 1)));
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function testInvalidPublicationUnAssignPublicationCategory()
    {
        $category = new Category($this->category());

        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($category));

        static::$container->set('catalog.category.repository', $mockManager);
        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->deleteMultiplePublicationToCategory(1, array(array('publication_xxx' => 1)));
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testNotFoundPublicationUnAssignPublicationCategory()
    {
        $category = new Category($this->category());

        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue($category));

        $publicationMockManager = $this
            ->getMockBuilder(self::PUBLICATION_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $publicationMockManager->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        static::$container->set('catalog.category.repository', $mockManager);
        static::$container->set('catalog.publication.repository', $publicationMockManager);

        $categoryHandler = static::$container->get('catalog.category.handler');
        $categoryHandler->deleteMultiplePublicationToCategory(1, array(array('publication_id' => 1)));
    }

    public function testassignParentCategory()
    {
        $mockManager = $this
            ->getMockBuilder(self::CATALOG_MOCK_OBJECT)
            ->disableOriginalConstructor()
            ->setMethods(['getListParent', 'find'])
            ->getMock();

        $mockManager->expects($this->once())
            ->method('getListParent')
            ->will($this->returnValue(array(1,2,3)));

        $mockManager->expects($this->any())
            ->method('find')
            ->will($this->returnValue([]));

        static::$container->set('catalog.category.repository', $mockManager);

        $categoryHandler = static::$container->get('catalog.category.handler');

        $result = $categoryHandler->assignParentCategory(1, new Publication(array('id'=>1)));

        $this->assertEmpty($result);
    }
}