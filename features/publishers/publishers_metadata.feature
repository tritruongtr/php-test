Feature: Publisher Metadata public API
    In order to control Publisher metadata from anywhere
    As an API user
    I need to be able to view, add, edit and delete Publisher metadata

Scenario: 1. Check JMS Groups
    Given Set of properties:
        | property          |
        | id                |
        | key_name          |
        | key_value         |
    Then JMS Group: "list" should correct in entity "AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata"
    Given Set of properties:
        | property          |
        | id                |
        | publisher_id      |
        | key_name          |
        | key_value         |
    Then JMS Group: "view" should correct in entity "AM\CatalogService\Domain\PublisherMetadata\PublisherMetadata"

Scenario: 2. Creating new publisher metadata
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 2.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 |             |              |
        | 2  | 2                 | key_name    | key_value    |
        | 3  |                   |             |              |
        | 4  | a                 | key_name    | key_value    |
        | 5  | 1                 | key_name    | key_value    |
        | 6  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.009"

    Then I set form data from record 2
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"

    Then I set form data from record 3
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.012"

    Then I set form data from record 4
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.011"
    
    Then I set form data from record 5
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object
    
    Then I set form data from record 6
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 409
    Then I should have internal code "003.001.014"

Scenario: 3. Seeing a list of existing metadata of a specific publisher
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 3.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    
    When I request "/catalog/v1/publishers/2/metadata" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"
    
    When I request "/catalog/v1/publishers/1/metadata" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | publisher_id      |
        | key_name          |
        | key_value         |
    When Found records
    Then Each record should have given properties

Scenario: 4. Seeing an existing publisher metadata
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 4.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    
    When I request "/catalog/v1/publishers/metadata/1" with method "get" and content type is "json"
    Given Set of properties:
        | property          |
        | id                |
        | publisher_id      |
        | key_name          |
        | key_value         |
    When Found records
    Then The record should have given properties
    
    When I request "/catalog/v1/publishers/metadata/2" with method "get" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.015"

Scenario: 5. Updating an existing publisher metadata
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 5.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 |             |              |
        | 2  | 2                 | key_name    | key_value    |
        | 3  |                   |             |              |
        | 4  | a                 | key_name    | key_value    |
        | 5  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.009"

    Then I set form data from record 2
    When I request "/catalog/v1/publishers/metadata/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"

    Then I set form data from record 3
    When I request "/catalog/v1/publishers/metadata/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.012"

    Then I set form data from record 4
    When I request "/catalog/v1/publishers/metadata/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.011"
    
    Then I set form data from record 5
    When I request "/catalog/v1/publishers/metadata/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

Scenario: 6. Deleting an existing publisher
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 5.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Given I have a set of data:
        | id | publisher_id      | key_name    | key_value    |
        | 1  | 1                 | key_name    | new_value    |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers/metadata" with method "post" and content type is "form"
    
    When I request "/catalog/v1/publishers/metadata/2" with method "delete" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.015"
    
    When I request "/catalog/v1/publishers/metadata/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200