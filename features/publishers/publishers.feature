Feature: Publishers public API
    In order to control Publishers from anywhere
    As an API user
    I need to be able to view, add, edit and delete Publishers

Scenario: 1. Check JMS Groups
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    Then JMS Group: "list" should correct in entity "AM\CatalogService\Domain\Publisher\Publisher"
    Given Set of properties:
        | property           |
        | id                 |
        | name               |
        | internal_name      |
        | description        |
        | logo               |
        | remote_identifier  |
        | country            |
        | status             |
        | publications       |
        | no_of_publications |
        | metadata           |
    Then JMS Group: "view" should correct in entity "AM\CatalogService\Domain\Publisher\Publisher"

Scenario: 2. Creating new publisher
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 2.1 |               |             |      |                   |              |        |
        | 2  |           |               |             |      |                   |              |        |
        | 3  | scene 2.3 |               |             |      |                   | VN           |        |
        | 4  | scene 2.4 |               |             |      |                   | VN           | 3      |
        | 5  | scene 2.5 |               |             |      |                   | VN           | 1      |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.005"

    Then I set form data from record 2
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.001"

    Then I set form data from record 3
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object

    Then I set form data from record 4
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.006"
    
    Then I set form data from record 5
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object

Scenario: 3. Seeing a list of existing publishers
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 3.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    When Found records
    Then Each record should have given properties
    Then Each record should have Property "country" as an Array
    
Scenario: 4. Seeing a list of existing publishers by country code
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 4.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers?country_code=VN" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    When Found records
    Then Each record should have given properties
    Then Each record should have Property "country" as an Array
    Then Each record should have country code equal to "VN"

Scenario: 5. Seeing a list of existing publishers by status
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 5.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers?status=1" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    When Found records
    Then Each record should have given properties
    Then Each record should have Property "country" as an Array
    Then Each record should have property "status" and equal to "1"

Scenario: 6. Seeing a list of existing publishers by status and country code
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 6.1 |               |             |      |                   | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers?status=1&country_code=VN" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    When Found records
    Then Each record should have given properties
    Then Each record should have Property "country" as an Array
    Then Each record should have property "status" and equal to "1"
    Then Each record should have country code equal to "VN"
    
Scenario: 7. Seeing a list of existing publishers by remote_identifier
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 7.1 |               |             |      | scene71           | VN           | 1      |
    
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers?status=1&remote_identifier=scene71" with method "get" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | internal_name     |
        | description       |
        | logo              |
        | remote_identifier |
        | country           |
        | status            |
    When Found records
    Then Each record should have given properties
    Then Each record should have Property "country" as an Array
    Then Each record should have property "remote_identifier" and equal to "scene71"

Scenario: 8. Seeing an existing publisher
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 8.1 |               |             |      |                   | VN           | 1      |

    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    When I request "/catalog/v1/publishers/1" with method "get" and content type is "json"
    Given Set of properties:
        | property           |
        | id                 |
        | name               |
        | internal_name      |
        | description        |
        | logo               |
        | remote_identifier  |
        | country            |
        | status             |
        | publications       |
        | no_of_publications |
        | metadata           |
    Then The response should be JSON
    Then HTTP status code should be 200
    When Found records
    Then The record should have given properties
    Then The record should have Property "country" as an Array
    Then The record should have Property "publications" as an Array
    Then The record should have Property "metadata" as an Array
    
    When I request "/catalog/v1/publishers/2" with method "get" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"

Scenario: 9. Updating an existing publisher
    Given I have a set of data:
        | id | name      | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 9.1 |               |             |      |                   | VN           | 1      |
        | 2  | scene 9.2 |               |             |      |                   |              |        |
        | 3  |           |               |             |      |                   |              |        |
        | 4  | scene 9.4 |               |             |      |                   | VN           |        |
        | 5  | scene 9.5 |               |             |      |                   | VN           | 3      |
        | 6  | scene 9.6 | internal name |             |      |                   | VN           | 1      |
        

    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    Then I set form data from record 2
    When I request "/catalog/v1/publishers/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.005"

    Then I set form data from record 3
    When I request "/catalog/v1/publishers/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.001"

    Then I set form data from record 4
    When I request "/catalog/v1/publishers/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Then I set form data from record 5
    When I request "/catalog/v1/publishers/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.001.006"
    
    Then I set form data from record 6
    When I request "/catalog/v1/publishers/1" with method "put" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
    
    Then I set form data from record 6
    When I request "/catalog/v1/publishers/2" with method "put" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"
    
Scenario: 10. Deleting an existing publisher
    Given I have a set of data:
        | id | name       | internal_name | description | logo | remote_identifier | country_code | status |
        | 1  | scene 10.1 |               |             |      |                   | VN           | 1      |
        
    Then I set form data from record 1
    When I request "/catalog/v1/publishers" with method "post" and content type is "form"
    
    When I request "/catalog/v1/publishers/2" with method "delete" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.001.007"
    
    When I request "/catalog/v1/publishers/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200