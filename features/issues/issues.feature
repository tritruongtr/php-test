Feature: Issue public API
    In order to control Issues from anywhere
    As an API user
    I need to be able to view, add, edit and delete Issues

@issue_check_jms_group
Scenario: 1. Check JMS Groups
    Given Set of properties:
        | property          |
        | id                |
        | publication_id    |
        | name              |
        | internal_name     |
        | issn              |
        | sequence_no       |
        | description       |
        | cover_image       |
        | cover_date        |
        | publish_date      |
        | remote_identifier |
        | legacy_issue_id   |
        | status            |
        | created           |
        | modified          |
        | created_by        |
        | modified_by       |
        | file_path         |
        | type              |
    Then JMS Group: "list" should correct in entity "AM\CatalogService\Domain\Issue\Issue"
    Given Set of properties:
        | property          |
        | id                |
        | publication_id    |
        | name              |
        | internal_name     |
        | issn              |
        | sequence_no       |
        | description       |
        | cover_image       |
        | cover_date        |
        | publish_date      |
        | remote_identifier |
        | legacy_issue_id   |
        | status            |
        | created           |
        | modified          |
        | created_by        |
        | modified_by       |
        | file_path         |
        | type              |
        | metadata          |
        | publication       |
        | issue_content     |
    Then JMS Group: "view" should correct in entity "AM\CatalogService\Domain\Issue\Issue"

@create_issue
Scenario: 2. Creating new issue
    Given I have a set of data:
        | id | publication_id |  name       | legacy_issue_id | type | status | created_by | modified_by | file_path        |
        | 1  |                |             |                 |      |        | 1          | 1           |                  |
        | 2  | 1              |             |                 |      |        | 1          | 1           |                  |
        | 2  | 1              | scene 2.1   |                 |      |        | 1          | 1           |                  |
        | 3  | 1              | scene 2.1   |                 |      |        | 1          | 1           |                  |
        | 4  | 1              | scene 2.1   | 1               |      |        | 1          | 1           |                  |
        | 5  | 1              | scene 2.1   | 1               | 1    |        | 1          | 1           |                  |
        | 6  | 1              | scene 2.1   | 1               | 1    | 1      | 1          | 1           | http://gogle.com |

    Then I set form data from record 1
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.006.001"

    Then I set form data from record 2
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.006.009"

    Then I set form data from record 3
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.006.009"

    Then I set form data from record 4
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.006.011"

    Then I set form data from record 5
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.006.011"

    Then I set form data from record 6
    When I request "/catalog/v1/issues" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object

@list_issues
Scenario: 3. Seeing a list of existing issues

    When I request "/catalog/v1/issues?publication_id=1" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
     Given Set of properties:
        | property          |
        | publication_id    |
    When Found records
    Then Each record should have property "publication_id" and equal to "1"

    When I request "/catalog/v1/issues?status=3" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
     Given Set of properties:
        | property |
        | status   |
    When Found records
    Then Each record should have property "status" and equal to "3"

    When I request "/catalog/v1/issues?type=1" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
     Given Set of properties:
        | property |
        | type     |
    When Found records
    Then Each record should have property "type" and equal to "1"

@update_issue
Scenario: 4. Updating an existing issue
    Given I have a set of data:
        | id |  name     | modified_by |
        | 1  | scene 4.2 | 2           |

    Then I set form data from record 1
    When I request "/catalog/v1/issues/9" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id | legacy_issue_id| modified_by |
        | 1  | 123            | 2           |

    Then I set form data from record 1
    When I request "/catalog/v1/issues/9" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id |  status     | modified_by | type |
        | 1  |    3        | 2           | 2    |

    Then I set form data from record 1
    When I request "/catalog/v1/issues/9" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id |  type     | modified_by |
        | 1  | 2         | 2           |

    Then I set form data from record 1
    When I request "/catalog/v1/issues/9" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200


@delete_issue
Scenario: 5. Deleting an existing issue
   Given I have a set of data:
        | id | publication_id |  name       | legacy_issue_id | type | status | created_by | modified_by | file_path        |
        | 1  | 1              | scene 5.1   | 1               | 1    | 1      | 1          | 1           | http://gogle.com |

    Then I set form data from record 1
    When I request "/catalog/v1/issues" with method "post" and content type is "form"

    When I request "/catalog/v1/issues/2" with method "delete" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.006.032"

    When I request "/catalog/v1/issues/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200