<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class IssueContext extends RestContext implements Context, SnippetAcceptingContext
{
    public function __construct($parameters)
    {
        parent::__construct($parameters);
    }
}
