<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class CategoryContext extends RestContext implements Context, SnippetAcceptingContext
{
    public function __construct($parameters, $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($parameters);
    }

    /**
     * @When /^I execute query "(.*)"$/
     */
    public function excuteQuery($query)
    {
        $this->entityManager->getConnection()->exec($query);
    }
}
