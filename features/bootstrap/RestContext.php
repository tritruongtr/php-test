<?php
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelDictionary;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Util\Inflector;

use JMS\Serializer\Annotation\Groups as JMSGroups;

use Zenith\CoreService\RestClient\RestContext as BaseRestContext;

class RestContext extends BaseRestContext implements Context, SnippetAcceptingContext
{
    public function __construct($parameters)
    {
        parent::__construct($parameters);
    }
        
    /**
     * @Then /^Each record should have Property "([^"]*)" as an Array$/
     */
    public function eachPropIsArray($propName)
    {
        foreach ($this->data['data'] as $record) {
            if (!isset($record[$propName]) || !is_array($record[$propName])) {
                $type = gettype($record[$propName]);
                throw new Exception("`$propName` property is not array, `$type` returned");
            }
        }
    }
    
    /**
     * @Then /^The record should have Property "([^"]*)" as an Array$/
     */
    public function recordPropIsArray($propName)
    {
        $record = $this->data['data'];
        if (!isset($record[$propName]) || !is_array($record[$propName])) {
            $type = gettype($record[$propName]);
            throw new Exception("`$propName` property is not array, `$type` returned");
        }
    }
    
    /**
     * @Then /^The record should have given properties$/
     */
    public function recordHasProperties()
    {
        $keys = array_keys($this->data['data']);
        $keys = $this->classifyProperties($keys);
        if ($keys !== $this->properties) {
            throw new \Exception("Properties in record are not equal with sample properties");
        }
    }

    /**
     * @Then /^JMS Group: "(.*)" should correct in entity "(.*)"$/
     */
    public function JMSGroupCorrect($group, $entityClassName)
    {
        if (empty($this->properties)) {
            throw new Exception("Sample properties could not be found");
        }

        $properties = $this->getPropertiesByJSMGroup($entityClassName, $group);
        if ($properties !== $this->properties) {
            throw new Exception("Properties in entity with group name $group are not equal with sample properties");
        }
    }
    
    public function getPropertiesByJSMGroup($className, $groupName)
    {
        $result = array();
        $reflectionClass = new ReflectionClass($className);
        $properties = $reflectionClass->getProperties();
        $reader = new AnnotationReader();
        foreach ($properties as $reflectionProperty) {
            $annotations = $reader->getPropertyAnnotations($reflectionProperty);
            if (empty($annotations)) {
                continue;
            }

            foreach ($annotations as $annotation) {
                if (!$annotation instanceof JMSGroups || empty($annotation->groups)) {
                    continue;
                }

                if (in_array($groupName, $annotation->groups)) {
                    $result[] = $reflectionProperty->name;
                }
            }
        }

        return $this->classifyProperties($result);
    }
}
