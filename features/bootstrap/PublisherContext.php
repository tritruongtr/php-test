<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class PublisherContext extends RestContext implements Context, SnippetAcceptingContext
{
    public function __construct($parameters)
    {
        parent::__construct($parameters);
    }

    /**
     * @Then /^Each record should have country code equal to "(.*)"$/
     */
    public function eachHasCountryCodeValue($value)
    {
        foreach ($this->data['data'] as $record) {
            if (!isset($record['country']) || $record['country']['code'] !== $value) {
                throw new Exception("The `country` property with value `$value` could not be found in record");
            }
        }
    }
}
