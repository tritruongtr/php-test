Feature: Publication public API
    In order to control publications from anywhere
    As an API user
    I need to be able to view, add, edit and delete publications

@publication_check_jms_group
Scenario: 1. Check JMS Groups
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | frequency         |
        | legacy_identifier |
        | internal_name     |
        | description       |
        | country           |
        | language          |
        | locale            |
        | publisher_id      |
        | content_rating    |
        | remote_identifier |
        | created           |
        | modified          |
        | created_by        |
        | modified_by       |
        | site_id           |
        | status            |
        | type              |
        | no_of_issues      |
        | logo              |
        | latest_issue      |
    Then JMS Group: "list" should correct in entity "AM\CatalogService\Domain\Publication\Publication"

    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | frequency         |
        | legacy_identifier |
        | internal_name     |
        | description       |
        | country           |
        | language          |
        | locale            |
        | publisher_id      |
        | content_rating    |
        | remote_identifier |
        | created           |
        | modified          |
        | created_by        |
        | modified_by       |
        | site_id           |
        | status            |
        | type              |
        | no_of_issues      |
        | publisher         |
        | logo              |
        | latest_issue      |
        | country_availability|
        | categories        |
        | issues            |
        | publicationsMetadata|
    Then JMS Group: "view" should correct in entity "AM\CatalogService\Domain\Publication\Publication"

@create_publication
Scenario: 2. Creating new publication
    Given I have a set of data:
        | id |publisher_id | name     | frequency   | legacy_identifier    | country_code | language | locale   | content_rating | remote_identifier | created_by | site_id  | status | type | no_of_issues |
        | 1  |             |          |             |                      |              |          |          |                |                   |            |          |        |      |              |
        | 2  | 1           | scene 2.3|             |                      |              |          |          |                |                   |            |          |        |      |              |
        | 3  | 1           | scene 2.3| requency2.1 |                      |              |          |          |                |                   |            |          |        |      |              |
        | 4  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 |              |          |          |                |                   |            |          |        |      |              |
        | 5  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           |          |          |                |                   |            |          |        |      |              |
        | 6  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      |          |                |                   |            |          |        |      |              |
        | 7  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    |                |                   |            |          |        |      |              |
        | 8  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              |                   |            |          |        |      |              |
        | 9  | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 |            |          |        |      |              |
        | 10 | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 | 1          |          |        |      |              |
        | 11 | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 | 1          | 1        |        |      |              |
        | 12 | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 | 1          | 1        | 1      |      |              |
        | 13 | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 | 1          | 1        | 1      | 1    |              |
        | 14 | 1           | scene 2.3| requency2.1 | legacy_identifier2.1 | VN           | vie      | en_GB    | 1              | 1                 | 1          | 1        | 1      | 1    | 1            |
    Then I set form data from record 1
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.004.041"

    Then I set form data from record 2
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.071"

    Then I set form data from record 3
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.074"

    Then I set form data from record 4
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.009"

    Then I set form data from record 5
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.011"

    Then I set form data from record 6
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.013"

    Then I set form data from record 7
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.017"

    Then I set form data from record 8
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.019"

    Then I set form data from record 9
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.028"

    Then I set form data from record 10
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.032"

    Then I set form data from record 11
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.040"

    Then I set form data from record 12
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.070"

    Then I set form data from record 13
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.004.077"

    Then I set form data from record 14
    When I request "/catalog/v1/publications" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object

@list_publications
Scenario: 3. Seeing a list of existing publications

    When I request "/catalog/v1/publications?publisher_id=1" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property          |
        | publisher_id    |
    When Found records
    Then Each record should have property "publisher_id" and equal to "1"

    When I request "/catalog/v1/publications?type=1" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
    Given Set of properties:
        | property |
        | type     |
    When Found records
    Then Each record should have property "type" and equal to "1"

@update_publication
Scenario: 4. Updating an existing publication
    Given I have a set of data:
        | id |  name     | modified_by |
        | 1  | scene 4.1 | 2           |

    Then I set form data from record 1
    When I request "/catalog/v1/publications/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id | country_code| modified_by |
        | 1  | VN            | 2           |

    Then I set form data from record 1
    When I request "/catalog/v1/publications/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

@delete_publication
Scenario: 5. Deleting an existing publication

    When I request "/catalog/v1/publications/1000" with method "delete" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.004.020"

    When I request "/catalog/v1/publications/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 409
    Then I should have internal code "003.004.051"

@assign_publication_to_catalog
Scenario: Assign a publication to catalog
    When I execute query "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE catalogs; SET FOREIGN_KEY_CHECKS = 1;"
    When I execute query "insert into catalogs(`id`, `name`, `description`, `remote_identifier`, `status`) values (1, 'catalog test 01', 'catalog description test 01', 11111, 1)"

    When I execute query "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE publishers; SET FOREIGN_KEY_CHECKS = 1; "
    When I execute query "insert into publishers(`id`, `name`, `internal_name`, `description`, `logo`, `remote_identifier`, `country_code`, `status`) values (1, 'BBC Worldwide', 'BBC Worldwide Limited', 'BBC Worldwide Limited Description', 'http://upload.wikimedia.org/wikipedia/en/thumb/e/eb/BBC_Worldwide_Logo.svg/1280px-BBC_Worldwide_Logo.svg.png', 'bcc', 'US', 1)"

    When I execute query "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE publications; SET FOREIGN_KEY_CHECKS = 1;"
    When I execute query "insert into publications(`id`, `publisher_id`, `name`, `internal_name`, `description`, `country_code`, `language`, `locale`, `content_rating`, `remote_identifier`, `created_by`, `site_id`, `status`, `type`, `no_of_issues`,`logo`) values (1, 1, 'BBC Good Food Magazine', 'BBC Good Food Magazine', 'BBC Worldwide Limited Description', 'US', 'eng', 'eng-US', 15, 11111, 14, 1, 1, 1, 1, 'http://testthuanlight-vn.audiencemedia.com/var/site_338/storage/images/media2/t3/76770-1-eng-GB/t3.png')"

    When I execute query "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE catalogs_publications; SET FOREIGN_KEY_CHECKS = 1;"

    Given I have a set of data:
        | id | created_by |
        | 1  | 14         |
    Then I set form data from record 1
    When I request "/catalog/v1/publications/1/assign/catalog/1" with method "post" and content type is "form"
    Then HTTP status code should be 200