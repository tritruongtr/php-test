Feature: Catalogs public API
    In order to control Catalogs from anywhere
    As an API user
    I need to be able to view, add, edit and delete Catalogs

@catalog_check_jms_group    
Scenario: 1. Check JMS Groups
    Given Set of properties:
        | property          |
        | id                |
        | name              |
        | description		|
        | remote_identifier |
        | status            |
    Then JMS Group: "list" should correct in entity "AM\CatalogService\Domain\Catalog\Catalog"

@create_catalog
Scenario: 2. Creating new catalog
    Given I have a set of data:
        | id | name 		  |  description | remote_identifier | status|
        | 1  |                |              |                   |      |
        | 2  | name 2         |              |                   |      |
        | 3  | name 3         | description3 |                   |      |
        | 4  | name 4         | description4 | remote_1          |      |
        | 5  | name 5         | description5 | remote_2          | 2    |
        | 6  | name 6         | description6 | remote_3          | 1    |

    Then I set form data from record 1
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.003.001"

    Then I set form data from record 2
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.003.004"

    Then I set form data from record 3
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.003.004"

    Then I set form data from record 4
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.003.009"

    Then I set form data from record 5
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.003.010"

    Then I set form data from record 6
    When I request "/catalog/v1/catalogs" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object
    
@list_catalog
Scenario: 3. Seeing a list of existing catalogs
    When I request "/catalog/v1/catalogs?status=1" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
     Given Set of properties:
        | property |
        | status   |
    When Found records
    Then Each record should have property "status" and equal to "1"

@update_catalog
Scenario: 4. Updating an existing catalog
    Given I have a set of data:
        | id |  name       | description |
        | 1  | update test | test        |

    Then I set form data from record 1
    When I request "/catalog/v1/catalogs/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id | remote_identifier| status |
        | 1  | remote_test    | 1           |

    Then I set form data from record 1
    When I request "/catalog/v1/catalogs/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

@delete_catalog
Scenario: 5. Deleting an existing catalog
  
    When I request "/catalog/v1/catalogs/2" with method "delete" and content type is "json"
    When Not found records
    Then The response should be JSON
    Then HTTP status code should be 404
    Then I should have internal code "003.003.011"

    When I request "/catalog/v1/catalogs/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200
