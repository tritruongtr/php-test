Feature: Category public API
    In order to control Categorys from anywhere
    As an API user
    I need to be able to view, add, edit and delete Categorys

@create_category
Scenario: 2. Creating new category
    When I execute query "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE categories; SET FOREIGN_KEY_CHECKS = 1;"

    Given I have a set of data:
        | id | project_id     |  name       | slug | parent_id | remote_identifier | locale | language |
        | 1  |                |             |      |           |                   |        |          |
        | 2  | 1              |             |      |           |                   |        |          |
        | 3  | 1              | category 01 |      |           |                   |        |          |
        | 4  | 1              | category 01 |cat-01|           |                   |        |          |
        | 5  | 1              | category 01 |cat-01| 0         |                   |        |          |
        | 6  | 1              | category 01 |cat-01| 0         | 1                 |        |          |
        | 7  | 1              | category 01 |cat-01| 0         |                   |        |          |

    Then I set form data from record 1
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.029"

    Then I set form data from record 2
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.001"

    Then I set form data from record 3
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.003"

    Then I set form data from record 4
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.005"

    Then I set form data from record 5
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.007"

    Then I set form data from record 6
    When I request "/catalog/v1/categories" with method "post" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 201
    Then I should have location of new object

@list_categories
Scenario: 3. Seeing a list of existing categories

    When I request "/catalog/v1/categories" with method "get" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200
     Given Set of properties:
        | property |
        | id       |
    When Found records
    Then Each record should have property "id" and equal to "1"

@update_category
Scenario: 4. Updating an existing category
    Given I have a set of data:
        | id |  name        |
        | 1  |cat 01 update |

    Then I set form data from record 1
    When I request "/catalog/v1/categories/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id |  slug        |
        | 1  |cat-01-update |

    Then I set form data from record 1
    When I request "/catalog/v1/categories/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

    Given I have a set of data:
        | id |  parent_id  |
        | 1  |  1          |

    Then I set form data from record 1
    When I request "/catalog/v1/categories/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 400
    Then I should have internal code "003.002.015"

    Given I have a set of data:
        | id |  remote_identifie |
        | 1  |  remote-update-01 |

    Then I set form data from record 1
    When I request "/catalog/v1/categories/1" with method "put" and content type is "form"
    Then The response should be JSON
    Then HTTP status code should be 200

@delete_category
Scenario: 5. Deleting an existing category
    When I request "/catalog/v1/categories/1" with method "delete" and content type is "json"
    Then The response should be JSON
    Then HTTP status code should be 200